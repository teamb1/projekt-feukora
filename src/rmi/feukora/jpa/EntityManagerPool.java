package feukora.jpa;

import feukora.exceptions.UnableException;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

/**
 * The EntityManagerPool implements the ObjectPool on specific operations for
 * the purpose to create entity managers.
 *
 * @author Christian Klauenboesch
 * @version 20.04.2014
 * @since 16.04.2014
 */
public class EntityManagerPool extends ObjectPool<EntityManager> {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(EntityManagerPool.class);

    /**
     * The utility.
     */
    private AbstractJpaUtility utility;

    /**
     * Instantiates a new entity manger pool.
     *
     * @param utility the utility
     */
    public EntityManagerPool(AbstractJpaUtility utility) {
        super();
        this.utility = utility;
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.jpa.ObjectPool#create()
     */
    @Override
    public EntityManager create() {
        EntityManager em = utility.createEntityManager();
        checkNewIn(em);
        return checkOut();
    }

    @Override
    protected void fill() {
        logger.debug("Fill object pool");
        for (int i = getUnlockedSize(); i <= getMinimalSize(); i++) {
            logger.trace("Create a pool object...");
            checkIn(create());
        }
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.jpa.ObjectPool#close(java.lang.Object)
         */
    @Override
    protected boolean close(EntityManager object) {
        try {
            logger.debug("Closed object: " + object);
            object.close();
            return true;
        } catch (IllegalStateException ise) {
            logger.warn("Unable to close entity manager");
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.jpa.ObjectPool#clean(java.lang.Object)
     */
    @Override
    protected void clean(EntityManager object) throws UnableException {
        if (object.getTransaction().isActive()) {
            try {
                object.getTransaction().rollback();
            } catch (Exception e) {
                logger.debug("Unable to rollback transaction", e);
                throw new UnableException("Unable to rollback transaction", e);
            }
        }
        if (!object.isOpen()) {
            expire(object);
            throw new UnableException("Expired object");
        }
    }

    /* (non-Javadoc)
     * @see feukora.jpa.ObjectPool#validate(java.lang.Object)
     */
    protected boolean validate(EntityManager object) {
        if (!object.isOpen()) {
            return false;
        }
        return true;
    }

}
