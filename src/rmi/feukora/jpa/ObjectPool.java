package feukora.jpa;

import feukora.annotations.MethodOnlyTest;
import feukora.exceptions.UnableException;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * An abstract implementation of an object pool The object pool servers object
 * with fifo and registers them in lists The pool can also create new objects if
 * no more are available.
 *
 * @author Christian Klauenboesch
 * @version 21.04.2014
 * @since 16.04.2014
 */
abstract class ObjectPool<T> {

    /**
     * log4j logger.
     */
    private static Logger logger = Logger.getLogger(ObjectPool.class);

    /**
     * The timer which schedules the clean up task.
     */
    private final Timer timer = new Timer("objectPoolCleanup");
    /**
     * Contains information about object expiration time.
     */
    private final Map<T, Long> expirationInformation = Collections
            .synchronizedMap(new HashMap<T, Long>());
    /**
     * Contains a list of unlocked items, served in fifo.
     */
    private final Queue<T> unlocked = new ConcurrentLinkedQueue<>();
    /**
     * Contains a list of locked items, locked items are served to a consumer.
     */
    private final List<T> locked = Collections
            .synchronizedList(new ArrayList<T>());
    /**
     * The duration in seconds how long a registered object is valid.
     */
    private long expirationTime = 60 * 30;
    private int minimalSize = 3;

    /**
     * Initialize the pool Must be called by any implementation!.
     */
    protected ObjectPool() {
        createCleanupTask();
    }

    /**
     * Method which initializes a new object.
     *
     * @return an initialized object
     */
    abstract public T create();

    /**
     * Close a given object, so it is no longer valid.
     *
     * @param object the object to close
     * @return whether we had success or not
     */
    abstract protected boolean close(T object);

    /**
     * Clean an object so that the object has the same state as a new object.
     *
     * @param object the object to clean
     * @throws UnableException if the implementation was unable to clean the object
     */
    abstract protected void clean(T object) throws UnableException;

    /**
     * Validates the status of the object
     * Returns true if the object is accessible or returns false, if must be replaced by a new object
     * The check out method calls this method an replaces - if necessary - it with a new object and closes the given one.
     *
     * @param object the object to check
     * @return true if accessible, false if must replaced
     */
    abstract protected boolean validate(T object);

    abstract protected void fill();

    public int getMinimalSize() {
        return minimalSize;
    }

    public void setMinimalSize(int minimalSize) {
        if (minimalSize > 1) {
            logger.debug("Update minimal size of pool: " + minimalSize);
            this.minimalSize = minimalSize;
        }
    }

    /**
     * Gets the locked size.
     *
     * @return the locked size
     */
    @MethodOnlyTest
    int getLockedSize() {
        return locked.size();
    }

    /**
     * Gets the unlocked size.
     *
     * @return the unlocked size
     */
    @MethodOnlyTest
    int getUnlockedSize() {
        return unlocked.size();
    }

    /**
     * Modify registration time.
     *
     * @param object the object
     * @param time   the time
     */
    @MethodOnlyTest
    void modifyRegistrationTime(T object, Long time) {
        expirationInformation.put(object, time);
    }

    /**
     * Gets the expiration time.
     *
     * @return the expiration time
     */
    public long getExpirationTime() {
        return expirationTime;
    }

    /**
     * Sets the expiration time.
     *
     * @param expirationTime the new expiration time
     */
    public void setExpirationTime(long expirationTime) {
        if (expirationTime < 60) {
            logger.debug("Expiration time has a minimum of 60 seconds, no change applied");
            return;
        }
        this.expirationTime = expirationTime;
    }

    /**
     * Initializes the clean up task which expires objects in the given
     * interval.
     */
    private void createCleanupTask() {
        logger.debug("Register cleanup task");
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                synchronized (unlocked) {
                    int size = unlocked.size();
                    if (size == 0) {
                        logger.debug("No unlocked items to clean up");
                    } else {
                        int i = 1;
                        logger.debug("Validate " + size + " in cleanup task");
                        do {
                            T object = unlocked.poll();
                            if (!isOutdated(object)) {
                                expire(object);
                            } else {
                                unlocked.add(object);
                            }
                            i++;
                        } while (i < size);
                    }
                }
                if (unlocked.size() < getMinimalSize()) {
                    logger.info("Underlay minimal size of pool, fill pool");
                    fill();
                }
            }
        };
        timer.schedule(task, 3000, expirationTime * 1000);
    }

    /**
     * Expire a given object manually.
     *
     * @param object the object to expire
     */
    protected void expire(T object) {
        expirationInformation.remove(object);
        close(object);
    }

    /**
     * Validates the expiration of an object.
     *
     * @param object true if still valid, false if not
     * @return true, if is valid
     */
    public boolean isOutdated(T object) {
        if (new Date().getTime() - expirationTime < expirationInformation
                .get(object)) {
            return true;
        }
        return false;
    }

    /**
     * Register a new object into the pool.
     *
     * @param object the new object to register
     */
    private void registerCreation(T object) {
        logger.debug("Create a new object: " + object + " at time "
                + new Date().getTime());
        expirationInformation.put(object, new Date().getTime());
    }

    /**
     * Check-out thread-safe a new object.
     *
     * @return a new object
     */
    public synchronized T checkOut() {
        if (unlocked.size() == 0) {
            return create();
        }
        T object = unlocked.poll();
        if (!validate(object)) {
            expire(object);
            return checkOut();
        }
        locked.add(object);
        logger.trace("Check out object of pool: " + object);
        return object;
    }

    /**
     * Check-in thread-safe a before checked-out object if it is no longer used
     * by the consumer.
     *
     * @param object the object to check-in
     */
    public synchronized void checkIn(T object) {
        if (!locked.contains(object)) {
            logger.debug("Unable to find this object in locked list");
            return;
        }
        locked.remove(object);
        try {
            clean(object);
            if (!unlocked.add(object)) {
                logger.warn("Unable to add item to object pool: " + object);
                return;
            }
            logger.trace("Checked in an object into pool: " + object);
        } catch (UnableException ue) {
            logger.debug(ue);
        }
    }

    /**
     * Check-in a new item which was not registered before by the object pool.
     *
     * @param object a newly created object
     */
    protected synchronized void checkNewIn(T object) {
        logger.trace("New object into pool: " + object);
        registerCreation(object);
        unlocked.add(object);
    }
}
