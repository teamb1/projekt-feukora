package feukora.jpa;

import feukora.exceptions.UnableException;
import feukora.settings.FeukoraSettings;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * The JpaUtility which opens the persistence unit and creates entity managers.
 *
 * @author Christian Klauenbösch
 * @version 20.04.2014
 * @since 04.03.2014
 */
public class JpaUtility extends AbstractJpaUtility {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(JpaUtility.class);

    /**
     * The entity manager factory.
     */
    private EntityManagerFactory entityManagerFactory = null;

    /**
     * Instantiates a new jpa utility.
     *
     * @throws UnableException the unable exception
     */
    public JpaUtility() throws UnableException {
        String persistenceUnit = FeukoraSettings.getInstance().getRealString(
                "persistenceUnit");
        try {
            if (persistenceUnit.length() == 0) {
                persistenceUnit = "FeukoraB1";
            }
            logger.info("Load persistence unit named '" + persistenceUnit + "'");
            entityManagerFactory = Persistence
                    .createEntityManagerFactory(persistenceUnit);
        } catch (Throwable e) {
            logger.warn("Unable to load persistence unit: " + persistenceUnit,
                    e);
            throw new UnableException("Unable to load persistence unit", e);
        }
    }

    /**
     * Gets the entity manager factory.
     *
     * @return the entity manager factory
     */
    private EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.jpa.AbstractJpaUtility#createEntityManager()
     */
    public EntityManager createEntityManager() {
        logger.info("Create new entity manager...");
        return getEntityManagerFactory().createEntityManager();
    }

    /* (non-Javadoc)
     * @see feukora.jpa.AbstractJpaUtility#close()
     */
    public void close() {
        entityManagerFactory.close();
    }
}