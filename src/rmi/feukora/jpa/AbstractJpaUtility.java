package feukora.jpa;

import javax.persistence.EntityManager;

/**
 * This abstract class is primarly used to generate junit tests over the object
 * pool without database connection
 *
 * @author Christian Klauenbösch
 * @version 30.05.2014
 * @since 21.04.2014
 */
abstract class AbstractJpaUtility {

    /**
     * Creates a new entity manager.
     *
     * @return a new entity manager
     */
    abstract public EntityManager createEntityManager();

    /**
     * Close.
     */
    abstract public void close();
}
