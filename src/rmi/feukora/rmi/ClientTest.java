package feukora.rmi;

import feukora.entity.BenutzerEntity;
import feukora.interfaces.ConcreteBenutzerAction;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.14
 * @since 09.04.14
 */
public class ClientTest {

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws RemoteException       the remote exception
     * @throws NotBoundException     the not bound exception
     * @throws MalformedURLException the malformed url exception
     */
    public static void main(String[] args) throws RemoteException,
            NotBoundException, MalformedURLException {
        String[] list = Naming.list("//:13003");
        System.out.println("-- Naming list:");
        for (String s : list) {
            System.out.println(s);
        }
        System.out.println("-- end of list");

        ConcreteBenutzerAction echo = (ConcreteBenutzerAction) Naming
                .lookup("//:13003/feukora/benutzer");

        try {
            addUser(echo, "hansmuster", "Muster", "Hans");
            listUser(echo);
            addUser(echo, "peterklein", "KLein", "Peter");
            listUser(echo);
            addUser(echo, "chrisklaui", "Klaui", "Christian");
            listUser(echo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * List user.
     *
     * @param echo the echo
     * @throws RemoteException the remote exception
     */
    public static void listUser(ConcreteBenutzerAction echo)
            throws RemoteException {
        List<BenutzerEntity> benutzerEntityList = echo.getAll();
        System.out.println("- List");
        for (BenutzerEntity item : benutzerEntityList) {
            System.out.println(item.getName() + " " + item.getVorname() + ": "
                    + item.getBenutzername());
        }
        System.out.println("- // EOL");
    }

    /**
     * Adds the user.
     *
     * @param echo         the echo
     * @param benutzername the benutzername
     * @param name         the name
     * @param vorname      the vorname
     * @throws Exception the exception
     */
    public static void addUser(ConcreteBenutzerAction echo,
                               String benutzername, String name, String vorname) throws Exception {
        BenutzerEntity benutzer = new BenutzerEntity();
        System.out.println("- Add user: " + benutzername);
        benutzer.setBenutzername(benutzername);
        benutzer.setIsKontrolleur(1);
        benutzer.setPasswort("blablabla");
        benutzer.setVorname(vorname);
        benutzer.setName(name);
        echo.add(benutzer);
    }
}
