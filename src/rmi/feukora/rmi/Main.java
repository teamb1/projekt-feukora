package feukora.rmi;

import feukora.exceptions.UnableException;
import feukora.jpa.EntityManagerPool;
import feukora.jpa.JpaUtility;
import feukora.rmi.logical.*;
import feukora.settings.FeukoraSettings;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * The main class for the rmi server.
 *
 * @author Christian Klauenboesch
 * @version 09.05.14
 * @since 3.04.14
 */
public class Main {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(Main.class);

    /**
     * The rmi path prefix.
     */
    private static String rmiPathPrefix;

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        try {
            logger.info("Starting RMI-Server...");
            // set up system properties
            System.setProperty("java.security.policy", "perm.policy");
            FeukoraSettings.setXmlFile("settings_rmi.xml");
            System.setProperty("java.rmi.server.hostname", FeukoraSettings.getInstance().getRealString("rmiHost"));

            // set up security manager
            logger.info("Set up security manager...");
            SecurityManager securityManager = new RMISecurityManager();
            System.setSecurityManager(securityManager);

            // initialize rmi variables
            int rmiPort = FeukoraSettings.getInstance().getRealInt("rmiPort");
            rmiPathPrefix = FeukoraSettings.getInstance().getRealString("rmiPath");

            // Start jpa utility
            logger.info("Set up jpa utility...");
            JpaUtility jpaUtility = new JpaUtility();
            EntityManagerPool pool = new EntityManagerPool(jpaUtility);

            // create rmi registry and auth container
            Registry registry = LocateRegistry.createRegistry(rmiPort);
            AuthorizationContainer container = new AuthorizationContainerImpl();

            // register arbitrary actions
            logger.info("Start binding rmi actions");
            rebind(registry, "benutzer", new BenutzerImpl(rmiPort, pool, container));
            rebind(registry, "brennerart", new BrennerartImpl(rmiPort, pool, container));
            rebind(registry, "brenner", new BrennerImpl(rmiPort, pool, container));
            rebind(registry, "brennstoff", new BrennstoffImpl(rmiPort, pool, container));
            rebind(registry, "eigentumer", new EigentumerImpl(rmiPort, pool, container));
            rebind(registry, "hauswart", new HauswartImpl(rmiPort, pool, container));
            rebind(registry, "kontrolleur", new KontrolleurImpl(rmiPort, pool, container));
            rebind(registry, "login", new LoginImpl(rmiPort, pool, container));
            rebind(registry, "messergebnis", new MessergebnisImpl(rmiPort, pool, container));
            rebind(registry, "messvorgang", new MessvorgangImpl(rmiPort, pool, container));
            rebind(registry, "ortschaft", new OrtschaftImpl(rmiPort, pool, container));
            rebind(registry, "rapport", new RapportImpl(rmiPort, pool, container));
            rebind(registry, "standort", new StandortImpl(rmiPort, pool, container));
            rebind(registry, "termin", new TerminImpl(rmiPort, pool, container));
            rebind(registry, "verwaltung", new VerwaltungImpl(rmiPort, pool, container));
            rebind(registry, "warmeerzeuger", new WarmeerzeugerImpl(rmiPort, pool, container));

            logger.info("Bound all actions, rmi server is running...");
        } catch (RemoteException e) {
            logger.warn("Got a remote exception while binding object", e);
        } catch (UnableException e) {
            logger.warn("Was unable to execute action", e);
        }
    }

    /**
     * Rebind.
     *
     * @param registry the registry
     * @param implPath the impl path
     * @param object   the object
     * @throws RemoteException the remote exception
     */
    private static void rebind(Registry registry, String implPath, AbstractRemoteObject object) throws RemoteException {
        registry.rebind(rmiPathPrefix + implPath, object);
    }
}
