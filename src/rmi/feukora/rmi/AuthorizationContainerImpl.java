package feukora.rmi;

import feukora.exceptions.NoSuchItemException;
import feukora.util.AbstractAuthContainer;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class AuthorizationContainerImpl.
 *
 * @author Christian Klauenboesch
 * @version 10.05.14
 * @since 10.05.14
 */
public class AuthorizationContainerImpl extends AbstractAuthContainer implements AuthorizationContainer {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(AuthorizationContainerImpl.class);

    /**
     * The authorized users.
     */
    private final ConcurrentHashMap<String, Date> authorizedUsers = new ConcurrentHashMap<>(50);

    public AuthorizationContainerImpl() {
        super("RMI-Auth");
        registerCleanup();
    }

    /* (non-Javadoc)
         * @see feukora.util.AuthorizationContainer#grantAuth(java.lang.String)
         */
    public void grantAuth(String id) {
        logger.info("Grant auth to user '" + id + "' auth");
        authorizedUsers.put(id, new Date());
    }

    public ConcurrentHashMap<String, Date> getAuthorizedUsers() {
        return authorizedUsers;
    }

    /* (non-Javadoc)
         * @see feukora.util.AuthorizationContainer#userHasAuth(java.lang.String)
         */
    public boolean userHasAuth(String id) {
        refreshAuthTime(id);
        return authorizedUsers.containsKey(id);
    }

    /* (non-Javadoc)
     * @see feukora.util.AuthorizationContainer#refuseAuth(java.lang.String)
     */
    public void refuseAuth(String id) throws NoSuchItemException {
        logger.info("Refuse auth of user '" + id + "'");
        if (authorizedUsers.remove(id) == null) {
            logger.debug("Unknown user information, unable to drop");
            throw new NoSuchItemException("Unknown user");
        }
    }
}
