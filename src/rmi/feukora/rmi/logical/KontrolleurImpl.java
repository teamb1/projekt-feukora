package feukora.rmi.logical;

import feukora.entity.KontrolleurEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteKontrolleurAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;

/**
 * @author Christian Klauenbösch
 * @version 19.04.14
 * @since 16.04.14
 */
public class KontrolleurImpl extends AbstractRemoteObject<KontrolleurEntity> implements
        ConcreteKontrolleurAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(KontrolleurImpl.class);

    private String defaultOrderClause = "name ASC";

    /**
     * The entity class.
     */
    private Class entityClass = KontrolleurEntity.class;

    /**
     * Instantiates a new kontrolleur impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public KontrolleurImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    @Override
    public void add(KontrolleurEntity object) throws InvalidDataException, RemoteException {
        thisUsersHasAuth();

        // There is a max of 6 kontrolleure allowed
        if (getAll().size() >= 6) {
            throw new RemoteException("Es sind bereits 6 Kontrolleure erfasst.");
        }
        super.add(object);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }
}
