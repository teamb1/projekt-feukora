package feukora.rmi.logical;

import feukora.entity.BrennstoffEntity;
import feukora.interfaces.ConcreteBrennstoffAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;

/**
 * Server Implenentation of the Brennstoff Entity
 *
 * @author Christian Klauenboesch
 * @version 16.04.14
 * @since 20.04.14
 */
public class BrennstoffImpl extends AbstractRemoteObject<BrennstoffEntity>
        implements ConcreteBrennstoffAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BrennstoffImpl.class);

    private String defaultOrderClause = "bezeichnung ASC";

    /**
     * The entity class.
     */
    private Class entityClass = BrennstoffEntity.class;

    /**
     * Instantiates a new brennstoff impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public BrennstoffImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }
}
