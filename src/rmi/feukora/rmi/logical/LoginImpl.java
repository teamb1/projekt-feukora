package feukora.rmi.logical;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.exceptions.NoSuchItemException;
import feukora.interfaces.ConcreteLoginAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.util.List;

/**
 * @author Tscherrig Sven
 * @version 10.05.14
 * @since 01.05.14
 */

public class LoginImpl extends AbstractRemoteObject<BenutzerEntity> implements ConcreteLoginAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(LoginImpl.class);

    /**
     * The entity class.
     */
    private Class entityClass = BenutzerEntity.class;

    /**
     * Instantiates a new login impl.
     *
     * @param port            the port
     * @param pool            the pool
     * @param authorizedUsers the authorized users
     * @throws RemoteException the remote exception
     */
    public LoginImpl(int port, EntityManagerPool pool, AuthorizationContainer authorizedUsers) throws RemoteException {
        super(port, pool, authorizedUsers);
    }

    /**
     * Key: IP-Address of remote user Value: Login time.
     *
     * @return the entity class
     */

    @Override
    Class getEntityClass() {
        return entityClass;
    }

    @Override
    String getDefaultOrderClause() {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * feukora.interfaces.ConcreteLoginAction#checkBenutzer(java.lang.String,
     * java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean checkBenutzer(String benutzername, String passwort) throws InvalidDataException, RemoteException {
        try {
            logger.trace("Try to log in user: " + benutzername);
            BenutzerEntity user = getByUsername(benutzername);
            if (user.getBenutzername().equals(benutzername) && user.getPasswort().equals(passwort)
                    && user.getIsKontrolleur() == 0) {
                try {
                    getAuthorizationContainer().grantAuth(RemoteServer.getClientHost());
                    return true;
                } catch (ServerNotActiveException e) {
                    logger.warn("This thread is no RMI-Connection", e);
                    throw new RemoteException("Keine erlaubte Aktion");
                }
            }
        } catch (RemoteException ex) {
            logger.warn("Unable to get items", ex);
            throw new RemoteException("Konnte Benutzerangaben nicht prüfen");
        } catch (InvalidDataException ex) {
            logger.warn("Invalid data input", ex);
            throw new InvalidDataException("Die Benutzerangaben sind nicht korrekt", ex);
        }
        logger.warn("Auth failed: " + benutzername);
        return false;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public boolean checkLimitedBenutzer(String benutzername, String password) throws RemoteException, InvalidDataException {
        try {
            logger.trace("Try to log in user: " + benutzername);
            BenutzerEntity user = getByUsername(benutzername);
            if (user.getBenutzername().equals(benutzername) && user.getPasswort().equals(password)
                    && user.getIsKontrolleur() != 0) {
                return true;
            }
        } catch (RemoteException ex) {
            logger.warn("Unable to get items", ex);
            throw new RemoteException("Konnte Benutzerangaben nicht prüfen");
        } catch (InvalidDataException ex) {
            logger.warn("Invalid data input", ex);
            throw new InvalidDataException("Die Benutzerangaben sind nicht korrekt", ex);
        }
        logger.warn("Auth failed: " + benutzername);
        return false;
    }


    /**
     * Gets the by username.
     *
     * @param username the username
     * @return the by username
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @SuppressWarnings({"unchecked"})
    private BenutzerEntity getByUsername(String username) throws RemoteException, InvalidDataException {
        EntityManager em = getNewEm();
        try {
            logger.trace("Get all Benutzer by Funktion");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t FROM " + getEntityClass().getName() + " t WHERE t.benutzername = ?1");
            query.setParameter(1, username);
            List list = query.getResultList();
            if (list.size() == 0) {
                throw new InvalidDataException("Benutzer kann nicht angemeldet werden.");
            }
            return ((List<BenutzerEntity>) query.getResultList()).get(0);
        } catch (Exception e) {
            logger.warn("Unable to get items", e);
            throw new InvalidDataException("Konnte keine Datenbankeinträge abrufen", e);
        } finally {
            em.getTransaction().commit();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.ConcreteLoginAction#getUserId(java.lang.String)
     */
    @Override
    @SuppressWarnings({"unchecked"})
    public int getUserId(String username) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to fetch user id");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t FROM " + getEntityClass().getName() + " t WHERE t.benutzername = ?1");
            query.setParameter(1, username);
            List list = query.getResultList();
            if (list.size() == 0) {
                throw new InvalidDataException("Kein entsprechender Benutzer.");
            }
            return ((List<BenutzerEntity>) list).get(0).getId();
        } catch (Exception e) {
            logger.warn("Unable to retrieve information", e);
            throw new InvalidDataException("Konnte keine Einträge abrufen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.ConcreteLoginAction#logout()
     */
    @Override
    public void logout() throws RemoteException, InvalidDataException {
        try {
            getAuthorizationContainer().refuseAuth(RemoteServer.getClientHost());
        } catch (NoSuchItemException e) {
            try {
                logger.warn("Unable to refuse auth: " + RemoteServer.getClientHost());
            } catch (ServerNotActiveException e1) {
                // ignore it
            }
        } catch (ServerNotActiveException e) {
            // ignore it
        }
    }
}
