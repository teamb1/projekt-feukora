package feukora.rmi.logical;

import feukora.entity.VerwaltungEntity;
import feukora.interfaces.ConcreteVerwaltungAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 20.04.14
 * @since20.04.14
 */
public class VerwaltungImpl extends AbstractRemoteObject<VerwaltungEntity> implements
        ConcreteVerwaltungAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(VerwaltungImpl.class);

    private String defaultOrderClause = "name ASC";

    /**
     * The entity class.
     */
    private Class entityClass = VerwaltungEntity.class;

    /**
     * Instantiates a new verwaltung impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public VerwaltungImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.interfaces.ConcreteVerwaltungAction#getByStandort(int)
         */
    @Override
    public VerwaltungEntity getByStandort(int standortId)
            throws RemoteException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get all entities of type");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM VerwaltungEntity t1 INNER JOIN t1.standort t2 " +
                    "WHERE t2.id = ?1");
            query.setParameter(1, standortId);
            List<Object[]> list = query.getResultList();
            return (VerwaltungEntity) list.get(0)[0];
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }
}
