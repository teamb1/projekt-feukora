package feukora.rmi.logical;

import feukora.entity.WarmeerzeugerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteWarmeerzeugerAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 20.04.14
 * @since 20.04.14
 */
public class WarmeerzeugerImpl extends AbstractRemoteObject<WarmeerzeugerEntity> implements
        ConcreteWarmeerzeugerAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(WarmeerzeugerImpl.class);

    private String defaultOrderClause = "fabrikat ASC";

    /**
     * The entity class.
     */
    private Class entityClass = WarmeerzeugerEntity.class;

    /**
     * Instantiates a new warmeerzeuger impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public WarmeerzeugerImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /**
     * (non-Javadoc).
     *
     * @param brennstoffId the brennstoff id
     * @return the all by brennstoff
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     * @see feukora.interfaces.ConcreteWarmeerzeugerAction#getAllByBrennstoff(int)
     */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<WarmeerzeugerEntity> getAllByBrennstoff(int brennstoffId)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Get all Brenner by Brennerart");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM WarmeerzeugerEntity t1 INNER JOIN t1.brennstoff " +
                    "WHERE t2.id = ?1");
            query.setParameter(1, brennstoffId);
            List<Object[]> list = query.getResultList();
            List<WarmeerzeugerEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((WarmeerzeugerEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            logger.warn("Unable to get items", e);
            throw new InvalidDataException("Konnte keine Datenbankeinträge abrufen", e);
        } finally {
            em.getTransaction().commit();
        }
    }
}
