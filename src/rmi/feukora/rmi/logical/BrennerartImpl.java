package feukora.rmi.logical;

import feukora.entity.BrennerartEntity;
import feukora.interfaces.ConcreteBrennerartAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;

/**
 * Server Implementation of the Brennerart Class
 *
 * @author Christian Klauenboesch
 * @version 16.04.14
 * @since 16.04.14
 */
public class BrennerartImpl extends AbstractRemoteObject<BrennerartEntity>
        implements ConcreteBrennerartAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BrennerartImpl.class);

    private String defaultOrderClause = "bezeichnung ASC";

    /**
     * The entity class.
     */
    private Class entityClass = BrennerartEntity.class;

    /**
     * Instantiates a new brennerart impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public BrennerartImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }
}
