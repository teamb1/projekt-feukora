package feukora.rmi.logical;

import feukora.entity.MessergebnisEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteMessergebnisAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 16.04.14
 * @since 16.04.14
 */
public class MessergebnisImpl extends AbstractRemoteObject<MessergebnisEntity> implements
        ConcreteMessergebnisAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(MessergebnisImpl.class);

    private String defaultOrderClause = "id ASC";
    /**
     * The entity class.
     */
    private Class entityClass = MessergebnisEntity.class;

    /**
     * Instantiates a new messergebnis impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public MessergebnisImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.interfaces.ConcreteMessergebnisAction#getByRapport(int)
         */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<MessergebnisEntity> getByRapport(int rapportId)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get entities by join");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM MessergebnisEntity t1 INNER JOIN t1.rapport t2 " +
                    "WHERE t2.id = ?1");
            query.setParameter(1, rapportId);
            List<Object[]> list = query.getResultList();
            List<MessergebnisEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((MessergebnisEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }

}
