package feukora.rmi.logical;

import feukora.entity.EigentumerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteEigentumerAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.14
 * @since 16.04.14
 */
public class EigentumerImpl extends AbstractRemoteObject<EigentumerEntity> implements
        ConcreteEigentumerAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(EigentumerImpl.class);

    private String defaultOrderClause = "name ASC";

    /**
     * The entity class.
     */
    private Class entityClass = EigentumerEntity.class;

    /**
     * Instantiates a new eigentumer impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public EigentumerImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.interfaces.ConcreteEigentumerAction#getByStandort(int)
         */
    @Override
    @SuppressWarnings({"unchecked"})
    public EigentumerEntity getByStandort(int id) throws RemoteException,
            InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get all entities of type");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM EigentumerEntity t1 INNER JOIN t1.standort t2 " +
                    "WHERE t2.id = ?1");
            query.setParameter(1, id);
            List<Object[]> list = query.getResultList();
            return (EigentumerEntity) list.get(0)[0];
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }
}
