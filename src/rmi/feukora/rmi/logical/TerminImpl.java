package feukora.rmi.logical;

import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteTerminAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.*;

/**
 * <br />
 * <br />
 * <i>feukora.rmi.logical</i>, added at <b>16.04.14</b>
 *
 * @author Florian Rieder<i>&lt;flo.rieder@bluewin.ch&rt;</i>
 * @version 00.01.00
 * @since 22.04.2014
 */
public class TerminImpl extends AbstractRemoteObject<TerminEntity> implements
        ConcreteTerminAction {
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(TerminImpl.class);
    private final int[] allowedHours = {8, 9, 10, 11, 13, 14, 15, 16};
    /**
     * The entity class.
     */
    private Class entityClass = TerminEntity.class;
    private String defaultOrderClause = "datum ASC";

    /**
     * Instantiates a new termin impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public TerminImpl(int port, EntityManagerPool pool,
                      AuthorizationContainer container) throws RemoteException {
        super(port, pool, container);
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    private Date getNextDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

    private Date getFirstDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        return cal.getTime();
    }

    private Date getLastDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return calendar.getTime();
    }

    private boolean isAllowedTerminHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        for (int h : allowedHours) {
            if (calendar.get(Calendar.HOUR_OF_DAY) == h) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void add(TerminEntity object) throws InvalidDataException,
            RemoteException {
        thisUsersHasAuth();

        // validate if there is already a metting at this time
        if (!isAllowedTerminHour(object.getDatum().getTime())) {
            throw new InvalidDataException(
                    "Diese Zeit ist für den Termin nicht erlaubt.");
        }
        if (!isFutureDate(object.getDatum().getTime())) {
            throw new InvalidDataException("Der Termin muss in der Zukunft liegen");
        }
        if (hasTerminAtDate(object.getKontrolleur().getId(), object.getDatum()
                .getTime())) {
            throw new RemoteException("Der Kontrolleur ist bereits ausgebucht");
        }

        super.add(object);
    }

    private boolean isFutureDate(Date date) {
        return (new Date().compareTo(date) <= 0);
    }

    @Override
    public void update(TerminEntity object) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        if (!isAllowedTerminHour(object.getDatum().getTime())) {
            throw new InvalidDataException("Diese Zeit ist für den Termin nicht erlaubt.");
        }
        if (!isFutureDate(object.getDatum().getTime())) {
            throw new InvalidDataException("Das Datum muss in der Zukunft liegen.");
        }
        if (hasTerminAtMovedDate(object.getId(), object.getKontrolleur().getId(), object.getDatum()
                .getTime())) {
            throw new RemoteException("Der Kontrolleur ist bereits ausgebucht");
        }

        super.update(object);
    }

    @SuppressWarnings({"unchecked"})
    private boolean hasTerminAtDate(int kontrolleurId, Date date)
            throws RemoteException {
        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get termin at date of kontrolleur");
            em.getTransaction().begin();
            Query query = em
                    .createQuery("SELECT t1, t2 FROM TerminEntity t1 INNER JOIN t1.kontrolleur t2 "
                            + "WHERE t2.id = ?1 AND t1.datum = ?2");
            query.setParameter(1, kontrolleurId);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            query.setParameter(2, calendar);
            List<Object[]> list = query.getResultList();
            return (list.size() > 0);
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().rollback();
            checkInEm(em);
        }
    }

    @SuppressWarnings({"unchecked"})
    private boolean hasTerminAtMovedDate(int terminId, int kontrolleurId, Date date)
            throws RemoteException {
        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get termin at date of kontrolleur");
            em.getTransaction().begin();
            Query query = em
                    .createQuery("SELECT t1, t2 FROM TerminEntity t1 INNER JOIN t1.kontrolleur t2 "
                            + "WHERE t2.id = ?1 AND t1.datum = ?2 AND t1.id <> " + terminId);
            query.setParameter(1, kontrolleurId);
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            query.setParameter(2, calendar);
            List<Object[]> list = query.getResultList();
            return (list.size() > 0);
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().rollback();
            checkInEm(em);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.ConcreteTerminAction#getAllByKontrolleur(int)
     */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<TerminEntity> getAllByKontrolleur(int kontrolleurId)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get all termine by kontrolleur");
            em.getTransaction().begin();
            Query query = em
                    .createQuery("SELECT t1, t2 FROM TerminEntity t1 INNER JOIN t1.kontrolleur t2 "
                            + "WHERE t2.id = ?1 ORDER BY t1." + getDefaultOrderClause());
            query.setParameter(1, kontrolleurId);
            List<Object[]> list = query.getResultList();
            List<TerminEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((TerminEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().rollback();
            checkInEm(em);
        }
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<TerminEntity> getNextDayByKontrolleur(int kontrolleurId)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get meetings of next day by kontrolleur");
            em.getTransaction().begin();
            Query query = em
                    .createQuery("SELECT t1, t2 FROM TerminEntity t1 INNER JOIN t1.kontrolleur t2 "
                            + "WHERE t2.id = ?1 and t1.datum = ?2 ORDER BY t1." + getDefaultOrderClause());
            query.setParameter(1, kontrolleurId);
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(getNextDay(new Date()));
            query.setParameter(2, calendar);
            List<Object[]> list = query.getResultList();
            List<TerminEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((TerminEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<TerminEntity> getThisWeek() throws RemoteException,
            InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get meetings of next day by kontrolleur");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM TerminEntity t1 INNER JOIN t1.kontrolleur t2 " +
                    "WHERE t1.datum >= ?1 and t1.datum <= ?2 ORDER BY t1." + getDefaultOrderClause());

            Calendar calendarFirst = new GregorianCalendar();
            calendarFirst.setTime(getFirstDayOfWeek(new Date()));
            query.setParameter(1, calendarFirst);

            Calendar calendarLast = new GregorianCalendar();
            calendarLast.setTime(getLastDayOfWeek(new Date()));
            query.setParameter(2, calendarLast);

            List<Object[]> list = query.getResultList();
            List<TerminEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((TerminEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }
}
