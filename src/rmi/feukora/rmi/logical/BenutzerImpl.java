package feukora.rmi.logical;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteBenutzerAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.List;

/**
 * The Server implementation of the ConcreteBenutzerAction.
 *
 * @author Christian Klauenboesch
 * @version 20.04.14
 * @since 04.04.14
 */
public class BenutzerImpl extends AbstractRemoteObject<BenutzerEntity>
        implements ConcreteBenutzerAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BenutzerImpl.class);
    private String defaultOrderClause = "name ASC";
    /**
     * The entity class.
     */
    private Class entityClass = BenutzerEntity.class;

    /**
     * Instantiates a new benutzer impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public BenutzerImpl(int port, EntityManagerPool pool,
                        AuthorizationContainer container) throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

	/*
     * (non-Javadoc)
	 * 
	 * @see feukora.interfaces.ConcreteBenutzerAction#getKontrolleure(boolean)
	 */

    /**
     * Gets the by mode.
     *
     * @param isKontrolleur the is kontrolleur
     * @return the by mode
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @SuppressWarnings({"unchecked"})
    private List<BenutzerEntity> getByMode(int isKontrolleur)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Get all Benutzer by Funktion");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t FROM "
                    + getEntityClass().getName()
                    + " t WHERE t.isKontrolleur = ?1 AND t.displayable <> 0");
            query.setParameter(1, isKontrolleur);
            return (List<BenutzerEntity>) query.getResultList();
        } catch (Exception e) {
            logger.warn("Unable to get items", e);
            throw new InvalidDataException(
                    "Konnte keine Datenbankeinträge abrufen", e);
        } finally {
            em.getTransaction().commit();
        }
    }

    /* (non-Javadoc)
     * @see feukora.interfaces.ConcreteBenutzerAction#getKontrolleure()
     */
    @Override
    public List<BenutzerEntity> getKontrolleure() throws RemoteException,
            InvalidDataException {
        return getByMode(1);
    }

    /* (non-Javadoc)
     * @see feukora.interfaces.ConcreteBenutzerAction#getRegularUsers()
     */
    @Override
    public List<BenutzerEntity> getRegularUsers() throws RemoteException, InvalidDataException {
        return getByMode(0);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public List<BenutzerEntity> getAll() throws RemoteException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get all entities of type");
            em.getTransaction().begin();
            String orderClause = "";
            if (getDefaultOrderClause() != null) {
                orderClause = "ORDER BY t." + getDefaultOrderClause();
            }
            Query query = em.createQuery("SELECT t FROM "
                    + getEntityClass().getName() + " t " +
                    "WHERE t.displayable <> 0 " + orderClause);
            return (List<BenutzerEntity>) query.getResultList();
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }
}
