package feukora.rmi.logical;

import feukora.entity.MessvorgangEntity;
import feukora.interfaces.ConcreteMessvorgangAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;

/**
 * @author Christian Klauenbösch
 * @version 16.04.14
 * @since 16.04.14
 */
public class MessvorgangImpl extends AbstractRemoteObject<MessvorgangEntity> implements
        ConcreteMessvorgangAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(MessvorgangImpl.class);

    /**
     * The entity class.
     */
    private Class entityClass = MessvorgangEntity.class;

    /**
     * Instantiates a new messvorgang impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public MessvorgangImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    @Override
    String getDefaultOrderClause() {
        return null;
    }
}
