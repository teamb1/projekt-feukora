package feukora.rmi.logical;

import feukora.entity.StandortEntity;
import feukora.exceptions.InvalidDataException;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Set;

/**
 * Extends the given UnicastRemoteObject with some application specific methods.
 *
 * @param <T> the generic type
 * @author Christian Klauenboesch
 * @version 09.05.14
 * @since 16.04.14
 */
public abstract class AbstractRemoteObject<T extends Object> extends
        UnicastRemoteObject {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(AbstractRemoteObject.class);
    /**
     * The Hibernate Validator factory.
     */
    private static ValidatorFactory VALIDATOR_FACTORY;
    /**
     * The Hibernate Validator.
     */
    private static Validator VALIDATOR;
    /**
     * The authorization container used for this implementation.
     */
    private AuthorizationContainer authorizationContainer;
    /**
     * The object pool with the entity managers.
     */
    private EntityManagerPool pool;

    /**
     * Instantiates a new abstract remote object.
     *
     * @throws RemoteException the remote exception
     */
    public AbstractRemoteObject() throws RemoteException {
        throw new RemoteException("Unsupported constructor");
    }

    /**
     * Initialize this class, inherited by UnicastRemoteObject.
     *
     * @param port                the port to run this object on
     * @param pool                the object pool
     * @param authorizedContainer the authorization container impl
     * @throws RemoteException the remote exception
     */
    public AbstractRemoteObject(int port, EntityManagerPool pool, AuthorizationContainer authorizedContainer)
            throws RemoteException {
        super(port);
        this.pool = pool;
        this.authorizationContainer = authorizedContainer;
        VALIDATOR_FACTORY = Validation.buildDefaultValidatorFactory();
        VALIDATOR = VALIDATOR_FACTORY.getValidator();
    }

    /**
     * Get the authorization container.
     *
     * @return the auth container
     */
    protected AuthorizationContainer getAuthorizationContainer() {
        return authorizationContainer;
    }

    /**
     * Checks if a given user has auth or not.
     *
     * @return whether the user has auth or not
     * @throws RemoteException the remote exception
     */
    protected boolean thisUsersHasAuth() throws RemoteException {
        try {
            if (getAuthorizationContainer().userHasAuth(RemoteServer.getClientHost())) {
                return true;
            }
        } catch (ServerNotActiveException e) {
            logger.warn("No RMI-Connection in this thread", e);
            return true;
        }
        try {
            logger.info("Unauthorized access: " + RemoteServer.getClientHost());
        } catch (ServerNotActiveException e) {
            // ignore it
        }
        throw new RemoteException("Benutzer ist nicht authentifiziert.");
    }


    /**
     * Handle validation.
     *
     * @param entity the entity
     * @throws InvalidDataException the invalid data exception
     */
    protected void handleValidation(T entity) throws InvalidDataException {
        Set<ConstraintViolation<T>> violations = validateEntity(entity);
        if (violations.size() != 0) {
            String error = getValidationErrorMessage(violations);
            logger.trace("Invalid entity: " + error);
            throw new InvalidDataException(error);
        }
    }

    /**
     * Validate a entity with hibernate validator.
     *
     * @param entity the entity to validate
     * @return the violations occurred (or an empty set if no errors occurred)
     */
    protected Set<ConstraintViolation<T>> validateEntity(T entity) {
        return VALIDATOR.validate(entity);
    }

    /**
     * Concats all violations to a single string that could be represented to the end user.
     *
     * @param violations the violations to concat
     * @return the user-displayable message
     */
    protected String getValidationErrorMessage(Set<ConstraintViolation<T>> violations) {
        if (violations.size() == 0) {
            return null;
        }
        String errorMessage = "";
        for (ConstraintViolation<T> violation : violations) {
            errorMessage += violation.getMessage() + "\n";
        }
        return errorMessage;
    }

    /**
     * Check out a new entity manager.
     *
     * @return a entity manager
     * @throws RemoteException the remote exception
     */
    protected EntityManager getNewEm() throws RemoteException {
        if (pool == null) {
            throw new RemoteException("Unable to locate object pool");
        }
        return pool.checkOut();
    }

    /**
     * Check in an unused entity manager.
     *
     * @param em the entity manager to check in
     * @throws RemoteException the remote exception
     */
    protected void checkInEm(EntityManager em) throws RemoteException {
        pool.checkIn(em);
    }

    /**
     * Gets the entity class.
     *
     * @return the entity class
     */
    abstract Class getEntityClass();

    abstract String getDefaultOrderClause();

    /**
     * Adds the.
     *
     * @param object the object
     * @throws InvalidDataException the invalid data exception
     * @throws RemoteException      the remote exception
     */
    public void add(T object) throws InvalidDataException, RemoteException {
        thisUsersHasAuth();
        handleValidation(object);

        EntityManager em = getNewEm();
        try {
            logger.trace("Persist object");
            em.getTransaction().begin();
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.warn("Got a problem", e);
            try {
                em.getTransaction().rollback();
            } catch (IllegalStateException ex) {
                throw new RemoteException("Der Eintrag ist nicht eindeutig.");
            }
            throw new RemoteException("Konnte Aktion nicht ausführen.", e);
        } finally {
            checkInEm(em);
        }
    }

    /**
     * Gets the by id.
     *
     * @param id the id
     * @return the by id
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @SuppressWarnings({"unchecked"})
    public T getById(int id) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Get object by id");
            em.getTransaction().begin();
            return (T) em.find(getEntityClass(), id);
        } catch (ClassCastException e) {
            logger.warn("Unable to cast class", e);
            throw new InvalidDataException(
                    "Konnte kein Resultat finden, das der Suche entspricht.");
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.", e);
        } finally {
            checkInEm(em);
        }
    }

    /**
     * Gets the all.
     *
     * @return the all
     * @throws RemoteException the remote exception
     */
    @SuppressWarnings({"unchecked"})
    public List<T> getAll() throws RemoteException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get all entities of type");
            em.getTransaction().begin();
            String orderClause = "";
            if (getDefaultOrderClause() != null) {
                orderClause = "ORDER BY t." + getDefaultOrderClause();
            }
            Query query = em.createQuery("SELECT t FROM "
                    + getEntityClass().getName() + " t " + orderClause);
            return (List<T>) query.getResultList();
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }

    /**
     * Update.
     *
     * @param object the object
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    public void update(T object) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();
        handleValidation(object);

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to update entity");
            em.getTransaction().begin();
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            logger.warn("Unable to execute update", e);
            throw new InvalidDataException(
                    "Konnte Aktualisierung nicht ausführen", e);
        } finally {
            checkInEm(em);
        }
    }

    /**
     * Delete.
     *
     * @param id the id
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @SuppressWarnings({"unchecked"})
    public void delete(int id) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to delete entity");
            em.getTransaction().begin();
            T entity = (T) em.find(getEntityClass(), id);
            em.remove(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.warn("Unable to execute delete", e);
            try {
                em.getTransaction().rollback();
            } catch (IllegalStateException ex) {
                throw new RemoteException("Der Eintrag wird noch referenziert.");
            }
            throw new InvalidDataException("Konnte Eintrag nicht löschen.");
        } finally {
            checkInEm(em);
        }
    }

    /**
     * Gets the standort.
     *
     * @param id the id
     * @return the standort
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    public StandortEntity getStandort(int id) throws RemoteException,
            InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get a Standort");
            em.getTransaction().begin();
            return em.find(StandortEntity.class, id);
        } catch (Exception e) {
            logger.warn("Unable to get standort", e);
            throw new InvalidDataException(
                    "Konnte keinen entsprechenden Standort finden", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }
}
