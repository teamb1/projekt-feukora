package feukora.rmi.logical;

import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteOrtschaftAction;
import feukora.jpa.EntityManagerPool;
import feukora.settings.FeukoraSettings;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 16.04.2014
 * @since 16.04.2014
 */
public class OrtschaftImpl extends AbstractRemoteObject<OrtschaftEntity> implements ConcreteOrtschaftAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(OrtschaftImpl.class);

    /**
     * The entity class.
     */
    private Class entityClass = OrtschaftEntity.class;

    private String defaultOrderClause = "plz ASC, t.gemeinde ASC";

    /**
     * Instantiates a new ortschaft impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public OrtschaftImpl(int port, EntityManagerPool pool, AuthorizationContainer container) throws RemoteException {
        super(port, pool, container);
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.rmi.logical.AbstractRemoteObject#add(java.lang.Object)
     */
    @Override
    public void add(OrtschaftEntity object) throws InvalidDataException, RemoteException {
        thisUsersHasAuth();
        handleValidation(object);

        EntityManager em = getNewEm();
        try {
            logger.trace("Get ortschaft...");
            em.getTransaction().begin();
            OrtschaftEntity read = (OrtschaftEntity) em.find(getEntityClass(), object.getPlz());
            if (read == null) {
                logger.trace("Persist " + object.getPlz());
                em.persist(object);
            } else {
                logger.trace("Ortschaft bereits in der Datenbank vorhanden: " + object.getPlz());
            }
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.warn("Unable to execute add", e);
            em.getTransaction().rollback();
        } finally {
            checkInEm(em);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.rmi.logical.AbstractRemoteObject#getById(int)
     */
    @Override
    public OrtschaftEntity getById(int id) throws RemoteException, InvalidDataException {
        throw new InvalidDataException("Kann keine Postleitzahl als Zahl abrufen");
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /**
     * Gets the by id.
     *
     * @param plz the plz
     * @return the by id
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @SuppressWarnings({"unchecked"})
    public OrtschaftEntity getById(String plz) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Get object by id");
            em.getTransaction().begin();
            return (OrtschaftEntity) em.find(getEntityClass(), plz);
        } catch (ClassCastException e) {
            logger.warn("Unable to cast class", e);
            throw new InvalidDataException("Konnte kein Resultat finden, das der Suche entspricht.");
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.rmi.logical.AbstractRemoteObject#delete(int)
     */
    @Override
    public void delete(int id) throws RemoteException, InvalidDataException {
        throw new InvalidDataException("Kann keine Postleitzahl über Zahl löschen");
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.ConcreteOrtschaftAction#delete(java.lang.String)
     */
    public void delete(String plz) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to delete entity");
            em.getTransaction().begin();
            OrtschaftEntity entity = (OrtschaftEntity) em.find(getEntityClass(), plz);
            em.remove(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            logger.warn("Unable to execute delete", e);
            em.getTransaction().rollback();
            throw new InvalidDataException("Konnte Eintrag nicht löschen.", e);
        } finally {
            checkInEm(em);
        }
    }

    @SuppressWarnings({"unchecked"})
    public List<OrtschaftEntity> getListByPattern(String pattern) throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to filter ortschaft list");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t FROM OrtschaftEntity t " +
                    "WHERE t.plz LIKE ?1 OR t.gemeinde LIKE ?2 ");
            int number;
            try {
                number = Integer.parseInt(pattern);
            } catch (NumberFormatException e) {
                number = 0;
            }
            pattern = pattern.substring(0, 1).toUpperCase() + pattern.substring(1);
            query.setParameter(1, number + "%");
            query.setParameter(2, pattern + "%");
            query.setMaxResults(FeukoraSettings.getInstance().getRealInt("limitedRowSetMaxCount"));

            List<Object[]> list = query.getResultList();
            List<OrtschaftEntity> returnList = new ArrayList<>(list.size());
            for (Object item : list) {
                returnList.add((OrtschaftEntity) item);
            }
            return returnList;
        } catch (Exception e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().rollback();
            checkInEm(em);
        }
    }
}
