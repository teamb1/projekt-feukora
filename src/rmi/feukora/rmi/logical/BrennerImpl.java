package feukora.rmi.logical;

import feukora.entity.BrennerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteBrennerAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * <br />
 * <br />
 * <i>feukora.rmi.logical</i>, added at <b>16.04.14</b>
 *
 * @author Christian Klauenboesch <i>&lt;christian@klit.ch&rt;</i>
 * @version 00.01.00
 * @since 00.01.00
 */
public class BrennerImpl extends AbstractRemoteObject<BrennerEntity> implements
        ConcreteBrennerAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BrennerImpl.class);

    private String defaultOrderClause = "fabrikat ASC";

    /**
     * The entity class.
     */
    private Class entityClass = BrennerEntity.class;

    /**
     * Instantiates a new brenner impl.
     *
     * @throws RemoteException the remote exception
     */
    public BrennerImpl() throws RemoteException {

    }

    /**
     * Instantiates a new brenner impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public BrennerImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.interfaces.ConcreteBrennerAction#getAllByBrennerart(int)
         */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<BrennerEntity> getAllByBrennerart(int id)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Get all Brenner by Brennerart");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM BrennerEntity t1 INNER JOIN t1.brennerart WHERE t2.id = ?1");
            query.setParameter(1, id);
            List<Object[]> list = query.getResultList();
            List<BrennerEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((BrennerEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            logger.warn("Unable to get items", e);
            throw new InvalidDataException("Konnte keine Datenbankeinträge abrufen", e);
        } finally {
            em.getTransaction().commit();
        }
    }

    /**
     * Gets the int.
     *
     * @return the int
     * @throws RemoteException the remote exception
     */
    public int getInt() throws RemoteException {
        System.out.println("Got it");
        return 1;
    }
}
