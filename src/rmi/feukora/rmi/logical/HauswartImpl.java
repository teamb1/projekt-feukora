package feukora.rmi.logical;

import feukora.entity.HauswartEntity;
import feukora.interfaces.ConcreteHauswartAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Hauswart Impl Test
 * <p/>
 * Test ok, 27.04.14, ckl
 *
 * @author Christian Klauenboesch
 * @version 27.04.14
 * @since 16.04.14
 */
public class HauswartImpl extends AbstractRemoteObject<HauswartEntity> implements
        ConcreteHauswartAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(HauswartImpl.class);

    private String defaultOrderClause = "name ASC";

    /**
     * The entity class.
     */
    private Class entityClass = HauswartEntity.class;

    /**
     * Instantiates a new hauswart impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public HauswartImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
             * (non-Javadoc)
             *
             * @see feukora.interfaces.ConcreteHauswartAction#getByStandort(int)
             */
    @Override
    @SuppressWarnings({"unchecked"})
    public HauswartEntity getByStandort(int id) throws RemoteException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get all entities of type");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM HauswartEntity t1 INNER JOIN t1.standort t2 " +
                    "WHERE t2.id = ?1");
            query.setParameter(1, id);
            List<Object[]> list = query.getResultList();
            return (HauswartEntity) list.get(0)[0];
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().commit();
            checkInEm(em);
        }
    }
}
