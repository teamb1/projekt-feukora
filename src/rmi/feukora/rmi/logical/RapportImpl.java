package feukora.rmi.logical;

import feukora.entity.RapportEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteRapportAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 16.04.14
 * @since 16.04.14
 */
public class RapportImpl extends AbstractRemoteObject<RapportEntity> implements
        ConcreteRapportAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(RapportImpl.class);

    private String defaultOrderClause = "termin.datum ASC";

    /**
     * The entity class.
     */
    private Class entityClass = RapportEntity.class;

    /**
     * Instantiates a new rapport impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public RapportImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.interfaces.ConcreteRapportAction#getAllByKontrolleur(int)
         */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<RapportEntity> getAllByKontrolleur(int kontrolleurId)
            throws InvalidDataException, RemoteException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get entities by join");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM RapportEntity t1 INNER JOIN t1.termin.kontrolleur t2 " +
                    "WHERE t2.id = ?1");
            query.setParameter(1, kontrolleurId);
            List<Object[]> list = query.getResultList();
            List<RapportEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((RapportEntity) item[0]);
            }
            return returnList;
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            em.getTransaction().rollback();
            checkInEm(em);
        }
    }
}
