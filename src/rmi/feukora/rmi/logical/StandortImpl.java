package feukora.rmi.logical;

import feukora.entity.StandortEntity;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.ConcreteStandortAction;
import feukora.jpa.EntityManagerPool;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 16.04.14
 * @since 16.04.14
 */
public class StandortImpl extends AbstractRemoteObject<StandortEntity> implements
        ConcreteStandortAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(StandortImpl.class);

    private String defaultOrderClause = "ortschaft.plz ASC";

    /**
     * The entity class.
     */
    private Class entityClass = StandortEntity.class;

    /**
     * Instantiates a new standort impl.
     *
     * @param port      the port
     * @param pool      the pool
     * @param container the container
     * @throws RemoteException the remote exception
     */
    public StandortImpl(int port, EntityManagerPool pool, AuthorizationContainer container)
            throws RemoteException {
        super(port, pool, container);
    }

    /* (non-Javadoc)
     * @see feukora.rmi.logical.AbstractRemoteObject#getEntityClass()
     */
    public Class getEntityClass() {
        return entityClass;
    }

    public String getDefaultOrderClause() {
        return defaultOrderClause;
    }

    /*
         * (non-Javadoc)
         *
         * @see feukora.interfaces.ConcreteStandortAction#getByOrtschaft(int)
         */
    @Override
    @SuppressWarnings({"unchecked"})
    public List<StandortEntity> getByOrtschaft(String plz)
            throws RemoteException, InvalidDataException {
        thisUsersHasAuth();

        EntityManager em = getNewEm();
        try {
            logger.trace("Try to get entities by join");
            em.getTransaction().begin();
            Query query = em.createQuery("SELECT t1, t2 FROM StandortEntity t1 INNER JOIN t1.ortschaft t2 " +
                    "WHERE t2.plz = ?1");
            query.setParameter(1, plz);
            List<Object[]> list = query.getResultList();
            List<StandortEntity> returnList = new ArrayList<>(list.size());
            for (Object[] item : list) {
                returnList.add((StandortEntity) item[0]);
            }
            em.getTransaction().commit();
            return returnList;
        } catch (Exception e) {
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        } finally {
            checkInEm(em);
        }
    }
}
