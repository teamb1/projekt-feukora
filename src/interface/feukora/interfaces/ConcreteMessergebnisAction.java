package feukora.interfaces;

import feukora.entity.MessergebnisEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * The Interface ConcreteMessergebnisAction.
 *
 * @author Patrick Schmed
 * @version 21.04.2014
 * @since 09.04.2014
 */
public interface ConcreteMessergebnisAction
        extends BusinessAction<MessergebnisEntity>, BusinessActionRemovable {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(MessergebnisEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    MessergebnisEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the by rapport.
     *
     * @param rapportId the rapport id
     * @return the by rapport
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    List<MessergebnisEntity> getByRapport(int rapportId)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<MessergebnisEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(MessergebnisEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessActionRemovable#delete(int)
     */
    @Override
    void delete(int id)
            throws RemoteException, InvalidDataException;
}
