package feukora.interfaces;

import feukora.entity.KontrolleurEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteKontrolleurAction extends
        BusinessAction<KontrolleurEntity>, BusinessActionRemovable {

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(KontrolleurEntity object) throws RemoteException,
            InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    KontrolleurEntity getById(int id) throws RemoteException,
            InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<KontrolleurEntity> getAll() throws RemoteException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(KontrolleurEntity object) throws RemoteException,
            InvalidDataException;

}
