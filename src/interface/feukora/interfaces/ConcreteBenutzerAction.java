package feukora.interfaces;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * The Interface ConcreteBenutzerAction.
 *
 * @author Patrick Schmed
 * @version 21.04.2014
 * @since 09.04.2014
 */
public interface ConcreteBenutzerAction
        extends BusinessAction<BenutzerEntity>, BusinessActionRemovable {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(BenutzerEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    BenutzerEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the by funktion.
     *
     * @return the by funktion
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    List<BenutzerEntity> getKontrolleure()
            throws RemoteException, InvalidDataException;

    /**
     * Gets the regular users.
     *
     * @return the regular users
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    List<BenutzerEntity> getRegularUsers()
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<BenutzerEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(BenutzerEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessActionRemovable#delete(int)
     */
    @Override
    void delete(int id)
            throws RemoteException, InvalidDataException;
}
