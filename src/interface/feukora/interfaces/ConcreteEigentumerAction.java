package feukora.interfaces;

import feukora.entity.EigentumerEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */

public interface ConcreteEigentumerAction
        extends BusinessAction<EigentumerEntity>, BusinessActionRemovable {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(EigentumerEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    EigentumerEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the by standort.
     *
     * @param id the id
     * @return the by standort
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    EigentumerEntity getByStandort(int id)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<EigentumerEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(EigentumerEntity object)
            throws RemoteException, InvalidDataException;
}
