package feukora.interfaces;

import feukora.entity.RapportEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteRapportAction extends BusinessAction<RapportEntity>, BusinessActionRemovable {

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(RapportEntity object) throws RemoteException, InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    RapportEntity getById(int id) throws RemoteException, InvalidDataException;

    /**
     * Gets the all by kontrolleur.
     *
     * @param kontrolleurId the kontrolleur id
     * @return the all by kontrolleur
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */

    List<RapportEntity> getAllByKontrolleur(int kontrolleurId)
            throws RemoteException, InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<RapportEntity> getAll() throws RemoteException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(RapportEntity object) throws RemoteException,
            InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessActionRemovable#delete(int)
     */
    @Override
    void delete(int id)
            throws RemoteException, InvalidDataException;
}
