package feukora.interfaces;

import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteTerminAction
        extends BusinessAction<TerminEntity>, BusinessActionRemovable {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(TerminEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    TerminEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the all by kontrolleur.
     *
     * @param kontrolleurId the kontrolleur id
     * @return the all by kontrolleur
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    List<TerminEntity> getAllByKontrolleur(int kontrolleurId)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<TerminEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(TerminEntity object)
            throws RemoteException, InvalidDataException;

    List<TerminEntity> getNextDayByKontrolleur(int kontrolleurId)
            throws RemoteException, InvalidDataException;

    List<TerminEntity> getThisWeek()
            throws RemoteException, InvalidDataException;

}
