package feukora.interfaces;

import feukora.entity.VerwaltungEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteVerwaltungAction
        extends BusinessAction<VerwaltungEntity>, BusinessActionRemovable {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(VerwaltungEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    VerwaltungEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the by standort.
     *
     * @param standortId the standort id
     * @return the by standort
     * @throws RemoteException the remote exception
     */
    VerwaltungEntity getByStandort(int standortId)
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<VerwaltungEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(VerwaltungEntity object)
            throws RemoteException, InvalidDataException;

}
