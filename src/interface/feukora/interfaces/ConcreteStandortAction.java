package feukora.interfaces;

import feukora.entity.StandortEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteStandortAction extends BusinessAction<StandortEntity>, BusinessActionRemovable {

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(StandortEntity object) throws RemoteException,
            InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    StandortEntity getById(int id) throws RemoteException, InvalidDataException;

    /**
     * Gets the by ortschaft.
     *
     * @param plz the ortschaft id
     * @return the by ortschaft
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    List<StandortEntity> getByOrtschaft(String plz)
            throws RemoteException, InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<StandortEntity> getAll() throws RemoteException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(StandortEntity object) throws RemoteException,
            InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessActionRemovable#delete(int)
     */
    @Override
    void delete(int id)
            throws RemoteException, InvalidDataException;
}
