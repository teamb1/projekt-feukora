package feukora.interfaces;

import feukora.entity.WarmeerzeugerEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteWarmeerzeugerAction extends
        BusinessAction<WarmeerzeugerEntity>, BusinessActionRemovable {

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(WarmeerzeugerEntity object) throws RemoteException,
            InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    WarmeerzeugerEntity getById(int id) throws RemoteException,
            InvalidDataException;

    /**
     * Gets the all by brennstoff.
     *
     * @param brennstoffId the brennstoff id
     * @return the all by brennstoff
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    List<WarmeerzeugerEntity> getAllByBrennstoff(int brennstoffId)
            throws RemoteException, InvalidDataException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<WarmeerzeugerEntity> getAll() throws RemoteException;

    /*
     * (non-Javadoc)
     *
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(WarmeerzeugerEntity object) throws RemoteException,
            InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessActionRemovable#delete(int)
     */
    @Override
    void delete(int id)
            throws RemoteException, InvalidDataException;
}
