package feukora.interfaces;

import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public interface ConcreteOrtschaftAction
        extends BusinessAction<OrtschaftEntity> {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(OrtschaftEntity object)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    OrtschaftEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<OrtschaftEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(OrtschaftEntity object)
            throws RemoteException, InvalidDataException;

    /**
     * Delete.
     *
     * @param plz the plz
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    void delete(String plz)
            throws RemoteException, InvalidDataException;

    List<OrtschaftEntity> getListByPattern(String pattern)
            throws RemoteException, InvalidDataException;
}
