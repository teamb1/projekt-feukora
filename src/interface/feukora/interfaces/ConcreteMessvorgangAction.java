package feukora.interfaces;

import feukora.entity.MessvorgangEntity;
import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * The Interface ConcreteMessvorgangAction.
 *
 * @author Patrick Schmed
 * @version 21.04.2014
 * @since 09.04.2014
 */
public interface ConcreteMessvorgangAction
        extends BusinessAction<MessvorgangEntity> {

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#add(java.lang.Object)
     */
    @Override
    void add(MessvorgangEntity object) throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getById(int)
     */
    @Override
    MessvorgangEntity getById(int id)
            throws RemoteException, InvalidDataException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#getAll()
     */
    @Override
    List<MessvorgangEntity> getAll()
            throws RemoteException;

    /* (non-Javadoc)
     * @see feukora.interfaces.BusinessAction#update(java.lang.Object)
     */
    @Override
    void update(MessvorgangEntity object)
            throws RemoteException, InvalidDataException;
}
