package feukora.interfaces;

import feukora.exceptions.InvalidDataException;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 20.04.2014
 */

public interface BusinessAction<T> extends Remote {

    /**
     * Adds the.
     *
     * @param object the object
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    void add(T object)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the by id.
     *
     * @param id the id
     * @return the by id
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    T getById(int id)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the all.
     *
     * @return the all
     * @throws RemoteException the remote exception
     */
    List<T> getAll()
            throws RemoteException;

    /**
     * Update.
     *
     * @param object the object
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    void update(T object)
            throws RemoteException, InvalidDataException;

}
