package feukora.interfaces;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * @author Tscherrig Sven
 * @version 14.05.2014
 * @since 01.05.2014
 */

public interface ConcreteLoginAction
        extends BusinessAction<BenutzerEntity>, Serializable {

    /**
     * Check benutzer.
     *
     * @param benutzername the benutzername
     * @param passwort     the passwort
     * @return true, if successful
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    boolean checkBenutzer(String benutzername, String passwort)
            throws RemoteException, InvalidDataException;

    /**
     * Validates whether the user is a limited user or not
     * Limited user are not allowed to connect trough any service
     *
     * @param benutzername the username
     * @param password     the passwort
     * @return true if successful
     * @throws RemoteException
     * @throws InvalidDataException
     * @author Christian Klauenbösch
     */
    boolean checkLimitedBenutzer(String benutzername, String password)
            throws RemoteException, InvalidDataException;

    /**
     * Gets the user id.
     *
     * @param username the username
     * @return the user id
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    int getUserId(String username)
            throws RemoteException, InvalidDataException;

    /**
     * Logout.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    void logout()
            throws RemoteException, InvalidDataException;
}
