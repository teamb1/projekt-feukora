package feukora.interfaces;

import feukora.exceptions.InvalidDataException;

import java.rmi.RemoteException;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public interface BusinessActionRemovable {

    /**
     * Delete.
     *
     * @param id the id
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    void delete(int id)
            throws RemoteException, InvalidDataException;
}
