package feukora.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to mark methods that must be removed before production.
 *
 * @author Christian Klauenboesch
 * @version 21.04.2014
 * @since 21.04.2014
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
public @interface MethodOnlyTest {

}
