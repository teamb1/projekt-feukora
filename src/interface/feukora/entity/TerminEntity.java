package feukora.entity;

import feukora.settings.FeukoraSettings;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Enthält die Zeitpunktsinformationen für einen Rapport
 *
 * @author Florian Rieder
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "termin")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cterminEntity", propOrder = {"id", "kontrolleur", "datum", "standort"})
public class TerminEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The datum.
     */
    @NotNull(message = "Das Datum darf nicht leer sein.")
    @Temporal(TemporalType.TIMESTAMP)
    private GregorianCalendar datum;

    /**
     * The rapport.
     */
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    @XmlTransient
    private RapportEntity rapport;

    /**
     * The standort.
     */
    @NotNull(message = "Es muss ein Standort ausgewählt werden.")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private StandortEntity standort;

    /**
     * The kontrolleur.
     */

    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private KontrolleurEntity kontrolleur;

    /**
     * Instantiates a new termin entity.
     */
    public TerminEntity() {

    }

    /**
     * Gets the kontrolleur.
     *
     * @return the kontrolleur
     */
    public KontrolleurEntity getKontrolleur() {
        return kontrolleur;
    }

    /**
     * Sets the kontrolleur.
     *
     * @param kontrolleur the kontrolleur to set
     */
    public void setKontrolleur(KontrolleurEntity kontrolleur) {
        this.kontrolleur = kontrolleur;
    }

    /**
     * Gets the id.
     *
     * @return the pk_termin_id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the rapport.
     *
     * @return the fk_rapport_i d
     */
    public RapportEntity getRapport() {
        return rapport;
    }

    /**
     * Sets the rapport.
     *
     * @param rapport the rapport to set
     */
    public void setRapport(RapportEntity rapport) {
        this.rapport = rapport;
    }

    /**
     * Gets the datum.
     *
     * @return the datum
     */
    public GregorianCalendar getDatum() {
        return datum;
    }

    /**
     * Sets the datum.
     *
     * @param jahr   the jahr
     * @param monat  the monat
     * @param tag    the tag
     * @param stunde the stunde
     * @param minute the minute
     */
    public void setDatum(int jahr, int monat, int tag, int stunde, int minute) {
        this.datum = new GregorianCalendar(jahr, monat, tag, stunde, minute);
    }

    /**
     * Sets the datum gregorian.
     *
     * @param calendar the new datum gregorian
     */
    public void setDatumGregorian(GregorianCalendar calendar) {
        this.datum = calendar;
    }

    /**
     * Gets the standort.
     *
     * @return the fk_standort_id
     */
    public StandortEntity getStandort() {
        return standort;
    }

    /**
     * Sets the standort.
     *
     * @param standort the standort to set
     */
    public void setStandort(StandortEntity standort) {
        this.standort = standort;
    }

    /**
     * Gets the datum string.
     *
     * @return the datum string
     */
    public String getDatetimeString() {
        DateFormat format = new SimpleDateFormat(FeukoraSettings.getInstance().getRealString("datetimeFormat"), new Locale("de"));
        return "Datum: \n" + format.format(getDatum().getTime());
    }

    public String getDateString() {
        DateFormat format = new SimpleDateFormat(FeukoraSettings.getInstance().getRealString("dateFormat"), new Locale("de"));
        return format.format(getDatum().getTime());
    }

    public String getTimeString() {
        DateFormat format = new SimpleDateFormat(FeukoraSettings.getInstance().getRealString("timeFormat"), new Locale("de"));
        return format.format(getDatum().getTime());
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    public String toString() {

        // Seperator holen damit systemunabhangikeit verbessert wird
        String seperator = "newline";

        // Standort holen
        String standort = "--" + seperator + "Standort: " + seperator + "kein Standort";
        if (this.getStandort() != null) {
            standort = "--" + seperator + "Standort: " + seperator + this.getStandort().getOrtschaft().getPlz() + ", " + this.getStandort().getOrtschaft().getGemeinde() + ", " + this.getStandort().getOrtschaft().getKanton();
        }

        // Hauswart holen
        String hauswart = "--" + seperator + "Hauswart: " + seperator + "kein Hauswart";
        if (this.getStandort().getHauswart() != null) {
            hauswart = "--" + seperator + "Hauswart: " + seperator + this.getStandort().getHauswart().getVorname() + " " + this.getStandort().getHauswart().getName() + seperator + "Telefonnummer: " + this.getStandort().getHauswart().getTelefon();
        }

        String eigentumer = "--" + seperator + "Verwaltung: " + seperator + "kein Eigentumer";
        if (this.getStandort().getEigentumer() != null) {
            eigentumer = "--" + seperator + "Eigentumer: " + seperator + this.getStandort().getEigentumer().getVorname() + " " + this.getStandort().getEigentumer().getName() + seperator + "Telefonnummer: " + this.getStandort().getEigentumer().getTelefon();
        }

        // Verwaltung holen
        String verwaltung = "--" + seperator + "Verwaltung: " + seperator + "keine Verwaltung" + seperator + "____________________________________________";
        if (this.getStandort().getVerwaltung() != null) {
            verwaltung = "--" + seperator + "Verwaltung: " + seperator + this.getStandort().getVerwaltung().getVorname() + " " + this.getStandort().getVerwaltung().getName() + seperator + "Telefonnummer: " + this.getStandort().getVerwaltung().getTelefon() + seperator + "____________________________________________";
        }

        String ausgabe = this.getDatetimeString() + seperator + standort + seperator + eigentumer + seperator + hauswart + seperator + verwaltung;
        return ausgabe;
    }
}
