/**
 *
 */
package feukora.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Patrick Schmed
 * @version 15.05.2014
 * @since 09.04.2014
 */

@Entity
@Table(name = "ortschaft")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cortschaftEntity", propOrder = {"gemeinde", "kanton", "plz"})
public class OrtschaftEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The plz.
     */
    @Id
    @NotNull(message = "Die Postleitzahl darf nicht leer sein.")
    @Range(min = 1000, max = 9999, message = "Die Postleitzahl muss 4-stellig sein.")
    private String plz;

    /**
     * The gemeinde.
     */
    @Id
    @Length(min = 2, message = "Der Gemeindename muss mindestens zwei Zeichen lang sein.")
    private String gemeinde;

    /**
     * The kanton.
     */
    @Length(max = 2, message = "Der Kanton darf maximal mit zwei Zeichen dargestellt werden.")
    private String kanton;

    // Standardkonstruktor

    /**
     * Instantiates a new ortschaft entity.
     */
    public OrtschaftEntity() {

    }

    // Benutzerdefinierte Konstruktoren

    /**
     * wird nur für Testfälle verwendet.
     *
     * @param plz the plz
     */
    public OrtschaftEntity(String plz) {
        super();
        this.plz = plz;
        this.gemeinde = "test";
        this.kanton = "TE";
    }

    // Setter- und Gettermethoden

    /**
     * Gets the plz.
     *
     * @return the plz
     */
    public String getPlz() {
        return plz;
    }

    /**
     * Sets the plz.
     *
     * @param plz the new plz
     */
    public void setPlz(String plz) {
        this.plz = plz;
    }

    /**
     * Gets the gemeinde.
     *
     * @return the gemeinde
     */
    public String getGemeinde() {
        return gemeinde;
    }

    /**
     * Sets the gemeinde.
     *
     * @param gemeinde the new gemeinde
     */
    public void setGemeinde(String gemeinde) {
        this.gemeinde = gemeinde;
    }

    /**
     * Gets the kanton.
     *
     * @return the kanton
     */
    public String getKanton() {
        return kanton;
    }

    /**
     * Sets the kanton.
     *
     * @param kanton the new kanton
     */
    public void setKanton(String kanton) {
        this.kanton = kanton;
    }

}
