package feukora.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Sven Neitzel
 * @version 12.05.2014
 * @since 07.04.2014
 */

@Entity
@Table(name = "warmeerzeuger")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cwarmeerzeugerEntity", propOrder = {"id", "baujahr", "brennstoff", "fabrikat"})
public class WarmeerzeugerEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The fabrikat.
     */
    @NotNull(message = "Das Fabrikat darf nicht leer sein.")
    @Length(min = 2, message = "Das Fabrikat muss mindestens zwei Zeichen lang sein.")
    private String fabrikat;

    /**
     * The baujahr.
     */
    @NotNull(message = "Das Baujahr darf nicht leer sein.")
    @Range(min = 1900, max = 9999, message = "Das Baujahr muss zwischen 1900 und 9999 liegen.")
    private Integer baujahr;

    /**
     * The brennstoff.
     */
    @NotNull
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private BrennstoffEntity brennstoff;

    // Standardkonstruktor

    /**
     * Instantiates a new warmeerzeuger entity.
     */
    public WarmeerzeugerEntity() {
    }

    // Setter- und Gettermethoden

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the fabrikat.
     *
     * @return the fabrikat
     */
    public String getFabrikat() {
        return fabrikat;
    }

    /**
     * Sets the fabrikat.
     *
     * @param fabrikat the new fabrikat
     */
    public void setFabrikat(String fabrikat) {
        this.fabrikat = fabrikat;
    }

    /**
     * Gets the baujahr.
     *
     * @return the baujahr
     */
    public int getBaujahr() {
        return baujahr;
    }

    /**
     * Sets the baujahr.
     *
     * @param baujahr the new baujahr
     */
    public void setBaujahr(Integer baujahr) {
        this.baujahr = baujahr;
    }

    /**
     * Gets the brennstoff.
     *
     * @return the brennstoff
     */
    public BrennstoffEntity getBrennstoff() {
        return brennstoff;
    }

    /**
     * Sets the brennstoff.
     *
     * @param brennstoff the new brennstoff
     */
    public void setBrennstoff(BrennstoffEntity brennstoff) {
        this.brennstoff = brennstoff;
    }

}
