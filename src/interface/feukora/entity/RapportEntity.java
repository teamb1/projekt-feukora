/**
 *
 */
package feukora.entity;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class RapportEntity.
 *
 * @author Tscherrig Sven
 * @version 18.04.14
 * @since 07.04.14
 */
@Entity
@Table(name = "rapport")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rrapportEntity", propOrder = {"id", "bemerkung", "beurteilung", "brenner",
        "eigentumer", "hauswart", "kontrollart", "messergebnisse", "termin", "regulierung", "verwaltung", "warmeerzeuger"})
public class RapportEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;
    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;
    /**
     * The bemerkung.
     */
    private String bemerkung;
    /**
     * The beurteilung.
     */
    private String beurteilung;
    /**
     * The regulierung.
     */
    private STATUS regulierung;
    /**
     * Kontrollart true= Routine, false= Abnahme.
     */
    @NotNull(message = "Die Kontrollart muss ausgefüllt sein.")
    @Range(min = 1, max = 2, message = "Die Kontrollart muss 1 oder 2 betragen")
    private int kontrollart;
    /**
     * The termin.
     */
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private TerminEntity termin;
    /**
     * The hauswart.
     */
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private HauswartEntity hauswart;
    /**
     * The verwaltung.
     */
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private VerwaltungEntity verwaltung;
    /**
     * The eigentumer.
     */
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private EigentumerEntity eigentumer;
    /**
     * The brenner.
     */
    @NotNull(message = "Der Brenner darf nicht leer sein.")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private BrennerEntity brenner;
    /**
     * The messergebnisse.
     */
    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @Valid
    @XmlElement(nillable = true)
    private List<MessergebnisEntity> messergebnisse = new ArrayList<>();
    /**
     * The warmeerzeuger.
     */
    @NotNull(message = "Der Wärmeerzeuger darf nicht leer sein.")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private WarmeerzeugerEntity warmeerzeuger;

    /**
     * Instantiates a new rapport entity.
     */
    public RapportEntity() {

    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the bemerkung.
     *
     * @return the bemerkung
     */
    public String getBemerkung() {
        return bemerkung;
    }

    /**
     * Sets the bemerkung.
     *
     * @param bemerkung the new bemerkung
     */
    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    /**
     * Gets the beurteilung.
     *
     * @return the beurteilung
     */
    public String getBeurteilung() {
        return beurteilung;
    }

    /**
     * Sets the beurteilung.
     *
     * @param beurteilung the new beurteilung
     */
    public void setBeurteilung(String beurteilung) {
        this.beurteilung = beurteilung;
    }

    public STATUS getRegulierung() {
        return regulierung;
    }

    public void setRegulierung(STATUS regulierung) {
        this.regulierung = regulierung;
    }

    /**
     * Gets the termin.
     *
     * @return the termin
     */
    public TerminEntity getTermin() {
        return termin;
    }

    /**
     * Sets the termin.
     *
     * @param termin the new termin
     */
    public void setTermin(TerminEntity termin) {
        this.termin = termin;
    }

    /**
     * Gets the hauswart.
     *
     * @return the hauswart
     */
    public HauswartEntity getHauswart() {
        return hauswart;
    }

    /**
     * Sets the hauswart.
     *
     * @param hauswart the new hauswart
     */
    public void setHauswart(HauswartEntity hauswart) {
        this.hauswart = hauswart;
    }

    /**
     * Gets the verwaltung.
     *
     * @return the verwaltung
     */
    public VerwaltungEntity getVerwaltung() {
        return verwaltung;
    }

    /**
     * Sets the verwaltung.
     *
     * @param verwaltung the new verwaltung
     */
    public void setVerwaltung(VerwaltungEntity verwaltung) {
        this.verwaltung = verwaltung;
    }

    /**
     * Gets the eigentumer.
     *
     * @return the eigentumer
     */
    public EigentumerEntity getEigentumer() {
        return eigentumer;
    }

    /**
     * Sets the eigentumer.
     *
     * @param eigentumer the new eigentumer
     */
    public void setEigentumer(EigentumerEntity eigentumer) {
        this.eigentumer = eigentumer;
    }

    /**
     * Gets the brenner.
     *
     * @return the brenner
     */
    public BrennerEntity getBrenner() {
        return brenner;
    }

    /**
     * Sets the brenner.
     *
     * @param brenner the new brenner
     */
    public void setBrenner(BrennerEntity brenner) {
        this.brenner = brenner;
    }

    /**
     * Gets the messergebnisse.
     *
     * @return the messergebnisse
     */
    public List<MessergebnisEntity> getMessergebnisse() {
        return messergebnisse;
    }

    /**
     * Sets the messergebnisse.
     *
     * @param messergebnisse the new messergebnisse
     */
    public void setMessergebnisse(List<MessergebnisEntity> messergebnisse) {
        this.messergebnisse = messergebnisse;
    }

    /**
     * Gets the warmeerzeuger.
     *
     * @return the warmeerzeuger
     */
    public WarmeerzeugerEntity getWarmeerzeuger() {
        return warmeerzeuger;
    }

    /**
     * Sets the warmeerzeuger.
     *
     * @param warmeerzeuger the new warmeerzeuger
     */
    public void setWarmeerzeuger(WarmeerzeugerEntity warmeerzeuger) {
        this.warmeerzeuger = warmeerzeuger;
    }

    /**
     * Methode um den Rapport drucken zu k�nnen von Florian Rieder.
     *
     * @return the string
     */
    public String toString() {
        String ausgabe = "";
        // Seperator holen damit systemunabhängikeit verbessert wird
        String seperator = "\n";
        // Datum
        String datum = this.getTermin().getDatetimeString();

        // Gemeinde + Kanton + Standortadresse
        String gemeinde = "Gemeinde: " + this.getTermin().getStandort().getOrtschaft().getGemeinde();
        String kanton = " Kanton: " + this.getTermin().getStandort().getOrtschaft().getKanton();

        String adresse = "Adresse: " + this.getTermin().getStandort().getAdresse();

        // Eigentümer + Verwaltung + Hauswart
        String eigentumer = "Eigentümer: kein Eigentümer erfasst";
        if (this.getEigentumer() != null) {
            eigentumer = "Eigentümer: " + this.getEigentumer().getVorname() + " " + this.getEigentumer().getName();

        }
        String verwaltung = "Verwaltung: keine Verwaltung erfasst";
        if (this.getVerwaltung() != null) {
            verwaltung = "Verwaltung: " + this.getVerwaltung().getVorname() + " " + this.getVerwaltung().getName();
        }
        String hauswart = "Hauswart: kein Hauswart erfasst";
        if (this.getHauswart() != null) {
            hauswart = "Hauswart: " + this.getHauswart().getVorname() + " " + this.getHauswart().getName() + ", Tel: " + this.getHauswart().getTelefon();
        }
        // Kontrollart
        String kontrollart = "keine Kontrollart eingegeben";
        if (this.getKontrollart() == 1) {
            kontrollart = "Routinekontrolle";
        } else if (2 == this.getKontrollart()) {
            kontrollart = "Abnahmekontrolle";
        }
        // Warmeerzeuger + Brenner
        String warmeerzeuger = "Wärmeerzeuger: " + seperator + "Baujahr: " + this.getWarmeerzeuger().getBaujahr() + " Fabrikat/Typ: " + this.getWarmeerzeuger().getFabrikat() + " Brennstoff: " + this.getWarmeerzeuger().getBrennstoff().getBezeichnung();

        String brenner = "Brenner: " + seperator + "Baufjahr: " + this.getBrenner().getBaujahr() + " Fabrikat/Typ: " + this.getBrenner().getFabrikat() + " Brennertyp: " + this.getBrenner().getBrennerart().getBezeichnung() + " Feuerungswärmeleistung: " + this.getBrenner().getFeuerungswarmeleistung() + "kW";
        // Messergebnise

        String ueberschrift = "Messstufe|Messvorgang|Russzahl|CO bezogen auf 3% O2|Ölanteile|NO2 bezobgen auf 3% O2|Abgastemp.|Wärmeerzeugertemp.|Verbrennungslufttemp.|O2 Gehalt|Abgasversluste";
        String mess1 = this.getMessergebnisse().get(0).toString();
        String mess2 = this.getMessergebnisse().get(1).toString();
        String mess3 = this.getMessergebnisse().get(2).toString();
        String mess4 = this.getMessergebnisse().get(3).toString();
        // Bemerkungen
        String bemerkung = "Bemerkungen: " + this.getBemerkung();
        // Ausgabe zusammensetzen
        ausgabe = datum + seperator + gemeinde + kanton + seperator + adresse + seperator + eigentumer + seperator + verwaltung + seperator + hauswart + seperator + "Kontrollart: " + kontrollart + seperator + warmeerzeuger + seperator + brenner + seperator + ueberschrift + seperator + mess1 + seperator + mess2 + seperator + mess3 + seperator + mess4 + seperator + bemerkung + seperator;

        return ausgabe;
    }

    /**
     * Gets the kontrollart.
     *
     * @return the kontrollart
     */
    public int getKontrollart() {
        return kontrollart;
    }

    /**
     * Sets the kontrollart.
     *
     * @param kontrollart the kontrollart to set
     */
    public void setKontrollart(int kontrollart) {
        this.kontrollart = kontrollart;
    }

    /**
     * Status für die Beurteilung OK ist ok REG muss einreguliert werden NM
     * Einregulierung nicht möglich
     */
    public static enum STATUS {
        OK, REG, NM
    }
}
