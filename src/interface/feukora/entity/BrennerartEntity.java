package feukora.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Sven Neitzel
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "brennerart")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BrennerartEntity", propOrder = {"id", "bezeichnung"})
public class BrennerartEntity implements Serializable {
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The bezeichnung.
     */
    @NotNull(message = "Die Bezeichnung darf nicht leer sein.")
    @Length(min = 2, message = "Die Bezeichnung muss mindestens zwei Zeichen lang sein.")
    /** The bezeichnung. */
    private String bezeichnung;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the bezeichnung.
     *
     * @return the bezeichnung
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * Sets the bezeichnung.
     *
     * @param bezeichnung the new bezeichnung
     */
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
