/**
 *
 */
package feukora.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Neitzel Sven
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "standort")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cstandortEntity", propOrder = {"id", "adresse", "eigentumer", "hauswart", "ortschaft", "verwaltung"})
public class StandortEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The adresse.
     */
    @Length(min = 2, message = "Die Adresse muss mindestens zwei Zeichen lang sein.")
    private String adresse;

    /**
     * The ortschaft.
     */
    @NotNull(message = "Es muss eine Ortschaft ausgewählt werden.")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private OrtschaftEntity ortschaft;

    /**
     * The hauswart.
     */
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private HauswartEntity hauswart;

    /**
     * The verwaltung.
     */
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private VerwaltungEntity verwaltung;

    /**
     * The eigentumer.
     */
    @OneToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private EigentumerEntity eigentumer;

    // Standardkonstruktor

    /**
     * Instantiates a new standort entity.
     */
    public StandortEntity() {

    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the adresse.
     *
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * Sets the adresse.
     *
     * @param adresse the new adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * Gets the ortschaft.
     *
     * @return the ortschaft
     */
    public OrtschaftEntity getOrtschaft() {
        return ortschaft;
    }

    /**
     * Sets the ortschaft.
     *
     * @param ortschaft the new ortschaft
     */
    public void setOrtschaft(OrtschaftEntity ortschaft) {
        this.ortschaft = ortschaft;
    }

    /**
     * Gets the hauswart.
     *
     * @return the hauswart
     */
    public HauswartEntity getHauswart() {
        return hauswart;
    }

    /**
     * Sets the hauswart.
     *
     * @param hauswart the new hauswart
     */
    public void setHauswart(HauswartEntity hauswart) {
        this.hauswart = hauswart;
    }

    /**
     * Gets the verwaltung.
     *
     * @return the verwaltung
     */
    public VerwaltungEntity getVerwaltung() {
        return verwaltung;
    }

    /**
     * Sets the verwaltung.
     *
     * @param verwaltung the new verwaltung
     */
    public void setVerwaltung(VerwaltungEntity verwaltung) {
        this.verwaltung = verwaltung;
    }

    /**
     * Gets the eigentumer.
     *
     * @return the eigentumer
     */
    public EigentumerEntity getEigentumer() {
        return eigentumer;
    }

    /**
     * Sets the eigentumer.
     *
     * @param eigentumer the new eigentumer
     */
    public void setEigentumer(EigentumerEntity eigentumer) {
        this.eigentumer = eigentumer;
    }

}
