/**
 *
 */
package feukora.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Sven Tscherrig
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "verwaltung")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cverwaltungEntity", propOrder = {"id", "name", "ortschaft", "strasse", "telefon", "vorname"})
public class VerwaltungEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The name.
     */
    @NotBlank(message = "Der Name darf nicht leer sein.")
    @Length(min = 2, message = "Der Name muss mindestens zwei Zeichen lang sein.")
    private String name;

    /**
     * The vorname.
     */
    @NotBlank(message = "Der Vorname darf nicht leer sein.")
    @Length(min = 2, message = "Der Vorname muss mindestens zwei Zeichen lang sein.")
    private String vorname;

    /**
     * The telefon.
     */
    @Length(min = 10, max = 25, message = "Die Telefonnummer muss mindesten 10 Zeichen haben")
    private String telefon;

    /**
     * The strasse.
     */
    @Length(min = 2, message = "Die Strasse muss mindestens zwei Zeichen lang sein.")
    private String strasse;

    /**
     * The plz.
     */
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @NotNull(message = "Es muss eine Ortschaft ausgewählt werden.")
    @Valid
    private OrtschaftEntity ortschaft;

    /**
     * Standardkonstruktor.
     */
    public VerwaltungEntity() {

    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the vorname.
     *
     * @return the vorname
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Sets the vorname.
     *
     * @param vorname the vorname to set
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * Gets the telefon.
     *
     * @return the telefon
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * Sets the telefon.
     *
     * @param telefon the telefon to set
     */
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * Gets the strasse.
     *
     * @return the strasse
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Sets the strasse.
     *
     * @param strasse the strasse to set
     */
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    /**
     * Gets the ortschaft.
     *
     * @return the ortschaft
     */
    public OrtschaftEntity getOrtschaft() {
        return ortschaft;
    }

    /**
     * Sets the ortschaft.
     *
     * @param ortschaft the new ortschaft
     */
    public void setOrtschaft(OrtschaftEntity ortschaft) {
        this.ortschaft = ortschaft;
    }

}
