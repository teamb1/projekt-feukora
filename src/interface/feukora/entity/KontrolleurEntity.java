package feukora.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "kontrolleur")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("1")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ckontrolleurEntity", propOrder = {"ortschaft", "strasse", "telefon"})
public class KontrolleurEntity extends BenutzerEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The telefon.
     */
    @Length(min = 10, max = 25, message = "Die Telefonnummer muss mindesten 10 Zeichen haben.")
    private String telefon;

    /**
     * The strasse.
     */
    @Length(min = 2, message = "Die Strasse muss mindestens zwei Zeichen lang sein.")
    private String strasse;

    /**
     * The plz.
     */
    @ManyToOne(cascade = CascadeType.MERGE)
    @NotNull(message = "Es muss eine Ortschaft ausgewählt werden.")
    @Valid
    private OrtschaftEntity ortschaft;

    /**
     * Instantiates a new kontrolleur entity.
     */
    public KontrolleurEntity() {

    }

    /**
     * Gets the telefon.
     *
     * @return the telefon
     */
    public String getTelefon() {
        return telefon;
    }

    /**
     * Sets the telefon.
     *
     * @param telefon the new telefon
     */
    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    /**
     * Gets the strasse.
     *
     * @return the strasse
     */
    public String getStrasse() {
        return strasse;
    }

    /**
     * Sets the strasse.
     *
     * @param strasse the new strasse
     */
    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    /**
     * Gets the ortschaft.
     *
     * @return the ortschaft
     */
    public OrtschaftEntity getOrtschaft() {
        return ortschaft;
    }

    /**
     * Sets the ortschaft.
     *
     * @param ortschaft the new ortschaft
     */
    public void setOrtschaft(OrtschaftEntity ortschaft) {
        this.ortschaft = ortschaft;
    }
}
