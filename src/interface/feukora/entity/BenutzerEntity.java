package feukora.entity;

import feukora.exceptions.UnableException;
import feukora.util.Encoder;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Enthält alle Benutzerinformationen, die für das Login notwendig sind.
 *
 * @author Florian Rieder
 * @version 07.05.2014
 * @since 06.04.2014
 */
@Entity
@Table(name = "benutzer")
@DiscriminatorColumn(name = "isKontrolleur", discriminatorType = DiscriminatorType.INTEGER, length = 1)
@DiscriminatorValue("0")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cbenutzerEntity", propOrder = {"id", "benutzername", "isKontrolleur", "name", "passwort", "vorname", "displayable"})
@XmlSeeAlso({KontrolleurEntity.class})
public class BenutzerEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The benutzername.
     */
    @NotNull(message = "Der Benutzername darf nicht leer sein.")
    @Length(min = 2, message = "Der Benutzername muss mindestens zwei Zeichen lang sein.")
    @Column(unique = true)
    private String benutzername;

    /**
     * The name.
     */
    @NotBlank(message = "Der Name darf nicht leer sein.")
    @Length(min = 2, message = "Der Name muss mindestens zwei Zeichen lang sein.")
    private String name;

    /**
     * The vorname.
     */
    @NotBlank(message = "Der Vorname darf nicht leer sein.")
    @Length(min = 2, message = "Der Vorname muss mindestens zwei Zeichen lang sein.")
    private String vorname;

    /**
     * Indicates whether this is a kontrolleur or not Any value except 0
     * indicates true
     * <p/>
     * This field is an indicator for limited service access
     */
    private int isKontrolleur;

    /**
     * Indicates whether display this user in gui Any value except 0 indicates
     * true
     */
    private int displayable = 1;

    /**
     * The passwort.
     */
    @NotNull(message = "Das Passwort darf nicht Null sein.")
    @Length(min = 2, message = "Das Passwort muss mindestens zwei Zeichen lang sein.")
    private String passwort;

    /**
     * Instantiates a new benutzer entity.
     */
    public BenutzerEntity() {

    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the benutzername.
     *
     * @return the benutzername
     */
    public String getBenutzername() {
        return benutzername;
    }

    /**
     * Sets the benutzername.
     *
     * @param benutzername the benutzername to set
     */
    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the vorname.
     *
     * @return the vorname
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Sets the vorname.
     *
     * @param vorname the vorname to set
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * Gets the checks if is kontrolleur.
     *
     * @return the funktion
     */
    public int getIsKontrolleur() {
        if (isKontrolleur != 0) {
            return 1;
        }
        return 0;
    }

    /**
     * Sets the checks if is kontrolleur.
     *
     * @param funktion the funktion to set
     */
    public void setIsKontrolleur(int funktion) {
        this.isKontrolleur = funktion;
    }

    public int getDisplayable() {
        if (displayable != 0) {
            return 1;
        }
        return 0;
    }

    /**
     * Gets the passwort.
     *
     * @return the passwort
     */
    public String getPasswort() {
        return passwort;
    }

    /**
     * Sets the passwort.
     *
     * @param passwort the passwort to set
     */
    public void setPasswort(String passwort) throws UnableException {
        this.passwort = Encoder.hashPassword(passwort);
    }

}
