package feukora.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Enthält die Stufen der Messung
 *
 * @author Florian Rieder
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "messvorgang")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cmessvorgangEntity", propOrder = {"id", "messung", "stufe"})
public class MessvorgangEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    private Integer id;

    /**
     * The stufe.
     */
    private int stufe;

    /**
     * The messung.
     */
    private int messung;

    /**
     * Instantiates a new messvorgang entity.
     */
    public MessvorgangEntity() {
    }

    /**
     * Gets the id.
     *
     * @return the pk_messvorgang_id
     */
    public Integer getId() {
        return id;
    }

    public void setId(int i) {
        this.id = i;

    }

    /**
     * Gets the stufe.
     *
     * @return the stufe
     */
    public int getStufe() {
        return stufe;
    }

    /**
     * Sets the stufe.
     *
     * @param stufe the new stufe
     */
    public void setStufe(int stufe) {
        this.stufe = stufe;
    }

    /**
     * Gets the messung.
     *
     * @return the messung
     */
    public int getMessung() {
        return messung;
    }

    /**
     * Sets the messung.
     *
     * @param messung the messung to set
     */
    public void setMessung(int messung) {
        this.messung = messung;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return this.getStufe() + "    |     " + this.getMessung();
    }
}
