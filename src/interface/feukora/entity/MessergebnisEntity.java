package feukora.entity;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Enthält die Daten der Messung
 *
 * @author Florian Rieder
 * @version 15.05.2014
 * @since 09.04.2014
 */
@Entity
@Table(name = "messergebnis")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cmessergebnisEntity", propOrder = {"id", "abgastemp",
        "abgasverlust", "co", "messvorgang", "no", "o2gehalt", "oelanteil",
        "russzahl", "verbrennungstemp", "warmeerzeugertemp"})
public class MessergebnisEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 10L;

    /**
     * The id.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * The messvorgang.
     */
    @NotNull(message = "Der Messvorgang darf nicht leer sein.")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @Valid
    private MessvorgangEntity messvorgang;

    /**
     * The russzahl.
     */
    @Range(min = 0, max = 999, message = "Die Russzahl muss zwischen 0 und 999 sein.")
    private int russzahl;

    /**
     * The co.
     */
    @Range(min = 0, max = 999, message = "Das CO2 muss zwischen 0 und 999 sein.")
    private int co;

    /**
     * The oelanteil. true=Ja, false=nein
     */
    private boolean oelanteil;

    /**
     * The no.
     */
    @Range(min = 0, max = 999, message = "Der No-Wert muss zwischen 0 und 999 sein.")
    private int no;

    /**
     * The abgastemp.
     */
    @Range(min = 0, max = 999, message = "Die Abgastemperatur muss zwischen 0 und 999 sein.")
    private int abgastemp;

    /**
     * The waermeerzeugertemp.
     */
    @Range(min = 0, max = 999, message = "Die Wärmeerzeugertemperatur muss zwischen 0 und 999 sein.")
    private int warmeerzeugertemp;

    /**
     * The verbrennungstemp.
     */
    @Range(min = 0, max = 999, message = "Die Verbrennungstemperatur muss zwischen 0 und 999 sein.")
    private int verbrennungstemp;

    /**
     * The o2gehalt.
     */
    @Range(min = 0, max = 999, message = "Der Sauerstoffgehalt muss zwischen 0 und 999 sein.")
    private int o2gehalt;

    /**
     * The abgasverlust.
     */
    @Range(min = 0, max = 999, message = "Der Abgasverlust muss zwischen 0 und 999 sein.")
    private int abgasverlust;

    /**
     * Instantiates a new messergebnis entity.
     */
    public MessergebnisEntity() {

    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the messvorgang.
     *
     * @return the messvorgang
     */
    public MessvorgangEntity getMessvorgang() {
        return messvorgang;
    }

    /**
     * Sets the messvorgang.
     *
     * @param messvorgang the new messvorgang
     */
    public void setMessvorgang(MessvorgangEntity messvorgang) {
        this.messvorgang = messvorgang;
    }

    /**
     * Gets the russzahl.
     *
     * @return the russzahl
     */
    public int getRusszahl() {
        return russzahl;
    }

    /**
     * Sets the russzahl.
     *
     * @param russzahl the russzahl to set
     */
    public void setRusszahl(int russzahl) {
        this.russzahl = russzahl;
    }

    /**
     * Gets the co.
     *
     * @return the co
     */
    public int getCo() {
        return co;
    }

    /**
     * Sets the co.
     *
     * @param co the co to set
     */
    public void setCo(int co) {
        this.co = co;
    }

    /**
     * Gets the no.
     *
     * @return the no
     */
    public int getNo() {
        return no;
    }

    /**
     * Sets the no.
     *
     * @param no the no to set
     */
    public void setNo(int no) {
        this.no = no;
    }

    /**
     * Checks if is oelanteil.
     *
     * @return the oelanteil
     */
    public boolean isOelanteil() {
        return oelanteil;
    }

    /**
     * Sets the oelanteil.
     *
     * @param oelanteil the oelanteil to set
     */
    public void setOelanteil(boolean oelanteil) {
        this.oelanteil = oelanteil;
    }

    /**
     * Gets the abgastemp.
     *
     * @return the abgastemp
     */
    public int getAbgastemp() {
        return abgastemp;
    }

    /**
     * Sets the abgastemp.
     *
     * @param abgastemp the abgastemp to set
     */
    public void setAbgastemp(int abgastemp) {
        this.abgastemp = abgastemp;
    }

    /**
     * Gets the waermeerzeugertemp.
     *
     * @return the waermeerzeugertemp
     */
    public int getWarmeerzeugertemp() {
        return warmeerzeugertemp;
    }

    /**
     * Sets the waermeerzeugertemp.
     *
     * @param warmeerzeugertemp the waermeerzeugertemp to set
     */
    public void setWarmeerzeugertemp(int warmeerzeugertemp) {
        this.warmeerzeugertemp = warmeerzeugertemp;
    }

    /**
     * Gets the verbrennungstemp.
     *
     * @return the verbrennungstemp
     */
    public int getVerbrennungstemp() {
        return verbrennungstemp;
    }

    /**
     * Sets the verbrennungstemp.
     *
     * @param verbrennungstemp the verbrennungstemp to set
     */
    public void setVerbrennungstemp(int verbrennungstemp) {
        this.verbrennungstemp = verbrennungstemp;
    }

    /**
     * Gets the o2gehalt.
     *
     * @return the o2gehalt
     */
    public int getO2gehalt() {
        return o2gehalt;
    }

    /**
     * Sets the o2gehalt.
     *
     * @param o2gehalt the o2gehalt to set
     */
    public void setO2gehalt(int o2gehalt) {
        this.o2gehalt = o2gehalt;
    }

    /**
     * Gets the abgasverlust.
     *
     * @return the abgasverlust
     */
    public int getAbgasverlust() {
        return abgasverlust;
    }

    /**
     * Sets the abgasverlust.
     *
     * @param abgasverlust the abgasverlust to set
     */
    public void setAbgasverlust(int abgasverlust) {
        this.abgasverlust = abgasverlust;
    }

    /**
     * Format druck.
     *
     * @param zahl  the zahl
     * @param lange the lange
     * @return the string
     */
    public String formatDruck(int zahl, int lange) {

        String ausgabe = "" + zahl;
        int hinten = lange / 2 - 2;
        int vorne = lange / 2 - 3;
        String vo = "";
        String hi = "";
        if (lange % 2 == 1) {
            vo += " ";
        }
        for (int i = 0; i < vorne; i++) {
            vo += " ";
        }
        for (int i = 0; i < hinten; i++) {
            hi += " ";
        }
        switch (ausgabe.length()) {
            case 1:
                ausgabe = vo + "  " + ausgabe + hi;
                break;
            case 2:
                ausgabe = vo + " " + ausgabe + hi;
                break;
            case 3:
                ausgabe = vo + ausgabe + hi;
                break;
        }
        return ausgabe;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    public String toString() {

        String t = " | ";
        String tc = " C| ";
        String tp = " %| ";
        String oel = "";
        if (isOelanteil()) {
            oel = "  " + "ja" + "   ";
        } else {
            oel = "  " + "nein" + " ";
        }
        String messvorgang = "    " + this.getMessvorgang().toString() + "    "
                + t;
        String russ = this.formatDruck(this.getRusszahl(), 8);
        String co = this.formatDruck(this.getCo(), 20);
        String no = this.formatDruck(this.getNo(), 22);
        String abgas = this.formatDruck(this.getAbgastemp(), 9);
        String warmetemp = this.formatDruck(this.getWarmeerzeugertemp(), 17);
        String verbrenntemp = this.formatDruck(this.getVerbrennungstemp(), 20);
        String O2 = this.formatDruck(this.getO2gehalt(), 8);
        String abgasverlust = this.formatDruck(this.getAbgasverlust(), 14);

        String ausgabe = messvorgang + russ + t + co + t + oel + t + no + t
                + abgas + tc + warmetemp + tc + verbrenntemp + tc + O2 + tp
                + abgasverlust;

        return ausgabe;

    }
}
