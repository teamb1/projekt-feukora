package feukora.exceptions;

/**
 * @author Christian Klaunenbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public class MergeException extends Exception {

    /**
     * Instantiates a new merge exception.
     */
    public MergeException() {
    }

    /**
     * Instantiates a new merge exception.
     *
     * @param message the message
     */
    public MergeException(String message) {
        super(message);
    }

    /**
     * Instantiates a new merge exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public MergeException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
