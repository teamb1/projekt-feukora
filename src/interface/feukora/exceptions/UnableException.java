package feukora.exceptions;

/**
 * Thrown if the application is unable to execute a specific action.
 *
 * @author Christian Klauenboesch
 * @version 20.04.2014
 * @since 20.04.2014
 */
public class UnableException extends Exception {

    /**
     * Instantiates a new unable exception.
     */
    public UnableException() {
    }

    /**
     * Instantiates a new unable exception.
     *
     * @param message the message
     */
    public UnableException(String message) {
        super(message);
    }

    /**
     * Instantiates a new unable exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public UnableException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
