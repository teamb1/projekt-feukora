package feukora.exceptions;

/**
 * @author Christian Klaunbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public class NoSuchItemException extends Exception {

    /**
     * Instantiates a new no such item exception.
     */
    public NoSuchItemException() {
    }

    /**
     * Instantiates a new no such item exception.
     *
     * @param message the message
     */
    public NoSuchItemException(String message) {
        super(message);
    }

    /**
     * Instantiates a new no such item exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public NoSuchItemException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
