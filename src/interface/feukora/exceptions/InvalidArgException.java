package feukora.exceptions;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public class InvalidArgException extends Exception {

    /**
     * Instantiates a new invalid arg exception.
     */
    public InvalidArgException() {
    }

    /**
     * Instantiates a new invalid arg exception.
     *
     * @param message the message
     */
    public InvalidArgException(String message) {
        super(message);
    }

    /**
     * Instantiates a new invalid arg exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public InvalidArgException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
