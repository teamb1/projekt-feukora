package feukora.exceptions;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public class NotProvidedException extends Exception {

    /**
     * Instantiates a new not provided exception.
     */
    public NotProvidedException() {
    }

    /**
     * Instantiates a new not provided exception.
     *
     * @param message the message
     */
    public NotProvidedException(String message) {
        super(message);
    }

    /**
     * Instantiates a new not provided exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public NotProvidedException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
