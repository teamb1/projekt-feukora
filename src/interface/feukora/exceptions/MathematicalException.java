package feukora.exceptions;

/**
 * Handles the Actions for the Hauswart Overiew.
 *
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public class MathematicalException extends Exception {

    /**
     * Instantiates a new mathematical exception.
     */
    public MathematicalException() {
    }

    /**
     * Instantiates a new mathematical exception.
     *
     * @param message the message
     */
    public MathematicalException(String message) {
        super(message);
    }

    /**
     * Instantiates a new mathematical exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public MathematicalException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
