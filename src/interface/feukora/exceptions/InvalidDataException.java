package feukora.exceptions;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 16.04.2014
 */

public class InvalidDataException extends Exception {

    /**
     * Instantiates a new invalid data exception.
     */
    public InvalidDataException() {
    }

    /**
     * Instantiates a new invalid data exception.
     *
     * @param message the message
     */
    public InvalidDataException(String message) {
        super(message);
    }

    /**
     * Instantiates a new invalid data exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public InvalidDataException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public synchronized Throwable getCause() {
        if (super.getCause() == null) {
            return this;
        }
        return super.getCause();
    }
}
