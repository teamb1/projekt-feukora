package feukora.util;

import feukora.exceptions.UnableException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Christian Klauenbösch
 * @version 15.05.14
 * @since 09.05.14
 */
public class Encoder {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(Encoder.class);

    /**
     * Convert a byte array to a hex string representation<br />
     * It is only possible to convert an even number of bytes, overhead will be ignored<br />
     * <br />
     * If you have no seperator, set separator to an empty string.
     *
     * @param bytes     the byte array
     * @param separator seperator after each 2 hex chars
     * @return the hex string
     */
    public static String byteArrayToHexString(byte[] bytes, String separator) {
        int items = bytes.length;
        int count = 1;
        String answer = "";
        for (byte aByte : bytes) {
            String thisByte = Integer.toHexString(aByte);
            if (thisByte.length() < 2) {
                answer += "0" + thisByte;
            } else {
                answer += thisByte.substring(thisByte.length() - 2);
            }
            if (count < items) {
                answer += separator;
            }
            count++;
        }
        return answer;
    }

    /**
     * Convert a hex string representation to it's original byte array<br />
     * It is only possible to convert an even number of bytes, overhead will be ignored<br />
     * <br />
     * If you have no seperator, set separator to an empty string or null.
     *
     * @param hexString the hex string
     * @param separator seperator after each 2 hex chars
     * @return the byte array
     */
    public static byte[] hexStringToByteArray(String hexString, String separator) {
        if (separator.length() != 0) {
            String[] splitString = hexString.split(separator);
            byte[] answer = new byte[splitString.length];
            int i = 0;
            for (String item : splitString) {
                answer[i] = (byte) Integer.parseInt(item, 16);
                i++;
            }
            return answer;
        } else {
            return hexStringToByteArray(hexString);
        }
    }

    /**
     * Convert a hex string representation to it's original byte array<br />
     * Always to chars are parsed to a byte.
     *
     * @param hexString the hex string
     * @return the byte array
     */
    private static byte[] hexStringToByteArray(String hexString) {
        int i = 0;
        String item;
        // length of byte array is string length divided by 2
        // always ceil, because if 3 chars given, string length must be 3 / 2 = 2 (not 1[.5])
        byte[] answer = new byte[(int) Math.ceil((double) (hexString.length() / 2))];
        for (int a = 0; a < hexString.length(); a++) {
            if (a % 2 == 0) {
                item = hexString.substring(i, 2);
                answer[i] = (byte) Integer.parseInt(item, 16);
                i += 2;
            }
        }
        return answer;
    }

    /**
     * Conceals a string<br />
     * Returns a string with asterisks and the same length as the input string.
     *
     * @param input the string to conceal
     * @return a same-length string with asterisks
     */
    public static String concealString(String input) {
        String output = "";
        for (int i = 0; i < input.length(); i++) {
            output += "*";
        }
        return output;
    }

    /**
     * This gets the relative path of fullPath based on basePath        <br />
     * e.g. fullPath = /full/path/to/a/file                             <br />
     * basePAth = /full/path                                       <br />
     * returns  = to/a/file                                        <br />
     *
     * @param basePath the base path
     * @param fullPath the full path
     * @return the relative path of full path based on base path
     */
    public static String getRelativePathFromBasePath(String basePath, String fullPath) {
        return new File(basePath).toURI().relativize(new File(fullPath).toURI()).getPath();
    }

    public static String hashPassword(String password) throws UnableException {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes("UTF-8"));
            return Encoder.byteArrayToHexString(hash, ":");
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            logger.warn("Unable to hash password", e);
            throw new UnableException("Password kann nicht verschlüsselt werden.");
        }
    }
}
