package feukora.util;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public class HostBuilderUtil {

    /**
     * The sep port path.
     */
    private static String SEP_PORT_PATH = "/";

    /**
     * The sep host port.
     */
    private static String SEP_HOST_PORT = ":";

    /**
     * The prefix rmi.
     */
    private static String PREFIX_RMI = "rmi://";

    /**
     * The prefix soap.
     */
    private static String PREFIX_SOAP = "http://";

    /**
     * Rmi.
     *
     * @param host the host
     * @param port the port
     * @param path the path
     * @return the string
     */
    public static String rmi(String host, int port, String path) {
        return PREFIX_RMI + host + SEP_HOST_PORT +
                port +
                SEP_PORT_PATH + path;
    }

    /**
     * Soap.
     *
     * @param host the host
     * @param port the port
     * @param path the path
     * @return the string
     */
    public static String soap(String host, int port, String path) {
        return PREFIX_SOAP + host + SEP_HOST_PORT +
                port +
                SEP_PORT_PATH + path;
    }
}
