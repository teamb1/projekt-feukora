package feukora.util;

import feukora.exceptions.NoSuchItemException;

/**
 * The Interface AuthorizationContainer.
 *
 * @author Christian Klauenboesch
 * @version 10.05.14
 * @since 10.05.14
 */
public interface AuthorizationContainer {

    /**
     * Grant auth to a user.
     *
     * @param id the identification of the user
     */
    void grantAuth(String id);

    /**
     * Validate if a user has authentication.
     *
     * @param id the identification of the user
     * @return whether the user has auth or not
     */
    boolean userHasAuth(String id);

    /**
     * Refuse the authentication of a user.
     *
     * @param id the identification of the user
     * @throws NoSuchItemException the no such item exception
     */
    void refuseAuth(String id) throws NoSuchItemException;

    /**
     * Set a maximal lifetime of the authorization without any interaction
     *
     * @param lifetime lifetime in seconds
     */
    void setMaxLifetime(int lifetime);
}
