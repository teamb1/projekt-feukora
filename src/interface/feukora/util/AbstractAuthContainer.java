package feukora.util;

import org.apache.log4j.Logger;

import java.util.*;

/**
 * @author Christian Klauenboesch <i>&lt;christian@klit.ch&rt;</i>
 * @since /version/ / 2014-05-17
 */
public abstract class AbstractAuthContainer implements AuthorizationContainer {
    private static Logger logger = Logger.getLogger(AbstractAuthContainer.class);
    private int lifetime = 3600;
    private Timer timer;
    private String name;

    public AbstractAuthContainer(String name) {
        this.timer = new Timer(name);
        this.name = name;
    }

    abstract protected Map<String, Date> getAuthorizedUsers();

    protected void refreshAuthTime(String id) {
        if (getAuthorizedUsers().containsKey(id)) {
            logger.trace("Update user access time: " + id);
            getAuthorizedUsers().put(id, new Date());
        }
    }

    protected Timer getTimer() {
        return timer;
    }

    @Override
    public void setMaxLifetime(int lifetime) {
        if (lifetime > 60) {
            this.lifetime = lifetime;
        }
    }

    protected int getLifetime() {
        return lifetime;
    }

    protected void registerCleanup() {
        logger.debug("Register cleanup task named '" + name + "', interval (seconds): " + getLifetime() / 3);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Set<String> keys = getAuthorizedUsers().keySet();
                for (String key : keys) {
                    Date lastActivity = getAuthorizedUsers().get(key);
                    lastActivity.compareTo(new Date(new Date().getTime() - getLifetime()));
                    if (lastActivity.compareTo(new Date(new Date().getTime() - getLifetime())) < 0) {
                        logger.debug("Dropped user, not active anymore: " + key);
                        getAuthorizedUsers().remove(key);
                    }
                }
            }
        };
        getTimer().schedule(task, getLifetime() * 1000, (getLifetime() / 3) * 1000);
    }
}
