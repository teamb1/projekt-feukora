package feukora.settings;

import java.io.File;
import java.util.HashMap;

/**
 * @author Christian Klauenbösch
 * @version 17.05.2014
 * @since 08.04.2014
 */
public class FeukoraSettings extends AbstractSettings {

    /**
     * Corresponding data file.
     */
    public static String xmlFile = "feukorasettings.xml";

    /**
     * Contains boolean settings.
     */
    private static HashMap<String, Boolean> booleanSettings = new HashMap<>();

    /**
     * Contains integer settings.
     */
    private static HashMap<String, Integer> integerSettings = new HashMap<>();

    /**
     * Contains string settings.
     */
    private static HashMap<String, String> stringSettings = new HashMap<>();

    /**
     * Contains char settings.
     */
    private static HashMap<String, char[]> charSettings = new HashMap<>();

    /**
     * The instance.
     */
    private static FeukoraSettings instance;

    /**
     * Instantiates a new feukora settings.
     */
    private FeukoraSettings() {
    }

    /**
     * Gets the single instance of FeukoraSettings.
     *
     * @return single instance of FeukoraSettings
     */
    public static AbstractSettings getInstance() {
        if (instance == null) {
            instance = new FeukoraSettings();
            instance.load();
        }
        return instance;
    }

    /**
     * Define xml file
     *
     * @param xmlFile the file path
     */
    public static void setXmlFile(String xmlFile) {
        File file = new File(xmlFile);
        if (file.exists()) {
            FeukoraSettings.xmlFile = xmlFile;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.settings.AbstractSettings#getBooleanSettings()
     */
    @Override
    protected HashMap<String, Boolean> getBooleanSettings() {
        return booleanSettings;
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.settings.AbstractSettings#getIntegerSettings()
     */
    @Override
    protected HashMap<String, Integer> getIntegerSettings() {
        return integerSettings;
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.settings.AbstractSettings#getStringSettings()
     */
    @Override
    protected HashMap<String, String> getStringSettings() {
        return stringSettings;
    }

    /*
     * (non-Javadoc)
     *
     * @see feukora.settings.AbstractSettings#getCharSettings()
     */
    @Override
    protected HashMap<String, char[]> getCharSettings() {
        return charSettings;
    }

    /**
     * Load.
     */
    private void load() {
        PropertyInjector.inject(
                PropertyReader.getInstanceFromResource("feukorasettings.xml"),
                FeukoraSettings.getInstance()
        );

        PropertyInjector.inject(
                PropertyReader.getInstanceFromRunDir(FeukoraSettings.xmlFile),
                FeukoraSettings.getInstance()
        );
    }
}
