package feukora.settings;

import feukora.exceptions.MathematicalException;
import org.apache.log4j.Logger;

import java.util.Properties;
import java.util.Set;

/**
 * This class allows to inject a java.util.Property class into a AbstractSettings class
 *
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public class PropertyInjector {

    /**
     * The Constant logger.
     */
    private static final Logger logger = Logger.getLogger(PropertyInjector.class);

    /**
     * Injects all properties inside the Properties instance into the AbstractSettings<br />
     * <br />
     * Allows override of existing settings, if no s:name=final is declared.
     *
     * @param properties       the properties to inject
     * @param abstractSettings the settings instance to inject properties
     */
    public static void inject(Properties properties, AbstractSettings abstractSettings) {
        // get list of properties
        Set<String> list = properties.stringPropertyNames();
        // iterate through list
        for (String item : list) {
            // if this is a type, a t: or a s: setting, ignore it
            if (item.substring(0, 2).equals("t:")
                    || item.substring(0, 2).equals("s:")
                    || (item.length() > 5 && item.substring(0, 5).equals("type:"))) {
                continue;
            }
            setProperty(abstractSettings, properties, item);
        }
    }

    /**
     * Set a property.
     *
     * @param abstractSettings the settings instance to inject the property
     * @param properties       the properties to inject
     * @param item             the item to inject
     */
    private static void setProperty(AbstractSettings abstractSettings, Properties properties, String item) {
        try {
            String type = properties.getProperty("t:" + item);
            if (type == null) {
                type = "string";
            }
            switch (type) {
                case "int":
                    abstractSettings.setSetting(item, properties.getProperty(item), "int");
                    break;
                case "bool":
                    abstractSettings.setSetting(item, properties.getProperty(item), "bool");
                    break;
                case "char":
                    abstractSettings.setSetting(item, properties.getProperty(item), "char");
                    break;
                case "string":
                default:
                    abstractSettings.setSetting(item, properties.getProperty(item), "string");
                    break;
            }
        } catch (MathematicalException me) {
            logger.warn("Got a problem while injecting setting", me);
        }
    }
}
