package feukora.settings;

import feukora.exceptions.InvalidArgException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.net.URI;
import java.util.Properties;

/**
 * Extends java.util.Properties Adds type safety for properties
 *
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */

public class PropertyReader extends Properties {

    /**
     * The logger.
     */
    private static final Logger logger = Logger.getLogger(PropertyReader.class);

    /**
     * Loads a property file which is located at the resources folder<br />
     * <br />
     * If there is no such file, an empt instance of the PropertyReader will be
     * returned.
     *
     * @param propertyXml the name of the property file
     * @return a PropertyReader instance
     */
    public static PropertyReader getInstanceFromResource(
            final String propertyXml) {
        try {
            // 1. look at resources
            // remove heading slash
            String stripped = propertyXml.startsWith("/") ? propertyXml
                    .substring(1) : propertyXml;
            // get context class loader
            ClassLoader classLoader = Thread.currentThread()
                    .getContextClassLoader();
            if (classLoader != null) {
                logger.info("Load property file from resource dir: "
                        + propertyXml);
                try (InputStream stream = classLoader
                        .getResourceAsStream(stripped)) {
                    PropertyReader propertyReader = new PropertyReader();
                    propertyReader.loadFromXML(stream);
                    return propertyReader;
                }
            }
        } catch (Exception e) {
            logger.warn("Unable to read resources property", e);
        }
        return new PropertyReader();
    }

    /**
     * Loads a property file which is located at the run dir<br />
     * <br />
     * If there is no such file, an empt instance of the PropertyReader will be
     * returned.
     *
     * @param propertyXml the name of the property file
     * @return a PropertyReader instance
     */
    public static PropertyReader getInstanceFromRunDir(final String propertyXml) {
        try {
            // 2. look at run directory
            File runDir = new File(System.getProperty("user.dir"));
            logger.trace("Run dir path: " + runDir.getAbsolutePath());
            File[] files = runDir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.equals(propertyXml);
                }
            });
            if (files.length > 0) {
                Properties p = new Properties();
                PropertyReader propertyReader = new PropertyReader();
                logger.info("Load property file from run dir: " + propertyXml);
                propertyReader.loadFromXML(new FileInputStream(files[0]));
                return propertyReader;
            } else {
                logger.info("No file found at run dir, looked for "
                        + propertyXml);
            }
        } catch (Exception e) {
            logger.warn("Unable to read run dir property", e);
        }
        return new PropertyReader();
    }

    /**
     * Loads a property file which is located a the jar dir<br />
     * <br />
     * If there is no such file, an empty instance of the PropertyReader will be
     * returned.
     *
     * @param propertyXml the name of the property file
     * @return a PropertyReader instance
     */
    public static PropertyReader getInstanceFromJarDir(final String propertyXml) {
        try {
            // 3. look at jar directory
            URI url = PropertyReader.class.getProtectionDomain()
                    .getCodeSource().getLocation().toURI();
            logger.trace("Jar dir path: " + url.getPath());
            File jarDir = new File(url).getParentFile();
            File[] files = jarDir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.equals(propertyXml);
                }
            });
            if (files.length > 0) {
                PropertyReader propertyReader = new PropertyReader();
                logger.info("Load property file from jar dir: " + propertyXml);
                propertyReader.loadFromXML(new FileInputStream(files[0]));
                return propertyReader;
            } else {
                logger.info("No file found at jar dir, looked for "
                        + propertyXml);
            }
        } catch (Exception e) {
            logger.warn("Unable to read jar dir property", e);
        }
        return new PropertyReader();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.util.Properties#setProperty(java.lang.String, java.lang.String)
     */
    @Override
    public synchronized Object setProperty(String key, String value) {
        super.setProperty("type:" + key, "string");
        return super.setProperty(key, value);
    }

    /**
     * Sets the property.
     *
     * @param key   the key
     * @param value the value
     * @param type  the type
     * @return the object
     * @throws InvalidArgException the invalid arg exception
     */
    public synchronized Object setProperty(String key, String value, String type)
            throws InvalidArgException {
        try {
            switch (type) {
                case "string":
                    break;
                case "int":
                    new Integer(value);
                case "bool":
                    if (!value.equals("true") && !value.equals("false")) {
                        throw new InvalidArgException("this is not a valid boolean");
                    }
                    break;
                case "char":
                    if (value.length() != 1) {
                        throw new InvalidArgException("this is not a valid char");
                    }
                    break;
                default:
                    throw new InvalidArgException("not a valid property type");
            }
        } catch (NumberFormatException e) {
            throw new InvalidArgException("this is not a valid integer");
        }
        super.setProperty("type:" + key, type);
        return super.setProperty(key, value);
    }

    /**
     * Gets the property as int.
     *
     * @param key the key
     * @return the property as int
     * @throws InvalidArgException the invalid arg exception
     */
    public int getPropertyAsInt(String key) throws InvalidArgException {
        if (!super.getProperty("type:" + key).equals("int")) {
            throw new InvalidArgException("this is not an integer");
        }
        try {
            return new Integer(super.getProperty(key));
        } catch (NumberFormatException e) {
            throw new InvalidArgException("this is not an integer");
        }
    }

    /**
     * Gets the property as bool.
     *
     * @param key the key
     * @return the property as bool
     * @throws InvalidArgException the invalid arg exception
     */
    public Boolean getPropertyAsBool(String key) throws InvalidArgException {
        if (!super.getProperty("type:" + key).equals("bool")) {
            throw new InvalidArgException("this is not a boolean");
        }
        return new Boolean(super.getProperty(key));
    }

    /**
     * Gets the property as char.
     *
     * @param key the key
     * @return the property as char
     * @throws InvalidArgException the invalid arg exception
     */
    public char getPropertyAsChar(String key) throws InvalidArgException {
        if (!super.getProperty("type:" + key).equals("char")) {
            throw new InvalidArgException("this is not a boolean");
        }
        try {
            return super.getProperty(key).toCharArray()[0];
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidArgException("no char at index 0");
        }
    }

    /**
     * Gets the property as char array.
     *
     * @param key the key
     * @return the property as char array
     * @throws InvalidArgException the invalid arg exception
     */
    public char[] getPropertyAsCharArray(String key) throws InvalidArgException {
        if (!super.getProperty("type:" + key).equals("string")) {
            throw new InvalidArgException("this is not a string");
        }
        return super.getProperty(key).toCharArray();
    }
}
