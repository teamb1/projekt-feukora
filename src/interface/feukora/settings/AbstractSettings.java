package feukora.settings;

import feukora.exceptions.MathematicalException;
import feukora.exceptions.NoSuchItemException;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * @author Christian Klauenbösch
 * @version 15.05.2014
 * @since 09.04.2014
 */
public abstract class AbstractSettings {

    /**
     * The directory relative to the run dir where the xml file is located<br />
     * With a tailing slash<br />
     * .
     */
    public static String xmlFileDir = "settings/";
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(AbstractSettings.class);

    /**
     * Get the boolean settings hash map.
     *
     * @return the boolean settings
     */
    abstract protected HashMap<String, Boolean> getBooleanSettings();

    /**
     * Get the integer settings hash map.
     *
     * @return the integer settings
     */
    abstract protected HashMap<String, Integer> getIntegerSettings();

    /**
     * Get the string settings hash map.
     *
     * @return the string settings
     */
    abstract protected HashMap<String, String> getStringSettings();

    /**
     * Get the char settings hash map.
     *
     * @return the char settings
     */
    abstract protected HashMap<String, char[]> getCharSettings();

    /**
     * Sets the setting.
     *
     * @param key   the key
     * @param value the value
     * @param type  the type
     * @throws MathematicalException the mathematical exception
     */
    public synchronized void setSetting(String key, String value, String type)
            throws MathematicalException {
        try {
            switch (type) {
                case "bool":
                    getBooleanSettings().put(key, new Boolean(value));
                    break;
                case "string":
                    getStringSettings().put(key, value);
                    break;
                case "int":
                    getIntegerSettings().put(key, new Integer(value));
                    break;
                case "char":
                    getCharSettings().put(key, value.toCharArray());
                    break;
                default:
                    logger.warn("Unknown setting type: " + key + " -> " + type);
            }
        } catch (NumberFormatException nfe) {
            throw new MathematicalException("not a valid number");
        }
    }

    /**
     * Set a boolean setting.
     *
     * @param key   the key of the setting
     * @param value the boolean value
     */
    private void setBooleanSetting(String key, boolean value) {
        try {
            getBool(key);
            getBooleanSettings().put(key, value);
        } catch (NoSuchItemException nsie) {
            getBooleanSettings().put(key, value);
        }
    }

    /**
     * Set a string setting.
     *
     * @param key   the key of the setting
     * @param value the string value
     */
    private void setStringSetting(String key, String value) {
        try {
            getString(key);
            getStringSettings().put(key, value);
        } catch (NoSuchItemException nsie) {
            getStringSettings().put(key, value);
        }
    }

    /**
     * Set an integer setting.
     *
     * @param key   the key of the setting
     * @param value the integer value
     */
    private void setIntegerSetting(String key, int value) {
        try {
            getInt(key);
            getIntegerSettings().put(key, value);
        } catch (NoSuchItemException nsie) {
            getIntegerSettings().put(key, value);
        }
    }

    /**
     * Set a char setting.
     *
     * @param key   the key of the setting
     * @param value the char value
     */
    private void setCharSetting(String key, char[] value) {
        try {
            getCharArray(key);
            getCharSettings().put(key, value);
        } catch (NoSuchItemException nsie) {
            getCharSettings().put(key, value);
        }
    }

    /**
     * Gets the bool.
     *
     * @param settingName the setting name
     * @return the bool
     * @throws NoSuchItemException the no such item exception
     */
    public boolean getBool(String settingName) throws NoSuchItemException {
        Boolean bool = getBooleanSettings().get(settingName);
        if (bool == null) {
            throw new NoSuchItemException("no such item: " + settingName);
        }
        return bool;
    }

    /**
     * Gets the real bool.
     *
     * @param settingName the setting name
     * @return the real bool or false of no one found
     */
    public boolean getRealBool(String settingName) {
        Boolean bool = getBooleanSettings().get(settingName);
        if (bool == null) {
            logger.info("Unknown boolean setting, return boolean false");
            return false;
        }
        return bool;
    }

    /**
     * Gets the int.
     *
     * @param settingName the setting name
     * @return the int
     * @throws NoSuchItemException the no such item exception
     */
    public int getInt(String settingName) throws NoSuchItemException {
        Integer integer = getIntegerSettings().get(settingName);
        if (integer == null) {
            throw new NoSuchItemException("no such item: " + settingName);
        }
        return integer;
    }

    /**
     * Gets the real int.
     *
     * @param settingName the setting name
     * @return the real int
     */
    public int getRealInt(String settingName) {
        Integer integer = getIntegerSettings().get(settingName);
        if (integer == null) {
            return 0;
        }
        return integer;
    }

    /**
     * Gets the string.
     *
     * @param settingName the setting name
     * @return the string
     * @throws NoSuchItemException the no such item exception
     */
    public String getString(String settingName) throws NoSuchItemException {
        String item = getStringSettings().get(settingName);
        if (item == null) {
            throw new NoSuchItemException("no such item: " + settingName);
        }
        return item;
    }

    /**
     * Gets the real string.
     *
     * @param settingName the setting name
     * @return the real string
     */
    public String getRealString(String settingName) {
        String item = getStringSettings().get(settingName);
        if (item == null) {
            logger.info("Unknown string setting, return empty string");
            return "";
        }
        return item;
    }

    /**
     * Gets the char array.
     *
     * @param settingName the setting name
     * @return the char array
     * @throws NoSuchItemException the no such item exception
     */
    public char[] getCharArray(String settingName) throws NoSuchItemException {
        String item = getStringSettings().get(settingName);
        if (item == null) {
            throw new NoSuchItemException("no such item: " + settingName);
        }
        return item.toCharArray();
    }

    /**
     * Gets the real char array.
     *
     * @param settingName the setting name
     * @return the real char array
     */
    public char[] getRealCharArray(String settingName) {
        String item = getStringSettings().get(settingName);
        if (item == null) {
            logger.info("Unknown char setting, return empty char array");
            return new char[0];
        }
        return item.toCharArray();
    }
}
