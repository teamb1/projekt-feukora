package feukora.soap;

import feukora.settings.FeukoraSettings;

/**
 * @author Neitzel Sven
 * @version 17.05.14
 * @since 1.04.14
 */
public class Main {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) throws InterruptedException {
        FeukoraSettings.setXmlFile("settings_soap.xml");

        Main main = new Main();
        main.startPublisher();
        main.waitUntilEndOfEarth();
    }

    public void startPublisher() {
        Publisher publisher = new Publisher();
        publisher.setDaemon(true);
        publisher.start();
    }

    public void waitUntilEndOfEarth() throws InterruptedException {
        while (true) {
            Thread.sleep(30000);
        }
    }
}
