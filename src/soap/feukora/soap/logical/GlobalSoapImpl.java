package feukora.soap.logical;

import feukora.entity.*;
import feukora.exceptions.InvalidDataException;
import feukora.exceptions.NoSuchItemException;
import feukora.exceptions.UnableException;
import feukora.interfaces.*;
import feukora.util.Encoder;
import org.apache.log4j.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlType;
import java.rmi.RemoteException;
import java.util.List;

/**
 * The Class GlobalSoapImpl.
 *
 * @author Christian Klauenboesch
 * @version 25.05.14
 * @since 29.04.14
 */
@WebService
@XmlType(namespace = "feukora")
public class GlobalSoapImpl extends AbstractSoapAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(GlobalSoapImpl.class);

    /**
     * Instantiates a new global soap impl.
     */
    public GlobalSoapImpl() {
        super();
    }


    public boolean initialize(String username, String password) throws UnableException, InvalidDataException, RemoteException {
        return ((ConcreteLoginAction) getRmiImplementation("login")).checkBenutzer(
                username,
                Encoder.hashPassword(password)
        );
    }

    /**
     * Login.
     *
     * @param benutzername the benutzername
     * @param passwort     the passwort
     * @return the string
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @WebMethod(operationName = "login_validateUser")
    public String login(String benutzername, String passwort) throws RemoteException, InvalidDataException {
        try {
            logger.info("Try to log in user: " + benutzername);
            boolean auth = ((ConcreteLoginAction) getRmiImplementation("login"))
                    .checkLimitedBenutzer(benutzername, passwort);
            if (auth) {
                int id = ((ConcreteLoginAction) getRmiImplementation("login")).getUserId(benutzername);
                if (((ConcreteBenutzerAction) getRmiImplementation("benutzer")).getById(id).getIsKontrolleur() == 0) {
                    throw new InvalidDataException("Benutzer kann sich nicht über diesen Service anmelden.");
                }
                return requestAuth(Integer.toString(id));
            }
            throw new InvalidDataException("Die Anmeldeinformationen sind ungültig.");
        } catch (RemoteException e) {
            logger.debug("Unable to execute action", e);
            throw new InvalidDataException("Konnte Aktion nicht ausführen");
        } catch (UnableException e) {
            logger.warn("Unable to find endpoint", e);
            throw new RemoteException("Konnte Aktion nicht ausführen", e);
        }
    }

    /**
     * Logout.
     *
     * @param token the token
     */
    @WebMethod(operationName = "login_logoutUser")
    public void logout(String token) {
        logger.info("Try to log out user");
        refuseAuth(token);
    }

    /**
     * Rapport add.
     *
     * @param token  the token
     * @param entity the entity
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @WebMethod(operationName = "rapport_add")
    public void rapportAdd(String token, RapportEntity entity) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to add rapport");
            ((ConcreteRapportAction) getRmiImplementation("rapport")).add(entity);
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Rapport nicht hinzufügen.");
        }
    }

    @WebMethod(operationName = "rapport_getAll")
    public List<RapportEntity> rapportGetAll(String token) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to get all rapporte by kontrolleur");
            return ((ConcreteRapportAction) getRmiImplementation("rapport")).getAllByKontrolleur(
                    getUserId(token)
            );
        } catch (NoSuchItemException e) {
            logger.warn("No such action found", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        } catch (UnableException e) {
            logger.warn("Unable to retrieve endpoint", e);
            throw new RemoteException("Konnte aktion nicht ausführen.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid user id input", e);
            throw new InvalidDataException("Der Kontrolleur wurde nicht in der Datenbank gefunden.");
        }
    }

    @WebMethod(operationName = "messergebnis_add")
    public void messergebnisAdd(String token, MessergebnisEntity entity) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to add messergebnis");
            ((ConcreteMessergebnisAction) getRmiImplementation("messergebnis")).add(entity);
        } catch (UnableException e) {
            logger.warn("Unable to texecute action", e);
            throw new RemoteException("Konnte Messergebnis nicht hinzufügen.");
        }
    }

    /**
     * Brenner add.
     *
     * @param token  the token
     * @param entity the entity
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @WebMethod(operationName = "brenner_add")
    public void brennerAdd(String token, BrennerEntity entity) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to add brenner");
            ((ConcreteBrennerAction) getRmiImplementation("brenner")).add(entity);
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Brenner nicht hinzufügen.");
        }
    }

    /**
     * Warmeerzeuger add.
     *
     * @param token  the token
     * @param entity the entity
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @WebMethod(operationName = "warmeerzeuger_add")
    public void warmeerzeugerAdd(String token, WarmeerzeugerEntity entity) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to add brenner");
            ((ConcreteWarmeerzeugerAction) getRmiImplementation("warmeerzeuger")).add(entity);
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Wärmeerzeuger nicht hinzufügen.");
        }
    }

    @WebMethod(operationName = "warmeerzeuger_getAll")
    public List<WarmeerzeugerEntity> warmeerzeugerGetAll(String token) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to get all warmeerzeuger");
            return ((ConcreteWarmeerzeugerAction) getRmiImplementation("warmeerzeuger")).getAll();
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte Wärmeerzeuger nicht auslesen.");
        }
    }

    /**
     * Termin get all.
     *
     * @param token the token
     * @return the list
     * @throws RemoteException the remote exception
     */
    @WebMethod(operationName = "termin_getAll")
    public List<TerminEntity> terminGetAll(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get all termine");
            return ((ConcreteTerminAction) getRmiImplementation("termin")).getAllByKontrolleur(
                    getUserId(token)
            );
        } catch (UnableException e) {
            logger.warn("Unable to retrieve items", e);
            throw new RemoteException("Konnte Termine nicht abrufen.");
        } catch (NoSuchItemException e) {
            logger.warn("Unable to retrieve user information", e);
            throw new RemoteException("Konnte Termine nicht abrufen.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid data input");
            throw new RemoteException("Konnte Termine nicht abrufen.");
        }
    }

    @WebMethod(operationName = "termin_add")
    public void terminAdd(String token, TerminEntity entity) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to add a termin");
            ((ConcreteTerminAction) getRmiImplementation("termin")).add(entity);
        } catch (UnableException e) {
            logger.warn("Unable to get rmi impl", e);
            throw new RemoteException("Konnte Termin nicht hinzufügen.");
        } catch (RemoteException e) {
            logger.warn("Remote problem", e);
            throw new RemoteException("Konnte Termin nicht hinzufügen.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid data input", e);
            throw e;
        }
    }

    @WebMethod(operationName = "termin_update")
    public void terminUpdate(String token, TerminEntity entity) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to update termin");
            ((ConcreteTerminAction) getRmiImplementation("termin")).update(entity);
        } catch (UnableException e) {
            logger.warn("Unable exception", e);
            throw new RemoteException("Konnte Aktion nicht ausführen");
        } catch (RemoteException e) {
            logger.warn("Remote problem", e);
            throw e;
        } catch (InvalidDataException e) {
            logger.warn("Invalid data input", e);
            throw e;
        }
    }

    /**
     * Termin get by kontrolleur.
     *
     * @param token the token
     * @return the list
     * @throws RemoteException the remote exception
     */
    @WebMethod(operationName = "termin_byKontrolleur")
    public List<TerminEntity> terminGetByKontrolleur(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get termin by kontrolleur");
            return ((ConcreteTerminAction) getRmiImplementation("termin")).getAllByKontrolleur(
                    getUserId(token)
            );
        } catch (UnableException e) {
            logger.warn("Unable to retrieve information", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid input given", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        } catch (NoSuchItemException e) {
            logger.warn("No such user found", e);
            throw new RemoteException("Kein entsprechender Kontrolleur gefunden.");
        }
    }

    @WebMethod(operationName = "termin_nextDayByKontrolleur")
    public List<TerminEntity> terminGetNextDayByKontrolleur(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get termin of next day by kontrolleur");
            return ((ConcreteTerminAction) getRmiImplementation("termin")).getNextDayByKontrolleur(
                    getUserId(token)
            );
        } catch (UnableException e) {
            logger.warn("Unable to retrieve information", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid input given", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        } catch (NoSuchItemException e) {
            logger.warn("No such user found", e);
            throw new RemoteException("Kein entsprechender Kontrolleur gefunden.");
        }
    }

    @WebMethod(operationName = "standort_byId")
    public StandortEntity standortGetById(String token, int id) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to get standort by id");
            return ((ConcreteStandortAction) getRmiImplementation("standort")).getById(id);
        } catch (UnableException e) {
            logger.warn("Unable to retrieve information", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid input given", e);
            throw new RemoteException("Konnte Aktion nicht ausführen.");
        }
    }

    @WebMethod(operationName = "brenner_getAll")
    public List<BrennerEntity> brennerGetAll(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get all brenner");
            return ((ConcreteBrennerAction) getRmiImplementation("brenner")).getAll();
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Die Aktion konnte nicht ausgeführt werden.");
        }
    }

    @WebMethod(operationName = "brennerart_getAll")
    public List<BrennerartEntity> brennerartGetAll(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get all brennerart");
            return ((ConcreteBrennerartAction) getRmiImplementation("brennerart")).getAll();
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Konnte die Brennerarten nicht auslesen.");
        }
    }

    @WebMethod(operationName = "brennstoff_getAll")
    public List<BrennstoffEntity> brennstoffGetAll(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get all brennstoff");
            return ((ConcreteBrennstoffAction) getRmiImplementation("brennstoff")).getAll();
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Die Aktion konnte nicht ausgeführt werden.");
        }
    }

    @WebMethod(operationName = "ortschaft_getList")
    public List<OrtschaftEntity> ortschaftGetList(String token, String pattern) throws RemoteException, InvalidDataException {
        userHasAuth(token);

        try {
            logger.info("Try to fetch filtered ortschaft list");
            return ((ConcreteOrtschaftAction) getRmiImplementation("ortschaft")).getListByPattern(pattern);
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Die Aktion konnte nicht ausgeführt werden.");
        } catch (InvalidDataException e) {
            logger.warn("Invalid data input", e);
            throw new InvalidDataException(e.getMessage());
        }
    }

    @WebMethod(operationName = "messvorgang_getAll")
    public List<MessvorgangEntity> messvorgangGetAll(String token) throws RemoteException {
        userHasAuth(token);

        try {
            logger.info("Try to get all messvorgang entities");
            return ((ConcreteMessvorgangAction) getRmiImplementation("messvorgang")).getAll();
        } catch (UnableException e) {
            logger.warn("Unable to execute action", e);
            throw new RemoteException("Die Aktion konnte nicht ausgeführt werden.");
        } catch (RemoteException e) {
            logger.warn("Unable to call remote method", e);
            throw new RemoteException(e.getMessage());
        }
    }


}
