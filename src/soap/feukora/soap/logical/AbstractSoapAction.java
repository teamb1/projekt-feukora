package feukora.soap.logical;

import feukora.exceptions.NoSuchItemException;
import feukora.exceptions.UnableException;
import feukora.interfaces.BusinessAction;
import feukora.settings.FeukoraSettings;
import feukora.util.HostBuilderUtil;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.*;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Florian Rieder
 * @version 15.05.14
 * @since 29.04.14
 */

public abstract class AbstractSoapAction {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(AbstractSoapAction.class);

    /**
     * The authorization container.
     */
    private AuthorizationContainerImpl authorizationContainer = new AuthorizationContainerImpl();

    /**
     * Instantiates a new abstract soap action.
     */
    public AbstractSoapAction() {
        this(true);
    }

    /**
     * Instantiates a new abstract soap action.
     *
     * @param disableProxy the disable proxy
     */
    public AbstractSoapAction(boolean disableProxy) {
        System.setProperty("java.security.policy", "perm.policy");
        System.setSecurityManager(new RMISecurityManager());
        if (disableProxy) {
            disableProxy();
        }
    }

    /**
     * User has auth.
     *
     * @param token the token
     * @return true, if successful
     * @throws RemoteException the remote exception
     */
    protected boolean userHasAuth(String token) throws RemoteException {
        if (!authorizationContainer.userHasAuth(token)) {
            throw new RemoteException("Benutzer ist nicht authentifiziert.");
        }
        return true;
    }

    protected int getUserId(String token) throws NoSuchItemException {
        try {
            return Integer.parseInt(authorizationContainer.getAdditionnalUserInformation(token));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Request auth.
     *
     * @param id the id
     * @return the string
     */
    protected String requestAuth(String id) {
        String uuid = UUID.randomUUID().toString();
        authorizationContainer.grantAuth(uuid);
        try {
            authorizationContainer.storeAdditionnalUserInformation(uuid, id);
        } catch (NoSuchItemException e) {
            logger.warn("Unable to store user information", e);
        }
        return uuid;
    }

    /**
     * Gets the user information.
     *
     * @param id the id
     * @return the user information
     */
    protected String getUserInformation(String id) {
        try {
            return authorizationContainer.getAdditionnalUserInformation(id);
        } catch (NoSuchItemException e) {
            logger.warn("Unable to retrieve user information", e);
            return "";
        }
    }

    /**
     * Refuse auth.
     *
     * @param token the token
     */
    protected void refuseAuth(String token) {
        try {
            authorizationContainer.refuseAuth(token);
        } catch (NoSuchItemException e) {
            logger.warn("Unable to refuse, no such auth found: " + token);
        }
    }

    /**
     * Gets the rmi implementation.
     *
     * @param endpoint the endpoint
     * @return the rmi implementation
     * @throws UnableException the unable exception
     */
    protected BusinessAction getRmiImplementation(String endpoint) throws UnableException {
        try {
            return (BusinessAction) Naming.lookup(getRmiEndpoint(endpoint));
        } catch (NotBoundException e) {
            logger.warn("No such action found", e);
        } catch (MalformedURLException e) {
            logger.warn("Malformed url", e);
        } catch (RemoteException e) {
            logger.warn("Remote problem", e);
        }
        throw new UnableException("Unable to execute action");
    }

    /**
     * Disable proxy.
     */
    private void disableProxy() {
        ProxySelector selector = new ProxySelector() {
            @Override
            public List<Proxy> select(URI uri) {
                List<Proxy> proxies = new ArrayList<>();
                proxies.add(Proxy.NO_PROXY);
                return proxies;
            }

            @Override
            public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
                System.out.println("Not supported");
                throw new UnsupportedOperationException("Actually not supported action");
            }
        };
        ProxySelector.setDefault(selector);
    }

    /**
     * Gets the rmi endpoint.
     *
     * @param action the action
     * @return the rmi endpoint
     */
    @SuppressWarnings({"unchecked"})
    protected String getRmiEndpoint(String action) {
        return HostBuilderUtil.rmi(FeukoraSettings.getInstance().getRealString("rmiHost"),
                FeukoraSettings.getInstance().getRealInt("rmiPort"),
                FeukoraSettings.getInstance().getRealString("rmiPath") + action);
    }
}
