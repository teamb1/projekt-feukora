package feukora.soap.logical;

import feukora.exceptions.NoSuchItemException;
import feukora.util.AbstractAuthContainer;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class AuthorizationContainerImpl.
 *
 * @author Christian Klauenbösch
 * @version 16.05.14
 * @since 15.05.14
 */
public class AuthorizationContainerImpl extends AbstractAuthContainer implements AuthorizationContainer {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(AuthorizationContainerImpl.class);

    /**
     * The authorized users.
     */
    private final ConcurrentHashMap<String, Date> authorizedUsers = new ConcurrentHashMap<>(50);

    /**
     * The user information.
     */
    private final ConcurrentHashMap<String, String> userInformation = new ConcurrentHashMap<>(50);

    public AuthorizationContainerImpl() {
        super("SOAP-Auth");
    }

    /* (non-Javadoc)
         * @see feukora.util.AuthorizationContainer#grantAuth(java.lang.String)
         */
    public void grantAuth(String id) {
        logger.info("Grant auth to user '" + id + "'");
        authorizedUsers.put(id, new Date());
    }

    public ConcurrentHashMap<String, Date> getAuthorizedUsers() {
        return authorizedUsers;
    }

    /* (non-Javadoc)
         * @see feukora.util.AuthorizationContainer#userHasAuth(java.lang.String)
         */
    public boolean userHasAuth(String id) {
        refreshAuthTime(id);
        return authorizedUsers.containsKey(id);
    }

    /* (non-Javadoc)
     * @see feukora.util.AuthorizationContainer#refuseAuth(java.lang.String)
     */
    public void refuseAuth(String id) throws NoSuchItemException {
        logger.info("Refuse auth of user '" + id + "'");
        if (authorizedUsers.remove(id) == null) {
            logger.debug("Unknown user information, unable to drop");
            throw new NoSuchItemException("Unknown user");
        }
    }

    /**
     * Gets the additionnal user information.
     *
     * @param id the id
     * @return the additionnal user information
     * @throws NoSuchItemException the no such item exception
     */
    public String getAdditionnalUserInformation(String id) throws NoSuchItemException {
        String information = userInformation.get(id);
        if (information == null) {
            return "";
        }
        return information;
    }

    /**
     * Store additionnal user information.
     *
     * @param id          the id
     * @param information the information
     * @throws NoSuchItemException the no such item exception
     */
    public void storeAdditionnalUserInformation(String id, String information) throws NoSuchItemException {
        if (authorizedUsers.containsKey(id)) {
            userInformation.put(id, information);
            return;
        }
        throw new NoSuchItemException("No such user found");
    }
}