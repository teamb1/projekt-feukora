package feukora.soap;

import feukora.exceptions.InvalidDataException;
import feukora.exceptions.UnableException;
import feukora.settings.FeukoraSettings;
import feukora.soap.logical.GlobalSoapImpl;
import org.apache.log4j.Logger;

import javax.xml.ws.Endpoint;
import java.rmi.RemoteException;


/**
 * @author Christian Klauenbösch
 * @version 17.05.14
 * @since 5.05.14
 */
public class Publisher extends Thread {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(Publisher.class);

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        logger.debug("Start SOAP-Service");

        GlobalSoapImpl impl = new GlobalSoapImpl();

        // log in rmi side
        try {
            logger.info("Log in soap service into rmi server");
            if (!impl.initialize(FeukoraSettings.getInstance().getRealString("rmiAuthName"),
                    FeukoraSettings.getInstance().getRealString("rmiAuthPassword"))) {
                throw new InvalidDataException("Unable to log in");
            }
            logger.info("Log in successful");
        } catch (UnableException | RemoteException | InvalidDataException e) {
            logger.warn("Unable to call remote action", e);
            return;
        }

        Endpoint.publish(getEndpoint("feukora"), impl);

        logger.info("All services are published, this thread remains as daemon...");
    }

    /**
     * Gets the endpoint.
     *
     * @param impl the impl
     * @return the endpoint
     */
    public String getEndpoint(String impl) {
        return FeukoraSettings.getInstance().getRealString("soapHost") + ":"
                + FeukoraSettings.getInstance().getRealInt("soapPort") + "/"
                + FeukoraSettings.getInstance().getRealString("soapPath")
                + impl;
    }
}
