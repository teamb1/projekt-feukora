package feukora.stubs;

import feukora.entity.*;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the feukora.soap.logical package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RapportGetAll_QNAME = new QName("http://logical.soap.feukora/", "rapport_getAll");
    private final static QName _TerminGetAll_QNAME = new QName("http://logical.soap.feukora/", "termin_getAll");
    private final static QName _RapportAddResponse_QNAME = new QName("http://logical.soap.feukora/", "rapport_addResponse");
    private final static QName _MessergebnisAddResponse_QNAME = new QName("http://logical.soap.feukora/", "messergebnis_addResponse");
    private final static QName _OrtschaftGetList_QNAME = new QName("http://logical.soap.feukora/", "ortschaft_getList");
    private final static QName _InvalidDataException_QNAME = new QName("http://logical.soap.feukora/", "InvalidDataException");
    private final static QName _TerminUpdateResponse_QNAME = new QName("http://logical.soap.feukora/", "termin_updateResponse");
    private final static QName _TerminNextDayByKontrolleur_QNAME = new QName("http://logical.soap.feukora/", "termin_nextDayByKontrolleur");
    private final static QName _RapportAdd_QNAME = new QName("http://logical.soap.feukora/", "rapport_add");
    private final static QName _TerminAddResponse_QNAME = new QName("http://logical.soap.feukora/", "termin_addResponse");
    private final static QName _LoginLogoutUserResponse_QNAME = new QName("http://logical.soap.feukora/", "login_logoutUserResponse");
    private final static QName _WarmeerzeugerAdd_QNAME = new QName("http://logical.soap.feukora/", "warmeerzeuger_add");
    private final static QName _BrennerGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "brenner_getAllResponse");
    private final static QName _BrennerartGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "brennerart_getAllResponse");
    private final static QName _TerminAdd_QNAME = new QName("http://logical.soap.feukora/", "termin_add");
    private final static QName _MessvorgangGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "messvorgang_getAllResponse");
    private final static QName _WarmeerzeugerGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "warmeerzeuger_getAllResponse");
    private final static QName _LoginLogoutUser_QNAME = new QName("http://logical.soap.feukora/", "login_logoutUser");
    private final static QName _TerminGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "termin_getAllResponse");
    private final static QName _BrennerartGetAll_QNAME = new QName("http://logical.soap.feukora/", "brennerart_getAll");
    private final static QName _LoginValidateUser_QNAME = new QName("http://logical.soap.feukora/", "login_validateUser");
    private final static QName _BrennstoffGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "brennstoff_getAllResponse");
    private final static QName _MessvorgangGetAll_QNAME = new QName("http://logical.soap.feukora/", "messvorgang_getAll");
    private final static QName _BrennstoffGetAll_QNAME = new QName("http://logical.soap.feukora/", "brennstoff_getAll");
    private final static QName _BrennerAdd_QNAME = new QName("http://logical.soap.feukora/", "brenner_add");
    private final static QName _MessergebnisAdd_QNAME = new QName("http://logical.soap.feukora/", "messergebnis_add");
    private final static QName _BrennerGetAll_QNAME = new QName("http://logical.soap.feukora/", "brenner_getAll");
    private final static QName _StandortById_QNAME = new QName("http://logical.soap.feukora/", "standort_byId");
    private final static QName _LoginValidateUserResponse_QNAME = new QName("http://logical.soap.feukora/", "login_validateUserResponse");
    private final static QName _OrtschaftGetListResponse_QNAME = new QName("http://logical.soap.feukora/", "ortschaft_getListResponse");
    private final static QName _RapportGetAllResponse_QNAME = new QName("http://logical.soap.feukora/", "rapport_getAllResponse");
    private final static QName _WarmeerzeugerAddResponse_QNAME = new QName("http://logical.soap.feukora/", "warmeerzeuger_addResponse");
    private final static QName _WarmeerzeugerGetAll_QNAME = new QName("http://logical.soap.feukora/", "warmeerzeuger_getAll");
    private final static QName _BrennerAddResponse_QNAME = new QName("http://logical.soap.feukora/", "brenner_addResponse");
    private final static QName _TerminUpdate_QNAME = new QName("http://logical.soap.feukora/", "termin_update");
    private final static QName _StandortByIdResponse_QNAME = new QName("http://logical.soap.feukora/", "standort_byIdResponse");
    private final static QName _TerminNextDayByKontrolleurResponse_QNAME = new QName("http://logical.soap.feukora/", "termin_nextDayByKontrolleurResponse");
    private final static QName _TerminByKontrolleur_QNAME = new QName("http://logical.soap.feukora/", "termin_byKontrolleur");
    private final static QName _TerminByKontrolleurResponse_QNAME = new QName("http://logical.soap.feukora/", "termin_byKontrolleurResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: feukora.soap.logical
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InvalidDataException }
     */
    public InvalidDataException createInvalidDataException() {
        return new InvalidDataException();
    }

    /**
     * Create an instance of {@link OrtschaftGetList }
     */
    public OrtschaftGetList createOrtschaftGetList() {
        return new OrtschaftGetList();
    }

    /**
     * Create an instance of {@link MessergebnisAddResponse }
     */
    public MessergebnisAddResponse createMessergebnisAddResponse() {
        return new MessergebnisAddResponse();
    }

    /**
     * Create an instance of {@link RapportAddResponse }
     */
    public RapportAddResponse createRapportAddResponse() {
        return new RapportAddResponse();
    }

    /**
     * Create an instance of {@link TerminGetAll }
     */
    public TerminGetAll createTerminGetAll() {
        return new TerminGetAll();
    }

    /**
     * Create an instance of {@link RapportGetAll }
     */
    public RapportGetAll createRapportGetAll() {
        return new RapportGetAll();
    }

    /**
     * Create an instance of {@link TerminGetAllResponse }
     */
    public TerminGetAllResponse createTerminGetAllResponse() {
        return new TerminGetAllResponse();
    }

    /**
     * Create an instance of {@link LoginLogoutUser }
     */
    public LoginLogoutUser createLoginLogoutUser() {
        return new LoginLogoutUser();
    }

    /**
     * Create an instance of {@link WarmeerzeugerGetAllResponse }
     */
    public WarmeerzeugerGetAllResponse createWarmeerzeugerGetAllResponse() {
        return new WarmeerzeugerGetAllResponse();
    }

    /**
     * Create an instance of {@link MessvorgangGetAllResponse }
     */
    public MessvorgangGetAllResponse createMessvorgangGetAllResponse() {
        return new MessvorgangGetAllResponse();
    }

    /**
     * Create an instance of {@link TerminAdd }
     */
    public TerminAdd createTerminAdd() {
        return new TerminAdd();
    }

    /**
     * Create an instance of {@link BrennerartGetAllResponse }
     */
    public BrennerartGetAllResponse createBrennerartGetAllResponse() {
        return new BrennerartGetAllResponse();
    }

    /**
     * Create an instance of {@link WarmeerzeugerAdd }
     */
    public WarmeerzeugerAdd createWarmeerzeugerAdd() {
        return new WarmeerzeugerAdd();
    }

    /**
     * Create an instance of {@link BrennerGetAllResponse }
     */
    public BrennerGetAllResponse createBrennerGetAllResponse() {
        return new BrennerGetAllResponse();
    }

    /**
     * Create an instance of {@link TerminAddResponse }
     */
    public TerminAddResponse createTerminAddResponse() {
        return new TerminAddResponse();
    }

    /**
     * Create an instance of {@link LoginLogoutUserResponse }
     */
    public LoginLogoutUserResponse createLoginLogoutUserResponse() {
        return new LoginLogoutUserResponse();
    }

    /**
     * Create an instance of {@link RapportAdd }
     */
    public RapportAdd createRapportAdd() {
        return new RapportAdd();
    }

    /**
     * Create an instance of {@link TerminNextDayByKontrolleur }
     */
    public TerminNextDayByKontrolleur createTerminNextDayByKontrolleur() {
        return new TerminNextDayByKontrolleur();
    }

    /**
     * Create an instance of {@link OrtschaftGetListResponse }
     */
    public OrtschaftGetListResponse createOrtschaftGetListResponse() {
        return new OrtschaftGetListResponse();
    }

    /**
     * Create an instance of {@link LoginValidateUserResponse }
     */
    public LoginValidateUserResponse createLoginValidateUserResponse() {
        return new LoginValidateUserResponse();
    }

    /**
     * Create an instance of {@link StandortByIdResponse }
     */
    public StandortByIdResponse createStandortByIdResponse() {
        return new StandortByIdResponse();
    }

    /**
     * Create an instance of {@link StandortById }
     */
    public StandortById createStandortById() {
        return new StandortById();
    }

    /**
     * Create an instance of {@link BrennerGetAll }
     */
    public BrennerGetAll createBrennerGetAll() {
        return new BrennerGetAll();
    }

    /**
     * Create an instance of {@link BrennerAdd }
     */
    public BrennerAdd createBrennerAdd() {
        return new BrennerAdd();
    }

    /**
     * Create an instance of {@link MessergebnisAdd }
     */
    public MessergebnisAdd createMessergebnisAdd() {
        return new MessergebnisAdd();
    }

    /**
     * Create an instance of {@link BrennstoffGetAll }
     */
    public BrennstoffGetAll createBrennstoffGetAll() {
        return new BrennstoffGetAll();
    }

    /**
     * Create an instance of {@link MessvorgangGetAll }
     */
    public MessvorgangGetAll createMessvorgangGetAll() {
        return new MessvorgangGetAll();
    }

    /**
     * Create an instance of {@link TerminUpdateResponse }
     */
    public TerminUpdateResponse createTerminUpdateResponse() {
        return new TerminUpdateResponse();
    }

    /**
     * Create an instance of {@link TerminUpdate }
     */
    public TerminUpdate createTerminUpdate() {
        return new TerminUpdate();
    }

    /**
     * Create an instance of {@link BrennstoffGetAllResponse }
     */
    public BrennstoffGetAllResponse createBrennstoffGetAllResponse() {
        return new BrennstoffGetAllResponse();
    }

    /**
     * Create an instance of {@link LoginValidateUser }
     */
    public LoginValidateUser createLoginValidateUser() {
        return new LoginValidateUser();
    }

    /**
     * Create an instance of {@link BrennerartGetAll }
     */
    public BrennerartGetAll createBrennerartGetAll() {
        return new BrennerartGetAll();
    }

    /**
     * Create an instance of {@link TerminByKontrolleurResponse }
     */
    public TerminByKontrolleurResponse createTerminByKontrolleurResponse() {
        return new TerminByKontrolleurResponse();
    }

    /**
     * Create an instance of {@link TerminByKontrolleur }
     */
    public TerminByKontrolleur createTerminByKontrolleur() {
        return new TerminByKontrolleur();
    }

    /**
     * Create an instance of {@link TerminNextDayByKontrolleurResponse }
     */
    public TerminNextDayByKontrolleurResponse createTerminNextDayByKontrolleurResponse() {
        return new TerminNextDayByKontrolleurResponse();
    }

    /**
     * Create an instance of {@link BrennerAddResponse }
     */
    public BrennerAddResponse createBrennerAddResponse() {
        return new BrennerAddResponse();
    }

    /**
     * Create an instance of {@link WarmeerzeugerGetAll }
     */
    public WarmeerzeugerGetAll createWarmeerzeugerGetAll() {
        return new WarmeerzeugerGetAll();
    }

    /**
     * Create an instance of {@link WarmeerzeugerAddResponse }
     */
    public WarmeerzeugerAddResponse createWarmeerzeugerAddResponse() {
        return new WarmeerzeugerAddResponse();
    }

    /**
     * Create an instance of {@link RapportGetAllResponse }
     */
    public RapportGetAllResponse createRapportGetAllResponse() {
        return new RapportGetAllResponse();
    }

    /**
     * Create an instance of {@link WarmeerzeugerEntity }
     */
    public WarmeerzeugerEntity createCwarmeerzeugerEntity() {
        return new WarmeerzeugerEntity();
    }

    /**
     * Create an instance of {@link BrennerEntity }
     */
    public BrennerEntity createCbrennerEntity() {
        return new BrennerEntity();
    }

    /**
     * Create an instance of {@link VerwaltungEntity }
     */
    public VerwaltungEntity createCverwaltungEntity() {
        return new VerwaltungEntity();
    }

    /**
     * Create an instance of {@link BrennerartEntity }
     */
    public BrennerartEntity createBrennerartEntity() {
        return new BrennerartEntity();
    }

    /**
     * Create an instance of {@link RapportEntity }
     */
    public RapportEntity createRrapportEntity() {
        return new RapportEntity();
    }

    /**
     * Create an instance of {@link MessvorgangEntity }
     */
    public MessvorgangEntity createCmessvorgangEntity() {
        return new MessvorgangEntity();
    }

    /**
     * Create an instance of {@link TerminEntity }
     */
    public TerminEntity createCterminEntity() {
        return new TerminEntity();
    }

    /**
     * Create an instance of {@link MessergebnisEntity }
     */
    public MessergebnisEntity createCmessergebnisEntity() {
        return new MessergebnisEntity();
    }

    /**
     * Create an instance of {@link BenutzerEntity }
     */
    public BenutzerEntity createCbenutzerEntity() {
        return new BenutzerEntity();
    }

    /**
     * Create an instance of {@link OrtschaftEntity }
     */
    public OrtschaftEntity createCortschaftEntity() {
        return new OrtschaftEntity();
    }

    /**
     * Create an instance of {@link KontrolleurEntity }
     */
    public KontrolleurEntity createCkontrolleurEntity() {
        return new KontrolleurEntity();
    }

    /**
     * Create an instance of {@link BrennstoffEntity }
     */
    public BrennstoffEntity createCbrennstoffEntity() {
        return new BrennstoffEntity();
    }

    /**
     * Create an instance of {@link EigentumerEntity }
     */
    public EigentumerEntity createCeigentumerEntity() {
        return new EigentumerEntity();
    }

    /**
     * Create an instance of {@link HauswartEntity }
     */
    public HauswartEntity createChauswartEntity() {
        return new HauswartEntity();
    }

    /**
     * Create an instance of {@link StandortEntity }
     */
    public StandortEntity createCstandortEntity() {
        return new StandortEntity();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RapportGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "rapport_getAll")
    public JAXBElement<RapportGetAll> createRapportGetAll(RapportGetAll value) {
        return new JAXBElement<RapportGetAll>(_RapportGetAll_QNAME, RapportGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_getAll")
    public JAXBElement<TerminGetAll> createTerminGetAll(TerminGetAll value) {
        return new JAXBElement<TerminGetAll>(_TerminGetAll_QNAME, TerminGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RapportAddResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "rapport_addResponse")
    public JAXBElement<RapportAddResponse> createRapportAddResponse(RapportAddResponse value) {
        return new JAXBElement<RapportAddResponse>(_RapportAddResponse_QNAME, RapportAddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessergebnisAddResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "messergebnis_addResponse")
    public JAXBElement<MessergebnisAddResponse> createMessergebnisAddResponse(MessergebnisAddResponse value) {
        return new JAXBElement<MessergebnisAddResponse>(_MessergebnisAddResponse_QNAME, MessergebnisAddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrtschaftGetList }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "ortschaft_getList")
    public JAXBElement<OrtschaftGetList> createOrtschaftGetList(OrtschaftGetList value) {
        return new JAXBElement<OrtschaftGetList>(_OrtschaftGetList_QNAME, OrtschaftGetList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidDataException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "InvalidDataException")
    public JAXBElement<InvalidDataException> createInvalidDataException(InvalidDataException value) {
        return new JAXBElement<InvalidDataException>(_InvalidDataException_QNAME, InvalidDataException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminNextDayByKontrolleur }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_nextDayByKontrolleur")
    public JAXBElement<TerminNextDayByKontrolleur> createTerminNextDayByKontrolleur(TerminNextDayByKontrolleur value) {
        return new JAXBElement<TerminNextDayByKontrolleur>(_TerminNextDayByKontrolleur_QNAME, TerminNextDayByKontrolleur.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RapportAdd }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "rapport_add")
    public JAXBElement<RapportAdd> createRapportAdd(RapportAdd value) {
        return new JAXBElement<RapportAdd>(_RapportAdd_QNAME, RapportAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminAddResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_addResponse")
    public JAXBElement<TerminAddResponse> createTerminAddResponse(TerminAddResponse value) {
        return new JAXBElement<TerminAddResponse>(_TerminAddResponse_QNAME, TerminAddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginLogoutUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "login_logoutUserResponse")
    public JAXBElement<LoginLogoutUserResponse> createLoginLogoutUserResponse(LoginLogoutUserResponse value) {
        return new JAXBElement<LoginLogoutUserResponse>(_LoginLogoutUserResponse_QNAME, LoginLogoutUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WarmeerzeugerAdd }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "warmeerzeuger_add")
    public JAXBElement<WarmeerzeugerAdd> createWarmeerzeugerAdd(WarmeerzeugerAdd value) {
        return new JAXBElement<WarmeerzeugerAdd>(_WarmeerzeugerAdd_QNAME, WarmeerzeugerAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennerGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brenner_getAllResponse")
    public JAXBElement<BrennerGetAllResponse> createBrennerGetAllResponse(BrennerGetAllResponse value) {
        return new JAXBElement<BrennerGetAllResponse>(_BrennerGetAllResponse_QNAME, BrennerGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennerartGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brennerart_getAllResponse")
    public JAXBElement<BrennerartGetAllResponse> createBrennerartGetAllResponse(BrennerartGetAllResponse value) {
        return new JAXBElement<BrennerartGetAllResponse>(_BrennerartGetAllResponse_QNAME, BrennerartGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminAdd }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_add")
    public JAXBElement<TerminAdd> createTerminAdd(TerminAdd value) {
        return new JAXBElement<TerminAdd>(_TerminAdd_QNAME, TerminAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessvorgangGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "messvorgang_getAllResponse")
    public JAXBElement<MessvorgangGetAllResponse> createMessvorgangGetAllResponse(MessvorgangGetAllResponse value) {
        return new JAXBElement<MessvorgangGetAllResponse>(_MessvorgangGetAllResponse_QNAME, MessvorgangGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WarmeerzeugerGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "warmeerzeuger_getAllResponse")
    public JAXBElement<WarmeerzeugerGetAllResponse> createWarmeerzeugerGetAllResponse(WarmeerzeugerGetAllResponse value) {
        return new JAXBElement<WarmeerzeugerGetAllResponse>(_WarmeerzeugerGetAllResponse_QNAME, WarmeerzeugerGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginLogoutUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "login_logoutUser")
    public JAXBElement<LoginLogoutUser> createLoginLogoutUser(LoginLogoutUser value) {
        return new JAXBElement<LoginLogoutUser>(_LoginLogoutUser_QNAME, LoginLogoutUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_getAllResponse")
    public JAXBElement<TerminGetAllResponse> createTerminGetAllResponse(TerminGetAllResponse value) {
        return new JAXBElement<TerminGetAllResponse>(_TerminGetAllResponse_QNAME, TerminGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennerartGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brennerart_getAll")
    public JAXBElement<BrennerartGetAll> createBrennerartGetAll(BrennerartGetAll value) {
        return new JAXBElement<BrennerartGetAll>(_BrennerartGetAll_QNAME, BrennerartGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginValidateUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "login_validateUser")
    public JAXBElement<LoginValidateUser> createLoginValidateUser(LoginValidateUser value) {
        return new JAXBElement<LoginValidateUser>(_LoginValidateUser_QNAME, LoginValidateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennstoffGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brennstoff_getAllResponse")
    public JAXBElement<BrennstoffGetAllResponse> createBrennstoffGetAllResponse(BrennstoffGetAllResponse value) {
        return new JAXBElement<BrennstoffGetAllResponse>(_BrennstoffGetAllResponse_QNAME, BrennstoffGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessvorgangGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "messvorgang_getAll")
    public JAXBElement<MessvorgangGetAll> createMessvorgangGetAll(MessvorgangGetAll value) {
        return new JAXBElement<MessvorgangGetAll>(_MessvorgangGetAll_QNAME, MessvorgangGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennstoffGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brennstoff_getAll")
    public JAXBElement<BrennstoffGetAll> createBrennstoffGetAll(BrennstoffGetAll value) {
        return new JAXBElement<BrennstoffGetAll>(_BrennstoffGetAll_QNAME, BrennstoffGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennerAdd }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brenner_add")
    public JAXBElement<BrennerAdd> createBrennerAdd(BrennerAdd value) {
        return new JAXBElement<BrennerAdd>(_BrennerAdd_QNAME, BrennerAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MessergebnisAdd }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "messergebnis_add")
    public JAXBElement<MessergebnisAdd> createMessergebnisAdd(MessergebnisAdd value) {
        return new JAXBElement<MessergebnisAdd>(_MessergebnisAdd_QNAME, MessergebnisAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennerGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brenner_getAll")
    public JAXBElement<BrennerGetAll> createBrennerGetAll(BrennerGetAll value) {
        return new JAXBElement<BrennerGetAll>(_BrennerGetAll_QNAME, BrennerGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginValidateUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "login_validateUserResponse")
    public JAXBElement<LoginValidateUserResponse> createLoginValidateUserResponse(LoginValidateUserResponse value) {
        return new JAXBElement<LoginValidateUserResponse>(_LoginValidateUserResponse_QNAME, LoginValidateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrtschaftGetListResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "ortschaft_getListResponse")
    public JAXBElement<OrtschaftGetListResponse> createOrtschaftGetListResponse(OrtschaftGetListResponse value) {
        return new JAXBElement<OrtschaftGetListResponse>(_OrtschaftGetListResponse_QNAME, OrtschaftGetListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RapportGetAllResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "rapport_getAllResponse")
    public JAXBElement<RapportGetAllResponse> createRapportGetAllResponse(RapportGetAllResponse value) {
        return new JAXBElement<RapportGetAllResponse>(_RapportGetAllResponse_QNAME, RapportGetAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WarmeerzeugerAddResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "warmeerzeuger_addResponse")
    public JAXBElement<WarmeerzeugerAddResponse> createWarmeerzeugerAddResponse(WarmeerzeugerAddResponse value) {
        return new JAXBElement<WarmeerzeugerAddResponse>(_WarmeerzeugerAddResponse_QNAME, WarmeerzeugerAddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WarmeerzeugerGetAll }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "warmeerzeuger_getAll")
    public JAXBElement<WarmeerzeugerGetAll> createWarmeerzeugerGetAll(WarmeerzeugerGetAll value) {
        return new JAXBElement<WarmeerzeugerGetAll>(_WarmeerzeugerGetAll_QNAME, WarmeerzeugerGetAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BrennerAddResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "brenner_addResponse")
    public JAXBElement<BrennerAddResponse> createBrennerAddResponse(BrennerAddResponse value) {
        return new JAXBElement<BrennerAddResponse>(_BrennerAddResponse_QNAME, BrennerAddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminNextDayByKontrolleurResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_nextDayByKontrolleurResponse")
    public JAXBElement<TerminNextDayByKontrolleurResponse> createTerminNextDayByKontrolleurResponse(TerminNextDayByKontrolleurResponse value) {
        return new JAXBElement<TerminNextDayByKontrolleurResponse>(_TerminNextDayByKontrolleurResponse_QNAME, TerminNextDayByKontrolleurResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminByKontrolleur }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_byKontrolleur")
    public JAXBElement<TerminByKontrolleur> createTerminByKontrolleur(TerminByKontrolleur value) {
        return new JAXBElement<TerminByKontrolleur>(_TerminByKontrolleur_QNAME, TerminByKontrolleur.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminByKontrolleurResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://logical.soap.feukora/", name = "termin_byKontrolleurResponse")
    public JAXBElement<TerminByKontrolleurResponse> createTerminByKontrolleurResponse(TerminByKontrolleurResponse value) {
        return new JAXBElement<TerminByKontrolleurResponse>(_TerminByKontrolleurResponse_QNAME, TerminByKontrolleurResponse.class, null, value);
    }

}
