package feukora.stubs;

import feukora.entity.StandortEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for standort_byIdResponse complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="standort_byIdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://logical.soap.feukora/}cstandortEntity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "c_standort_byIdResponse", propOrder = {
        "_return"
})
public class StandortByIdResponse {

    @XmlElement(name = "return")
    protected StandortEntity _return;

    /**
     * Gets the value of the return property.
     *
     * @return possible object is
     * {@link StandortEntity }
     */
    public StandortEntity getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     *
     * @param value allowed object is
     *              {@link StandortEntity }
     */
    public void setReturn(StandortEntity value) {
        this._return = value;
    }

}
