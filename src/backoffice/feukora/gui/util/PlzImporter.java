package feukora.gui.util;

import feukora.exceptions.UnableException;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * PLZ Importer um PLZ-Listen der Schweizer Post zu importieren.
 *
 * @author Christian Klauenboesch
 * @version 2014-05-12
 * @since 2014-05-09
 */
public class PlzImporter {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(PlzImporter.class);

    /**
     * The file to read from.
     */
    private File source;

    /**
     * The column index of the "plz" value, 0 based.
     */
    private int indexPlz = 2;

    /**
     * The column index of the "city name" value, 0 based.
     */
    private int indexName = 4;

    /**
     * The column index of the "country name" value, 0 based.
     */
    private int indexCountry = 6;

    /**
     * The temporal store of the cities.
     */
    private Map<String, List<String>> list = new HashMap<>(2500);

    /**
     * Instantiates a new plz importer.
     *
     * @param source the source
     */
    public PlzImporter(File source) {
        this.source = source;
    }

    /**
     * Gets the indexes plus1.
     *
     * @return the indexes plus1
     */
    public static int[] getIndexesPlus1() {
        return new int[]{2, 4, 6};
    }

    /**
     * Gets the indexes light.
     *
     * @return the indexes light
     */
    public static int[] getIndexesLight() {
        return new int[]{2, 4, 6};
    }


    /**
     * Sets the indexes.
     *
     * @param indexPlz     the index plz
     * @param indexName    the index name
     * @param indexCountry the index country
     */
    public void setIndexes(int indexPlz, int indexName, int indexCountry) {
        this.indexPlz = indexPlz;
        this.indexName = indexName;
        this.indexCountry = indexCountry;
    }

    /**
     * Returns a list of cities read from the txt file
     * Map<String,  List<String>>
     * \- The postal code
     * \- List with
     * 1. City name
     * 2. Country name
     *
     * @return the cities list
     * @throws UnableException if an error occurs
     */
    public Map<String, List<String>> getList() throws UnableException {
        try {
            importList();
            logger.debug("Got a list of " + list.size() + " items inside the list");
            return list;
        } catch (IOException e) {
            logger.warn("Got an io problem", e);
        } catch (IndexOutOfBoundsException e) {
            // ignore
        }
        throw new UnableException("Konnte PLZ-Datei nicht verarbeiten");
    }

    /**
     * Gets the list.
     *
     * @param indexPlz  the index plz
     * @param indexName the index name
     * @return the list
     * @throws UnableException the unable exception
     */
    public Map<String, List<String>> getList(int indexPlz, int indexName) throws UnableException {
        setIndexes(indexPlz, indexName, indexCountry);
        return getList();
    }

    /**
     * Import list.
     *
     * @throws IOException               Signals that an I/O exception has occurred.
     * @throws IndexOutOfBoundsException the index out of bounds exception
     */
    private void importList() throws IOException, IndexOutOfBoundsException {
        BufferedReader reader = getReader(source);
        String line = "";
        while (null != (line = reader.readLine())) {
            String[] s = line.split("\t");
            try {
                List<String> tmpList = new ArrayList<>(2);
                tmpList.add(s[indexName]);
                tmpList.add(s[indexCountry]);
                list.put(s[indexPlz], tmpList);
            } catch (IndexOutOfBoundsException e) {
                logger.warn("Index out of bounds: " + e);
                throw e;
            }
        }
    }

    /**
     * Gets the reader.
     *
     * @param file the file
     * @return the reader
     * @throws FileNotFoundException the file not found exception
     */
    private BufferedReader getReader(File file) throws FileNotFoundException {
        return new BufferedReader(new FileReader(file));
    }
}
