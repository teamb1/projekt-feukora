package feukora.gui.util;

import feukora.entity.TerminEntity;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.List;

/**
 * The Class Drucker.
 * 
 * @author Florian Rieder
 */
public class Drucker {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(Drucker.class);

    /**
     * The file.
     */
    File file;

    /**
     * The writer.
     */
    FileWriter writer;

    /**
     * Drucken string.
     *
     * @param ausgabe the ausgabe
     * @param pfad    the pfad
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void druckenString(String ausgabe, String pfad) throws IOException {

        file = new File(pfad + ".txt");

        // zweiter Versuch
        // PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
        // file)));
        // out.println(ausgabe);
        // out.write(ausgabe);
        // out.flush();
        // out.close();
        // System.out.println(ausgabe);

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            String[] toArr = ausgabe.split("newLine");
            for (int i = 0; i < toArr.length; i++) {
                out.write(toArr[i]);
                out.println();

            }
            out.println();
            out.flush();
            out.close();
        } catch (IOException e) {
            logger.warn("IO Problem konnte nicht drucken", e);
        }
    }

    /**
     * Drucken array.
     *
     * @param termine the termine
     * @param pfad    the pfad
     * @throws IOException Signals that an I/O exception has occurred.
     * @author Sven Neitzel
     */
    public void druckenArray(List<TerminEntity> termine, String pfad) throws IOException {

        file = new File(pfad + ".txt");
        int sizeArray = termine.size();
        int count = 0;

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));

            for (; count < sizeArray; count++) {
                String[] toArr = termine.get(count).toString().split("newline");
                for (int i = 0; i < toArr.length; i++) {
                    out.write(toArr[i]);
                    out.println();
                }
            }
            out.println();
            out.flush();
            out.close();
        } catch (IOException e) {
            logger.warn("IO Problem konnte nicht drucken", e);
        }
    }
}
