package feukora.gui.util;

import feukora.settings.FeukoraSettings;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Klasse zum Testen der Server-Verbindung.
 *
 * @author Sven Neitzel
 * @version 2014-05-10
 * @since 2014-05-10
 */
public class ConnectionChecker {

    /**
     * The connection ok.
     */
    static boolean connectionOk;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(ConnectionChecker.class);
    /**
     * The host.
     */
    String host;

    /**
     * Connection check.
     *
     * @param host the host
     * @return true, if successful
     * @throws HeadlessException the headless exception
     * @throws IOException       Signals that an I/O exception has occurred.
     */
    public static boolean ConnectionCheck(String host) throws HeadlessException, IOException {
        try {
            if (InetAddress.getByName(host).isReachable(
                    FeukoraSettings.getInstance().getRealInt("rmiConnectionCheckTimeout")
            ) && !FeukoraSettings.getInstance().getRealBool("forceSoap")) {
                connectionOk = true;
            } else {
                if (FeukoraSettings.getInstance().getRealBool("forceSoap")) {
                    logger.debug("Force SOAP is active!");
                }
                connectionOk = false;
            }
        } catch (HeadlessException e) {
            logger.warn("Got an HeadlessException", e);
            e.printStackTrace();
        } catch (IOException e) {
            logger.warn("Got an IOException", e);
            e.printStackTrace();
        }
        return connectionOk;
    }
}
