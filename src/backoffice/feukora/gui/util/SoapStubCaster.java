package feukora.gui.util;

import feukora.entity.*;
import feukora.exceptions.InvalidDataException;
import feukora.interfaces.*;
import feukora.stubs.GlobalSoapImpl;
import feukora.stubs.InvalidDataException_Exception;

import java.rmi.RemoteException;
import java.util.List;

/**
 * The Class SoapStubCaster.
 *
 * @author Christian Klauenboesch <i>&lt;christian@klit.ch&rt;</i>
 * @version 2014-05-17
 * @since 2014-05-15
 */
public class SoapStubCaster {

    /**
     * The token.
     */
    private static String TOKEN = "";

    /**
     * Cast to login.
     *
     * @param impl the impl
     * @return the concrete login action
     */
    public static ConcreteLoginAction castToLogin(final GlobalSoapImpl impl) {
        return new ConcreteLoginAction() {
            @Override
            public boolean checkBenutzer(String benutzername, String passwort) throws RemoteException, InvalidDataException {
                try {
                    String r = impl.loginValidateUser(benutzername, passwort);
                    if (r == null) {
                        return false;
                    }
                    SoapStubCaster.TOKEN = r;
                    return true;
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public boolean checkLimitedBenutzer(String benutzername, String password) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindugnstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public int getUserId(String username) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void logout() throws RemoteException, InvalidDataException {
                impl.loginLogoutUser(SoapStubCaster.TOKEN);
            }

            @Override
            public void add(BenutzerEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public BenutzerEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<BenutzerEntity> getAll() throws RemoteException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void update(BenutzerEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    /**
     * Cast to rapport.
     *
     * @param impl the impl
     * @return the concrete rapport action
     */
    public static ConcreteRapportAction castToRapport(final GlobalSoapImpl impl) {
        return new ConcreteRapportAction() {
            @Override
            public void add(RapportEntity object) throws RemoteException, InvalidDataException {
                try {
                    impl.rapportAdd(SoapStubCaster.TOKEN, object);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public RapportEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<RapportEntity> getAllByKontrolleur(int kontrolleurId) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<RapportEntity> getAll() throws RemoteException {
                try {
                    return impl.rapportGetAll(SoapStubCaster.TOKEN);
                } catch (InvalidDataException_Exception e) {
                    throw new RemoteException(e.getMessage());
                }
            }

            @Override
            public void update(RapportEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void delete(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    /**
     * Cast to brenner.
     *
     * @param impl the impl
     * @return the concrete brenner action
     */
    public static ConcreteBrennerAction castToBrenner(final GlobalSoapImpl impl) {
        return new ConcreteBrennerAction() {
            @Override
            public void add(BrennerEntity object) throws RemoteException, InvalidDataException {
                try {
                    impl.brennerAdd(SoapStubCaster.TOKEN, object);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public BrennerEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<BrennerEntity> getAllByBrennerart(int typId) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<BrennerEntity> getAll() throws RemoteException {
                return impl.brennerGetAll(SoapStubCaster.TOKEN);
            }

            @Override
            public void update(BrennerEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void delete(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    /**
     * Cast to warmeerzeuger.
     *
     * @param impl the impl
     * @return the concrete warmeerzeuger action
     */
    public static ConcreteWarmeerzeugerAction castToWarmeerzeuger(final GlobalSoapImpl impl) {
        return new ConcreteWarmeerzeugerAction() {
            @Override
            public void add(WarmeerzeugerEntity object) throws RemoteException, InvalidDataException {
                try {
                    impl.warmeerzeugerAdd(SoapStubCaster.TOKEN, object);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public WarmeerzeugerEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<WarmeerzeugerEntity> getAllByBrennstoff(int brennstoffId) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<WarmeerzeugerEntity> getAll() throws RemoteException {
                try {
                    return impl.warmeerzeugerGetAll(SoapStubCaster.TOKEN);
                } catch (InvalidDataException_Exception e) {
                    throw new RemoteException(e.getMessage());
                }
            }

            @Override
            public void update(WarmeerzeugerEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void delete(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    /**
     * Cast to termin.
     *
     * @param impl the impl
     * @return the concrete termin action
     */
    public static ConcreteTerminAction castToTermin(final GlobalSoapImpl impl) {
        return new ConcreteTerminAction() {
            @Override
            public void add(TerminEntity object) throws RemoteException, InvalidDataException {
                try {
                    impl.terminAdd(SoapStubCaster.TOKEN, object);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public TerminEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<TerminEntity> getAllByKontrolleur(int kontrolleurId) throws RemoteException, InvalidDataException {
                return impl.terminByKontrolleur(SoapStubCaster.TOKEN);
            }

            @Override
            public List<TerminEntity> getAll() throws RemoteException {
                return impl.terminGetAll(SoapStubCaster.TOKEN);
            }

            @Override
            public void update(TerminEntity object) throws RemoteException, InvalidDataException {
                try {
                    impl.terminUpdate(SoapStubCaster.TOKEN, object);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public void delete(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<TerminEntity> getNextDayByKontrolleur(int kontrolleurId) throws RemoteException, InvalidDataException {
                return impl.terminNextDayByKontrolleur(SoapStubCaster.TOKEN);
            }

            @Override
            public List<TerminEntity> getThisWeek() throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    /**
     * Cast to ortschaft
     *
     * @param impl the impl
     * @return the concrete termin action
     */
    public static ConcreteOrtschaftAction castToOrtschaft(final GlobalSoapImpl impl) {
        return new ConcreteOrtschaftAction() {
            @Override
            public void add(OrtschaftEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public OrtschaftEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<OrtschaftEntity> getAll() throws RemoteException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void update(OrtschaftEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void delete(String plz) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<OrtschaftEntity> getListByPattern(String pattern) throws RemoteException, InvalidDataException {
                try {
                    return impl.ortschaftGetList(SoapStubCaster.TOKEN, pattern);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }
        };
    }

    public static ConcreteBrennstoffAction castToBrennstoff(final GlobalSoapImpl impl) {
        return new ConcreteBrennstoffAction() {
            @Override
            public void add(BrennstoffEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public BrennstoffEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<BrennstoffEntity> getAll() throws RemoteException {
                return impl.brennstoffGetAll(SoapStubCaster.TOKEN);
            }

            @Override
            public void update(BrennstoffEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    public static ConcreteMessvorgangAction castToMessvorgang(final GlobalSoapImpl soapImpl) {
        return new ConcreteMessvorgangAction() {
            @Override
            public void add(MessvorgangEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public MessvorgangEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<MessvorgangEntity> getAll() throws RemoteException {
                return soapImpl.messvorgangGetAll(SoapStubCaster.TOKEN);
            }

            @Override
            public void update(MessvorgangEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    public static ConcreteBrennerartAction castToBrennerart(final GlobalSoapImpl soapImpl) {
        return new ConcreteBrennerartAction() {
            @Override
            public void add(BrennerartEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public BrennerartEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<BrennerartEntity> getAll() throws RemoteException {
                return soapImpl.brennerartGetAll(SoapStubCaster.TOKEN);
            }

            @Override
            public void update(BrennerartEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    public static ConcreteMessergebnisAction castToMessergebnis(final GlobalSoapImpl soapImpl) {
        return new ConcreteMessergebnisAction() {
            @Override
            public void add(MessergebnisEntity object) throws RemoteException, InvalidDataException {
                try {
                    soapImpl.messergebnisAdd(SoapStubCaster.TOKEN, object);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public MessergebnisEntity getById(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<MessergebnisEntity> getByRapport(int rapportId) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<MessergebnisEntity> getAll() throws RemoteException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void update(MessergebnisEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void delete(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }

    public static ConcreteStandortAction castToStandort(final GlobalSoapImpl soapImpl) {
        return new ConcreteStandortAction() {
            @Override
            public void add(StandortEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public StandortEntity getById(int id) throws RemoteException, InvalidDataException {
                try {
                    return soapImpl.standortById(SoapStubCaster.TOKEN, id);
                } catch (InvalidDataException_Exception e) {
                    throw new InvalidDataException(e.getMessage());
                }
            }

            @Override
            public List<StandortEntity> getByOrtschaft(String plz) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public List<StandortEntity> getAll() throws RemoteException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void update(StandortEntity object) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }

            @Override
            public void delete(int id) throws RemoteException, InvalidDataException {
                throw new RemoteException("Diese Aktion wird über diesen Verbindungstyp (SOAP) nicht unterstützt.");
            }
        };
    }
}
