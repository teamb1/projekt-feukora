package feukora.gui.util;

import feukora.interfaces.BusinessAction;
import feukora.settings.FeukoraSettings;
import feukora.stubs.GlobalSoapImpl;
import feukora.stubs.GlobalSoapImplService;
import feukora.util.HostBuilderUtil;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

/**
 * Registry to check if connection over rmi is possible and stores the business
 * action object.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 13.05.2014
 */
public class ActionRegistry {

    /**
     * The Constant rmiActions.
     */
    @SuppressWarnings("rawtypes")
    private static final Map<String, BusinessAction> rmiActions = new HashMap<>();
    /**
     * The Constant soapActions.
     */
    @SuppressWarnings("rawtypes")
    private static final Map<String, BusinessAction> soapActions = new HashMap<>();
    /**
     * The rmi connection.
     */
    private static boolean rmiConnection;
    /**
     * The soap impl service.
     */
    private static GlobalSoapImplService soapImplService;

    /**
     * The soap impl.
     */
    private static GlobalSoapImpl soapImpl;

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(ActionRegistry.class);

    /**
     * Test if the RMI connection is possible.
     *
     * @return true if the connection is possible
     * @throws HeadlessException the headless exception
     * @throws IOException       Signals that an I/O exception has occurred.
     */
    public static boolean isRmiPossible() throws HeadlessException, IOException {
        String host = FeukoraSettings.getInstance().getRealString("rmiHost");
        rmiConnection = ConnectionChecker.ConnectionCheck(host);
        if (rmiConnection) {
            logger.info("Able to connect to RMI-Host");
        } else {
            logger.info("Unable to connect to RMI-Host, use SOAP?");
        }
        return rmiConnection;
    }

    /**
     * Store the busniess actions in a hashmap and load it from there if it
     * already exists.
     *
     * @param action the name of the business action
     * @return the business action from rmi or soap
     * @throws RemoteException if the rmi connection failed
     */
    @SuppressWarnings("rawtypes")
    public static BusinessAction getInterface(String action) throws RemoteException {
        if (rmiConnection) {
            logger.info("Use RMI-Connection, load: " + action);
            if (rmiActions.containsKey(action)) {
                logger.trace("Got it cached, use cached object");
                return rmiActions.get(action);
            } else {
                String lookup = HostBuilderUtil.rmi(FeukoraSettings.getInstance().getRealString("rmiHost"), FeukoraSettings.getInstance().getRealInt("rmiPort"), FeukoraSettings.getInstance().getRealString("rmiPath") + action);

                // add the action to the rmi hash map
                try {
                    rmiActions.put(action, (BusinessAction) Naming.lookup(lookup));
                } catch (MalformedURLException | NotBoundException e) {
                    logger.warn("Unable to get Action", e);
                }
                return rmiActions.get(action);
            }
        } else {
            logger.info("Use SOAP-Connection, load: " + action);
            // if key already exists
            if (soapActions.containsKey(action)) {
                logger.trace("Got it cached, use cached object");
                return soapActions.get(action);
            }

            // if no service impl is set, init
            if (soapImplService == null) {
                logger.info("Initialize SOAP-Service...");
                soapImplService = new GlobalSoapImplService();
                soapImpl = soapImplService.getGlobalSoapImplPort();
            }

            // validate requested action
            BusinessAction businessAction;
            switch (action) {
                case "login":
                    businessAction = SoapStubCaster.castToLogin(soapImpl);
                    break;
                case "rapport":
                    businessAction = SoapStubCaster.castToRapport(soapImpl);
                    break;
                case "brenner":
                    businessAction = SoapStubCaster.castToBrenner(soapImpl);
                    break;
                case "warmeerzeuger":
                    businessAction = SoapStubCaster.castToWarmeerzeuger(soapImpl);
                    break;
                case "termin":
                    businessAction = SoapStubCaster.castToTermin(soapImpl);
                    break;
                case "brennerart":
                    businessAction = SoapStubCaster.castToBrennerart(soapImpl);
                    break;
                case "brennstoff":
                    businessAction = SoapStubCaster.castToBrennstoff(soapImpl);
                    break;
                case "messvorgang":
                    businessAction = SoapStubCaster.castToMessvorgang(soapImpl);
                    break;
                case "messergebnis":
                    businessAction = SoapStubCaster.castToMessergebnis(soapImpl);
                    break;
                case "standort":
                    businessAction = SoapStubCaster.castToStandort(soapImpl);
                    break;
                default:
                    logger.warn("Try to retrieve unknown soap action: " + action);
                    throw new RemoteException("No such SOAP-Action found: " + action);
            }

            logger.trace("Cache soap action");
            // put and return business action
            soapActions.put(action, businessAction);
            return businessAction;
        }
    }
}
