package feukora.gui;

import feukora.exceptions.InvalidDataException;
import feukora.gui.controller.view.LoginController;
import feukora.gui.controller.view.RootLayoutController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteLoginAction;
import feukora.settings.FeukoraSettings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Main Class to start the Graphical User Interface.
 *
 * @author Patrick Schmed
 * @version 15.05.2014
 * @since 23.03.2014
 */
public class MainApp extends Application {

    /**
     * The primary stage.
     */
    private static Stage primaryStage;

    /**
     * The root layout.
     */
    private static BorderPane rootLayout;

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(MainApp.class);

    /**
     * Standard constructor for the mainApp.
     */
    public MainApp() {
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) throws RemoteException, InvalidDataException {
        FeukoraSettings.setXmlFile("settings_gui.xml");
        launch(args);

        // call logout
        logger.info("Log user out...");
        ((ConcreteLoginAction) ActionRegistry.getInterface("login")).logout();
    }

    /**
     * Gets the main stage.
     *
     * @return the main stage
     */
    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Gets the root layout.
     *
     * @return the root layout
     */
    public static BorderPane getRootLayout() {
        return rootLayout;
    }

    /**
     * Shows the login screen.
     *
     * @param primaryStage the primary stage for this application, onto which the
     *                     application scene can be set
     * @throws HeadlessException the headless exception
     * @throws IOException       Signals that an I/O exception has occurred.
     */

    @Override
    public void start(Stage primaryStage) throws HeadlessException, IOException {
        MainApp.primaryStage = primaryStage;
        // Set the title for the gui
        primaryStage.setTitle("Feukora");
        ActionRegistry.isRmiPossible();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/Login.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Feukora Login");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(overviewPage);
            dialogStage.setScene(scene);

            // Give the controller access to the main app
            LoginController controller = loader.getController();
            controller.setMainApp(this);
            controller.setDialogStage(dialogStage);

            dialogStage.showAndWait();
        } catch (IOException e) {
            logger.warn("Unable to load the Login.fxml file", e);
        }

    }

    /**
     * Shows the rapport overview after successful login.
     *
     * @param primaryStage the primary stage for this application, onto which the
     *                     application scene can be set
     */
    public void showStartScreen(Stage primaryStage) {
        MainApp.primaryStage = primaryStage;
        // Set the title for the gui
        primaryStage.setTitle("Feukora");

        try {
            // Load the root layout from the specified fxml-file
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            RootLayoutController controller = new RootLayoutController();
            if (ActionRegistry.isRmiPossible() == false) {
                controller.setMainApp(this);
            }
            controller.listRapport();
            primaryStage.show();

        } catch (IOException e) {
            logger.warn("Unable to load the RootLayout.fxml file", e);
        }
    }

}
