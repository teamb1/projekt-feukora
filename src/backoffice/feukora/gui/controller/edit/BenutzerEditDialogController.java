package feukora.gui.controller.edit;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.exceptions.UnableException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.BenutzerOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteBenutzerAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Handles the actions for the benutzer edit Dialog.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 11.05.2014
 */
public class BenutzerEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BenutzerEditDialogController.class);
    /**
     * The benutzername field.
     */
    @FXML
    private TextField benutzernameField;
    /**
     * The passwort field.
     */
    @FXML
    private TextField passwortField;
    /**
     * The vorname field.
     */
    @FXML
    private TextField vornameField;
    /**
     * The name field.
     */
    @FXML
    private TextField nameField;
    /**
     * The kontrolleur.
     */
    private BenutzerEntity benutzer;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the benutzer edit dialog.
     *
     * @param benutzer the benutzer that should be edited
     * @return true if the user clicked ok
     */
    public boolean showBenutzerEditDialog(BenutzerEntity benutzer) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/BenutzerEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Benutzer bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            BenutzerEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setBenutzer(benutzer);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the KontrolleurEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the benutzer which will be edited.
     *
     * @param benutzer the benutzer which will be edited
     */
    public void setBenutzer(BenutzerEntity benutzer) {
        this.benutzer = benutzer;

        benutzernameField.setText(benutzer.getBenutzername());
        passwortField.setText("*******");
        vornameField.setText(benutzer.getVorname());
        nameField.setText(benutzer.getName());
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the ok button is clicked.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleOk() {
        try {
            benutzer.setBenutzername(benutzernameField.getText());
            benutzer.setPasswort(passwortField.getText());
            benutzer.setName(nameField.getText());
            benutzer.setVorname(vornameField.getText());
            benutzer.setIsKontrolleur(0);
            ConcreteBenutzerAction benutzerAction = (ConcreteBenutzerAction) ActionRegistry.getInterface("benutzer");
            if (editDialog) {
                benutzerAction.update(benutzer);
                editDialog = false;
            } else {
                benutzerAction.add(benutzer);
                List<BenutzerEntity> dataList = benutzerAction.getAll();

                BenutzerOverviewController.getBenutzerData().removeAll(BenutzerOverviewController.getBenutzerData());

                for (BenutzerEntity entity : dataList) {
                    if (entity.getIsKontrolleur() == 0) {
                        BenutzerOverviewController.getBenutzerData().add(entity);
                    }
                }
            }

            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Benutzer konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Benutzer konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException | UnableException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Benutzer ändern", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialog if the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                // provide that the informations in the gui don't change
                ConcreteBenutzerAction benutzerAction = (ConcreteBenutzerAction) ActionRegistry.getInterface("benutzer");
                int id = benutzer.getId();
                BenutzerEntity dataBenutzer = benutzerAction.getById(id);

                benutzer.setName(dataBenutzer.getName());
                benutzer.setVorname(dataBenutzer.getVorname());
                benutzer.setBenutzername(dataBenutzer.getBenutzername());
                benutzer.setPasswort(dataBenutzer.getVorname());
                benutzer.setIsKontrolleur(dataBenutzer.getIsKontrolleur());
            } catch (RemoteException e) {
                logger.warn("Die Änderungen am Benutzer konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Benutzer konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                logger.warn("Die Änderungen am Benutzer konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", e.getCause().getMessage(), "Fehler beim Speichern", DialogOptions.OK);
            } catch (UnableException e) {
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Das Passwort konnte nicht verschlüsselt werden.", "Fehler beim Speichern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage the new Stage for the dialog
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Set the edit dialog.
     *
     * @param editDialog true if the edit dialog is called
     */
    public void setEditDialog(boolean editDialog) {
        BenutzerEditDialogController.editDialog = editDialog;
    }
}
