package feukora.gui.controller.edit;

import feukora.entity.KontrolleurEntity;
import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;
import feukora.exceptions.UnableException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.KontrolleurOverviewController;
import feukora.gui.controller.view.OrtschaftOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteKontrolleurAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the kontrolleur edit dialog.
 *
 * @author Florian Rieder
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class KontrolleurEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(KontrolleurEditDialogController.class);
    /**
     * The benutzername field.
     */
    @FXML
    private TextField benutzernameField;
    /**
     * The passwort field.
     */
    @FXML
    private TextField passwortField;
    /**
     * The vorname field.
     */
    @FXML
    private TextField vornameField;
    /**
     * The name field.
     */
    @FXML
    private TextField nameField;
    /**
     * The telefon field.
     */
    @FXML
    private TextField telefonField;
    /**
     * The strasse field.
     */
    @FXML
    private TextField strasseField;
    /**
     * The ortschaft field.
     */
    @FXML
    private TextField ortschaftField;
    /**
     * The kontrolleur.
     */
    private KontrolleurEntity kontrolleur;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ortschaft.
     */
    private OrtschaftEntity ortschaft;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the kontrolleur edit dialog.
     *
     * @param kontrolleur the kontrolleur that should be edited
     * @return true if the user clicked ok
     */
    public boolean showKontrolleurEditDialog(KontrolleurEntity kontrolleur) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/KontrolleurEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Kontrolleur bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            KontrolleurEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setKontrolleur(kontrolleur);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the KontrolleurEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the kontrolleur which will be edited.
     *
     * @param kontrolleur the kontrolleur which will be edited
     */
    public void setKontrolleur(KontrolleurEntity kontrolleur) {
        this.kontrolleur = kontrolleur;

        benutzernameField.setText(kontrolleur.getBenutzername());
        passwortField.setText("*******");
        vornameField.setText(kontrolleur.getVorname());
        nameField.setText(kontrolleur.getName());
        telefonField.setText(kontrolleur.getTelefon());
        strasseField.setText(kontrolleur.getStrasse());
        if (kontrolleur.getOrtschaft() != null) {
            this.ortschaft = kontrolleur.getOrtschaft();
            ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Florian Rieder, Patrick Schmed
     */
    @FXML
    public void handleOk() {
        try {
            kontrolleur.setBenutzername(benutzernameField.getText());
            kontrolleur.setPasswort(passwortField.getText());
            kontrolleur.setName(nameField.getText());
            kontrolleur.setVorname(vornameField.getText());
            kontrolleur.setTelefon(telefonField.getText());
            kontrolleur.setStrasse(strasseField.getText());
            kontrolleur.setOrtschaft(ortschaft);
            kontrolleur.setIsKontrolleur(1);

            ConcreteKontrolleurAction kontrolleurAction = (ConcreteKontrolleurAction) ActionRegistry.getInterface("kontrolleur");
            if (editDialog) {
                kontrolleurAction.update(kontrolleur);
                editDialog = false;
            } else {
                kontrolleurAction.add(kontrolleur);
                List<KontrolleurEntity> dataList = kontrolleurAction.getAll();
                Iterator<KontrolleurEntity> dataItr = dataList.iterator();

                KontrolleurOverviewController.getkontrolleurData().removeAll(KontrolleurOverviewController.getkontrolleurData());

                while (dataItr.hasNext()) {
                    KontrolleurEntity dataBrenner = dataItr.next();
                    KontrolleurOverviewController.getkontrolleurData().add(dataBrenner);
                }
            }
            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Änderung am Kontrolleur konnten nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Die Änderungen konnten nicht in die Datenbank übernommen werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Kontrolleur ändern", DialogOptions.OK);
        } catch (UnableException e) {
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Das Passwort konnte nicht verschlüsselt werden.", "Fehler beim Speichern", DialogOptions.OK);
        }
    }

    /**
     * Handle choose ortschaft.
     */
    @FXML
    public void handleChooseOrtschaft() {
        OrtschaftOverviewController controller = new OrtschaftOverviewController();
        controller.showOrtschaftOverview();
        this.ortschaft = controller.getOrtschaft();
        ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
    }

    /**
     * Closes the dialog if the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                ConcreteKontrolleurAction kontrolleurAction = (ConcreteKontrolleurAction) ActionRegistry.getInterface("kontrolleur");
                int id = kontrolleur.getId();
                KontrolleurEntity dataKontrolleur = kontrolleurAction.getById(id);

                kontrolleur.setBenutzername(dataKontrolleur.getBenutzername());
                kontrolleur.setPasswort(dataKontrolleur.getPasswort());
                kontrolleur.setName(dataKontrolleur.getName());
                kontrolleur.setVorname(dataKontrolleur.getVorname());
                kontrolleur.setTelefon(dataKontrolleur.getTelefon());
                kontrolleur.setStrasse(dataKontrolleur.getStrasse());
                kontrolleur.setOrtschaft(dataKontrolleur.getOrtschaft());
                kontrolleur.setIsKontrolleur(dataKontrolleur.getIsKontrolleur());
            } catch (RemoteException e) {
                logger.warn("Änderung am Kontrolleur konnten nicht in die Datenbank geschrieben werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Die Änderungen konnten nicht in die Datenbank übernommen werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Kontrolleur ändern", DialogOptions.OK);
            } catch (UnableException e) {
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Das Passwort konnte nicht verschlüsselt werden.", "Fehler beim Speichern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage the new Stage for the dialog
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Set the edit dialog.
     *
     * @param editDialog true if the edit dialog is called
     */
    public void setEditDialog(boolean editDialog) {
        KontrolleurEditDialogController.editDialog = editDialog;
    }
}
