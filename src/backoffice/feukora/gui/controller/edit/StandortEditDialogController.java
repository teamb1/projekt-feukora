package feukora.gui.controller.edit;

import feukora.entity.*;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.*;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteStandortAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the standort edit dialog.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 30.04.2014
 */
public class StandortEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(StandortEditDialogController.class);
    /**
     * The address field.
     */
    @FXML
    private TextField addressField;
    /**
     * The ortschaft field.
     */
    @FXML
    private TextField ortschaftField;
    /**
     * The eigentumer field.
     */
    @FXML
    private TextField eigentumerField;
    /**
     * The verwaltung field.
     */
    @FXML
    private TextField verwaltungField;
    /**
     * The hauswart field.
     */
    @FXML
    private TextField hauswartField;
    /**
     * The eigentumer check.
     */
    @FXML
    private CheckBox eigentumerCheck;
    /**
     * The verwaltung check.
     */
    @FXML
    private CheckBox verwaltungCheck;
    /**
     * The standort.
     */
    private StandortEntity standort;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The eigentumer.
     */
    private EigentumerEntity eigentumer;
    /**
     * The verwaltung.
     */
    private VerwaltungEntity verwaltung;
    /**
     * The hauswart.
     */
    private HauswartEntity hauswart;
    /**
     * The ortschaft.
     */
    private OrtschaftEntity ortschaft;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the standort edit dialog.
     *
     * @param standort the standort that should be edited
     * @return true if the user clicked ok
     */
    public boolean showStandortEditDialog(StandortEntity standort) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/StandortEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Standort bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            StandortEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setStandort(standort);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the StandortEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleOk() {

        try {
            standort.setAdresse(addressField.getText());
            standort.setOrtschaft(ortschaft);
            if (eigentumerCheck.isSelected()) {
                standort.setEigentumer(eigentumer);
            }

            if (verwaltungCheck.isSelected()) {
                standort.setVerwaltung(verwaltung);
            }

            standort.setHauswart(hauswart);

            ConcreteStandortAction standortAction = (ConcreteStandortAction) ActionRegistry.getInterface("standort");
            if (editDialog) {
                standortAction.update(standort);
                editDialog = false;
            } else {
                standortAction.add(standort);
                List<StandortEntity> dataList = standortAction.getAll();
                Iterator<StandortEntity> dataItr = dataList.iterator();

                StandortOverviewController.getStandortData().removeAll(StandortOverviewController.getStandortData());

                while (dataItr.hasNext()) {
                    StandortEntity dataStandort = dataItr.next();
                    StandortOverviewController.getStandortData().add(dataStandort);
                }
            }

            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Standort konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getMessage(), "Der Standort konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getMessage(), "Standort ändern", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialog if the user clicks CANCEL.
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                ConcreteStandortAction standortAction = (ConcreteStandortAction) ActionRegistry.getInterface("standort");
                int id = standort.getId();
                StandortEntity dataStandort = standortAction.getById(id);

                standort.setAdresse(dataStandort.getAdresse());
                standort.setOrtschaft(dataStandort.getOrtschaft());
                if (eigentumerCheck.isSelected()) {
                    standort.setEigentumer(dataStandort.getEigentumer());
                }

                standort.setHauswart(dataStandort.getHauswart());
            } catch (RemoteException e) {
                logger.warn("Der Standort konnte nicht in die Datenbank geschrieben werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Standort konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Standort ändern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the dialog stage.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Handle choose eigentumer.
     */
    @FXML
    public void handleChooseEigentumer() {
        EigentumerOverviewController controller = new EigentumerOverviewController();
        controller.setChooseDialog(true);
        controller.showEigentumerOverview();
        controller.setChooseDialog(false);
        this.eigentumer = controller.getEigentumer();
        eigentumerField.setText(eigentumer.getVorname() + " " + eigentumer.getName());

    }

    /**
     * Handle choose verwaltung.
     */
    @FXML
    public void handleChooseVerwaltung() {
        VerwaltungOverviewController controller = new VerwaltungOverviewController();
        controller.setChooseDialog(true);
        controller.showVerwaltungOverview();
        controller.setChooseDialog(false);
        this.verwaltung = controller.getVerwaltung();
        verwaltungField.setText(verwaltung.getVorname() + " " + verwaltung.getName());
    }

    /**
     * Handle choose hauswart.
     */
    @FXML
    public void handleChooseHauswart() {
        HauswartOverviewController controller = new HauswartOverviewController();
        controller.setChooseDialog(true);
        controller.showHauswartOverview();
        controller.setChooseDialog(false);
        this.hauswart = controller.getHauswart();
        hauswartField.setText(hauswart.getVorname() + " " + hauswart.getName());
    }

    /**
     * Handle choose ortschaft.
     */
    @FXML
    public void handleChooseOrtschaft() {
        OrtschaftOverviewController controller = new OrtschaftOverviewController();
        controller.showOrtschaftOverview();
        this.ortschaft = controller.getOrtschaft();
        ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
    }

    /**
     * Gets the standort.
     *
     * @return the standort
     */
    public StandortEntity getStandort() {
        return standort;
    }

    /**
     * Sets the information of the standort which will be edited.
     *
     * @param standort the new standort which will be edited
     */
    public void setStandort(StandortEntity standort) {
        this.standort = standort;
        addressField.setText(standort.getAdresse());
        if (standort.getOrtschaft() != null) {
            // do provide that the ortschaft will not be deleted if only another
            // field changed
            this.ortschaft = standort.getOrtschaft();
            ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
        }
        if (standort.getEigentumer() != null) {
            // do provide that the eigentumer will not be deleted if only
            // another field changed
            this.eigentumer = standort.getEigentumer();
            eigentumerField.setText(eigentumer.getVorname() + " " + eigentumer.getName());
        }
        if (standort.getVerwaltung() != null) {
            // do provide that the verwaltung will not be deleted if only
            // another field changed
            this.verwaltung = standort.getVerwaltung();
            verwaltungField.setText(verwaltung.getVorname() + " " + verwaltung.getName());
        }
        if (standort.getHauswart() != null) {
            // do provide that the hauswart will not be deleted if only another
            // field changed
            this.hauswart = standort.getHauswart();
            hauswartField.setText(hauswart.getVorname() + " " + hauswart.getName());
        }
    }

    /**
     * Sets the edits the dialog.
     *
     * @param editDialog the new edits the dialog
     */
    public void setEditDialog(boolean editDialog) {
        StandortEditDialogController.editDialog = editDialog;
    }
}
