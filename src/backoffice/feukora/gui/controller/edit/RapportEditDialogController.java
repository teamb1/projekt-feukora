package feukora.gui.controller.edit;

import feukora.entity.*;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.BrennerOverviewController;
import feukora.gui.controller.view.RapportOverviewController;
import feukora.gui.controller.view.TerminOverviewController;
import feukora.gui.controller.view.WarmeerzeugerOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteMessergebnisAction;
import feukora.interfaces.ConcreteMessvorgangAction;
import feukora.interfaces.ConcreteRapportAction;
import feukora.interfaces.ConcreteTerminAction;
import feukora.settings.FeukoraSettings;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Handle the actions for the rapport edit dialog.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 30.03.2014
 */
public class RapportEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(RapportEditDialogController.class);
    /**
     * The dialog stage.
     */
    public Stage dialogStage;
    /**
     * The russzahl11 ok.
     */
    boolean russzahl11Ok = false;
    /**
     * The co11 ok.
     */
    boolean co11Ok = false;
    /**
     * The no211 ok.
     */
    boolean no211Ok = false;
    /**
     * The abgasverlust11 ok.
     */
    boolean abgasverlust11Ok = false;
    /**
     * The russzahl12 ok.
     */
    boolean russzahl12Ok = false;
    /**
     * The co12 ok.
     */
    boolean co12Ok = false;
    /**
     * The no212 ok.
     */
    boolean no212Ok = false;
    /**
     * The abgasverlust12 ok.
     */
    boolean abgasverlust12Ok = false;
    /**
     * The russzahl21 ok.
     */
    boolean russzahl21Ok = false;
    /**
     * The co21 ok.
     */
    boolean co21Ok = false;
    /**
     * The no221 ok.
     */
    boolean no221Ok = false;
    /**
     * The abgasverlust21 ok.
     */
    boolean abgasverlust21Ok = false;
    /**
     * The russzahl22 ok.
     */
    boolean russzahl22Ok = false;
    /**
     * The co22 ok.
     */
    boolean co22Ok = false;
    /**
     * The no222 ok.
     */
    boolean no222Ok = false;
    /**
     * The abgasverlust22 ok.
     */
    boolean abgasverlust22Ok = false;
    /**
     * The gemeinde field.
     */
    @FXML
    private TextField gemeindeField;
    /**
     * The kanton field.
     */
    @FXML
    private TextField kantonField;
    /**
     * The eigentumer field.
     */
    @FXML
    private TextField eigentumerField;
    /**
     * The verwaltung field.
     */
    @FXML
    private TextField verwaltungField;
    /**
     * The hauswart field.
     */
    @FXML
    private TextField hauswartField;
    /**
     * The standort field.
     */
    @FXML
    private TextField standortField;
    /**
     * The kontrollart field.
     */
    @FXML
    private TextField kontrollartField;
    /**
     * The w fabrikat field.
     */
    @FXML
    private TextField wFabrikatField;
    /**
     * The w baujahr field.
     */
    @FXML
    private TextField wBaujahrField;
    /**
     * The w brennstoff field.
     */
    @FXML
    private TextField wBrennstoffField;
    /**
     * The b fabrikat field.
     */
    @FXML
    private TextField bFabrikatField;
    /**
     * The b baujahr field.
     */
    @FXML
    private TextField bBaujahrField;
    /**
     * The b brennerart field.
     */
    @FXML
    private TextField bBrennerartField;
    /**
     * The b leistung field.
     */
    @FXML
    private TextField bLeistungField;
    /**
     * The ru11.
     */
    @FXML
    private TextField ru11;
    /**
     * The ru12.
     */
    @FXML
    private TextField ru12;
    /**
     * The ru21.
     */
    @FXML
    private TextField ru21;
    /**
     * The ru22.
     */
    @FXML
    private TextField ru22;
    /**
     * The co11.
     */
    @FXML
    private TextField co11;
    /**
     * The co12.
     */
    @FXML
    private TextField co12;
    /**
     * The co21.
     */
    @FXML
    private TextField co21;
    /**
     * The co22.
     */
    @FXML
    private TextField co22;
    /**
     * The ol check11.
     */
    @FXML
    private CheckBox olCheck11;
    /**
     * The ol check12.
     */
    @FXML
    private CheckBox olCheck12;
    /**
     * The ol check21.
     */
    @FXML
    private CheckBox olCheck21;
    /**
     * The ol check22.
     */
    @FXML
    private CheckBox olCheck22;
    /**
     * The no11.
     */
    @FXML
    private TextField no11;
    /**
     * The no12.
     */
    @FXML
    private TextField no12;
    /**
     * The no21.
     */
    @FXML
    private TextField no21;
    /**
     * The no22.
     */
    @FXML
    private TextField no22;
    /**
     * The at11.
     */
    @FXML
    private TextField at11;
    /**
     * The at12.
     */
    @FXML
    private TextField at12;
    /**
     * The at21.
     */
    @FXML
    private TextField at21;
    /**
     * The at22.
     */
    @FXML
    private TextField at22;
    /**
     * The wt11.
     */
    @FXML
    private TextField wt11;
    /**
     * The wt12.
     */
    @FXML
    private TextField wt12;
    /**
     * The wt21.
     */
    @FXML
    private TextField wt21;
    /**
     * The wt22.
     */
    @FXML
    private TextField wt22;
    /**
     * The vt11.
     */
    @FXML
    private TextField vt11;
    /**
     * The vt12.
     */
    @FXML
    private TextField vt12;
    /**
     * The vt21.
     */
    @FXML
    private TextField vt21;
    /**
     * The vt22.
     */
    @FXML
    private TextField vt22;
    /**
     * The og11.
     */
    @FXML
    private TextField og11;
    /**
     * The og12.
     */
    @FXML
    private TextField og12;
    /**
     * The og21.
     */
    @FXML
    private TextField og21;
    /**
     * The og22.
     */
    @FXML
    private TextField og22;
    /**
     * The av11.
     */
    @FXML
    private TextField av11;
    /**
     * The av12.
     */
    @FXML
    private TextField av12;
    /**
     * The av21.
     */
    @FXML
    private TextField av21;
    /**
     * The av22.
     */
    @FXML
    private TextField av22;
    /**
     * The bemerkungen field.
     */
    @FXML
    private TextField bemerkungenField;
    /**
     * The beurteilung field.
     */
    @FXML
    private TextField beurteilungField;
    /**
     * The check eigentumer.
     */
    @FXML
    private CheckBox checkEigentumer;
    /**
     * The check verwaltung.
     */
    @FXML
    private CheckBox checkVerwaltung;
    /**
     * The test button.
     */
    @FXML
    private Button testButton;
    /**
     * The ok button.
     */
    @FXML
    private Button okButton;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The rapport.
     */
    private RapportEntity rapport;
    /**
     * The brenner.
     */
    private BrennerEntity brenner;
    /**
     * The termin.
     */
    private TerminEntity termin;
    /**
     * The ergebnis11.
     */
    private MessergebnisEntity ergebnis11;
    /**
     * The ergebnis12.
     */
    private MessergebnisEntity ergebnis12;
    /**
     * The ergebnis21.
     */
    private MessergebnisEntity ergebnis21;
    /**
     * The ergebnis22.
     */
    private MessergebnisEntity ergebnis22;
    /**
     * The eigentumer.
     */
    private EigentumerEntity eigentumer;
    /**
     * The verwaltung.
     */
    private VerwaltungEntity verwaltung;
    /**
     * The hauswart.
     */
    private HauswartEntity hauswart;
    /**
     * The warmeerzeuger.
     */
    private WarmeerzeugerEntity warmeerzeuger;
    /**
     * The einregulierung check.
     */
    @FXML
    private CheckBox einregulierungCheck;

    /**
     * The nicht check.
     */
    @FXML
    private CheckBox nichtCheck;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    public void initialize() {

    }

    /**
     * Shows the rapport edit dialog.
     *
     * @param rapport the rapport that should be edited
     * @return true if the user clicked ok
     */
    public boolean showRapportEditDialog(RapportEntity rapport) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/RapportEditDialogScrollable.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            // Give the new dialog a name
            dialogStage.setTitle("Rapport bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            RapportEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setRapport(rapport);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the RapportEditDialogScrollable.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage the new Stage for the dialog
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        okButton.setDisable(true);
        okButton.setOpacity(0.5);
    }

    /**
     * Sets the information of the rapport which will be edited.
     *
     * @param rapport the rapport which will be edited
     */
    public void setRapport(RapportEntity rapport) {
        this.rapport = rapport;
        if (rapport.getTermin() != null) {
            // do provide that the termin will not be deleted if only another
            // field changed
            this.termin = rapport.getTermin();
            gemeindeField.setText(termin.getStandort().getOrtschaft().getGemeinde());
            kantonField.setText(termin.getStandort().getOrtschaft().getKanton());
            standortField.setText(termin.getStandort().getAdresse());

            if (rapport.getEigentumer() != null) {
                // do provide that the eigentumer will not be deleted if only
                // another field changed
                this.eigentumer = rapport.getEigentumer();
                checkEigentumer.setSelected(true);
                eigentumerField.setText(eigentumer.getVorname() + " " + eigentumer.getName() + ", Tel.: " + eigentumer.getTelefon());
            } else if (termin.getStandort().getEigentumer() != null) {
                this.eigentumer = rapport.getTermin().getStandort().getEigentumer();
                checkEigentumer.setSelected(true);
                eigentumerField.setText(eigentumer.getVorname() + " " + eigentumer.getName() + ", Tel.: " + eigentumer.getTelefon());
            }

            if (rapport.getVerwaltung() != null) {
                // do provide that the verwaltung will not be deleted if only
                // another field changed
                this.verwaltung = rapport.getVerwaltung();
                checkVerwaltung.setSelected(true);
                verwaltungField.setText(verwaltung.getVorname() + " " + verwaltung.getName() + ", Tel.: " + verwaltung.getTelefon());
            } else if (termin.getStandort().getVerwaltung() != null) {
                this.verwaltung = rapport.getTermin().getStandort().getVerwaltung();
                checkVerwaltung.setSelected(true);
                verwaltungField.setText(verwaltung.getVorname() + " " + verwaltung.getName() + ", Tel.: " + verwaltung.getTelefon());
            }

            if (rapport.getHauswart() != null) {
                // do provide that the hauswart will not be deleted if only
                // another field changed
                this.hauswart = rapport.getHauswart();
                hauswartField.setText(hauswart.getVorname() + " " + hauswart.getName() + ", Tel.: " + rapport.getHauswart().getTelefon());
            } else if (termin.getStandort().getHauswart() != null) {
                this.hauswart = rapport.getTermin().getStandort().getHauswart();
                hauswartField.setText(hauswart.getVorname() + " " + hauswart.getName() + ", Tel.: " + hauswart.getTelefon());
            }
        }

        if (rapport.getBrenner() != null) {
            bBaujahrField.setText(Integer.toString(rapport.getBrenner().getBaujahr()));
            bBrennerartField.setText(rapport.getBrenner().getBrennerart().getBezeichnung());
            bLeistungField.setText(Integer.toString(rapport.getBrenner().getFeuerungswarmeleistung()));
            bFabrikatField.setText(rapport.getBrenner().getFabrikat());
        }

        if (rapport.getWarmeerzeuger() != null) {
            wBaujahrField.setText(Integer.toString(rapport.getWarmeerzeuger().getBaujahr()));
            wBrennstoffField.setText(rapport.getWarmeerzeuger().getBrennstoff().getBezeichnung());
            wFabrikatField.setText(rapport.getWarmeerzeuger().getFabrikat());
        }

        kontrollartField.setText(Integer.toString(rapport.getKontrollart()));
        setMessergebnisse();
        beurteilungField.setText(rapport.getBeurteilung());
        bemerkungenField.setText(rapport.getBemerkung());
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleOk() {
        try {
            rapport.setTermin(termin);
            rapport.setEigentumer(eigentumer);
            rapport.setVerwaltung(verwaltung);
            rapport.setHauswart(hauswart);
            rapport.setKontrollart(Integer.parseInt(kontrollartField.getText()));
            rapport.setBeurteilung(beurteilungField.getText());
            rapport.setBemerkung(bemerkungenField.getText());

            if (einregulierungCheck.isSelected()) {
                rapport.setRegulierung(RapportEntity.STATUS.REG);
            } else if (nichtCheck.isSelected()) {
                rapport.setRegulierung(RapportEntity.STATUS.NM);
            } else {
                rapport.setRegulierung(RapportEntity.STATUS.OK);
            }

            List<MessergebnisEntity> list = rapport.getMessergebnisse();
            Iterator<MessergebnisEntity> itr = list.iterator();

            ConcreteMessergebnisAction messergebnisAction = (ConcreteMessergebnisAction) ActionRegistry.getInterface("messergebnis");

            while (itr.hasNext()) {
                MessergebnisEntity messergebnis = itr.next();
                if (editDialog) {
                    messergebnisAction.update(messergebnis);
                } else {
                    messergebnisAction.add(messergebnis);
                }
            }
            ConcreteRapportAction rapportAction = (ConcreteRapportAction) ActionRegistry.getInterface("rapport");
            if (editDialog) {
                rapportAction.update(rapport);
                editDialog = false;
            } else {
                rapportAction.add(rapport);

                List<RapportEntity> dataList = rapportAction.getAll();
                Iterator<RapportEntity> dataItr = dataList.iterator();

                RapportOverviewController.getRapportData().removeAll(RapportOverviewController.getRapportData());

                while (dataItr.hasNext()) {
                    RapportEntity dataRapport = dataItr.next();
                    RapportOverviewController.getRapportData().add(dataRapport);

                    TerminEntity termin = TerminOverviewController.getTermin();
                    if (termin.getId().equals(dataRapport.getTermin().getId())) {
                        termin.setRapport(dataRapport);
                        ConcreteTerminAction terminAction = (ConcreteTerminAction) ActionRegistry.getInterface("termin");
                        terminAction.update(termin);
                    }
                }
            }

            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Rapport konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getMessage(), "Der Rapport konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getMessage(), "Rapport ändern", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialog if the user clicks cancel.
     */
    @FXML
    public void handleCancel() {
        if (editDialog) {
            try {
                ConcreteRapportAction rapportAction = (ConcreteRapportAction) ActionRegistry.getInterface("rapport");
                int id = rapport.getId();
                RapportEntity dataRapport = rapportAction.getById(id);

                rapport.setTermin(dataRapport.getTermin());
                if (dataRapport.getEigentumer() != null) {
                    rapport.setEigentumer(dataRapport.getEigentumer());
                }
                if (dataRapport.getVerwaltung() != null) {
                    rapport.setVerwaltung(dataRapport.getVerwaltung());
                }
                rapport.setHauswart(dataRapport.getHauswart());
                rapport.setKontrollart(dataRapport.getKontrollart());
                rapport.setBeurteilung(dataRapport.getBeurteilung());
                rapport.setBemerkung(dataRapport.getBemerkung());
            } catch (RemoteException e) {
                logger.warn("Der Rapport konnte nicht in die Datenbank geschrieben werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Rapport konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Rapport ändern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Set the edit dialog.
     *
     * @param editDialog true if the edit dialog is called
     */
    public void setEditDialog(boolean editDialog) {
        RapportEditDialogController.editDialog = editDialog;
    }

    /**
     * Handle choose brenner.
     */
    @FXML
    public void handleChooseBrenner() {
        BrennerOverviewController controller = new BrennerOverviewController();
        controller.showBrennerChooseDialog();
        this.brenner = controller.getBrenner();
        rapport.setBrenner(brenner);
        bBaujahrField.setText(Integer.toString(brenner.getBaujahr()));
        bFabrikatField.setText(brenner.getFabrikat());
        bBrennerartField.setText(brenner.getBrennerart().getBezeichnung());
        bLeistungField.setText(Integer.toString(brenner.getFeuerungswarmeleistung()));
    }

    /**
     * Handle test the messergebniss data.
     *
     * @author Sven Neitzel
     */
    @FXML
    public void handleTest() {
        List<MessergebnisEntity> list = saveMessergebnisse();
        rapport.setMessergebnisse(list);

        String brennstoff;
        try {
            brennstoff = rapport.getWarmeerzeuger().getBrennstoff().getBezeichnung();
        } catch (NullPointerException e) {
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), "Wählen Sie einen Wärmeerzeuger aus.", "Es ist kein Wärmeerzeuger angewählt.", "Unvollständiges Formualr", DialogOptions.OK);
            return;
        }
        // Check if Wärmeerzeuger has Brennstoff Öl
        if (brennstoff.equals("Öl")) {
            if (rapport.getBrenner().getBrennerart().getBezeichnung().equals("Gebläse")) {
                if (ergebnis21.getMessvorgang() == null) {
                    this.OelGeblaese1Stufig(rapport);
                } else if (ergebnis21.getMessvorgang() != null) {
                    this.OelGeblaese2Stufig(rapport);
                }
            } else if (rapport.getBrenner().getBrennerart().getBezeichnung().equals("Verdampfer")) {
                this.OelVerdampfer(rapport);
            }
            // Check if Wärmeerzeuger has Brennstoff Erdgas or Flüssiggas
        } else if (brennstoff.equals("Erdgas") || brennstoff.equals("Flüssiggas")) {
            if (rapport.getBrenner().getBrennerart().getBezeichnung().equals("atmosphärisch")) {
                this.GasAtmosphaerisch(rapport);
            } else if (rapport.getBrenner().getBrennerart().getBezeichnung().equals("Gebläse")) {
                if (ergebnis21.getMessvorgang() == null) {
                    this.GasGeblaese1Stufig(rapport);
                } else if (ergebnis21.getMessvorgang() != null) {
                    this.GasGeblaese2Stufig(rapport);
                }
            }
        }
        okButton.setDisable(false);
        okButton.setOpacity(1);
    }

    /**
     * Öl-Gebläse 1-stufig.
     *
     * @param rapport the rapport
     * @author Sven Neitzel
     */
    public void OelGeblaese1Stufig(RapportEntity rapport) {
        // Erste Messung Stufe 1
        if (ergebnis11.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOG")) {
            russzahl11Ok = true;
        }
        if (ergebnis11.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOG")) {
            co11Ok = true;
        } else {
            olCheck11.setSelected(true);
        }
        if (ergebnis11.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no211Ok = true;
            }
        } else if (ergebnis11.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no211Ok = true;
            }
        }
        if (ergebnis11.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOG21")) {
            abgasverlust11Ok = true;
        }
        // Zweite Messung Stufe 1
        if (ergebnis12.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOG")) {
            russzahl12Ok = true;
        }
        if (ergebnis12.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOG")) {
            co12Ok = true;
        } else {
            olCheck12.setSelected(true);
        }
        if (ergebnis12.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no212Ok = true;
            }
        } else if (ergebnis12.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no212Ok = true;
            }
        }
        if (ergebnis12.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOG21")) {
            abgasverlust12Ok = true;
        }
        String beurteilungOk = "Die geltenden LRV-Grenzwertewerden eingehalten. Es sind keine Massnahmen nötig.";
        String beurteilungNichtOk = "Die Anlage wird beanstandet wegen Überschreitung von: ";

        // create the text for the beurteilung field if something is not ok
        if (russzahl11Ok == false || russzahl12Ok == false) {
            beurteilungNichtOk += "Russzahl, ";
        }
        if (co11Ok == false || co12Ok == false) {
            beurteilungNichtOk += "CO in mg/m3, Ölanteilen, ";
        }
        if (abgasverlust11Ok == false || abgasverlust12Ok == false) {
            beurteilungNichtOk += "Abgasverluste, ";
        }
        if (no211Ok == false || no212Ok == false) {
            beurteilungNichtOk += "NO2 in mg/m3 ";
        }
        // set the text of the beurteilung field
        if (beurteilungNichtOk.equals("Die Anlage wird beanstandet wegen Überschreitung von: ")) {
            beurteilungField.setText(beurteilungOk);
        } else {
            beurteilungField.setText(beurteilungNichtOk);
        }
    }

    /**
     * Öl-Gebläse 2-stufig.
     *
     * @param rapport the rapport
     * @author Sven Neitzel
     */
    public void OelGeblaese2Stufig(RapportEntity rapport) {

        // Erste Messung Stufe 1
        if (ergebnis11.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOG")) {
            russzahl11Ok = true;
        }
        if (ergebnis11.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOG")) {
            co11Ok = true;
        } else {
            olCheck11.setSelected(true);
        }
        if (ergebnis11.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no211Ok = true;
            }
        } else if (ergebnis11.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no211Ok = true;
            }
        }
        if (ergebnis11.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOG21")) {
            abgasverlust11Ok = true;
        }
        // Zweite Messung Stufe 1
        if (ergebnis12.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOG")) {
            russzahl12Ok = true;
        }
        if (ergebnis12.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOG")) {
            co12Ok = true;
        } else {
            olCheck12.setSelected(true);
        }
        if (ergebnis12.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no212Ok = true;
            }
        } else if (ergebnis12.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no212Ok = true;
            }
        }
        if (ergebnis12.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOG21")) {
            abgasverlust12Ok = true;
        }
        // Erste Messung Stufe 2
        if (ergebnis21.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOG")) {
            russzahl21Ok = true;
        }
        if (ergebnis21.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOG")) {
            co21Ok = true;
        } else {
            olCheck21.setSelected(true);
        }
        if (ergebnis21.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis21.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no221Ok = true;
            }
        } else if (ergebnis21.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis21.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no221Ok = true;
            }
        }
        if (ergebnis21.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOG21")) {
            abgasverlust21Ok = true;
        }
        // Zweite Messung Stufe 2
        if (ergebnis22.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOG")) {
            russzahl22Ok = true;
        }
        if (ergebnis22.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOG")) {
            co22Ok = true;
        } else {
            olCheck22.setSelected(true);
        }
        if (ergebnis22.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis22.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no222Ok = true;
            }
        } else if (ergebnis22.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis22.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no222Ok = true;
            }
        }
        if (ergebnis22.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOG22")) {
            abgasverlust22Ok = true;
        }

        String beurteilungOk = "Die geltenden LRV-Grenzwertewerden eingehalten. Es sind keine Massnahmen nötig.";
        String beurteilungNichtOk = "Die Anlage wird beanstandet wegen Überschreitung von: ";

        // create the text for the beurteilung field if something is not ok
        if (russzahl11Ok == false || russzahl12Ok == false || russzahl21Ok == false || russzahl22Ok == false) {
            beurteilungNichtOk += "Russzahl, ";
        }
        if (co11Ok == false || co12Ok == false || co21Ok == false || co22Ok == false) {
            beurteilungNichtOk += "CO in mg/m3, Ölanteilen, ";
        }
        if (abgasverlust11Ok == false || abgasverlust12Ok == false || abgasverlust21Ok == false || abgasverlust22Ok == false) {
            beurteilungNichtOk += "Abgasverluste, ";
        }
        if (no211Ok == false || no212Ok == false || no221Ok == false || no222Ok == false) {
            beurteilungNichtOk += "NO2 in mg/m3, ";
        }
        // set the text of the beurteilung field
        if (beurteilungNichtOk.equals("Die Anlage wird beanstandet wegen Überschreitung von: ")) {
            beurteilungField.setText(beurteilungOk);
        } else {
            beurteilungField.setText(beurteilungNichtOk);
        }
    }

    /**
     * Öl-Verdampfer.
     *
     * @param rapport the rapport
     * @author Sven Neitzel
     */
    public void OelVerdampfer(RapportEntity rapport) {

        // Erste Messung
        if (ergebnis11.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOV")) {
            russzahl11Ok = true;
        }
        if (ergebnis11.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOV")) {
            co11Ok = true;
        } else {
            olCheck11.setSelected(true);
        }
        if (ergebnis11.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no211Ok = true;
            }
        } else if (ergebnis11.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no211Ok = true;
            }
        }
        if (ergebnis11.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOV")) {
            abgasverlust11Ok = true;
        }
        // Zweite Messung
        if (ergebnis12.getRusszahl() <= FeukoraSettings.getInstance().getRealInt("gwRzOV")) {
            russzahl12Ok = true;
        }
        if (ergebnis12.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoOV")) {
            co12Ok = true;
        } else {
            olCheck12.setSelected(true);
        }
        if (ergebnis12.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTh")) {
                no212Ok = true;
            }
        } else if (ergebnis12.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2OTt")) {
                no212Ok = true;
            }
        }
        if (ergebnis12.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvOV")) {
            abgasverlust12Ok = true;
        }

        String beurteilungOk = "Die geltenden LRV-Grenzwerte werden eingehalten. Es sind keine Massnahmen nötig.";
        String beurteilungNichtOk = "Die Anlage wird beanstandet wegen Überschreitung von: ";

        // create the text for the beurteilung field if something is not ok
        if (russzahl11Ok == false || russzahl12Ok == false) {
            beurteilungNichtOk += "Russzahl, ";
        }
        if (co11Ok == false || co12Ok == false) {
            beurteilungNichtOk += "CO in mg/m3, Ölanteilen, ";
        }
        if (abgasverlust11Ok == false || abgasverlust12Ok == false) {
            beurteilungNichtOk += "Abgasverluste, ";
        }
        if (no211Ok == false || no212Ok == false) {
            beurteilungNichtOk += "NO2 in mg/m3 ";
        }
        // set the text of the beurteilung field
        if (beurteilungNichtOk.equals("Die Anlage wird beanstandet wegen Überschreitung von: ")) {
            beurteilungField.setText(beurteilungOk);
        } else {
            beurteilungField.setText(beurteilungNichtOk);
        }
    }

    /**
     * Gas-Gebläse 1-stufig.
     *
     * @param rapport the rapport
     * @author Sven Neitzel
     */
    public void GasGeblaese1Stufig(RapportEntity rapport) {

        // Erste Messung
        if (ergebnis11.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co11Ok = true;
        } else {
            olCheck11.setSelected(true);
        }
        if (ergebnis11.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTh")) {
                no211Ok = true;
            }
        } else if (ergebnis11.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTt")) {
                no211Ok = true;
            }
        }
        if (ergebnis11.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGG1")) {
            abgasverlust11Ok = true;
        }
        // Zweite Messung
        if (ergebnis12.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co12Ok = true;
        } else {
            olCheck12.setSelected(true);
        }
        if (ergebnis12.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTh")) {
                no212Ok = true;
            }
        } else if (ergebnis12.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTt")) {
                no212Ok = true;
            }
        }
        if (ergebnis12.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGG1")) {
            abgasverlust12Ok = true;
        }

        String beurteilungOk = "Die geltenden LRV-Grenzwertewerden eingehalten. Es sind keine Massnahmen nötig.";
        String beurteilungNichtOk = "Die Anlage wird beanstandet wegen Überschreitung von: ";

        // create the text for the beurteilung field if something is not ok
        if (russzahl11Ok == false || russzahl12Ok == false) {
            beurteilungNichtOk += "Russzahl, ";
        }
        if (co11Ok == false || co12Ok == false) {
            beurteilungNichtOk += "CO in mg/m3, Ölanteilen, ";
        }
        if (abgasverlust11Ok == false || abgasverlust12Ok == false) {
            beurteilungNichtOk += "Abgasverluste, ";
        }
        if (no211Ok == false || no212Ok == false) {
            beurteilungNichtOk += "NO2 in mg/m3 ";
        }
        // set the text of the beurteilung field
        if (beurteilungNichtOk.equals("Die Anlage wird beanstandet wegen Überschreitung von: ")) {
            beurteilungField.setText(beurteilungOk);
        } else {
            beurteilungField.setText(beurteilungNichtOk);
        }
    }

    /**
     * Gas-Gebläse 2-stufig.
     *
     * @param rapport the rapport
     * @author Sven Neitzel
     */
    public void GasGeblaese2Stufig(RapportEntity rapport) {
        // Erste Messung Stufe 1
        if (ergebnis11.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co11Ok = true;
        } else {
            olCheck11.setSelected(true);
        }
        if (ergebnis11.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTh")) {
                no211Ok = true;
            }
        } else if (ergebnis11.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTt")) {
                no211Ok = true;
            }
        }
        if (ergebnis11.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGG21")) {
            abgasverlust11Ok = true;
        }
        // Zweite Messung Stufe 1
        if (ergebnis12.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co12Ok = true;
        } else {
            olCheck12.setSelected(true);
        }
        if (ergebnis12.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTh")) {
                no212Ok = true;
            }
        } else if (ergebnis12.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTt")) {
                no212Ok = true;
            }
        }
        if (ergebnis12.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGG21")) {
            abgasverlust12Ok = true;
        }
        // Erste Messung Stufe 2
        if (ergebnis21.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co21Ok = true;
        } else {
            olCheck21.setSelected(true);
        }
        if (ergebnis21.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis21.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTh")) {
                no221Ok = true;
            }
        } else if (ergebnis21.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis21.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTt")) {
                no221Ok = true;
            }
        }
        if (ergebnis21.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGG22")) {
            abgasverlust21Ok = true;
        }
        // Zweite Messung Stufe 2
        if (ergebnis22.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co22Ok = true;
        } else {
            olCheck22.setSelected(true);
        }
        if (ergebnis22.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis22.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTh")) {
                no222Ok = true;
            }
        } else if (ergebnis22.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (ergebnis22.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2GGTt")) {
                no222Ok = true;
            }
        }
        if (ergebnis22.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGG22")) {
            abgasverlust22Ok = true;
        }

        String beurteilungOk = "Die geltenden LRV-Grenzwerte werden eingehalten. Es sind keine Massnahmen nötig.";
        String beurteilungNichtOk = "Die Anlage wird beanstandet wegen Überschreitung von: ";

        // create the text for the beurteilung field if something is not ok
        if (russzahl11Ok == false || russzahl12Ok == false || russzahl21Ok == false || russzahl22Ok == false) {
            beurteilungNichtOk += "Russzahl, ";
        }
        if (co11Ok == false || co12Ok == false || co21Ok == false || co22Ok == false) {
            beurteilungNichtOk += "CO in mg/m3, Ölanteilen, ";
        }
        if (abgasverlust11Ok == false || abgasverlust12Ok == false || abgasverlust21Ok == false || abgasverlust22Ok == false) {
            beurteilungNichtOk += "Abgasverluste, ";
        }
        if (no211Ok == false || no212Ok == false || no221Ok == false || no222Ok == false) {
            beurteilungNichtOk += "NO2 in mg/m3, ";
        }
        // set the text of the beurteilung field
        if (beurteilungNichtOk.equals("Die Anlage wird beanstandet wegen Überschreitung von: ")) {
            beurteilungField.setText(beurteilungOk);
        } else {
            beurteilungField.setText(beurteilungNichtOk);
        }
    }

    /**
     * Gas-atmosphärisch.
     *
     * @param rapport the rapport
     * @author Sven Neitzel
     */
    public void GasAtmosphaerisch(RapportEntity rapport) {

        // Erste Messung
        if (ergebnis11.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co11Ok = true;
        } else {
            olCheck11.setSelected(true);
        }
        if (ergebnis11.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (rapport.getBrenner().getFeuerungswarmeleistung() <= FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGThB12")) {
                no211Ok = true;
            } else if (rapport.getBrenner().getFeuerungswarmeleistung() > FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGThUe12")) {
                no211Ok = true;
            }
        } else if (ergebnis11.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (rapport.getBrenner().getFeuerungswarmeleistung() <= FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGTtB12")) {
                no211Ok = true;
            } else if (rapport.getBrenner().getFeuerungswarmeleistung() > FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis11.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGTtUe12")) {
                no211Ok = true;
            }
        }
        if (ergebnis11.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGA")) {
            abgasverlust11Ok = true;
        }

        // Zweite Messung
        if (ergebnis12.getCo() <= FeukoraSettings.getInstance().getRealInt("gwCoGG")) {
            co12Ok = true;
        } else {
            olCheck12.setSelected(true);
        }
        if (ergebnis12.getAbgastemp() > FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (rapport.getBrenner().getFeuerungswarmeleistung() <= FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGThB12")) {
                no212Ok = true;
            } else if (rapport.getBrenner().getFeuerungswarmeleistung() > FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGThUe12")) {
                no212Ok = true;
            }
        } else if (ergebnis12.getAbgastemp() <= FeukoraSettings.getInstance().getRealInt("swTemp")) {
            if (rapport.getBrenner().getFeuerungswarmeleistung() <= FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGTtB12")) {
                no212Ok = true;
            } else if (rapport.getBrenner().getFeuerungswarmeleistung() > FeukoraSettings.getInstance().getRealInt("WlWeAb") & ergebnis12.getNo() <= FeukoraSettings.getInstance().getRealInt("gwNo2AGTtUe12")) {
                no211Ok = true;
            }
        }
        if (ergebnis12.getAbgasverlust() <= FeukoraSettings.getInstance().getRealInt("gwAvGA")) {
            abgasverlust12Ok = true;
        }

        String beurteilungOk = "Die geltenden LRV-Grenzwertewerden eingehalten. Es sind keine Massnahmen nötig.";
        String beurteilungNichtOk = "Die Anlage wird beanstandet wegen Überschreitung von: ";

        // create the text for the beurteilung field if something is not ok
        if (russzahl11Ok == false || russzahl12Ok == false) {
            beurteilungNichtOk += "Russzahl, ";
        }
        if (co11Ok == false || co12Ok == false) {
            beurteilungNichtOk += "CO in mg/m3, Ölanteilen, ";
        }
        if (abgasverlust11Ok == false || abgasverlust12Ok == false) {
            beurteilungNichtOk += "Abgasverluste, ";
        }
        if (no211Ok == false || no212Ok == false) {
            beurteilungNichtOk += "NO2 in mg/m3 ";
        }
        // set the text of the beurteilung field
        if (beurteilungNichtOk.equals("Die Anlage wird beanstandet wegen Überschreitung von: ")) {
            beurteilungField.setText(beurteilungOk);
        } else {
            beurteilungField.setText(beurteilungNichtOk);
        }
    }

    /**
     * Handles choose wärmeerzeuger.
     */
    @FXML
    public void handleChooseWarmeerzeuger() {
        WarmeerzeugerOverviewController controller = new WarmeerzeugerOverviewController();
        controller.showWarmeerzeugerChooseDialog();
        this.warmeerzeuger = controller.getWarmeerzeuger();
        rapport.setWarmeerzeuger(warmeerzeuger);
        wBaujahrField.setText(Integer.toString(warmeerzeuger.getBaujahr()));
        wFabrikatField.setText(warmeerzeuger.getFabrikat());
        wBrennstoffField.setText(warmeerzeuger.getBrennstoff().getBezeichnung());
    }

    /**
     * Fill out the messergebnisse fields.
     */
    public void setMessergebnisse() {
        List<MessergebnisEntity> list = rapport.getMessergebnisse();
        Iterator<MessergebnisEntity> itr = list.iterator();
        ergebnis11 = new MessergebnisEntity();
        ergebnis12 = new MessergebnisEntity();
        ergebnis21 = new MessergebnisEntity();
        ergebnis22 = new MessergebnisEntity();

        // Fill out the mess result table with the mess result from the chosen
        // rapport
        while (itr.hasNext()) {
            MessergebnisEntity ergebnis = itr.next();
            if (ergebnis.getMessvorgang().getStufe() == 1 && ergebnis.getMessvorgang().getMessung() == 1) {
                this.ergebnis11 = ergebnis;
                ru11.setText(Integer.toString(ergebnis.getRusszahl()));
                co11.setText(Integer.toString(ergebnis.getCo()));
                if (ergebnis.isOelanteil() == true) {
                    olCheck11.setSelected(true);
                } else {
                    olCheck11.setSelected(false);
                }
                no11.setText(Integer.toString(ergebnis.getNo()));
                at11.setText(Integer.toString(ergebnis.getAbgastemp()));
                wt11.setText(Integer.toString(ergebnis.getWarmeerzeugertemp()));
                vt11.setText(Integer.toString(ergebnis.getVerbrennungstemp()));
                og11.setText(Integer.toString(ergebnis.getO2gehalt()));
                av11.setText(Integer.toString(ergebnis.getAbgasverlust()));
            }
            if (ergebnis.getMessvorgang().getStufe() == 1 && ergebnis.getMessvorgang().getMessung() == 2) {
                this.ergebnis12 = ergebnis;
                ru12.setText(Integer.toString(ergebnis.getRusszahl()));
                co12.setText(Integer.toString(ergebnis.getCo()));
                if (ergebnis.isOelanteil() == true) {
                    olCheck12.setSelected(true);
                } else {
                    olCheck12.setSelected(false);
                }
                no12.setText(Integer.toString(ergebnis.getNo()));
                at12.setText(Integer.toString(ergebnis.getAbgastemp()));
                wt12.setText(Integer.toString(ergebnis.getWarmeerzeugertemp()));
                vt12.setText(Integer.toString(ergebnis.getVerbrennungstemp()));
                og12.setText(Integer.toString(ergebnis.getO2gehalt()));
                av12.setText(Integer.toString(ergebnis.getAbgasverlust()));
            }
            if (ergebnis.getMessvorgang().getStufe() == 2 && ergebnis.getMessvorgang().getMessung() == 1) {
                this.ergebnis21 = ergebnis;
                ru21.setText(Integer.toString(ergebnis.getRusszahl()));
                co21.setText(Integer.toString(ergebnis.getCo()));
                if (ergebnis.isOelanteil() == true) {
                    olCheck21.setSelected(true);
                } else {
                    olCheck21.setSelected(false);
                }
                no21.setText(Integer.toString(ergebnis.getNo()));
                at21.setText(Integer.toString(ergebnis.getAbgastemp()));
                wt21.setText(Integer.toString(ergebnis.getWarmeerzeugertemp()));
                vt21.setText(Integer.toString(ergebnis.getVerbrennungstemp()));
                og21.setText(Integer.toString(ergebnis.getO2gehalt()));
                av21.setText(Integer.toString(ergebnis.getAbgasverlust()));
            }
            if (ergebnis.getMessvorgang().getStufe() == 2 && ergebnis.getMessvorgang().getMessung() == 2) {
                this.ergebnis22 = ergebnis;
                ru22.setText(Integer.toString(ergebnis.getRusszahl()));
                co22.setText(Integer.toString(ergebnis.getCo()));
                if (ergebnis.isOelanteil() == true) {
                    olCheck22.setSelected(true);
                } else {
                    olCheck22.setSelected(false);
                }
                no22.setText(Integer.toString(ergebnis.getNo()));
                at22.setText(Integer.toString(ergebnis.getAbgastemp()));
                wt22.setText(Integer.toString(ergebnis.getWarmeerzeugertemp()));
                vt22.setText(Integer.toString(ergebnis.getVerbrennungstemp()));
                og22.setText(Integer.toString(ergebnis.getO2gehalt()));
                av22.setText(Integer.toString(ergebnis.getAbgasverlust()));
            }
        }
    }

    /**
     * Write the new values to the messergebnisse and save them in the database.
     *
     * @return the list of messergebnisse
     */
    public List<MessergebnisEntity> saveMessergebnisse() {
        List<MessergebnisEntity> list = new ArrayList<MessergebnisEntity>();
        try {
            ConcreteMessvorgangAction messvorgangAction = (ConcreteMessvorgangAction) ActionRegistry.getInterface("messvorgang");
            List<MessvorgangEntity> listM = messvorgangAction.getAll();
            Iterator<MessvorgangEntity> itr = listM.iterator();
            while (itr.hasNext()) {
                MessvorgangEntity messvorgang = itr.next();
                try {
                    if (messvorgang.getStufe() == 1 && messvorgang.getMessung() == 1) {
                        ergebnis11.setRusszahl(Integer.parseInt(ru11.getText()));
                        ergebnis11.setCo(Integer.parseInt(co11.getText()));
                        ergebnis11.setOelanteil(olCheck11.isSelected());
                        ergebnis11.setNo(Integer.parseInt(no11.getText()));
                        ergebnis11.setAbgastemp(Integer.parseInt(at11.getText()));
                        ergebnis11.setWarmeerzeugertemp(Integer.parseInt(wt11.getText()));
                        ergebnis11.setVerbrennungstemp(Integer.parseInt(vt11.getText()));
                        ergebnis11.setO2gehalt(Integer.parseInt(og11.getText()));
                        ergebnis11.setAbgasverlust(Integer.parseInt(av11.getText()));
                        ergebnis11.setMessvorgang(messvorgang);
                        list.add(ergebnis11);
                    } else if (messvorgang.getStufe() == 1 && messvorgang.getMessung() == 2) {
                        ergebnis12.setRusszahl(Integer.parseInt(ru12.getText()));
                        ergebnis12.setCo(Integer.parseInt(co12.getText()));
                        ergebnis12.setOelanteil(olCheck12.isSelected());
                        ergebnis12.setNo(Integer.parseInt(no12.getText()));
                        ergebnis12.setAbgastemp(Integer.parseInt(at12.getText()));
                        ergebnis12.setWarmeerzeugertemp(Integer.parseInt(wt12.getText()));
                        ergebnis12.setVerbrennungstemp(Integer.parseInt(vt12.getText()));
                        ergebnis12.setO2gehalt(Integer.parseInt(og12.getText()));
                        ergebnis12.setAbgasverlust(Integer.parseInt(av12.getText()));
                        ergebnis12.setMessvorgang(messvorgang);
                        list.add(ergebnis12);
                    } else if (messvorgang.getStufe() == 2 && messvorgang.getMessung() == 1) {
                        ergebnis21.setRusszahl(Integer.parseInt(ru21.getText()));
                        ergebnis21.setCo(Integer.parseInt(co21.getText()));
                        ergebnis21.setOelanteil(olCheck21.isSelected());
                        ergebnis21.setNo(Integer.parseInt(no21.getText()));
                        ergebnis21.setAbgastemp(Integer.parseInt(at21.getText()));
                        ergebnis21.setWarmeerzeugertemp(Integer.parseInt(wt21.getText()));
                        ergebnis21.setVerbrennungstemp(Integer.parseInt(vt21.getText()));
                        ergebnis21.setO2gehalt(Integer.parseInt(og21.getText()));
                        ergebnis21.setAbgasverlust(Integer.parseInt(av21.getText()));
                        ergebnis21.setMessvorgang(messvorgang);
                        list.add(ergebnis21);
                    } else if (messvorgang.getStufe() == 2 && messvorgang.getMessung() == 2) {
                        ergebnis22.setRusszahl(Integer.parseInt(ru22.getText()));
                        ergebnis22.setCo(Integer.parseInt(co22.getText()));
                        ergebnis22.setOelanteil(olCheck22.isSelected());
                        ergebnis22.setNo(Integer.parseInt(no22.getText()));
                        ergebnis22.setAbgastemp(Integer.parseInt(at22.getText()));
                        ergebnis22.setWarmeerzeugertemp(Integer.parseInt(wt22.getText()));
                        ergebnis22.setVerbrennungstemp(Integer.parseInt(vt22.getText()));
                        ergebnis22.setO2gehalt(Integer.parseInt(og22.getText()));
                        ergebnis22.setAbgasverlust(Integer.parseInt(av22.getText()));
                        ergebnis22.setMessvorgang(messvorgang);
                        list.add(ergebnis22);
                    }
                } catch (NumberFormatException e) {
                    // ignore because not every line must be filled out by the
                    // kontrolleur
                }
            }
        } catch (RemoteException e) {
            logger.warn("Der Messvorgang konnte nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Der Messvorgang konnte nicht aus der Datenbank geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
        return list;
    }

}
