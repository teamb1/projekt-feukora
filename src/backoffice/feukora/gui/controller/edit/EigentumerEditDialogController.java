package feukora.gui.controller.edit;

import feukora.entity.EigentumerEntity;
import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.EigentumerOverviewController;
import feukora.gui.controller.view.OrtschaftOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteEigentumerAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the eigentümer edit dialog.
 *
 * @author Tscherrig Sven
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class EigentumerEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(EigentumerEditDialogController.class);
    /**
     * The vorname field.
     */
    @FXML
    private TextField vornameField;
    /**
     * The name field.
     */
    @FXML
    private TextField nameField;
    /**
     * The telefon field.
     */
    @FXML
    private TextField telefonField;
    /**
     * The strasse field.
     */
    @FXML
    private TextField strasseField;
    /**
     * The ortschaft field.
     */
    @FXML
    private TextField ortschaftField;
    /**
     * The eigentumer.
     */
    private EigentumerEntity eigentumer;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ortschaft.
     */
    private OrtschaftEntity ortschaft;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the eigentümer edit dialog.
     *
     * @param tempEigentumer the eigentumer that should be edited
     * @return true if the user clicked ok
     */
    public boolean showEigentumerEditDialog(EigentumerEntity tempEigentumer) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/EigentumerEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Eigentümer bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            EigentumerEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setEigentumer(tempEigentumer);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the EigentumerDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the eigentümer which will be edited.
     *
     * @param eigentumer the eigentümer which will be edited
     */
    public void setEigentumer(EigentumerEntity eigentumer) {
        this.eigentumer = eigentumer;
        vornameField.setText(eigentumer.getVorname());
        nameField.setText(eigentumer.getName());
        telefonField.setText(eigentumer.getTelefon());
        strasseField.setText(eigentumer.getStrasse());
        if (eigentumer.getOrtschaft() != null) {
            this.ortschaft = eigentumer.getOrtschaft();
            ortschaftField.setText(eigentumer.getOrtschaft().getPlz() + " " + eigentumer.getOrtschaft().getGemeinde());
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    public void handleOk() {
        try {
            eigentumer.setName(nameField.getText());
            eigentumer.setVorname(vornameField.getText());
            eigentumer.setTelefon(telefonField.getText());
            eigentumer.setStrasse(strasseField.getText());
            eigentumer.setOrtschaft(ortschaft);
            ConcreteEigentumerAction eigentumerAction = (ConcreteEigentumerAction) ActionRegistry.getInterface("eigentumer");
            if (editDialog) {
                eigentumerAction.update(eigentumer);
                editDialog = false;
            } else {
                eigentumerAction.add(eigentumer);
                List<EigentumerEntity> dataList = eigentumerAction.getAll();
                Iterator<EigentumerEntity> dataItr = dataList.iterator();

                EigentumerOverviewController.getEigentumerData().removeAll(EigentumerOverviewController.getEigentumerData());

                while (dataItr.hasNext()) {
                    EigentumerEntity dataEigentumer = dataItr.next();
                    EigentumerOverviewController.getEigentumerData().add(dataEigentumer);
                }

            }
            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Eigentumer konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Der Eigentümer konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Eigentümer ändern", DialogOptions.OK);
        }

    }

    /**
     * Handle choose ortschaft.
     */
    @FXML
    public void handleChooseOrtschaft() {
        OrtschaftOverviewController controller = new OrtschaftOverviewController();
        controller.showOrtschaftOverview();
        this.ortschaft = controller.getOrtschaft();
        ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
    }

    /**
     * Closes the dialog if the user clicks CANCEL.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                ConcreteEigentumerAction eigentumerAction = (ConcreteEigentumerAction) ActionRegistry.getInterface("eigentumer");
                int id = eigentumer.getId();
                EigentumerEntity dataEigentumer = eigentumerAction.getById(id);

                eigentumer.setName(dataEigentumer.getName());
                eigentumer.setVorname(dataEigentumer.getVorname());
                eigentumer.setTelefon(dataEigentumer.getTelefon());
                eigentumer.setStrasse(dataEigentumer.getStrasse());
                eigentumer.setOrtschaft(dataEigentumer.getOrtschaft());
            } catch (RemoteException e) {
                logger.warn("Die Änderungen am Eigentumer konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(dialogStage, e.getMessage(), "Der Eigentümer konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                logger.warn("Die Änderungen am Eigentumer konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(dialogStage, " ", e.getMessage(), "Fehler beim Speichern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage the new Stage for the dialog
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Set the edit dialog.
     *
     * @param editDialog true if the edit dialog is called
     */
    public void setEditDialog(boolean editDialog) {
        EigentumerEditDialogController.editDialog = editDialog;
    }
}
