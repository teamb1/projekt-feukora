package feukora.gui.controller.edit;

import feukora.entity.BrennerEntity;
import feukora.entity.BrennerartEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.BrennerOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteBrennerAction;
import feukora.interfaces.ConcreteBrennerartAction;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the brenner edit dialog.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class BrennerEditDialogController {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BrennerEditDialogController.class);
    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The fabrikat field.
     */
    @FXML
    private TextField fabrikatField;
    /**
     * The baujahr field.
     */
    @FXML
    private TextField baujahrField;
    /**
     * The brennerart box.
     */
    @FXML
    private ChoiceBox<String> brennerartBox = new ChoiceBox<String>();
    /**
     * The warmeleistung field.
     */
    @FXML
    private TextField warmeleistungField;
    /**
     * The brenner.
     */
    private BrennerEntity brenner;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {
        brennerartBox.setItems(FXCollections.observableArrayList("Gebläse", "atmosphärisch", "Verdampfer"));
    }

    /**
     * Shows the brenner edit dialog.
     *
     * @param brenner the brenner that should be edited
     * @return true if the user clicked ok
     */
    public boolean showBrennerEditDialog(BrennerEntity brenner) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/BrennerEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Brenner bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            BrennerEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setBrenner(brenner);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the BrennerEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the brenner which will be edited.
     *
     * @param brenner the new brenner
     */
    public void setBrenner(BrennerEntity brenner) {
        this.brenner = brenner;

        if (brenner.getFabrikat() == null) {
            fabrikatField.setText(brenner.getFabrikat());
            baujahrField.setText(Integer.toString(0));
            warmeleistungField.setText(Integer.toString(0));
        } else {
            fabrikatField.setText(brenner.getFabrikat());
            baujahrField.setText(Integer.toString(brenner.getBaujahr()));
            brennerartBox.setValue(brenner.getBrennerart().getBezeichnung());
            warmeleistungField.setText(Integer.toString(brenner.getFeuerungswarmeleistung()));
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the ok button is clicked.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleOk() {
        try {
            brenner.setFabrikat(fabrikatField.getText());
            brenner.setBaujahr(Integer.parseInt(baujahrField.getText()));
            brenner.setFeuerungswarmeleistung(Integer.parseInt(warmeleistungField.getText()));

            ConcreteBrennerartAction brennerartAction = (ConcreteBrennerartAction) ActionRegistry.getInterface("brennerart");
            List<BrennerartEntity> list = brennerartAction.getAll();
            Iterator<BrennerartEntity> itr = list.iterator();
            while (itr.hasNext()) {
                BrennerartEntity brennerart = itr.next();
                if (brennerart.getBezeichnung().equals(brennerartBox.getValue())) {
                    brenner.setBrennerart(brennerart);
                }
            }
            ConcreteBrennerAction brennerAction = (ConcreteBrennerAction) ActionRegistry.getInterface("brenner");
            if (editDialog) {
                brennerAction.update(brenner);
                editDialog = false;
            } else {
                brennerAction.add(brenner);
                List<BrennerEntity> dataList = brennerAction.getAll();
                Iterator<BrennerEntity> dataItr = dataList.iterator();

                BrennerOverviewController.getBrennerData().removeAll(BrennerOverviewController.getBrennerData());

                while (dataItr.hasNext()) {
                    BrennerEntity dataBrenner = dataItr.next();
                    BrennerOverviewController.getBrennerData().add(dataBrenner);
                }
            }
            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Brenner konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Brenner konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Brenner ändern", DialogOptions.OK);
        }
    }

    /**
     * Set the edit dialog.
     *
     * @param editDialog true if the edit dialog is called
     */
    public void setEditDialog(boolean editDialog) {
        BrennerEditDialogController.editDialog = editDialog;
    }

    /**
     * Closes the dialog if the user clicks CANCEL.
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                // provide that the informations in the gui don't change
                ConcreteBrennerAction brennerAction = (ConcreteBrennerAction) ActionRegistry.getInterface("brenner");
                int id = brenner.getId();
                BrennerEntity dataBrenner = brennerAction.getById(id);

                brenner.setFabrikat(dataBrenner.getFabrikat());
                brenner.setBaujahr(dataBrenner.getBaujahr());
                brenner.setBrennerart(dataBrenner.getBrennerart());
                brenner.setFeuerungswarmeleistung(dataBrenner.getFeuerungswarmeleistung());
            } catch (RemoteException e) {
                logger.warn("Die Änderungen am Brenner konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Brenner konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                logger.warn("Die Änderungen am Benutzer konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", e.getCause().getMessage(), "Fehler beim Speichern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the dialog stage.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }
}
