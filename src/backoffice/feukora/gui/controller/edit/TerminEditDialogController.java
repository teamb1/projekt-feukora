package feukora.gui.controller.edit;

import feukora.entity.KontrolleurEntity;
import feukora.entity.StandortEntity;
import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.KontrolleurOverviewController;
import feukora.gui.controller.view.StandortOverviewController;
import feukora.gui.controller.view.TerminOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteStandortAction;
import feukora.interfaces.ConcreteTerminAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the Actions for the termin edit dialog.
 *
 * @author Tscherrig Sven
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class TerminEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(TerminEditDialogController.class);
    /**
     * The next termin.
     */
    private static boolean nextTermin = false;
    /**
     * The day field.
     */
    @FXML
    private TextField dayField;
    /**
     * The month field.
     */
    @FXML
    private TextField monthField;
    /**
     * The year field.
     */
    @FXML
    private TextField yearField;
    /**
     * The hour field.
     */
    @FXML
    private TextField hourField;
    /**
     * The kontrolleur field.
     */
    @FXML
    private TextField kontrolleurField;
    /**
     * The standort field.
     */
    @FXML
    private TextField standortField;
    /**
     * The kontrolleur button.
     */
    @FXML
    private Button kontrolleurButton;
    /**
     * The standort button.
     */
    @FXML
    private Button standortButton;
    /**
     * The termin.
     */
    private TerminEntity termin;
    /**
     * The kontrolleur.
     */
    private KontrolleurEntity kontrolleur;
    /**
     * The standort.
     */
    private StandortEntity standort;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the termin edit dialog.
     *
     * @param termin the termin that should be edited
     * @return true if the user clicked ok
     */
    public boolean showTerminEditDialog(TerminEntity termin) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/TerminEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Termin bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            TerminEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setTermin(termin);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the TerminEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the termin which will be edited.
     *
     * @param termin the termin which will be edited
     */
    public void setTermin(TerminEntity termin) {
        if (nextTermin) {
            this.termin = new TerminEntity();
            if (termin.getKontrolleur() != null) {
                // do provide that the kontrolleur will not be deleted if only
                // another field changed
                this.kontrolleur = termin.getKontrolleur();
                kontrolleurField.setText(kontrolleur.getVorname() + " " + kontrolleur.getName());
            }
            if (termin.getStandort() != null) {
                // do provide that the standort will not be deleted if only
                // another field changed
                try {
                    ConcreteStandortAction standortAction = (ConcreteStandortAction) ActionRegistry.getInterface("standort");
                    this.standort = standortAction.getById(termin.getStandort().getId());
                    standortField.setText(standort.getAdresse());
                } catch (RemoteException e) {
                    logger.warn("Der Standort konnte nicht aus der Datenbank geladen werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Standort konnte nicht aus der Datenbank geladen werden", "Fehler beim Laden", DialogOptions.OK);
                } catch (InvalidDataException e) {
                    Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Termin ändern", DialogOptions.OK);
                }
            }

        } else {
            this.termin = termin;
            if (termin.getDatum() != null) {
                dayField.setText(Integer.toString(termin.getDatum().get(Calendar.DAY_OF_MONTH)));
                monthField.setText(Integer.toString(termin.getDatum().get(Calendar.MONTH) + 1));
                yearField.setText(Integer.toString(termin.getDatum().get(Calendar.YEAR)));
                hourField.setText(Integer.toString(termin.getDatum().get(Calendar.HOUR_OF_DAY)));
            }
            if (termin.getKontrolleur() != null) {
                // do provide that the kontrolleur will not be deleted if only
                // another field changed
                this.kontrolleur = termin.getKontrolleur();
                kontrolleurField.setText(kontrolleur.getVorname() + " " + kontrolleur.getName());
            }
            if (termin.getStandort() != null) {
                // do provide that the standort will not be deleted if only
                // another
                // field changed
                this.standort = termin.getStandort();
                standortField.setText(standort.getAdresse());
            }
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    public void handleOk() {

        try {
            try {
                termin.setDatum(Integer.parseInt(yearField.getText()), Integer.parseInt(monthField.getText()) - 1, Integer.parseInt(dayField.getText()), Integer.parseInt(hourField.getText()), 00);
            } catch (NumberFormatException e) {
                throw new InvalidDataException("Das Datum ist ungültig.");
            }
            termin.setKontrolleur(kontrolleur);
            termin.setStandort(standort);

            ConcreteTerminAction terminAction = (ConcreteTerminAction) ActionRegistry.getInterface("termin");
            if (editDialog) {
                terminAction.update(termin);
                editDialog = false;
                nextTermin = false;
            } else {
                terminAction.add(termin);
                nextTermin = false;
                List<TerminEntity> dataList = terminAction.getAll();
                Iterator<TerminEntity> dataItr = dataList.iterator();

                TerminOverviewController.getTerminData().removeAll(TerminOverviewController.getTerminData());

                while (dataItr.hasNext()) {
                    TerminEntity dataTermin = dataItr.next();
                    TerminOverviewController.getTerminData().add(dataTermin);
                }
            }

            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Termin konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Termin konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Termin ändern", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialog if the user clicks cancel.
     */
    @FXML
    public void handleCancel() {
        if (editDialog) {
            try {
                ConcreteTerminAction terminAction = (ConcreteTerminAction) ActionRegistry.getInterface("termin");
                int id = termin.getId();
                TerminEntity dataTermin = terminAction.getById(id);

                termin.setDatumGregorian(dataTermin.getDatum());
                termin.setKontrolleur(dataTermin.getKontrolleur());
                termin.setStandort(dataTermin.getStandort());
            } catch (RemoteException e) {
                logger.warn("Die Änderungen am Termin konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(dialogStage, e.getMessage(), "Der Termin konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                logger.warn("Die Änderungen am Termin konnten nicht übernommen werden", e);
                Dialogs.showErrorDialog(dialogStage, " ", e.getMessage(), "Fehler beim Speichern", DialogOptions.OK);
            }
        }
        editDialog = false;
        nextTermin = false;
        dialogStage.close();
    }

    /**
     * Handle choose standort.
     */
    @FXML
    public void handleChooseStandort() {
        StandortOverviewController controller = new StandortOverviewController();
        controller.showStandortChooseDialog();
        this.standort = controller.getStandort();
        standortField.setText(standort.getAdresse());
    }

    /**
     * Handle choose kontrolleur.
     */
    @FXML
    public void handleChooseKontrolleur() {
        KontrolleurOverviewController controller = new KontrolleurOverviewController();
        controller.showKontrolleurChooseDialog();
        this.kontrolleur = controller.getKontrolleur();
        kontrolleurField.setText(kontrolleur.getVorname() + " " + kontrolleur.getName());
    }

    /**
     * Sets the dialog stage.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        if (nextTermin) {
            standortButton.setDisable(true);
            kontrolleurButton.setDisable(true);
        }
    }

    /**
     * Set the edit dialog.
     *
     * @param editDialog true if the edit dialog is called
     */
    public void setEditDialog(boolean editDialog) {
        TerminEditDialogController.editDialog = editDialog;
    }

    /**
     * Sets the folge termin.
     *
     * @param folgeTermin the new folge termin
     */
    public void setFolgeTermin(boolean folgeTermin) {
        TerminEditDialogController.nextTermin = folgeTermin;
    }

}
