package feukora.gui.controller.edit;

import feukora.entity.BrennstoffEntity;
import feukora.entity.WarmeerzeugerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.WarmeerzeugerOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteBrennstoffAction;
import feukora.interfaces.ConcreteWarmeerzeugerAction;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the wärmeerzeuger edit dialog.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 11.04.2014
 */
public class WarmeerzeugerEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(WarmeerzeugerEditDialogController.class);
    /**
     * The fabrikat field.
     */
    @FXML
    private TextField fabrikatField;
    /**
     * The baujahr field.
     */
    @FXML
    private TextField baujahrField;
    /**
     * The brennstoff box.
     */
    @FXML
    private ChoiceBox<String> brennstoffBox = new ChoiceBox<String>();
    /**
     * The warmeerzeuger.
     */
    private WarmeerzeugerEntity warmeerzeuger;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {
        brennstoffBox.setItems(FXCollections.observableArrayList("Öl", "Erdgas", "Flüssiggas"));
    }

    /**
     * Shows the wärmeerzeuger edit dialog.
     *
     * @param warmeerzeuger the wärmeerzeuger that should be edited
     * @return true if the user clicked ok
     */
    public boolean showWarmeerzeugerEditDialog(WarmeerzeugerEntity warmeerzeuger) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/WarmeerzeugerEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Wärmeerzeuger bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            WarmeerzeugerEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setWarmeerzeuger(warmeerzeuger);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            logger.warn("Unable to load the WarmeerzeugerEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the warmeerzeuger which will be edited.
     *
     * @param warmerzeuger the warmeerzeuger that will be changed
     */
    public void setWarmeerzeuger(WarmeerzeugerEntity warmerzeuger) {
        this.warmeerzeuger = warmerzeuger;

        if (warmeerzeuger.getFabrikat() == null) {
            fabrikatField.setText(warmeerzeuger.getFabrikat());
            baujahrField.setText(Integer.toString(0));
        } else {
            fabrikatField.setText(warmeerzeuger.getFabrikat());
            baujahrField.setText(Integer.toString(warmeerzeuger.getBaujahr()));
            brennstoffBox.setValue(warmeerzeuger.getBrennstoff().getBezeichnung());
        }

    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleOk() {
        try {
            warmeerzeuger.setFabrikat(fabrikatField.getText());
            warmeerzeuger.setBaujahr(Integer.parseInt(baujahrField.getText()));
            ConcreteBrennstoffAction brennstoffAction = (ConcreteBrennstoffAction) ActionRegistry.getInterface("brennstoff");
            List<BrennstoffEntity> list = brennstoffAction.getAll();
            Iterator<BrennstoffEntity> itr = list.iterator();
            while (itr.hasNext()) {
                BrennstoffEntity brennstoff = itr.next();
                if (brennstoff.getBezeichnung().equals(brennstoffBox.getValue())) {
                    warmeerzeuger.setBrennstoff(brennstoff);
                }
            }

            ConcreteWarmeerzeugerAction warmeerzeugerAction = (ConcreteWarmeerzeugerAction) ActionRegistry.getInterface("warmeerzeuger");
            if (editDialog) {
                warmeerzeugerAction.update(warmeerzeuger);
                editDialog = false;
            } else {
                warmeerzeugerAction.add(warmeerzeuger);
                List<WarmeerzeugerEntity> dataList = warmeerzeugerAction.getAll();
                Iterator<WarmeerzeugerEntity> dataItr = dataList.iterator();

                WarmeerzeugerOverviewController.getWarmeerzeugerData().removeAll(WarmeerzeugerOverviewController.getWarmeerzeugerData());

                while (dataItr.hasNext()) {
                    WarmeerzeugerEntity dataWarmeerzeuger = dataItr.next();
                    WarmeerzeugerOverviewController.getWarmeerzeugerData().add(dataWarmeerzeuger);
                }
            }
            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Wärmeerzeuger konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Wärmerzeuger konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getMessage(), "Wärmeerzeuger ändern", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialog if the user clicks CANCEL.
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                ConcreteWarmeerzeugerAction warmeerzeugerAction = (ConcreteWarmeerzeugerAction) ActionRegistry.getInterface("warmeerzeuger");
                int id = warmeerzeuger.getId();
                WarmeerzeugerEntity dataWarmeerzeuger = warmeerzeugerAction.getById(id);

                warmeerzeuger.setFabrikat(dataWarmeerzeuger.getFabrikat());
                warmeerzeuger.setBaujahr(dataWarmeerzeuger.getBaujahr());
                warmeerzeuger.setBrennstoff(dataWarmeerzeuger.getBrennstoff());
            } catch (RemoteException e) {
                logger.warn("Der Wärmeerzeuger konnte nicht in die Datenbank geschrieben werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Wärmerzeuger konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Wärmeerzeuger ändern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the dialog stage.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Sets the edits the dialog.
     *
     * @param editDialog the new edits the dialog
     */
    public void setEditDialog(boolean editDialog) {
        WarmeerzeugerEditDialogController.editDialog = editDialog;
    }
}
