package feukora.gui.controller.edit;

import feukora.entity.OrtschaftEntity;
import feukora.entity.VerwaltungEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.OrtschaftOverviewController;
import feukora.gui.controller.view.VerwaltungOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteVerwaltungAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the verwaltung edit dialog controller.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 01.05.2014
 */
public class VerwaltungEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog = false;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(VerwaltungEditDialogController.class);
    /**
     * The vorname field.
     */
    @FXML
    private TextField vornameField;
    /**
     * The name field.
     */
    @FXML
    private TextField nameField;
    /**
     * The telefon field.
     */
    @FXML
    private TextField telefonField;
    /**
     * The strasse field.
     */
    @FXML
    private TextField strasseField;
    /**
     * The ortschaft field.
     */
    @FXML
    private TextField ortschaftField;
    /**
     * The verwaltung.
     */
    private VerwaltungEntity verwaltung;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ortschaft.
     */
    private OrtschaftEntity ortschaft;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the verwaltung edit dialog.
     *
     * @param tempVerwaltung the verwaltung that should be edited
     * @return true if the user clicked ok
     */
    public boolean showVerwaltungEditDialog(VerwaltungEntity tempVerwaltung) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/VerwaltungEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Verwaltung bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            VerwaltungEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setVerwaltung(tempVerwaltung);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the VerwaltungEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the verwaltung which will be edited.
     *
     * @param verwaltung the new verwaltung
     */
    public void setVerwaltung(VerwaltungEntity verwaltung) {
        this.verwaltung = verwaltung;
        vornameField.setText(verwaltung.getVorname());
        nameField.setText(verwaltung.getName());
        telefonField.setText(verwaltung.getTelefon());
        strasseField.setText(verwaltung.getStrasse());
        if (verwaltung.getOrtschaft() != null) {
            this.ortschaft = verwaltung.getOrtschaft();
            ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the OK button is clicked.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleOk() {

        try {
            verwaltung.setName(nameField.getText());
            verwaltung.setVorname(vornameField.getText());
            verwaltung.setTelefon(telefonField.getText());
            verwaltung.setStrasse(strasseField.getText());
            verwaltung.setOrtschaft(ortschaft);

            ConcreteVerwaltungAction verwaltungAction = (ConcreteVerwaltungAction) ActionRegistry.getInterface("verwaltung");
            if (editDialog) {
                verwaltungAction.update(verwaltung);
                editDialog = false;
            } else {
                verwaltungAction.add(verwaltung);
                List<VerwaltungEntity> dataList = verwaltungAction.getAll();
                Iterator<VerwaltungEntity> dataItr = dataList.iterator();

                VerwaltungOverviewController.getVerwaltungData().removeAll(VerwaltungOverviewController.getVerwaltungData());

                while (dataItr.hasNext()) {
                    VerwaltungEntity dataVerwaltung = dataItr.next();
                    VerwaltungOverviewController.getVerwaltungData().add(dataVerwaltung);
                }
            }

            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Die Verwaltung konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Die Verwaltung konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Verwaltung ändern", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialog if the user clicks CANCEL.
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                ConcreteVerwaltungAction verwaltungAction = (ConcreteVerwaltungAction) ActionRegistry.getInterface("verwaltung");
                int id = verwaltung.getId();
                VerwaltungEntity dataVerwaltung = verwaltungAction.getById(id);
                verwaltung.setName(dataVerwaltung.getName());
                verwaltung.setVorname(dataVerwaltung.getVorname());
                verwaltung.setOrtschaft(dataVerwaltung.getOrtschaft());
                verwaltung.setStrasse(dataVerwaltung.getStrasse());
                verwaltung.setTelefon(dataVerwaltung.getTelefon());
            } catch (RemoteException e) {
                logger.warn("Die Verwaltung konnte nicht in die Datenbank geschrieben werden", e);
                Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Die Verwaltung konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Verwaltung ändern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage the new Stage for the dialog
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Handle choose ortschaft.
     */
    @FXML
    public void handleChooseOrtschaft() {
        OrtschaftOverviewController controller = new OrtschaftOverviewController();
        controller.showOrtschaftOverview();
        this.ortschaft = controller.getOrtschaft();
        ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
    }

    /**
     * Sets the edits the dialog.
     *
     * @param editDialog the new edits the dialog
     */
    public void setEditDialog(boolean editDialog) {
        VerwaltungEditDialogController.editDialog = editDialog;
    }
}
