package feukora.gui.controller.edit;

import feukora.entity.HauswartEntity;
import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.view.HauswartOverviewController;
import feukora.gui.controller.view.OrtschaftOverviewController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteHauswartAction;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the hauswart edit dialog.
 *
 * @author Sven Neitzel
 * @version 20.05.2014
 * @since 16.04.2014
 */

public class HauswartEditDialogController {

    /**
     * The edit dialog.
     */
    private static boolean editDialog;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(HauswartEditDialogController.class);
    /**
     * The vorname field.
     */
    @FXML
    private TextField vornameField;
    /**
     * The name field.
     */
    @FXML
    private TextField nameField;
    /**
     * The telefon field.
     */
    @FXML
    private TextField telefonField;
    /**
     * The strasse field.
     */
    @FXML
    private TextField strasseField;
    /**
     * The ortschaft field.
     */
    @FXML
    private TextField ortschaftField;
    /**
     * The hauswart.
     */
    private HauswartEntity hauswart;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ortschaft.
     */
    private OrtschaftEntity ortschaft;

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    private void initialize() {

    }

    /**
     * Shows the hauswart edit dialog.
     *
     * @param tempHauswart the hauswart that should be edited
     * @return true if the user clicked ok
     */
    public boolean showHauswartEditDialog(HauswartEntity tempHauswart) {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/HauswartEditDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Hauswart bearbeiten");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            HauswartEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setHauswart(tempHauswart);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the HauswartEditDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Sets the information of the hauswart which will be edited.
     *
     * @param hauswart the auswart which will be edited
     */
    public void setHauswart(HauswartEntity hauswart) {
        this.hauswart = hauswart;
        vornameField.setText(hauswart.getVorname());
        nameField.setText(hauswart.getName());
        telefonField.setText(hauswart.getTelefon());
        strasseField.setText(hauswart.getStrasse());
        if (hauswart.getOrtschaft() != null) {
            this.ortschaft = hauswart.getOrtschaft();
            ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
        }
    }

    /**
     * Shows if the user clicked the OK button or not.
     *
     * @return if the ok button is clicked or not
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called if the ok button is clicked.
     *
     * @author Sven Neitzel, Patrick Schmed
     */
    @FXML
    public void handleOk() {
        try {
            hauswart.setName(nameField.getText());
            hauswart.setVorname(vornameField.getText());
            hauswart.setTelefon(telefonField.getText());
            hauswart.setStrasse(strasseField.getText());
            hauswart.setOrtschaft(ortschaft);
            ConcreteHauswartAction hauswartAction = (ConcreteHauswartAction) ActionRegistry.getInterface("hauswart");
            if (editDialog) {
                hauswartAction.update(hauswart);
                editDialog = false;
            } else {
                hauswartAction.add(hauswart);
                List<HauswartEntity> dataList = hauswartAction.getAll();
                Iterator<HauswartEntity> dataItr = dataList.iterator();

                HauswartOverviewController.getHauswartData().removeAll(HauswartOverviewController.getHauswartData());

                while (dataItr.hasNext()) {
                    HauswartEntity dataHauswart = dataItr.next();
                    HauswartOverviewController.getHauswartData().add(dataHauswart);
                }
            }
            okClicked = true;
            dialogStage.close();
        } catch (RemoteException e) {
            logger.warn("Der Hauswart konnte nicht in die Datenbank geschrieben werden", e);
            Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "der Hauswart konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
        } catch (InvalidDataException e) {
            Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Hauswart ändern", DialogOptions.OK);
        }
    }

    /**
     * Handle choose ortschaft.
     */
    @FXML
    public void handleChooseOrtschaft() {
        OrtschaftOverviewController controller = new OrtschaftOverviewController();
        controller.showOrtschaftOverview();
        this.ortschaft = controller.getOrtschaft();
        ortschaftField.setText(ortschaft.getPlz() + " " + ortschaft.getGemeinde());
    }

    /**
     * Closes the dialog if the user clicks cancel.
     *
     * @author Sven Neitzel, Patrick Schmed
     */
    @FXML
    private void handleCancel() {
        if (editDialog) {
            try {
                ConcreteHauswartAction hauswartAction = (ConcreteHauswartAction) ActionRegistry.getInterface("hauswart");
                int id = hauswart.getId();
                HauswartEntity dataHauswart = hauswartAction.getById(id);

                hauswart.setName(dataHauswart.getName());
                hauswart.setVorname(dataHauswart.getVorname());
                hauswart.setTelefon(dataHauswart.getTelefon());
                hauswart.setStrasse(dataHauswart.getStrasse());
                hauswart.setOrtschaft(dataHauswart.getOrtschaft());
            } catch (RemoteException e) {
                logger.warn("Der Hauswart konnte nicht in die Datenbank geschrieben werden", e);
                Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Der Hauswart konnte nicht in die Datenbank geschrieben werden", "Fehler beim Speichern", DialogOptions.OK);
            } catch (InvalidDataException e) {
                Dialogs.showErrorDialog(dialogStage, " ", e.getCause().getMessage(), "Hauswart ändern", DialogOptions.OK);
            }
        }
        editDialog = false;
        dialogStage.close();
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage the new Stage for the dialog
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Sets the edits the dialog.
     *
     * @param editDialog the new edits the dialog
     */
    public void setEditDialog(boolean editDialog) {
        HauswartEditDialogController.editDialog = editDialog;
    }
}
