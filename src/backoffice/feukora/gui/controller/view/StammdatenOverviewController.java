package feukora.gui.controller.view;

import feukora.entity.OrtschaftEntity;
import feukora.gui.MainApp;
import feukora.gui.util.ActionRegistry;
import feukora.gui.util.PlzImporter;
import feukora.interfaces.ConcreteOrtschaftAction;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialogs;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Handles the actions for the stammdaten overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 11.05.2014
 */
public class StammdatenOverviewController {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(StammdatenOverviewController.class);

    /**
     * The pfad field.
     */
    @FXML
    private TextField pfadField;
    /**
     * The source file.
     */
    private File sourceFile;

    private Stage dialogStage;

    /**
     * Standard constructor which is called before the initialize method.
     */
    public StammdatenOverviewController() {

    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {

    }

    /**
     * Shows the stammdaten overview.
     */
    public void showStammdatenOverview() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/StammdatenOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);
        } catch (IOException e) {
            logger.warn("Unable to load the StammdatenOverview.fxml file", e);
        }
    }

    /**
     * Opens a new dialog to choose the file to import.
     */
    @FXML
    public void handleChooseFile() {

        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(MainApp.getPrimaryStage());
        if (selectedFile != null) {
            pfadField.setText(selectedFile.getAbsolutePath());
            sourceFile = selectedFile;
        }
    }

    /**
     * Add the data from the file to the database.
     *
     * @author Patrick Schmed, Christian Klauenbösch
     */
    @FXML
    public void handleImport() {
        Task worker = createWorker();
        Thread thread = new Thread(worker);
        thread.start();
        try {
            thread.join();
            Dialogs.showInformationDialog(null, " ", "Der Import ist abgeschlossen.", "Import abgeschlossen");
        } catch (InterruptedException e) {
            logger.warn("Unable to join thread", e);
            Dialogs.showErrorDialog(null, " ", "Der Import konnte nicht ausgeführt werden.", "Import fehlgeschlagen");
        }

    }

    private Task createWorker() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                OrtschaftEntity ortschaft = new OrtschaftEntity();
                PlzImporter importer = new PlzImporter(sourceFile);

                Map<String, List<String>> list = importer.getList();
                Set<String> keys = list.keySet();

                int i = 0;
                int max = keys.size();

                ConcreteOrtschaftAction ortschaftAction = null;
                try {
                    ortschaftAction = (ConcreteOrtschaftAction) ActionRegistry.getInterface("ortschaft");
                } catch (RemoteException e) {
                    Dialogs.showErrorDialog(null, e.getMessage(), "Die Aktion kann nicht ausgeführt werden", "Verarbeitungsfehler");
                    return false;
                }

                for (String plz : keys) {
                    i++;
                    if (i % 5 == 0) {
                        final double pc = (double) i / (double) max;
                        logger.trace("Update progress to " + pc);
                        new Thread(new Task() {
                            @Override
                            protected Object call() throws Exception {
                                return true;
                            }
                        }).start();
                    }
                    if (i % 100 == 0) {
                        logger.info("Got " + i + " of " + max + " items imported");
                    }
                    ortschaft.setPlz(plz);

                    List<String> tempList = list.get(plz);
                    ortschaft.setGemeinde(tempList.get(0));
                    ortschaft.setKanton(tempList.get(1));

                    try {
                        ortschaftAction.add(ortschaft);
                    } catch (Exception e) {
                        // ignore
                    }
                }
                return true;
            }
        };
    }
}
