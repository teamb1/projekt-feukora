package feukora.gui.controller.view;

import feukora.entity.BrennerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.BrennerEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteBrennerAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the brenner overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class BrennerOverviewController {
    /**
     * The brenner.
     */
    private static BrennerEntity brenner;
    /**
     * The ok clicked.
     */
    private static boolean okClicked = false;
    /**
     * The brenner data.
     */
    private static ObservableList<BrennerEntity> brennerData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BrennerOverviewController.class);
    /**
     * The brenner table.
     */
    @FXML
    private TableView<BrennerEntity> brennerTable;
    /**
     * The fabrikat row.
     */
    @FXML
    private TableColumn<BrennerEntity, String> fabrikatRow;
    /**
     * The baujahr row.
     */
    @FXML
    private TableColumn<BrennerEntity, String> baujahrRow;
    /**
     * The brennerart row.
     */
    @FXML
    private TableColumn<BrennerEntity, String> brennerartRow;
    /**
     * The warmeleistung row.
     */
    @FXML
    private TableColumn<BrennerEntity, String> warmeleistungRow;
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * the filter data.
     */
    private ObservableList<BrennerEntity> filteredData = FXCollections.observableArrayList();
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The bearbeiten button.
     */
    @FXML
    private Button bearbeitenButton;

    /**
     * The loeschen button.
     */
    @FXML
    private Button loeschenButton;

    /**
     * Standard constructor which is called before the initialize method.
     */
    public BrennerOverviewController() {

    }

    /**
     * Gets the brenner data.
     *
     * @return the brenner data
     */
    public static ObservableList<BrennerEntity> getBrennerData() {
        return brennerData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            brennerData.removeAll(brennerData);
            ConcreteBrennerAction brennerAction = (ConcreteBrennerAction) ActionRegistry.getInterface("brenner");
            brennerData.addAll(brennerAction.getAll());
            fabrikatRow.setCellValueFactory(new PropertyValueFactory<BrennerEntity, String>("fabrikat"));
            baujahrRow.setCellValueFactory(new PropertyValueFactory<BrennerEntity, String>("baujahr"));
            brennerartRow.setCellValueFactory(new Callback<CellDataFeatures<BrennerEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<BrennerEntity, String> brenner) {
                    if (brenner.getValue().getBrennerart() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(brenner.getValue().getBrennerart().getBezeichnung());
                    }
                }
            });
            warmeleistungRow.setCellValueFactory(new PropertyValueFactory<BrennerEntity, String>("feuerungswarmeleistung"));
            brennerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Brennerdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Brennerdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all brenner from the database in a table overview.
     */
    public void showBrennerOverview() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/BrennerOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);

            // Give the controller access to the main app
            BrennerOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the BrennerOverview.fxml file", e);
        }
    }

    /**
     * Shows all brenner from the database in a table overview on a new stage.
     *
     * @return if the user clicked auswählen
     */
    public boolean showBrennerChooseDialog() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/BrennerChooseDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Brenner Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            BrennerOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the BrennerOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (BrennerEntity brenner : brennerData) {
            if (matchesFilter(brenner)) {
                filteredData.add(brenner);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<BrennerEntity, ?>> sortOrder = new ArrayList<>(brennerTable.getSortOrder());
        brennerTable.getSortOrder().clear();
        brennerTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param brenner the brenner object
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(BrennerEntity brenner) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (brenner.getFabrikat().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (brenner.getBrennerart().getBezeichnung().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the brenner into the table.
     *
     * @param mainApp the main application
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        filteredData.addAll(brennerData);
        brennerTable.setItems(filteredData);
        brennerData.addListener(new ListChangeListener<BrennerEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends BrennerEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Set the brenner into the table.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        try {
            if (ActionRegistry.isRmiPossible() == false) {
                bearbeitenButton.setDisable(true);
                loeschenButton.setDisable(true);
            }
        } catch (HeadlessException | IOException e) {
            e.printStackTrace();
        }
        brennerTable.setItems(filteredData);
        filteredData.addAll(brennerData);
        brennerData.addListener(new ListChangeListener<BrennerEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends BrennerEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Opens a new dialog to create a new brenner.
     */
    @FXML
    private void handleNewBrenner() {
        BrennerEntity tempBrenner = new BrennerEntity();
        BrennerEditDialogController controller = new BrennerEditDialogController();
        controller.setEditDialog(false);
        controller.showBrennerEditDialog(tempBrenner);
    }

    /**
     * Deletes the selected brenner.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleDeleteBrenner() {
        int selectedIndex = brennerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Wollen Sie diesen Brenner löschen?", "Bitte bestätigen Sie mit YES", "Brenner löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = brennerTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteBrennerAction brennerAction = (ConcreteBrennerAction) ActionRegistry.getInterface("brenner");
                    brennerAction.delete(id);

                    brennerTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Brenner konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Brenner konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Brenner auswählen", "Brenner löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected brenner.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleEditBrenner() {
        BrennerEntity selectedBrenner = brennerTable.getSelectionModel().getSelectedItem();
        if (selectedBrenner != null) {
            BrennerEditDialogController controller = new BrennerEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showBrennerEditDialog(selectedBrenner);
            if (okClicked) {
                refreshBrennerTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Brenner auswählen", "Brenner bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Set the selected brenner and give it to the rapportEditDialog.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = brennerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            brenner = brennerTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Brenner auswählen", "Brenner wählen", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    public void refreshBrennerTable() {
        int selectedIndex = brennerTable.getSelectionModel().getSelectedIndex();
        brennerTable.setItems(null);
        brennerTable.layout();
        brennerTable.setItems(filteredData);
        brennerTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the brenner.
     *
     * @return the brenner
     */
    public BrennerEntity getBrenner() {
        return brenner;
    }
}
