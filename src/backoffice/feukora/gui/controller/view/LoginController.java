/*
 * 
 */
package feukora.gui.controller.view;

import feukora.exceptions.InvalidDataException;
import feukora.exceptions.UnableException;
import feukora.gui.MainApp;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.BusinessAction;
import feukora.interfaces.ConcreteLoginAction;
import feukora.util.Encoder;
import javafx.fxml.FXML;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import javax.xml.ws.WebServiceException;
import java.rmi.RemoteException;

/**
 * Handles the login actions.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 24.04.2014
 */
public class LoginController {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(LoginController.class);
    /**
     * The benutzer field.
     */
    @FXML
    private TextField benutzerField;
    /**
     * The passwort field.
     */
    @FXML
    private PasswordField passwortField;
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;

    /**
     * Handle login.
     *
     * @author Christian Klauenbösch
     */
    @FXML
    @SuppressWarnings("rawtypes")
    public void handleLogin() {
        String benutzername = benutzerField.getText();
        String passwort = passwortField.getText();

        try {
            String hashAsString = Encoder.hashPassword(passwort);
            BusinessAction action = ActionRegistry.getInterface("login");
            ConcreteLoginAction loginAction = (ConcreteLoginAction) action;

            if (loginAction.checkBenutzer(benutzername, hashAsString)) {
                dialogStage.close();
                mainApp.showStartScreen(MainApp.getPrimaryStage());
            } else {
                Dialogs.showErrorDialog(dialogStage, " ", "Benutzername oder Passwort falsch eingegeben ", "Falsche Benutzerdaten", DialogOptions.OK);
            }
        } catch (UnableException | InvalidDataException | RemoteException e) {
            logger.warn("Unable to login", e);
            Dialogs.showErrorDialog(dialogStage, " ", e.getMessage(), "Login nicht möglich", DialogOptions.OK);
        } catch (WebServiceException ex) {
            logger.warn("Unable to connect to rmi", ex);
            Dialogs.showErrorDialog(dialogStage, " ", "Es konnte keine Verbindung zum Server aufgbaut werden.", "Login nicht möglich", DialogOptions.OK);
        }
    }

    /**
     * If the user clicks cancel.
     */
    @FXML
    public void handleCancel() {
        dialogStage.close();
    }

    /**
     * Give the controller access to the MainApp.
     *
     * @param mainApp the MainApp.
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

    }

    /**
     * Sets the stage.
     *
     * @param dialogStage the stage in which the dialog is shown.
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
    }
}
