package feukora.gui.controller.view;

import feukora.entity.EigentumerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.EigentumerEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteEigentumerAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * * Handles the actions for the eigentümer overview.
 *
 * @author Tscherrig Sven
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class EigentumerOverviewController {

    /**
     * The choose dialog.
     */
    private static boolean chooseDialog = false;
    /**
     * The eigentumer.
     */
    private static EigentumerEntity eigentumer;
    /**
     * The eigentumer data.
     */
    private static ObservableList<EigentumerEntity> eigentumerData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(EigentumerOverviewController.class);
    /**
     * The eigentumer table.
     */
    @FXML
    private TableView<EigentumerEntity> eigentumerTable;
    /**
     * The vorname row.
     */
    @FXML
    private TableColumn<EigentumerEntity, String> vornameRow;
    /**
     * The name row.
     */
    @FXML
    private TableColumn<EigentumerEntity, String> nameRow;
    /**
     * The telefon row.
     */
    @FXML
    private TableColumn<EigentumerEntity, String> telefonRow;
    /**
     * The strasse row.
     */
    @FXML
    private TableColumn<EigentumerEntity, String> strasseRow;
    /**
     * The ortschaft row.
     */
    @FXML
    private TableColumn<EigentumerEntity, String> ortschaftRow;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The choose button.
     */
    @FXML
    private Button chooseButton;
    /**
     * The close button.
     */
    @FXML
    private Button closeButton;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The filtered data.
     */
    private ObservableList<EigentumerEntity> filteredData = FXCollections.observableArrayList();
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * Standard constructor which is called before the initialize method.
     */
    public EigentumerOverviewController() {

    }

    /**
     * Gets the eigentumer data.
     *
     * @return the eigentumer data
     */
    public static ObservableList<EigentumerEntity> getEigentumerData() {
        return eigentumerData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    public void initialize() {
        try {
            eigentumerData.removeAll(eigentumerData);
            ConcreteEigentumerAction eigentumerAction = (ConcreteEigentumerAction) ActionRegistry.getInterface("eigentumer");
            eigentumerData.addAll(eigentumerAction.getAll());
            vornameRow.setCellValueFactory(new PropertyValueFactory<EigentumerEntity, String>("vorname"));
            nameRow.setCellValueFactory(new PropertyValueFactory<EigentumerEntity, String>("name"));
            telefonRow.setCellValueFactory(new PropertyValueFactory<EigentumerEntity, String>("telefon"));
            strasseRow.setCellValueFactory(new PropertyValueFactory<EigentumerEntity, String>("strasse"));
            ortschaftRow.setCellValueFactory(new Callback<CellDataFeatures<EigentumerEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<EigentumerEntity, String> eigentumer) {
                    if (eigentumer.getValue().getOrtschaft() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(eigentumer.getValue().getOrtschaft().getPlz() + " " + eigentumer.getValue().getOrtschaft().getGemeinde() + ", " + eigentumer.getValue().getOrtschaft().getKanton());
                    }
                }
            });
            eigentumerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Eigentumerdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Eigentumerdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all eigentümer from the database in a table overview.
     *
     * @return if the user clicked schliessen or auswählen
     * @author Sven Tscherrig, Patrick Schmed
     */
    public boolean showEigentumerOverview() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/EigentumerOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Eigentümer Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            EigentumerOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the EigentumerOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the selected eigentümer and give it to the standortEditDialog.
     *
     * @author Sven Tscherrig, Sven Neitzel
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = eigentumerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            eigentumer = eigentumerTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Kontrolleur auswählen", "Kontrolleur wählen", DialogOptions.OK);
        }
    }

    /**
     * Set the eigentümer into the table and deactivates the close or choose
     * button.
     *
     * @param dialogStage the dialogStage
     * @author Sven Tscherrig, Patrick Schmed
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        if (chooseDialog == true) {
            closeButton.setVisible(false);
        } else {
            chooseButton.setVisible(false);
        }
        eigentumerTable.setItems(filteredData);
        filteredData.addAll(eigentumerData);
        eigentumerData.addListener(new ListChangeListener<EigentumerEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends EigentumerEntity> change) {
                updateFilteredData();
            }
        });

    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Sven Tscherrig, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (EigentumerEntity eigentumer : eigentumerData) {
            if (matchesFilter(eigentumer)) {
                filteredData.add(eigentumer);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Sven Tscherrig, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<EigentumerEntity, ?>> sortOrder = new ArrayList<>(eigentumerTable.getSortOrder());
        eigentumerTable.getSortOrder().clear();
        eigentumerTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param eigentumer the eigentumer object
     * @return true if filter matches an object
     * @author Sven Tscherrig, Sven Neitzel
     */
    private boolean matchesFilter(EigentumerEntity eigentumer) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (eigentumer.getName().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (eigentumer.getVorname().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Opens a new dialog to create a new eigentümer.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    private void handleNewEigentumer() {
        EigentumerEntity tempEigentumer = new EigentumerEntity();
        EigentumerEditDialogController controller = new EigentumerEditDialogController();
        controller.setEditDialog(false);
        controller.showEigentumerEditDialog(tempEigentumer);
    }

    /**
     * Deletes the selected eigentümer.
     *
     * @author Sven Tscherrig, Sven Neitzel, Patrick Schmed
     */
    @FXML
    private void handleDeleteEigentumer() {
        int selectedIndex = eigentumerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(dialogStage, "Wollen Sie diesen Eigentümer löschen?", "Bitte bestätigen Sie mit YES", "Eigentümer löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = eigentumerTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteEigentumerAction eigentumerAction = (ConcreteEigentumerAction) ActionRegistry.getInterface("eigentumer");
                    eigentumerAction.delete(id);

                    eigentumerTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Eigentümer konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Der Eigentümer konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Eigentümer auswählen", "Eigentümer löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected eigentümer.
     *
     * @author Sven Tscherrig, Sven Neitzel, Patrick Schmed
     */
    @FXML
    private void handleEditEigentumer() {
        EigentumerEntity selectedEigentumer = eigentumerTable.getSelectionModel().getSelectedItem();
        if (selectedEigentumer != null) {
            EigentumerEditDialogController controller = new EigentumerEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showEigentumerEditDialog(selectedEigentumer);
            if (okClicked) {
                refreshEigentumerTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Eigentümer auswählen", "Eigentümer bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialogStage if the user clicked schliessen.
     */
    @FXML
    public void handleOk() {
        dialogStage.close();
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshEigentumerTable() {
        int selectedIndex = eigentumerTable.getSelectionModel().getSelectedIndex();
        eigentumerTable.setItems(null);
        eigentumerTable.layout();
        eigentumerTable.setItems(filteredData);
        eigentumerTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the eigentumer.
     *
     * @return the eigentumer
     */
    public EigentumerEntity getEigentumer() {
        return eigentumer;
    }

    /**
     * Sets the choose dialog.
     *
     * @param chooseDialog the new choose dialog
     */
    public void setChooseDialog(boolean chooseDialog) {
        EigentumerOverviewController.chooseDialog = chooseDialog;
    }
}
