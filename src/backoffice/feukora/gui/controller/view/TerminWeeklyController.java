package feukora.gui.controller.view;

import feukora.entity.RapportEntity;
import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.util.ActionRegistry;
import feukora.gui.util.Drucker;
import feukora.interfaces.ConcreteTerminAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Calendar;

/**
 * The Class TerminWeeklyController.
 *
 * @author Christian Klauenboesch <i>&lt;christian@klit.ch&rt;</i>
 * @version 2014-05-18
 * @since 2014-05-17
 */
public class TerminWeeklyController {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(TerminWeeklyController.class);
    /**
     * The weekly termin data.
     */
    private static ObservableList<TerminEntity> weeklyTerminData = FXCollections.observableArrayList();
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The weekly status row.
     */
    @FXML
    private TableColumn<TerminEntity, String> weeklyStatusRow;
    /**
     * The weekly time row.
     */
    @FXML
    private TableColumn<TerminEntity, String> weeklyTimeRow;
    /**
     * The weekly date row.
     */
    @FXML
    private TableColumn<TerminEntity, String> weeklyDateRow;
    /**
     * The weekly standort row.
     */
    @FXML
    private TableColumn<TerminEntity, String> weeklyStandortRow;
    /**
     * The weekly kontrolleur row.
     */
    @FXML
    private TableColumn<TerminEntity, String> weeklyKontrolleurRow;
    /**
     * The termin weekly table.
     */
    @FXML
    private TableView<TerminEntity> terminWeeklyTable;
    /**
     * The titel label.
     */
    @FXML
    private Label titelLabel;

    /**
     * Initialize.
     */
    @FXML
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void initialize() {
        weeklyStandortRow.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TerminEntity, String> terminEntityStringCellDataFeatures) {
                if (terminEntityStringCellDataFeatures.getValue().getDatum() == null) {
                    return null;
                }
                return new ReadOnlyObjectWrapper<String>(terminEntityStringCellDataFeatures.getValue().getStandort().getAdresse() + ", " + terminEntityStringCellDataFeatures.getValue().getStandort().getOrtschaft().getPlz() + " " + terminEntityStringCellDataFeatures.getValue().getStandort().getOrtschaft().getGemeinde());
            }
        });

        weeklyDateRow.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TerminEntity, String> termin) {
                if (termin.getValue().getDatum() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(termin.getValue().getDateString());
                }
            }
        });

        weeklyTimeRow.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TerminEntity, String> termin) {
                if (termin.getValue().getDatum() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(termin.getValue().getTimeString() + " Uhr");
                }
            }
        });
        weeklyKontrolleurRow.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TerminEntity, String> termin) {
                if (termin.getValue().getKontrolleur() == null) {
                    return new ReadOnlyObjectWrapper("kein Kontrolleur");
                } else {
                    return new ReadOnlyObjectWrapper(termin.getValue().getKontrolleur().getVorname() + " " + termin.getValue().getKontrolleur().getName() + ", Tel.: " + termin.getValue().getKontrolleur().getTelefon());
                }
            }
        });
        weeklyStatusRow.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TerminEntity, String> termin) {
                if (termin.getValue().getRapport() == null) {
                    return new ReadOnlyObjectWrapper("kein Rapport");
                } else if (termin.getValue().getRapport().getRegulierung() == RapportEntity.STATUS.REG) {
                    return new ReadOnlyObjectWrapper("Regulierung nötig");
                } else if (termin.getValue().getRapport().getRegulierung() == RapportEntity.STATUS.NM) {
                    return new ReadOnlyObjectWrapper("Regulierung nicht möglich");
                } else {
                    return new ReadOnlyObjectWrapper("OK");
                }
            }
        });
        terminWeeklyTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    /**
     * Sets the dialog stage.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        titelLabel.setText("Wochenübersicht KW" + Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
        try {
            weeklyTerminData.removeAll(weeklyTerminData);
            weeklyTerminData.addAll(((ConcreteTerminAction) ActionRegistry.getInterface("termin")).getThisWeek());
            terminWeeklyTable.setItems(weeklyTerminData);
        } catch (InvalidDataException | RemoteException e) {
            logger.warn("Unable to execute action", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getMessage(), "Die Termine können nicht abgerufen werden.", "Fehler beim Datenabruf", Dialogs.DialogOptions.OK);
            dialogStage.close();
        }
    }

    /**
     * Handle print weekly to output.
     */
    public void handlePrintWeeklyToOutput() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Wählen Sie einen Speicherort aus");
        File file = fileChooser.showSaveDialog(MainApp.getPrimaryStage());
        if (file != null) {
            String pfad = file.getAbsolutePath();
            Drucker drucker = new Drucker();
            try {
                drucker.druckenArray(weeklyTerminData, pfad);
                Dialogs.showInformationDialog(MainApp.getPrimaryStage(), " ", "Die Termine wurden gedruckt", "Drucken erfolgreich");
            } catch (IOException e) {
                logger.warn("Unable to print the file", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Termine konnten nicht gedruckt werden", "Fehler beim Drucken", Dialogs.DialogOptions.OK);
            }
        }
    }

    /**
     * Close window.
     *
     * @param actionEvent the action event
     */
    public void closeWindow(ActionEvent actionEvent) {
        dialogStage.close();
    }
}
