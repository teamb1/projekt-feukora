package feukora.gui.controller.view;

import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteOrtschaftAction;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the ortschaft overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 05.05.2014
 */
public class OrtschaftOverviewController {

    /**
     * The ortschaft.
     */
    private static OrtschaftEntity ortschaft;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(OrtschaftOverviewController.class);
    /**
     * The Eigentumer table.
     */
    @FXML
    private TableView<OrtschaftEntity> ortschaftTable;
    /**
     * The vorname row.
     */
    @FXML
    private TableColumn<OrtschaftEntity, String> plzRow;
    /**
     * The name row.
     */
    @FXML
    private TableColumn<OrtschaftEntity, String> gemeindeRow;
    /**
     * The telefon row.
     */
    @FXML
    private TableColumn<OrtschaftEntity, String> kantonRow;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<OrtschaftEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public OrtschaftOverviewController() {

    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        plzRow.setCellValueFactory(new PropertyValueFactory<OrtschaftEntity, String>("plz"));
        gemeindeRow.setCellValueFactory(new PropertyValueFactory<OrtschaftEntity, String>("gemeinde"));
        kantonRow.setCellValueFactory(new PropertyValueFactory<OrtschaftEntity, String>("kanton"));

        ortschaftTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        searchField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                updateFilteredData();
            }
        });
    }

    /**
     * Shows all ortschaften from the database in a table overview on a new
     * stage.
     *
     * @return if the user clicked ok
     */
    public boolean showOrtschaftOverview() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/OrtschaftOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ortschaft Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            OrtschaftOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the OrtschaftOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the selected ortschaft and give it to the editDialog.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = ortschaftTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            ortschaft = ortschaftTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte eine Ortschaft auswählen", "Ortschaft wählen", DialogOptions.OK);
        }
    }

    /**
     * Set the ortschaften into the table.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        ortschaftTable.setItems(filteredData);
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        matchesFilter();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<OrtschaftEntity, ?>> sortOrder = new ArrayList<>(ortschaftTable.getSortOrder());
        ortschaftTable.getSortOrder().clear();
        ortschaftTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter() {
        String filterString = searchField.getText();
        if (filterString.length() >= 2) {
            try {
                ConcreteOrtschaftAction action = (ConcreteOrtschaftAction) ActionRegistry.getInterface("ortschaft");
                filteredData.addAll(action.getListByPattern(filterString));

                reapplyTableSortOrder();
            } catch (RemoteException | InvalidDataException e) {
                logger.warn("Termindaten konnten nicht geladen werden", e);
                Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Termindaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
            }
            return true;
        }
        return false;
    }

    /**
     * Gets the ortschaft.
     *
     * @return the ortschaft
     */
    public OrtschaftEntity getOrtschaft() {
        return ortschaft;
    }
}
