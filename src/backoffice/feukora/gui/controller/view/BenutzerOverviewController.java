package feukora.gui.controller.view;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.BenutzerEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteBenutzerAction;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the benutzer overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 11.05.2014
 */
public class BenutzerOverviewController {

    /**
     * The benutzer data.
     */
    private static ObservableList<BenutzerEntity> benutzerData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(BenutzerOverviewController.class);
    /**
     * The benutzer table.
     */
    @FXML
    private TableView<BenutzerEntity> benutzerTable;
    /**
     * The vorname row.
     */
    @FXML
    private TableColumn<BenutzerEntity, String> vornameRow;
    /**
     * The name row.
     */
    @FXML
    private TableColumn<BenutzerEntity, String> nameRow;
    /**
     * The benutzername row.
     */
    @FXML
    private TableColumn<BenutzerEntity, String> benutzernameRow;
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<BenutzerEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public BenutzerOverviewController() {

    }

    /**
     * Gets the benutzer data.
     *
     * @return the benutzer data
     */
    public static ObservableList<BenutzerEntity> getBenutzerData() {
        return benutzerData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            benutzerData.removeAll(benutzerData);
            ConcreteBenutzerAction benutzerAction = (ConcreteBenutzerAction) ActionRegistry.getInterface("benutzer");
            List<BenutzerEntity> list = benutzerAction.getAll();
            Iterator<BenutzerEntity> itr = list.iterator();
            while (itr.hasNext()) {
                BenutzerEntity benutzer = itr.next();
                if (benutzer.getIsKontrolleur() == 0) {
                    benutzerData.add(benutzer);
                }
            }
            nameRow.setCellValueFactory(new PropertyValueFactory<BenutzerEntity, String>("name"));
            vornameRow.setCellValueFactory(new PropertyValueFactory<BenutzerEntity, String>("vorname"));
            benutzernameRow.setCellValueFactory(new PropertyValueFactory<BenutzerEntity, String>("benutzername"));

            benutzerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Benutzerdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Benutzerdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all benutzer from the database in a table overview.
     */
    public void showBenutzerOverview() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/BenutzerOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);
            BenutzerOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the BenutzerOverview.fxml file", e);
        }
    }

    /**
     * Set the benutzer into the table.
     *
     * @param mainApp the main application
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        filteredData.addAll(benutzerData);
        benutzerTable.setItems(filteredData);
        benutzerData.addListener(new ListChangeListener<BenutzerEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends BenutzerEntity> change) {
                updateFilteredData();
            }
        });

    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (BenutzerEntity benutzer : benutzerData) {
            if (matchesFilter(benutzer)) {
                filteredData.add(benutzer);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<BenutzerEntity, ?>> sortOrder = new ArrayList<>(benutzerTable.getSortOrder());
        benutzerTable.getSortOrder().clear();
        benutzerTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with the table rows.
     *
     * @param benutzer the benutzer object
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(BenutzerEntity benutzer) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (benutzer.getName().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (benutzer.getVorname().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Opens a new dialog to create a new benutzer.
     */
    @FXML
    private void handleNewBenutzer() {
        BenutzerEntity tempBenutzer = new BenutzerEntity();
        BenutzerEditDialogController controller = new BenutzerEditDialogController();
        controller.setEditDialog(false);
        controller.showBenutzerEditDialog(tempBenutzer);
    }

    /**
     * Deletes the selected benutzer.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleDeleteBenutzer() {
        int selectedIndex = benutzerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Wollen Sie diesen Benutzer löschen?", "Bitte bestätigen Sie mit YES", "Benutzer löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = benutzerTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteBenutzerAction benutzerAction = (ConcreteBenutzerAction) ActionRegistry.getInterface("benutzer");
                    benutzerAction.delete(id);
                    benutzerTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Benutzer konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Benutzer konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Benutzer auswählen", "Benutzer löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected benutzer.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleEditBenutzer() {
        BenutzerEntity selectedBenutzer = benutzerTable.getSelectionModel().getSelectedItem();
        if (selectedBenutzer != null) {
            BenutzerEditDialogController controller = new BenutzerEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showBenutzerEditDialog(selectedBenutzer);
            if (okClicked) {
                refreshBenutzerTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Benutzer auswählen", "Benutzer bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshBenutzerTable() {
        int selectedIndex = benutzerTable.getSelectionModel().getSelectedIndex();
        benutzerTable.setItems(null);
        benutzerTable.layout();
        benutzerTable.setItems(filteredData);
        benutzerTable.getSelectionModel().select(selectedIndex);
    }
}
