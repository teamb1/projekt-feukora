package feukora.gui.controller.view;

import feukora.entity.WarmeerzeugerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.WarmeerzeugerEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteWarmeerzeugerAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the brenner overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 11.04.2014
 */
public class WarmeerzeugerOverviewController {

    /**
     * The warmeerzeuger data.
     */
    private static ObservableList<WarmeerzeugerEntity> warmeerzeugerData = FXCollections.observableArrayList();
    /**
     * The ok clicked.
     */
    private static boolean okClicked;
    /**
     * The warmeerzeuger.
     */
    private static WarmeerzeugerEntity warmeerzeuger;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(WarmeerzeugerOverviewController.class);
    /**
     * The warmeerzeuger table.
     */
    @FXML
    private TableView<WarmeerzeugerEntity> warmeerzeugerTable;
    /**
     * The fabrikat row.
     */
    @FXML
    private TableColumn<WarmeerzeugerEntity, String> fabrikatRow;
    /**
     * The baujahr row.
     */
    @FXML
    private TableColumn<WarmeerzeugerEntity, String> baujahrRow;
    /**
     * The brennstoff row.
     */
    @FXML
    private TableColumn<WarmeerzeugerEntity, String> brennstoffRow;
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The bearbeiten button.
     */
    @FXML
    private Button bearbeitenButton;

    /**
     * The loeschen button.
     */
    @FXML
    private Button loeschenButton;

    /**
     * The filtered data.
     */
    private ObservableList<WarmeerzeugerEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public WarmeerzeugerOverviewController() {

    }

    /**
     * Gets the warmeerzeuger data.
     *
     * @return the warmeerzeuger data
     */
    public static ObservableList<WarmeerzeugerEntity> getWarmeerzeugerData() {
        return warmeerzeugerData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            warmeerzeugerData.removeAll(warmeerzeugerData);
            ConcreteWarmeerzeugerAction warmeerzeugerAction = (ConcreteWarmeerzeugerAction) ActionRegistry.getInterface("warmeerzeuger");
            warmeerzeugerData.addAll(warmeerzeugerAction.getAll());
            fabrikatRow.setCellValueFactory(new PropertyValueFactory<WarmeerzeugerEntity, String>("fabrikat"));
            baujahrRow.setCellValueFactory(new PropertyValueFactory<WarmeerzeugerEntity, String>("baujahr"));
            brennstoffRow.setCellValueFactory(new Callback<CellDataFeatures<WarmeerzeugerEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<WarmeerzeugerEntity, String> warmeerzeuger) {
                    if (warmeerzeuger.getValue().getBrennstoff().getBezeichnung() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(warmeerzeuger.getValue().getBrennstoff().getBezeichnung());
                    }
                }
            });

            warmeerzeugerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Wärmeerzeugerdate konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Wärmeerzeugerdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all wärmeerzeuger from the database in a table overview.
     */
    public void showWarmeerzeugerOverview() {
        try {
            // Load the fxml-file and set it to center of the root layout
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/WarmeerzeugerOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);

            // Give the controller access to the main app
            WarmeerzeugerOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the WarmeerzeugerOverview.fxml file", e);
        }
    }

    /**
     * Shows all wärmeerzeuger from the database in a table overview on a new
     * stage.
     *
     * @return if the user clicked auswählen
     */
    public boolean showWarmeerzeugerChooseDialog() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/WarmeerzeugerChooseDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Wärmeerzeuger Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            WarmeerzeugerOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the Warmeerzeuger Overview.fxml file", e);
            return false;
        }

    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the wärmeerzeuger into the table.
     *
     * @param mainApp the main application
     */
    public void setMainApp(MainApp mainApp) {
        warmeerzeugerTable.setItems(filteredData);
        filteredData.addAll(warmeerzeugerData);
        warmeerzeugerTable.setItems(filteredData);
        warmeerzeugerData.addListener(new ListChangeListener<WarmeerzeugerEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends WarmeerzeugerEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (WarmeerzeugerEntity warmeerzeuger : warmeerzeugerData) {
            if (matchesFilter(warmeerzeuger)) {
                filteredData.add(warmeerzeuger);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<WarmeerzeugerEntity, ?>> sortOrder = new ArrayList<>(warmeerzeugerTable.getSortOrder());
        warmeerzeugerTable.getSortOrder().clear();
        warmeerzeugerTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param warmeerzeuger the warmeerzeuger
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(WarmeerzeugerEntity warmeerzeuger) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (warmeerzeuger.getFabrikat().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (warmeerzeuger.getBrennstoff().getBezeichnung().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Opens a new dialog to create a new wärmeerzeuger.
     */
    @FXML
    private void handleNewWarmeerzeuger() {
        WarmeerzeugerEntity tempWarmeerzeuger = new WarmeerzeugerEntity();
        WarmeerzeugerEditDialogController controller = new WarmeerzeugerEditDialogController();
        controller.setEditDialog(false);
        controller.showWarmeerzeugerEditDialog(tempWarmeerzeuger);
    }

    /**
     * Deletes the selected wärmeerzeuger.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleDeleteWarmeerzeuger() {
        int selectedIndex = warmeerzeugerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Wollen Sie diesen Wärmeerzeuger löschen?", "Bitte bestätigen Sie mit YES", "Wärmeerzeuger löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = warmeerzeugerTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteWarmeerzeugerAction warmeerzeugerAction = (ConcreteWarmeerzeugerAction) ActionRegistry.getInterface("warmeerzeuger");
                    warmeerzeugerAction.delete(id);
                    warmeerzeugerTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Warmeerzeuger konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Der Wärmeerzeuger konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Wärmeerzeuger auswählen", "Wärmeerzeuger löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected wärmeerzeuger.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleEditWarmeerzeuger() {
        WarmeerzeugerEntity selectedWarmeerzeuger = warmeerzeugerTable.getSelectionModel().getSelectedItem();
        if (selectedWarmeerzeuger != null) {
            WarmeerzeugerEditDialogController controller = new WarmeerzeugerEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showWarmeerzeugerEditDialog(selectedWarmeerzeuger);
            if (okClicked) {
                refreshWarmeerzeugerTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Wärmeerzeuger auswählen", "Wärmeerzeuger bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Set the wärmeerzeuger into the table.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        try {
            if (ActionRegistry.isRmiPossible() == false) {
                bearbeitenButton.setDisable(true);
                loeschenButton.setDisable(true);
            }
        } catch (HeadlessException | IOException e) {
            e.printStackTrace();
        }
        filteredData.addAll(warmeerzeugerData);
        warmeerzeugerTable.setItems(filteredData);
        warmeerzeugerData.addListener(new ListChangeListener<WarmeerzeugerEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends WarmeerzeugerEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Set the selected wärmeerzeuger and give it to the rapportEditDialog.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = warmeerzeugerTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            warmeerzeuger = warmeerzeugerTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Wärmeerzeuger auswählen", "Wärmeerzeuger wählen", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshWarmeerzeugerTable() {
        int selectedIndex = warmeerzeugerTable.getSelectionModel().getSelectedIndex();
        warmeerzeugerTable.setItems(null);
        warmeerzeugerTable.layout();
        warmeerzeugerTable.setItems(filteredData);
        warmeerzeugerTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the warmeerzeuger.
     *
     * @return the warmeerzeuger
     */
    public WarmeerzeugerEntity getWarmeerzeuger() {
        return warmeerzeuger;
    }

}
