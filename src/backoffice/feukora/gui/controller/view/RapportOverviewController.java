package feukora.gui.controller.view;

import feukora.entity.MessergebnisEntity;
import feukora.entity.RapportEntity;
import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.RapportEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.gui.util.Drucker;
import feukora.interfaces.ConcreteMessergebnisAction;
import feukora.interfaces.ConcreteRapportAction;
import feukora.interfaces.ConcreteTerminAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the rapport overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class RapportOverviewController {

    /**
     * The rapport table.
     */
    @FXML
    private static TableView<RapportEntity> rapportTable;
    /**
     * The rapport data.
     */
    private static ObservableList<RapportEntity> rapportData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(RapportOverviewController.class);
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The standort row.
     */
    @FXML
    private TableColumn<RapportEntity, String> standortRow;
    /**
     * The standort row.
     */
    @FXML
    private TableColumn<RapportEntity, String> terminRow;
    /**
     * The standort row.
     */
    @FXML
    private TableColumn<RapportEntity, String> kontrolleurRow;
    /**
     * The warmeerzeuger row.
     */
    @FXML
    private TableColumn<RapportEntity, String> warmeerzeugerRow;
    /**
     * The brenner row.
     */
    @FXML
    private TableColumn<RapportEntity, String> brennerRow;
    /**
     * The beurteilung row.
     */
    @FXML
    private TableColumn<RapportEntity, String> beurteilungRow;
    /**
     * The vorgehen row.
     */
    @FXML
    private TableColumn<RapportEntity, String> vorgehenRow;
    /**
     * The bearbeiten button.
     */
    @FXML
    private Button bearbeitenButton;
    /**
     * The loeschen button.
     */
    @FXML
    private Button loeschenButton;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<RapportEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public RapportOverviewController() {

    }

    /**
     * Gets the rapport data.
     *
     * @return the rapport data
     */
    public static ObservableList<RapportEntity> getRapportData() {
        return rapportData;
    }

    /**
     * Gets the rapport table.
     *
     * @return the rapport table
     */
    public static TableView<RapportEntity> getRapportTable() {
        return rapportTable;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            rapportData.removeAll(rapportData);
            ConcreteRapportAction rapportAction = (ConcreteRapportAction) ActionRegistry.getInterface("rapport");
            rapportData.addAll(rapportAction.getAll());
        } catch (RemoteException e) {
            logger.warn("Rapportdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Rapportdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }

        standortRow.setCellValueFactory(new Callback<CellDataFeatures<RapportEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<RapportEntity, String> rapport) {
                if (rapport.getValue().getTermin() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(rapport.getValue().getTermin().getStandort().getAdresse() + ", " + rapport.getValue().getTermin().getStandort().getOrtschaft().getPlz() + " " + rapport.getValue().getTermin().getStandort().getOrtschaft().getGemeinde());
                }
            }
        });
        terminRow.setCellValueFactory(new Callback<CellDataFeatures<RapportEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<RapportEntity, String> rapport) {
                if (rapport.getValue().getTermin() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(rapport.getValue().getTermin().getDateString() + " " + rapport.getValue().getTermin().getTimeString());
                }
            }
        });
        kontrolleurRow.setCellValueFactory(new Callback<CellDataFeatures<RapportEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<RapportEntity, String> rapport) {
                if (rapport.getValue().getTermin().getKontrolleur() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(rapport.getValue().getTermin().getKontrolleur().getVorname() + " " + rapport.getValue().getTermin().getKontrolleur().getName() + ", Tel.: " + rapport.getValue().getTermin().getKontrolleur().getTelefon());
                }
            }
        });

        warmeerzeugerRow.setCellValueFactory(new Callback<CellDataFeatures<RapportEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<RapportEntity, String> rapport) {
                if (rapport.getValue().getWarmeerzeuger() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(rapport.getValue().getWarmeerzeuger().getFabrikat());
                }
            }
        });

        brennerRow.setCellValueFactory(new Callback<CellDataFeatures<RapportEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"rawtypes", "unchecked"})
            public ObservableValue<String> call(CellDataFeatures<RapportEntity, String> rapport) {
                if (rapport.getValue().getBrenner() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(rapport.getValue().getBrenner().getFabrikat());
                }
            }
        });

        beurteilungRow.setCellValueFactory(new PropertyValueFactory<RapportEntity, String>("beurteilung"));
        vorgehenRow.setCellValueFactory(new Callback<CellDataFeatures<RapportEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"rawtypes", "unchecked"})
            public ObservableValue<String> call(CellDataFeatures<RapportEntity, String> rapport) {
                if (rapport.getValue().getRegulierung() == RapportEntity.STATUS.OK) {
                    return new ReadOnlyObjectWrapper("Kein weiteres Vorgehen");
                } else if (rapport.getValue().getRegulierung() == RapportEntity.STATUS.REG) {
                    return new ReadOnlyObjectWrapper("Einregulierung nötig");
                } else {
                    return new ReadOnlyObjectWrapper("Einregulierung nicht möglich");
                }
            }
        });

        rapportTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        searchField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                updateFilteredData();
            }
        });
    }

    /**
     * Shows all rapports from the database in a table overview.
     */
    public void showRapportOverview() {
        try {
            // Load the fxml-file and set it to center of the root layout
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/RapportOverview.fxml"));

            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);

            // Give the controller access to the main app
            RapportOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the RapportOverview.fxml file", e);
        }
    }

    /**
     * Set the rapports into the table.
     *
     * @param mainApp the main application
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        try {
            if (ActionRegistry.isRmiPossible() == false) {
                bearbeitenButton.setDisable(true);
                loeschenButton.setDisable(true);
            }
        } catch (HeadlessException | IOException e) {
            e.printStackTrace();
        }
        filteredData.addAll(rapportData);
        rapportTable.setItems(filteredData);

        rapportData.addListener(new ListChangeListener<RapportEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends RapportEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (RapportEntity rapport : rapportData) {
            if (matchesFilter(rapport)) {
                filteredData.add(rapport);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<RapportEntity, ?>> sortOrder = new ArrayList<>(rapportTable.getSortOrder());
        rapportTable.getSortOrder().clear();
        rapportTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param rapport the rapport
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(RapportEntity rapport) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (rapport.getTermin().getStandort().getOrtschaft().getGemeinde().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (rapport.getTermin().getDatetimeString().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (rapport.getWarmeerzeuger().getFabrikat().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (rapport.getBrenner().getFabrikat().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (rapport.getBeurteilung().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (rapport.getBemerkung().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }

        return false;
    }

    /**
     * Opens a new dialog to chose the termin for the rapport.
     */
    @FXML
    public void handleNewRapport() {
        TerminOverviewController controller = new TerminOverviewController();
        controller.setChooseDialog(true);
        controller.showTerminChooseDialog();
    }

    /**
     * Opens a new dialog to create a new termin.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    public void handleChosenTermin() {
        RapportEntity tempRapport = new RapportEntity();
        tempRapport.setTermin(TerminOverviewController.getTermin());
        RapportEditDialogController controller = new RapportEditDialogController();
        controller.setEditDialog(false);
        controller.showRapportEditDialog(tempRapport);
    }

    /**
     * Deletes the selected rapport.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleDeleteRapport() {
        int selectedIndex = rapportTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Bitte bestätigen Sie mit YES", "Wollen Sie diesen Rapport löschen?", "Rapport löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = rapportTable.getSelectionModel().getSelectedItem().getId();
                    RapportEntity rapport = rapportTable.getSelectionModel().getSelectedItem();

                    ConcreteRapportAction rapportAction = (ConcreteRapportAction) ActionRegistry.getInterface("rapport");

                    List<MessergebnisEntity> list = rapport.getMessergebnisse();
                    Iterator<MessergebnisEntity> itr = list.iterator();

                    ConcreteTerminAction terminAction = (ConcreteTerminAction) ActionRegistry.getInterface("termin");

                    TerminEntity termin = rapport.getTermin();
                    rapport.setTermin(null);
                    rapportAction.update(rapport);

                    terminAction.delete(termin.getId());

                    rapportAction.delete(id);

                    while (itr.hasNext()) {
                        MessergebnisEntity ergebnis = itr.next();
                        int idM = ergebnis.getId();

                        ConcreteMessergebnisAction messergebnisAction = (ConcreteMessergebnisAction) ActionRegistry.getInterface("messergebnis");
                        messergebnisAction.delete(idM);
                    }

                    rapportTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Rapport konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Rapport konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Rapport auswählen", "Rapport löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected rapport.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleEditRapport() {
        RapportEntity selectedRapport = rapportTable.getSelectionModel().getSelectedItem();
        if (selectedRapport != null) {
            RapportEditDialogController controller = new RapportEditDialogController();
            controller.setEditDialog(true);
            boolean okClickedRapport = controller.showRapportEditDialog(selectedRapport);
            if (okClickedRapport) {
                refreshRapportTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Rapport auswählen", "Rapport bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Opens a dialog to choose the place to save the print file.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handlePrint() {
        RapportEntity selectedRapport = rapportTable.getSelectionModel().getSelectedItem();
        if (selectedRapport != null) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Wählen Sie einen Speicherort aus");
            File file = fileChooser.showSaveDialog(MainApp.getPrimaryStage());
            if (file != null) {
                String pfad = file.getAbsolutePath();
                Drucker drucker = new Drucker();
                try {
                    drucker.druckenString(selectedRapport.toString(), pfad);
                    Dialogs.showInformationDialog(MainApp.getPrimaryStage(), " ", "Der Rapport wurde gedruckt", "Drucken erfolgreich");
                } catch (IOException e) {
                    logger.warn("Unable to print the file", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Der Rapport konnte nicht gedruckt werden", "Fehler beim Drucken", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Rapport auswählen", "Rapport drucken", DialogOptions.OK);
        }
    }

    /**
     * Shows the messergebnisse for the selected rapport.
     */
    @FXML
    public void listMessergebnisse() {
        RapportEntity selectedRapport = rapportTable.getSelectionModel().getSelectedItem();
        if (selectedRapport != null) {
            MessergebnisOverviewController controller = new MessergebnisOverviewController();
            controller.showMessResultOverview();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Rapport auswählen", "Messergebnisse anzeigen", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshRapportTable() {
        int selectedIndex = rapportTable.getSelectionModel().getSelectedIndex();
        rapportTable.setItems(null);
        rapportTable.layout();
        rapportTable.setItems(filteredData);
        rapportTable.getSelectionModel().select(selectedIndex);
    }
}
