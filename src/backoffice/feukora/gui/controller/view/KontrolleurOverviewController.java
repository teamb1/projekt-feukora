package feukora.gui.controller.view;

import feukora.entity.KontrolleurEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.KontrolleurEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteKontrolleurAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the kontrolleur overview.
 *
 * @author Florian Rieder
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class KontrolleurOverviewController {

    /**
     * The kontrolleur.
     */
    private static KontrolleurEntity kontrolleur;
    /**
     * The ok clicked.
     */
    private static boolean okClicked = false;
    /**
     * The kontrolleur data.
     */
    private static ObservableList<KontrolleurEntity> kontrolleurData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(KontrolleurOverviewController.class);
    /**
     * The kontrolleur table.
     */
    @FXML
    private TableView<KontrolleurEntity> kontrolleurTable;
    /**
     * The vorname row.
     */
    @FXML
    private TableColumn<KontrolleurEntity, String> vornameRow;
    /**
     * The name row.
     */
    @FXML
    private TableColumn<KontrolleurEntity, String> nameRow;
    /**
     * The telefon row.
     */
    @FXML
    private TableColumn<KontrolleurEntity, String> telefonRow;
    /**
     * The strasse row.
     */
    @FXML
    private TableColumn<KontrolleurEntity, String> strasseRow;
    /**
     * The ortschaft row.
     */
    @FXML
    private TableColumn<KontrolleurEntity, String> ortschaftRow;
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<KontrolleurEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public KontrolleurOverviewController() {

    }

    /**
     * Gets the kontrolleur data.
     *
     * @return the kontrolleur data
     */
    public static ObservableList<KontrolleurEntity> getkontrolleurData() {
        return kontrolleurData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     *
     * @author Florian Rieder, Patrick Schmed
     */
    @FXML
    public void initialize() {
        try {
            kontrolleurData.removeAll(kontrolleurData);
            ConcreteKontrolleurAction kontrolleurAction = (ConcreteKontrolleurAction) ActionRegistry.getInterface("kontrolleur");
            kontrolleurData.addAll(kontrolleurAction.getAll());
            nameRow.setCellValueFactory(new PropertyValueFactory<KontrolleurEntity, String>("name"));
            vornameRow.setCellValueFactory(new PropertyValueFactory<KontrolleurEntity, String>("vorname"));
            telefonRow.setCellValueFactory(new PropertyValueFactory<KontrolleurEntity, String>("telefon"));
            strasseRow.setCellValueFactory(new PropertyValueFactory<KontrolleurEntity, String>("strasse"));
            ortschaftRow.setCellValueFactory(new Callback<CellDataFeatures<KontrolleurEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<KontrolleurEntity, String> kontrolleur) {
                    if (kontrolleur.getValue().getOrtschaft() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(kontrolleur.getValue().getOrtschaft().getPlz() + " " + kontrolleur.getValue().getOrtschaft().getGemeinde() + ", " + kontrolleur.getValue().getOrtschaft().getKanton());
                    }
                }
            });

            kontrolleurTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Kontrolleurdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Kontrolleurdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all kontrolleure from the database in a table overview.
     */
    public void showKontrolleurOverview() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/KontrolleurOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);

            // Give the controller access to the main app
            KontrolleurOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the KontrolleurOverview.fxml file", e);
        }
    }

    /**
     * Shows all kontrolleure from the database in a table overview on a new
     * stage.
     *
     * @return if the user clicked ok
     */
    public boolean showKontrolleurChooseDialog() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/KontrolleurChooseDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Kontrolleur Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            KontrolleurOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the BrennerOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    private boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the kontrolleure into the table.
     *
     * @param dialogStage the new dialog stage
     * @author Florian Rieder, Patrick Schmed
     */
    private void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        filteredData.addAll(kontrolleurData);
        kontrolleurTable.setItems(filteredData);
        kontrolleurData.addListener(new ListChangeListener<KontrolleurEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends KontrolleurEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Set the selected kontrolleur and give it to the standortEditDialog.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleChoose() {
        int selectedIndex = kontrolleurTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            kontrolleur = kontrolleurTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Kontrolleur auswählen", "Kontrolleur wählen", DialogOptions.OK);
        }
    }

    /**
     * Set the kontrolleure into the table.
     *
     * @param mainApp the main application
     * @author Florian Rieder, Patrick Schmed
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        filteredData.addAll(kontrolleurData);
        kontrolleurTable.setItems(filteredData);
        kontrolleurData.addListener(new ListChangeListener<KontrolleurEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends KontrolleurEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (KontrolleurEntity kontrolleur : kontrolleurData) {
            if (matchesFilter(kontrolleur)) {
                filteredData.add(kontrolleur);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<KontrolleurEntity, ?>> sortOrder = new ArrayList<>(kontrolleurTable.getSortOrder());
        kontrolleurTable.getSortOrder().clear();
        kontrolleurTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param kontrolleur the kontrolleur
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(KontrolleurEntity kontrolleur) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (kontrolleur.getName().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (kontrolleur.getVorname().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Opens a new dialog to create a new kontrolleur.
     */
    @FXML
    private void handleNewKontrolleur() {
        KontrolleurEntity tempKontrolleur = new KontrolleurEntity();
        KontrolleurEditDialogController controller = new KontrolleurEditDialogController();
        controller.setEditDialog(false);
        controller.showKontrolleurEditDialog(tempKontrolleur);
    }

    /**
     * Deletes the selected kontrolleur.
     *
     * @author Florian Rieder, Patrick Schmed
     */
    @FXML
    private void handleDeleteKontrolleur() {
        int selectedIndex = kontrolleurTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Wollen Sie diesen Kontrolleur löschen?", "Bitte bestätigen Sie mit YES", "Kontrolleur löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = kontrolleurTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteKontrolleurAction kontrolleurAction = (ConcreteKontrolleurAction) ActionRegistry.getInterface("kontrolleur");
                    kontrolleurAction.delete(id);
                    kontrolleurTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Kontrolleur konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Kontrolleur konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Kontrolleur auswählen", "Kontrolleur löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected kontrolleur.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleEditKontrolleur() {
        KontrolleurEntity selectedKontrolleur = kontrolleurTable.getSelectionModel().getSelectedItem();
        if (selectedKontrolleur != null) {
            KontrolleurEditDialogController controller = new KontrolleurEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showKontrolleurEditDialog(selectedKontrolleur);
            if (okClicked) {
                refreshKontrolleurTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Kontrolleur auswählen", "Kontrolleur bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshKontrolleurTable() {
        int selectedIndex = kontrolleurTable.getSelectionModel().getSelectedIndex();
        kontrolleurTable.setItems(null);
        kontrolleurTable.layout();
        kontrolleurTable.setItems(filteredData);
        kontrolleurTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the kontrolleur.
     *
     * @return the kontrolleur
     */
    public KontrolleurEntity getKontrolleur() {
        return kontrolleur;
    }

}
