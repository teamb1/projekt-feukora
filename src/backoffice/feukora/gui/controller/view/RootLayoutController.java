package feukora.gui.controller.view;

import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteLoginAction;
import javafx.fxml.FXML;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;

/**
 * The controller for the root layout. The root layout provides the basic
 * application layout containing a menu bar.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class RootLayoutController {

    /**
     * The label.
     */
    @FXML
    private static Label label;

    /**
     * The verwaltung menu.
     */
    @FXML
    private static Menu verwaltungMenu;

    /**
     * The mitarbeiter menu.
     */
    @FXML
    private static Menu mitarbeiterMenu;

    /**
     * The maschinen menu.
     */
    @FXML
    private static Menu maschinenMenu;
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(RootLayoutController.class);
    /**
     * The main app.
     */
    private MainApp mainApp;

    /**
     * Gets the label.
     *
     * @return the label
     */
    public static Label getLabel() {
        return label;
    }

    /**
     * Called when the user chose the raport menu item.
     */
    @FXML
    public void listRapport() {
        label.setText("Rapport Übersicht");
        RapportOverviewController controller = new RapportOverviewController();
        controller.showRapportOverview();
    }

    /**
     * Called when the user chose the date menu item.
     */
    @FXML
    private void listTermin() {
        label.setText("Termin Übersicht");
        TerminOverviewController controller = new TerminOverviewController();
        controller.showTerminOverview();
    }

    /**
     * Called when the user chose the burner menu item.
     */
    @FXML
    private void listBrenner() {
        label.setText("Brenner Übersicht");
        BrennerOverviewController controller = new BrennerOverviewController();
        controller.showBrennerOverview();
    }

    /**
     * Called when the user chose the heater menu item.
     */
    @FXML
    private void listWarmeerzeuger() {
        label.setText("Wärmeerzeuger Übersicht");
        WarmeerzeugerOverviewController controller = new WarmeerzeugerOverviewController();
        controller.showWarmeerzeugerOverview();
    }

    /**
     * Called when the user chose the controller menu item.
     */
    @FXML
    private void listKontrolleur() {
        label.setText("Kontrolleur Übersicht");
        KontrolleurOverviewController controller = new KontrolleurOverviewController();
        controller.showKontrolleurOverview();
    }

    /**
     * Called when the user chose the administrator menu item.
     */
    @FXML
    private void listSachbearbeiter() {
        label.setText("Sachbearbeiter Übersicht");
        BenutzerOverviewController controller = new BenutzerOverviewController();
        controller.showBenutzerOverview();
    }

    /**
     * Called when the user chose the address menu item.
     */
    @FXML
    private void listStandort() {
        label.setText("Standort Übersicht");
        StandortOverviewController controller = new StandortOverviewController();
        controller.showStandortOverview();
    }

    /**
     * Called when the user chose the owner menu item.
     */
    @FXML
    private void listStammdaten() {
        label.setText("Stammdaten Übersicht");
        StammdatenOverviewController controller = new StammdatenOverviewController();
        controller.showStammdatenOverview();
    }

    /**
     * Sets the main app.
     *
     * @param mainApp the new main app
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        verwaltungMenu.setVisible(false);
        mitarbeiterMenu.setVisible(false);
        maschinenMenu.setVisible(false);
    }

    /**
     * Handle logout.
     */
    @FXML
    private void handleLogout() {
        try {
            ConcreteLoginAction loginAction = (ConcreteLoginAction) ActionRegistry.getInterface("login");
            loginAction.logout();
            MainApp.getPrimaryStage().close();
        } catch (RemoteException e) {
            logger.warn("Der User konnte nicht abgemeldet werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", e.getCause().getMessage(), "Logout nicht möglich", DialogOptions.OK);
        } catch (InvalidDataException e) {
            //
        }
    }
}
