package feukora.gui.controller.view;

import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.TerminEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.gui.util.Drucker;
import feukora.interfaces.ConcreteTerminAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Handles the actions for the termin overview.
 *
 * @author Tscherrig Sven
 * @version 20.05.2014
 * @since 23.03.2014
 */
public class TerminOverviewController {

    /**
     * The choose dialog.
     */
    private static boolean chooseDialog;
    /**
     * The termin data.
     */
    private static ObservableList<TerminEntity> terminData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(TerminOverviewController.class);
    /**
     * The termin.
     */
    private static TerminEntity termin;
    /**
     * The ok clicked.
     */
    private static boolean okClicked = false;
    /**
     * The termin table.
     */
    @FXML
    private TableView<TerminEntity> terminTable;
    /**
     * The datum row.
     */
    @FXML
    private TableColumn<TerminEntity, String> datumRow;
    /**
     * The zeit row.
     */
    @FXML
    private TableColumn<TerminEntity, String> zeitRow;
    /**
     * The standort row.
     */
    @FXML
    private TableColumn<TerminEntity, String> standortRow;
    /**
     * The kontrolleur row.
     */
    @FXML
    private TableColumn<TerminEntity, String> kontrolleurRow;
    /**
     * The neu button.
     */
    @FXML
    private Button neuButton;
    /**
     * The bearbeiten button.
     */
    @FXML
    private Button bearbeitenButton;
    /**
     * The loeschen button.
     */
    @FXML
    private Button loeschenButton;
    /**
     * The weekly button.
     */
    @FXML
    private Button weeklyButton;
    /**
     * The filtered data.
     */
    private ObservableList<TerminEntity> filteredData = FXCollections.observableArrayList();
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * Standard constructor which is called before the initialize method.
     */
    public TerminOverviewController() {

    }

    /**
     * Gets the termin data.
     *
     * @return the termin data
     */
    public static ObservableList<TerminEntity> getTerminData() {
        return terminData;
    }

    /**
     * Gets the termin.
     *
     * @return the termin
     */
    public static TerminEntity getTermin() {
        return termin;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void initialize() {
        try {
            terminData.removeAll(terminData);
            ConcreteTerminAction terminAction = (ConcreteTerminAction) ActionRegistry.getInterface("termin");
            terminData.addAll(terminAction.getAll());
            datumRow.setCellValueFactory(new Callback<CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
                public ObservableValue<String> call(CellDataFeatures<TerminEntity, String> termin) {
                    if (termin.getValue().getDatum() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(termin.getValue().getDateString());
                    }
                }
            });
            zeitRow.setCellValueFactory(new Callback<CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
                public ObservableValue<String> call(CellDataFeatures<TerminEntity, String> termin) {
                    if (termin.getValue().getDatum() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(termin.getValue().getTimeString() + " Uhr");
                    }
                }
            });
            kontrolleurRow.setCellValueFactory(new Callback<CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
                public ObservableValue<String> call(CellDataFeatures<TerminEntity, String> termin) {
                    if (termin.getValue().getKontrolleur() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(termin.getValue().getKontrolleur().getVorname() + " " + termin.getValue().getKontrolleur().getName() + ", Tel.: " + termin.getValue().getKontrolleur().getTelefon());
                    }
                }
            });
            standortRow.setCellValueFactory(new Callback<CellDataFeatures<TerminEntity, String>, ObservableValue<String>>() {
                public ObservableValue<String> call(CellDataFeatures<TerminEntity, String> termin) {
                    if (termin.getValue().getStandort() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(termin.getValue().getStandort().getAdresse() + ", " + termin.getValue().getStandort().getOrtschaft().getPlz() + " " + termin.getValue().getStandort().getOrtschaft().getGemeinde());
                    }
                }
            });
            terminTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            terminTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Termindaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Termindaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all termin from the database in a table overview.
     */
    public void showTerminOverview() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/TerminOverview.fxml"));
            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);

            // Give the controller access to the main app
            TerminOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the TerminOverview.fxml file", e);
        }
    }

    /**
     * Shows all termine from the database in a table overview on a new stage.
     *
     * @return if the user clicked ok
     */
    public boolean showTerminChooseDialog() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/TerminChooseDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Termin Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            TerminOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the TerminChooseDialog.fxml file", e);
            return false;
        }
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    private boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the termin into the table.
     *
     * @param dialogStage the new dialog stage
     * @author Sven Tscherrig, Patrick Schmed
     */
    private void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        try {
            if (ActionRegistry.isRmiPossible() == false) {
                neuButton.setVisible(false);
                bearbeitenButton.setVisible(false);
                loeschenButton.setVisible(false);
            }
        } catch (HeadlessException | IOException e) {
            e.printStackTrace();
        }
        if (chooseDialog) {
            ListIterator<TerminEntity> itr = terminData.listIterator();
            while (itr.hasNext()) {
                TerminEntity termin = itr.next();
                if (termin.getRapport() == null) {
                    filteredData.add(termin);
                }
            }
        } else {
            filteredData.addAll(terminData);
        }
        terminTable.setItems(filteredData);
        terminData.addListener(new ListChangeListener<TerminEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends TerminEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Set the selected termin and open the rapportEditDialog.
     *
     * @author Sven Tscherrig, Sven Neitzel, Patrick Schmed
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = terminTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            termin = terminTable.getSelectionModel().getSelectedItem();
            chooseDialog = false;
            dialogStage.close();
            RapportOverviewController controller = new RapportOverviewController();
            controller.handleChosenTermin();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Termin auswählen", "Termin wählen", DialogOptions.OK);
        }
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (TerminEntity termin : terminData) {
            if (matchesFilter(termin)) {
                filteredData.add(termin);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<TerminEntity, ?>> sortOrder = new ArrayList<>(terminTable.getSortOrder());
        terminTable.getSortOrder().clear();
        terminTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param termin the brenner object
     * @return true if filter matches an object
     * @author Sven Neitzel
     */
    private boolean matchesFilter(TerminEntity termin) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (termin.getDateString().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (termin.getKontrolleur().getName().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }

        return false;
    }

    /**
     * Set the termin into the table.
     *
     * @param mainApp the main application
     * @author Sven Tscherrig, Patrick Schmed
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        try {
            if (ActionRegistry.isRmiPossible() == false) {
                neuButton.setDisable(true);
                bearbeitenButton.setDisable(true);
                loeschenButton.setDisable(true);
                weeklyButton.setDisable(true);
            }
        } catch (HeadlessException | IOException e) {
            e.printStackTrace();
        }
        filteredData.addAll(terminData);
        terminTable.setItems(filteredData);
        terminData.addListener(new ListChangeListener<TerminEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends TerminEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Opens a new dialog to create a new termin.
     *
     * @author Sven Tscherrig, Sven Neitzel
     */
    @FXML
    private void handleNewTermin() {
        TerminEntity tempTermin = new TerminEntity();
        TerminEditDialogController controller = new TerminEditDialogController();
        controller.showTerminEditDialog(tempTermin);
    }

    /**
     * Deletes the selected termin.
     *
     * @author Sven Tscherrig, Patrick Schmed
     */
    @FXML
    private void handleDeleteTermin() {
        int selectedIndex = terminTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Wollen Sie diesen Termin löschen?", "Bitte bestätigen Sie mit YES", "Termin löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = terminTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteTerminAction terminAction = (ConcreteTerminAction) ActionRegistry.getInterface("termin");
                    terminAction.delete(id);
                    terminTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Termin konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Der Termin konnte nicht aus der Datenbank gelöscht werden", "Fehler beim LÃ¶schen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Termin auswählen", "Termin löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected termin.
     *
     * @author Sven Tscherrig, Sven Neitzel
     */
    @FXML
    private void handleEditTermin() {
        TerminEntity selectedTermin = terminTable.getSelectionModel().getSelectedItem();
        if (selectedTermin != null) {
            TerminEditDialogController controller = new TerminEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showTerminEditDialog(selectedTermin);
            if (okClicked) {
                refreshTerminTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Termin auswählen", "Termin bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Opens a dialog to choose the place to save the print file.
     *
     * @author Sven Tscherrig, Sven Neitzel, Patrick Schmed
     */
    @FXML
    public void handlePrint() {
        ObservableList<TerminEntity> selectedTermin = terminTable.getSelectionModel().getSelectedItems();
        if (selectedTermin != null) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Wählen Sie einen Speicherort aus");
            File file = fileChooser.showSaveDialog(MainApp.getPrimaryStage());
            if (file != null) {
                String pfad = file.getAbsolutePath();
                Drucker drucker = new Drucker();
                try {
                    ArrayList<TerminEntity> termine = new ArrayList<TerminEntity>();
                    Iterator<TerminEntity> itr = selectedTermin.iterator();
                    while (itr.hasNext()) {
                        TerminEntity termin = itr.next();
                        termine.add(termin);
                    }
                    drucker.druckenArray(termine, pfad);
                    Dialogs.showInformationDialog(MainApp.getPrimaryStage(), " ", "Die Termine wurden gedruckt", "Drucken erfolgreich");
                } catch (IOException e) {
                    logger.warn("Unable to print the file", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Termine konnten nicht gedruckt werden", "Fehler beim Drucken", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Termin auswählen", "Termin drucken", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshTerminTable() {
        int selectedIndex = terminTable.getSelectionModel().getSelectedIndex();
        terminTable.setItems(null);
        terminTable.layout();
        terminTable.setItems(filteredData);
        terminTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Handle weekly overview.
     */
    @FXML
    public void handleWeeklyOverview() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/TerminWeeklyOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            TerminWeeklyController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

        } catch (IOException e) {
            logger.warn("Unable to load the TerminWeeklyOverview.fxml file", e);
        }
    }

    /**
     * Handle next termin.
     */
    @FXML
    public void handleNextTermin() {
        TerminEntity selectedTermin = terminTable.getSelectionModel().getSelectedItem();
        if (selectedTermin != null) {
            TerminEditDialogController controller = new TerminEditDialogController();
            controller.setFolgeTermin(true);
            controller.showTerminEditDialog(selectedTermin);
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Termin auswählen", "Termin bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Sets the choose dialog.
     *
     * @param chooseDialog the new choose dialog
     */
    public void setChooseDialog(boolean chooseDialog) {
        TerminOverviewController.chooseDialog = chooseDialog;
    }
}