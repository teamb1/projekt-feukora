package feukora.gui.controller.view;

import feukora.entity.HauswartEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.HauswartEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteHauswartAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the hauswart overview.
 *
 * @author Patrick Schmed
 * @version 20.05.05.2014
 * @since 01.05.2014
 */
public class HauswartOverviewController {

    /**
     * The hauswart.
     */
    private static HauswartEntity hauswart;
    /**
     * The choose dialog.
     */
    private static boolean chooseDialog;
    /**
     * The hauswart data.
     */
    private static ObservableList<HauswartEntity> hauswartData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(HauswartOverviewController.class);
    /**
     * The hauswart table.
     */
    @FXML
    private TableView<HauswartEntity> hauswartTable;
    /**
     * The vorname row.
     */
    @FXML
    private TableColumn<HauswartEntity, String> vornameRow;
    /**
     * The name row.
     */
    @FXML
    private TableColumn<HauswartEntity, String> nameRow;
    /**
     * The telefon row.
     */
    @FXML
    private TableColumn<HauswartEntity, String> telefonRow;
    /**
     * The strasse row.
     */
    @FXML
    private TableColumn<HauswartEntity, String> strasseRow;
    /**
     * The ortschaft row.
     */
    @FXML
    private TableColumn<HauswartEntity, String> ortschaftRow;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The close button.
     */
    @FXML
    private Button closeButton;
    /**
     * The choose button.
     */
    @FXML
    private Button chooseButton;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<HauswartEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public HauswartOverviewController() {

    }

    /**
     * Gets the hauswart data.
     *
     * @return the hauswart data
     */
    public static ObservableList<HauswartEntity> getHauswartData() {
        return hauswartData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            hauswartData.removeAll(hauswartData);
            ConcreteHauswartAction hauswartAction = (ConcreteHauswartAction) ActionRegistry.getInterface("hauswart");
            hauswartData.addAll(hauswartAction.getAll());
            vornameRow.setCellValueFactory(new PropertyValueFactory<HauswartEntity, String>("vorname"));
            nameRow.setCellValueFactory(new PropertyValueFactory<HauswartEntity, String>("name"));
            telefonRow.setCellValueFactory(new PropertyValueFactory<HauswartEntity, String>("telefon"));
            strasseRow.setCellValueFactory(new PropertyValueFactory<HauswartEntity, String>("strasse"));
            ortschaftRow.setCellValueFactory(new Callback<CellDataFeatures<HauswartEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<HauswartEntity, String> hauswart) {
                    if (hauswart.getValue().getOrtschaft() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(hauswart.getValue().getOrtschaft().getPlz() + " " + hauswart.getValue().getOrtschaft().getGemeinde() + ", " + hauswart.getValue().getOrtschaft().getKanton());
                    }
                }
            });
            hauswartTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Hauswartdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Hauswartdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all hauswarte from the database in a table overview.
     *
     * @return if the user clicked schliessen or auswählen
     */
    public boolean showHauswartOverview() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/HauswartOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Hauswart Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            HauswartOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the HauswartOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the hauswart into the table and deactivates the close or choose
     * button.
     *
     * @param dialogStage the dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        if (chooseDialog == true) {
            closeButton.setVisible(false);
        } else {
            chooseButton.setVisible(false);
        }
        filteredData.addAll(hauswartData);
        hauswartTable.setItems(filteredData);
        hauswartData.addListener(new ListChangeListener<HauswartEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends HauswartEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (HauswartEntity hauswart : hauswartData) {
            if (matchesFilter(hauswart)) {
                filteredData.add(hauswart);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<HauswartEntity, ?>> sortOrder = new ArrayList<>(hauswartTable.getSortOrder());
        hauswartTable.getSortOrder().clear();
        hauswartTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param hauswart the hauswart
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(HauswartEntity hauswart) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (hauswart.getName().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (hauswart.getVorname().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Opens a new dialog to create a new hauswart.
     */
    @FXML
    private void handleNewHauswart() {
        HauswartEntity tempHauswart = new HauswartEntity();
        HauswartEditDialogController controller = new HauswartEditDialogController();
        controller.setEditDialog(false);
        controller.showHauswartEditDialog(tempHauswart);
    }

    /**
     * Deletes the selected hauswart.
     */
    @FXML
    private void handleDeleteHauswart() {
        int selectedIndex = hauswartTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(dialogStage, "Wollen Sie diesen Hauswart löschen?", "Bitte bestätigen Sie mit YES", "Hauswart löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                int id = hauswartTable.getSelectionModel().getSelectedItem().getId();
                try {
                    ConcreteHauswartAction hauswartAction = (ConcreteHauswartAction) ActionRegistry.getInterface("hauswart");
                    hauswartAction.delete(id);
                    hauswartTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Hauswart konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Der Hauswart konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Hauswart auswählen", "Hauswart löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected hauswart.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleEditHauswart() {
        HauswartEntity selectedHauswart = hauswartTable.getSelectionModel().getSelectedItem();
        if (selectedHauswart != null) {
            HauswartEditDialogController controller = new HauswartEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showHauswartEditDialog(selectedHauswart);
            if (okClicked) {
                refreshHauswartTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Hauswart auswählen", "Hauswart bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Set the selected hauswart and give it to the standortEditDialog.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = hauswartTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            hauswart = hauswartTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Hauswart auswählen", "Hauswart wählen", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialogStage if the user clicked schliessen.
     */
    @FXML
    public void handleOk() {
        dialogStage.close();
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshHauswartTable() {
        int selectedIndex = hauswartTable.getSelectionModel().getSelectedIndex();
        hauswartTable.setItems(null);
        hauswartTable.layout();
        hauswartTable.setItems(filteredData);
        hauswartTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the hauswart.
     *
     * @return the hauswart
     */
    public HauswartEntity getHauswart() {
        return hauswart;
    }

    /**
     * Sets the choose dialog.
     *
     * @param chooseDialog the new choose dialog
     */
    public void setChooseDialog(boolean chooseDialog) {
        HauswartOverviewController.chooseDialog = chooseDialog;
    }

}
