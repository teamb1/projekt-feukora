package feukora.gui.controller.view;

import feukora.entity.MessergebnisEntity;
import feukora.entity.RapportEntity;
import feukora.gui.MainApp;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Handles the actions for the messergebnis overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 01.04.2014
 */
public class MessergebnisOverviewController {

    /**
     * The mess data.
     */
    private static ObservableList<MessergebnisEntity> messData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(MessergebnisOverviewController.class);
    /**
     * The mess table.
     */
    @FXML
    private TableView<MessergebnisEntity> messTable;
    /**
     * The stufe row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> stufeRow;
    /**
     * The mess row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> messRow;
    /**
     * The ru row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> ruRow;
    /**
     * The co row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> coRow;
    /**
     * The ol row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> olRow;
    /**
     * The no row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> noRow;
    /**
     * The abt row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> abtRow;
    /**
     * The wa row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> waRow;
    /**
     * The ve row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> veRow;
    /**
     * The o2 row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> o2Row;
    /**
     * The abv row.
     */
    @FXML
    private TableColumn<MessergebnisEntity, String> abvRow;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;

    /**
     * Standard Constructor which is called before the initialize method.
     */
    public MessergebnisOverviewController() {

    }

    /**
     * Gets the mess data.
     *
     * @return the mess data
     */
    public static ObservableList<MessergebnisEntity> getMessData() {
        return messData;
    }

    /**
     * Gets the mess table.
     *
     * @return the mess table
     */
    public TableView<MessergebnisEntity> getMessTable() {
        return messTable;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        stufeRow.setCellValueFactory(new Callback<CellDataFeatures<MessergebnisEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<MessergebnisEntity, String> messergebnis) {
                if (messergebnis.getValue().getMessvorgang() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(messergebnis.getValue().getMessvorgang().getStufe());
                }
            }
        });
        messRow.setCellValueFactory(new Callback<CellDataFeatures<MessergebnisEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<MessergebnisEntity, String> messergebnis) {
                if (messergebnis.getValue().getMessvorgang() == null) {
                    return null;
                } else {
                    return new ReadOnlyObjectWrapper(messergebnis.getValue().getMessvorgang().getMessung());
                }
            }
        });
        ruRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("russzahl"));
        coRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("co"));
        olRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("oelanteil"));

        olRow.setCellValueFactory(new Callback<CellDataFeatures<MessergebnisEntity, String>, ObservableValue<String>>() {
            @SuppressWarnings({"unchecked", "rawtypes"})
            public ObservableValue<String> call(CellDataFeatures<MessergebnisEntity, String> messergebnis) {
                if (messergebnis.getValue().isOelanteil() == true) {
                    return new ReadOnlyObjectWrapper("ja");
                } else {
                    return new ReadOnlyObjectWrapper("nein");
                }
            }
        });
        noRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("no"));
        abtRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("abgastemp"));
        waRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("warmeerzeugertemp"));
        veRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("verbrennungstemp"));
        o2Row.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("o2gehalt"));
        abvRow.setCellValueFactory(new PropertyValueFactory<MessergebnisEntity, String>("abgasverlust"));

        messTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    /**
     * Shows the messergebnisse for the selected rapport on a new stage.
     */
    public void showMessResultOverview() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/MessergebnisOverview.fxml"));
            AnchorPane aPane = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Messergebnisse");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(aPane);
            dialogStage.setScene(scene);

            MessergebnisOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            dialogStage.show();
        } catch (IOException e) {
            logger.warn("Unable to load the MessergebnisOverview.fxml file", e);
        }
    }

    /**
     * Set the messergebniss for the selected rapport into the table view.
     *
     * @param dialogStage the new dialog stage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        // Get the selected rapport
        RapportEntity selectedRapport = RapportOverviewController.getRapportTable().getSelectionModel().getSelectedItem();
        List<MessergebnisEntity> list = selectedRapport.getMessergebnisse();
        Iterator<MessergebnisEntity> itr = list.iterator();
        while (itr.hasNext()) {
            MessergebnisEntity messergebnis = itr.next();
            messData.add(messergebnis);
        }
        messTable.setItems(messData);
    }

    /**
     * Closes the dialog if the user clicks schliessen.
     */
    @FXML
    public void handleClose() {
        messData.removeAll(messData);
        dialogStage.close();
    }
}
