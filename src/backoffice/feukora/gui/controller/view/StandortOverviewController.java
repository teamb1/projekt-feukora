package feukora.gui.controller.view;

import feukora.entity.StandortEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.StandortEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteStandortAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the standort overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 30.04.2014
 */
public class StandortOverviewController {

    /**
     * The standort data.
     */
    private static ObservableList<StandortEntity> standortData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(StandortOverviewController.class);
    /**
     * The standort.
     */
    private static StandortEntity standort;
    /**
     * The ok clicked.
     */
    private static boolean okClicked = false;
    /**
     * The standort table.
     */
    @FXML
    private TableView<StandortEntity> standortTable;
    /**
     * The adresse row.
     */
    @FXML
    private TableColumn<StandortEntity, String> adresseRow;
    /**
     * The ortschaft row.
     */
    @FXML
    private TableColumn<StandortEntity, String> ortschaftRow;
    /**
     * The eigentumer row.
     */
    @FXML
    private TableColumn<StandortEntity, String> eigentumerRow;
    /**
     * The verwaltung row.
     */
    @FXML
    private TableColumn<StandortEntity, String> verwaltungRow;
    /**
     * The hauswart row.
     */
    @FXML
    private TableColumn<StandortEntity, String> hauswartRow;
    /**
     * The main app.
     */
    private MainApp mainApp;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<StandortEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public StandortOverviewController() {
    }

    /**
     * Gets the standort data.
     *
     * @return the standort data
     */
    public static ObservableList<StandortEntity> getStandortData() {
        return standortData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            standortData.removeAll(standortData);
            ConcreteStandortAction standortAction = (ConcreteStandortAction) ActionRegistry.getInterface("standort");
            standortData.addAll(standortAction.getAll());
            adresseRow.setCellValueFactory(new PropertyValueFactory<StandortEntity, String>("adresse"));
            ortschaftRow.setCellValueFactory(new Callback<CellDataFeatures<StandortEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<StandortEntity, String> standort) {
                    if (standort.getValue().getOrtschaft() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(standort.getValue().getOrtschaft().getPlz() + " " + standort.getValue().getOrtschaft().getGemeinde() + ", " + standort.getValue().getOrtschaft().getKanton());
                    }
                }
            });
            eigentumerRow.setCellValueFactory(new Callback<CellDataFeatures<StandortEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<StandortEntity, String> standort) {
                    if (standort.getValue().getEigentumer() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(standort.getValue().getEigentumer().getVorname() + " " + standort.getValue().getEigentumer().getName());
                    }
                }
            });
            verwaltungRow.setCellValueFactory(new Callback<CellDataFeatures<StandortEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<StandortEntity, String> standort) {
                    if (standort.getValue().getVerwaltung() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(standort.getValue().getVerwaltung().getVorname() + " " + standort.getValue().getVerwaltung().getName());
                    }
                }
            });
            hauswartRow.setCellValueFactory(new Callback<CellDataFeatures<StandortEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<StandortEntity, String> standort) {
                    if (standort.getValue().getHauswart() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(standort.getValue().getHauswart().getVorname() + " " + standort.getValue().getHauswart().getName());
                    }
                }
            });

            standortTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Standortdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Standortdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }

    }

    /**
     * Shows all standorte from the database in a table overview.
     */
    public void showStandortOverview() {
        try {
            // Load the fxml-file and set it to center of the root layout
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/StandortOverview.fxml"));

            AnchorPane overviewPage = (AnchorPane) loader.load();
            MainApp.getRootLayout().setCenter(overviewPage);

            // Give the controller access to the main app
            StandortOverviewController controller = loader.getController();
            controller.setMainApp(mainApp);
        } catch (IOException e) {
            logger.warn("Unable to load the StandortOverview.fxml file", e);
        }
    }

    /**
     * Shows all standorte from the database in a table overview on a new stage.
     *
     * @return if the user clicked ok
     */
    public boolean showStandortChooseDialog() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/StandortChooseDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Standort Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            StandortOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the StandortOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Set the standorte into the table.
     *
     * @param mainApp the main application
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        filteredData.addAll(standortData);
        standortTable.setItems(filteredData);
        standortData.addListener(new ListChangeListener<StandortEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends StandortEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (StandortEntity standort : standortData) {
            if (matchesFilter(standort)) {
                filteredData.add(standort);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<StandortEntity, ?>> sortOrder = new ArrayList<>(standortTable.getSortOrder());
        standortTable.getSortOrder().clear();
        standortTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param standort the standort
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(StandortEntity standort) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (standort.getAdresse().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (standort.getOrtschaft().getGemeinde().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Gets the standort table.
     *
     * @return the standort table
     */
    public TableView<StandortEntity> getStandortTable() {
        return standortTable;
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    private boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the standorte into the table.
     *
     * @param dialogStage the new dialog stage
     */
    private void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        filteredData.addAll(standortData);
        standortTable.setItems(filteredData);
        standortData.addListener(new ListChangeListener<StandortEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends StandortEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Set the selected standort and give it to the editDialog.
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = standortTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            standort = standortTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Standort auswählen", "Standort wählen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to create a new standort.
     */
    @FXML
    public void handleNewStandort() {
        StandortEntity standort = new StandortEntity();
        StandortEditDialogController controller = new StandortEditDialogController();
        controller.setEditDialog(false);
        controller.showStandortEditDialog(standort);
    }

    /**
     * Deletes the selected standort.
     */
    @FXML
    public void handleDeleteStandort() {
        int selectedIndex = standortTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(MainApp.getPrimaryStage(), "Bitte bestätigen Sie mit YES", "Wollen Sie diesen Standort löschen?", "Standort löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = standortTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteStandortAction standortAction = (ConcreteStandortAction) ActionRegistry.getInterface("standort");
                    standortAction.delete(id);
                    standortTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Änderung am Standort konnten nicht in die Datenbank geschrieben werden", e);
                    Dialogs.showErrorDialog(MainApp.getPrimaryStage(), e.getCause().getMessage(), "Die Änderungen konnten nicht in die Datenbank übernommen werden", "Fehler beim Speichern", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Standort auswählen", "Standort löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected standort.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    public void handleEditStandort() {
        StandortEntity selectedStandort = standortTable.getSelectionModel().getSelectedItem();
        if (selectedStandort != null) {
            StandortEditDialogController controller = new StandortEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showStandortEditDialog(selectedStandort);
            if (okClicked) {
                refreshStandortTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte einen Standort auswählen", "Standort bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Shows the eigentumer overview.
     */
    @FXML
    public void handleShowEigentumer() {
        EigentumerOverviewController controller = new EigentumerOverviewController();
        controller.showEigentumerOverview();
    }

    /**
     * Shows the verwaltung overview.
     */
    @FXML
    public void handleShowVerwaltung() {
        VerwaltungOverviewController controller = new VerwaltungOverviewController();
        controller.showVerwaltungOverview();
    }

    /**
     * Shows hauswart overview.
     */
    @FXML
    public void handleShowHauswart() {
        HauswartOverviewController controller = new HauswartOverviewController();
        controller.showHauswartOverview();
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    private void refreshStandortTable() {
        int selectedIndex = standortTable.getSelectionModel().getSelectedIndex();
        standortTable.setItems(null);
        standortTable.layout();
        standortTable.setItems(filteredData);
        standortTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the standort.
     *
     * @return the standort
     */
    public StandortEntity getStandort() {
        return standort;
    }
}
