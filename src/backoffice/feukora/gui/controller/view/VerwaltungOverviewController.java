package feukora.gui.controller.view;

import feukora.entity.VerwaltungEntity;
import feukora.exceptions.InvalidDataException;
import feukora.gui.MainApp;
import feukora.gui.controller.edit.VerwaltungEditDialogController;
import feukora.gui.util.ActionRegistry;
import feukora.interfaces.ConcreteVerwaltungAction;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Dialogs.DialogOptions;
import javafx.scene.control.Dialogs.DialogResponse;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Handles the actions for the verwaltung overview.
 *
 * @author Patrick Schmed
 * @version 20.05.2014
 * @since 01.05.2014
 */
public class VerwaltungOverviewController {

    /**
     * The choose dialog.
     */
    private static boolean chooseDialog;
    /**
     * The verwaltung data.
     */
    private static ObservableList<VerwaltungEntity> verwaltungData = FXCollections.observableArrayList();
    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(VerwaltungOverviewController.class);
    /**
     * The verwaltung.
     */
    private static VerwaltungEntity verwaltung;
    /**
     * The verwaltung table.
     */
    @FXML
    private TableView<VerwaltungEntity> verwaltungTable;
    /**
     * The vorname row.
     */
    @FXML
    private TableColumn<VerwaltungEntity, String> vornameRow;
    /**
     * The name row.
     */
    @FXML
    private TableColumn<VerwaltungEntity, String> nameRow;
    /**
     * The telefon row.
     */
    @FXML
    private TableColumn<VerwaltungEntity, String> telefonRow;
    /**
     * The strasse row.
     */
    @FXML
    private TableColumn<VerwaltungEntity, String> strasseRow;
    /**
     * The ortschaft row.
     */
    @FXML
    private TableColumn<VerwaltungEntity, String> ortschaftRow;
    /**
     * The close button.
     */
    @FXML
    private Button closeButton;
    /**
     * The choose button.
     */
    @FXML
    private Button chooseButton;
    /**
     * The dialog stage.
     */
    private Stage dialogStage;
    /**
     * The ok clicked.
     */
    private boolean okClicked = false;
    /**
     * The search field.
     */
    @FXML
    private TextField searchField;

    /**
     * The filtered data.
     */
    private ObservableList<VerwaltungEntity> filteredData = FXCollections.observableArrayList();

    /**
     * Standard constructor which is called before the initialize method.
     */
    public VerwaltungOverviewController() {

    }

    /**
     * Gets the verwaltung data.
     *
     * @return the verwaltung data
     */
    public static ObservableList<VerwaltungEntity> getVerwaltungData() {
        return verwaltungData;
    }

    /**
     * Initialize the controller class automatically after the fxml-File is
     * loaded.
     */
    @FXML
    public void initialize() {
        try {
            verwaltungData.removeAll(verwaltungData);
            ConcreteVerwaltungAction verwaltungAction = (ConcreteVerwaltungAction) ActionRegistry.getInterface("verwaltung");
            verwaltungData.addAll(verwaltungAction.getAll());
            vornameRow.setCellValueFactory(new PropertyValueFactory<VerwaltungEntity, String>("vorname"));
            nameRow.setCellValueFactory(new PropertyValueFactory<VerwaltungEntity, String>("name"));
            telefonRow.setCellValueFactory(new PropertyValueFactory<VerwaltungEntity, String>("telefon"));
            strasseRow.setCellValueFactory(new PropertyValueFactory<VerwaltungEntity, String>("strasse"));
            ortschaftRow.setCellValueFactory(new Callback<CellDataFeatures<VerwaltungEntity, String>, ObservableValue<String>>() {
                @SuppressWarnings({"unchecked", "rawtypes"})
                public ObservableValue<String> call(CellDataFeatures<VerwaltungEntity, String> verwaltung) {
                    if (verwaltung.getValue().getOrtschaft() == null) {
                        return null;
                    } else {
                        return new ReadOnlyObjectWrapper(verwaltung.getValue().getOrtschaft().getPlz() + " " + verwaltung.getValue().getOrtschaft().getGemeinde() + ", " + verwaltung.getValue().getOrtschaft().getKanton());
                    }
                }
            });
            verwaltungTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            searchField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    updateFilteredData();
                }
            });
        } catch (RemoteException e) {
            logger.warn("Verwaltungsdaten konnten nicht geladen werden", e);
            Dialogs.showErrorDialog(MainApp.getPrimaryStage(), " ", "Die Verwaltungsdaten konnten nicht geladen werden", "Fehler beim Laden", DialogOptions.OK);
        }
    }

    /**
     * Shows all verwaltungen from the database in a table overview.
     *
     * @return if the user clicked schliessen or auswählen
     */
    public boolean showVerwaltungOverview() {
        try {
            // Load the fxml-file and create a new Popup-Stage
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/VerwaltungOverview.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Verwaltung Übersicht");
            // Make sure that the user have to close the new stage before
            // returning
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(MainApp.getPrimaryStage());
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the burner into the controller
            VerwaltungOverviewController controller = loader.getController();
            controller.setDialogStage(dialogStage);

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

            return controller.isOkClicked();

        } catch (IOException e) {
            logger.warn("Unable to load the VerwaltungOverview.fxml file", e);
            return false;
        }
    }

    /**
     * Checks if is ok clicked.
     *
     * @return true, if is ok clicked
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Set the eigentümer into the table and deactivates the close or choose
     * button.
     *
     * @param dialogStage the dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        dialogStage.setResizable(false);
        if (chooseDialog == true) {
            closeButton.setVisible(false);
        } else {
            chooseButton.setVisible(false);
        }
        filteredData.addAll(verwaltungData);
        verwaltungTable.setItems(filteredData);
        verwaltungData.addListener(new ListChangeListener<VerwaltungEntity>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends VerwaltungEntity> change) {
                updateFilteredData();
            }
        });
    }

    /**
     * Update filtered Data if brennerData has changed.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void updateFilteredData() {
        filteredData.clear();
        for (VerwaltungEntity verwaltung : verwaltungData) {
            if (matchesFilter(verwaltung)) {
                filteredData.add(verwaltung);
            }
        }

        reapplyTableSortOrder();
    }

    /**
     * Reorder the objects on the table.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    private void reapplyTableSortOrder() {
        ArrayList<TableColumn<VerwaltungEntity, ?>> sortOrder = new ArrayList<>(verwaltungTable.getSortOrder());
        verwaltungTable.getSortOrder().clear();
        verwaltungTable.getSortOrder().addAll(sortOrder);
    }

    /**
     * Compare the text from the searchField with.
     *
     * @param verwaltung the verwaltung
     * @return true if filter matches an object
     * @author Patrick Schmed, Sven Neitzel
     */
    private boolean matchesFilter(VerwaltungEntity verwaltung) {
        String filterString = searchField.getText();
        if (filterString == null || filterString.isEmpty()) {
            return true;
        }
        String lowerCaseFilterString = filterString.toLowerCase();
        if (verwaltung.getVorname().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (verwaltung.getName().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (verwaltung.getTelefon().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (verwaltung.getStrasse().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (verwaltung.getOrtschaft().getGemeinde().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        }
        return false;
    }

    /**
     * Opens a new dialog to create a new verwaltung.
     */
    @FXML
    private void handleNewVerwaltung() {
        VerwaltungEntity tempVerwaltung = new VerwaltungEntity();
        VerwaltungEditDialogController controller = new VerwaltungEditDialogController();
        controller.setEditDialog(false);
        controller.showVerwaltungEditDialog(tempVerwaltung);
    }

    /**
     * Deletes the selected verwaltung.
     */
    @FXML
    private void handleDeleteVerwaltung() {
        int selectedIndex = verwaltungTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            DialogResponse response = Dialogs.showConfirmDialog(dialogStage, "Wollen Sie diese Verwaltung löschen?", "Bitte bestätigen Sie mit YES", "Verwaltung löschen", DialogOptions.YES_NO);
            if (response == DialogResponse.YES) {
                try {
                    int id = verwaltungTable.getSelectionModel().getSelectedItem().getId();
                    ConcreteVerwaltungAction verwaltungAction = (ConcreteVerwaltungAction) ActionRegistry.getInterface("verwaltung");
                    verwaltungAction.delete(id);
                    verwaltungTable.getItems().remove(selectedIndex);
                } catch (RemoteException | InvalidDataException e) {
                    logger.warn("Verwaltung konnte nicht aus der Datenbank gelöscht werden", e);
                    Dialogs.showErrorDialog(dialogStage, e.getCause().getMessage(), "Die Verwaltung konnte nicht aus der Datenbank gelöscht werden", "Fehler beim Löschen", DialogOptions.OK);
                }
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte eine Verwaltung auswählen", "Verwaltung löschen", DialogOptions.OK);
        }
    }

    /**
     * Opens a new dialog to edit the details for the selected verwaltung.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleEditVerwaltung() {
        VerwaltungEntity selectedVerwaltung = verwaltungTable.getSelectionModel().getSelectedItem();
        if (selectedVerwaltung != null) {
            VerwaltungEditDialogController controller = new VerwaltungEditDialogController();
            controller.setEditDialog(true);
            boolean okClicked = controller.showVerwaltungEditDialog(selectedVerwaltung);
            if (okClicked) {
                refreshVerwaltungTable();
            }
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte eine Verwaltung auswählen", "Verwaltung bearbeiten", DialogOptions.OK);
        }
    }

    /**
     * Closes the dialogStage if the user clicked schliessen.
     */
    @FXML
    public void handleOk() {
        dialogStage.close();
    }

    /**
     * Set the selected verwaltung and give it to the standortEditDialog.
     *
     * @author Patrick Schmed, Sven Neitzel
     */
    @FXML
    private void handleChoose() {
        int selectedIndex = verwaltungTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            verwaltung = verwaltungTable.getSelectionModel().getSelectedItem();
            dialogStage.close();
        } else {
            Dialogs.showWarningDialog(MainApp.getPrimaryStage(), " ", "Bitte eine Verwaltung auswählen", "Verwaltung wählen", DialogOptions.OK);
        }
    }

    /**
     * Refreshes the table. This is only necessary if an item that is already in
     * the table is changed. New and deleted items are refreshed automatically.
     */
    public void refreshVerwaltungTable() {
        int selectedIndex = verwaltungTable.getSelectionModel().getSelectedIndex();
        verwaltungTable.setItems(null);
        verwaltungTable.layout();
        verwaltungTable.setItems(filteredData);
        verwaltungTable.getSelectionModel().select(selectedIndex);
    }

    /**
     * Gets the verwaltung.
     *
     * @return the verwaltung
     */
    public VerwaltungEntity getVerwaltung() {
        return verwaltung;
    }

    /**
     * Sets the choose dialog.
     *
     * @param chooseDialog the new choose dialog
     */
    public void setChooseDialog(boolean chooseDialog) {
        VerwaltungOverviewController.chooseDialog = chooseDialog;
    }
}
