package feukora.rmi.logical;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.Test;

import feukora.entity.KontrolleurEntity;
import feukora.entity.OrtschaftEntity;
import feukora.exceptions.InvalidDataException;
import feukora.exceptions.UnableException;

/**
 * The Class KontrolleurImplTest.
 * 
 * Test ok, 27.04.2014
 * 
 * @author Sven Tscherrig, Florian Rieder
 * @version 21.04.14
 * @since 21.04.14
 */
public class KontrolleurImplTest extends ImplTest {

	/** The erster kontrolleur. */
	private KontrolleurEntity entity1;

	/** The zweiter kontrolleur. */
	private KontrolleurEntity entity2;

	/** The kontrolleur. */
	private static KontrolleurImpl kontrolleur;

	/**
	 * Sets the this up.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setThisUp() throws Exception {
		kontrolleur = new KontrolleurImpl(13000, getPool(), getUserlist());
	}

	/**
	 * Initialize objects.
	 */
	@Before
	public void initializeObjects() throws UnableException {

		// ersten Kontrolleur erstellen
		entity1 = new KontrolleurEntity();
		entity1.setBenutzername("nutzername1");
		entity1.setName("name1");
		entity1.setStrasse("strasse1");
		entity1.setTelefon("124 51 21 214");
		entity1.setVorname("vorname1");

		OrtschaftEntity ortschaft = new OrtschaftEntity();
		ortschaft.setGemeinde("luzern");
		ortschaft.setPlz("6003");
		ortschaft.setKanton("LU");

		entity1.setOrtschaft(ortschaft);
		entity1.setPasswort("banane123");
		entity1.setIsKontrolleur(1);

		// zweiten Kontrolleur erstellen
		entity2 = new KontrolleurEntity();
		entity2.setBenutzername("nutzername2");
		entity2.setName("name2");
		entity2.setStrasse("strasse2");
		entity2.setTelefon("124 51 21 214");
		entity2.setVorname("vorname2");
		entity2.setOrtschaft(ortschaft);
		entity2.setPasswort("banane123");
	}

	/**
	 * Test add.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @throws InvalidDataException
	 *             the invalid data exception
	 */
	@Test
	public void testAdd() throws RemoteException, InvalidDataException, UnableException {

		int i = kontrolleur.getAll().size();

		kontrolleur.add(entity1);
		kontrolleur.add(entity2);

		assertEquals(i + 2, kontrolleur.getAll().size());

		kontrolleur.delete(entity1.getId());
		kontrolleur.delete(entity2.getId());

	}

	/**
	 * Test get by id.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @throws InvalidDataException
	 *             the invalid data exception
	 */
	@Test
	public void testGetById() throws RemoteException, InvalidDataException {
		kontrolleur.add(entity1);
		kontrolleur.add(entity2);

		KontrolleurEntity get1 = kontrolleur.getById(entity1.getId());
		assertEquals(entity1.getId(), get1.getId());

		KontrolleurEntity get2 = kontrolleur.getById(entity2.getId());
		assertEquals(entity2.getId(), get2.getId());

		kontrolleur.delete(entity1.getId());
		kontrolleur.delete(entity2.getId());
	}

	/**
	 * Test get all.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @throws InvalidDataException
	 *             the invalid data exception
	 */
	@Test
	public void testGetAll() throws RemoteException, InvalidDataException {
		int i = kontrolleur.getAll().size();

		kontrolleur.add(entity1);
		kontrolleur.add(entity2);

		assertEquals(i, kontrolleur.getAll().size());

		kontrolleur.delete(entity1.getId());
		kontrolleur.delete(entity2.getId());
	}

	/**
	 * Test get by benutzer.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @throws InvalidDataException
	 *             the invalid data exception
	 */
	public void testGetByBenutzer() throws RemoteException, InvalidDataException {
		kontrolleur.add(entity1);
		kontrolleur.add(entity2);

		kontrolleur.delete(entity1.getId());
		kontrolleur.delete(entity2.getId());
	}

	/**
	 * Test update.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @throws InvalidDataException
	 *             the invalid data exception
	 */
	@Test
	public void testUpdate() throws RemoteException, InvalidDataException {
		kontrolleur.add(entity1);
		kontrolleur.add(entity2);

		entity1.setName("Updatetest");
		kontrolleur.update(entity1);
		KontrolleurEntity entity = kontrolleur.getById(entity1.getId());
		assertEquals(entity1.getName(), entity.getName());

		kontrolleur.delete(entity1.getId());
		kontrolleur.delete(entity2.getId());
	}

	/**
	 * Test delete.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @throws InvalidDataException
	 *             the invalid data exception
	 */
	@Test
	public void testDelete() throws RemoteException, InvalidDataException {
		kontrolleur.add(entity1);
		kontrolleur.add(entity2);

		int i = kontrolleur.getAll().size();

		kontrolleur.delete(entity1.getId());
		kontrolleur.delete(entity2.getId());

		assertEquals(i - 2, kontrolleur.getAll().size());
	}
}
