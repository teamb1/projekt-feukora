package feukora.rmi.logical;

import feukora.entity.BrennerEntity;
import feukora.entity.BrennerartEntity;
import feukora.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;

import static org.junit.Assert.assertEquals;

/**
 * The Class BrennerImplTest.
 * 
 * @author Sven Neitzel, Florian Rieder
 * @version 24.04.14
 * @since 21.04.14
 */
public class BrennerImplTest extends ImplTest {

    /**
     * The brenner.
     */
    private static BrennerImpl brenner;

    /**
     * The brenner art.
     */
    private static BrennerartImpl brennerArt;
    /**
     * The entity a.
     */
    private BrennerEntity entityA;

    /**
     * The entity b.
     */
    private BrennerEntity entityB;

    /**
     * Sets the this up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setThisUp() throws Exception {

        brenner = new BrennerImpl(13000, getPool(), getUserlist());
        brennerArt = new BrennerartImpl(13000, getPool(), getUserlist());
    }

    /**
     * Initialize objects.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Before
    public void initializeObjects() throws RemoteException,
            InvalidDataException {
        entityA = new BrennerEntity();
        entityA.setFabrikat("BTestFabrikat");
        entityA.setBaujahr(1990);
        entityA.setFeuerungswarmeleistung(20);
        BrennerartEntity typA = brennerArt.getById(1);
        BrennerartEntity typB = brennerArt.getById(2);

        entityA.setBrennerart(typA);

        entityB = new BrennerEntity();
        entityB.setFabrikat("ATestFabrikat");
        entityB.setBaujahr(1980);
        entityB.setFeuerungswarmeleistung(20);
        entityB.setBrennerart(typB);
    }

    /**
     * Test add.
     *
     * @throws Exception the exception
     */
    @Test
    public void testAdd() throws Exception {
        brenner.add(entityA);

        int idA = entityA.getId();

        BrennerEntity brennerA = brenner.getById(idA);
        assertEquals(brennerA.getFabrikat(), entityA.getFabrikat());
        assertEquals(brennerA.getBaujahr(), entityA.getBaujahr());
        assertEquals(brennerA.getFeuerungswarmeleistung(),
                entityA.getFeuerungswarmeleistung());
        brenner.delete(entityA.getId());
    }

    /**
     * Test get by id.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetById() throws RemoteException, InvalidDataException {
        brenner.add(entityA);
        brenner.add(entityB);

        int idA = entityA.getId();
        int idB = entityB.getId();

        BrennerEntity brennerA = brenner.getById(idA);
        assertEquals(entityA.getId(), brennerA.getId());

        BrennerEntity brennerB = brenner.getById(idB);
        assertEquals(entityB.getId(), brennerB.getId());

        brenner.delete(entityA.getId());
        brenner.delete(entityB.getId());
    }

    /**
     * Test get all.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetAll() throws RemoteException, InvalidDataException {
        int i = brenner.getAll().size();
        brenner.add(entityA);
        brenner.add(entityB);
        assertEquals(i + 2, brenner.getAll().size());

        brenner.delete(entityA.getId());
        brenner.delete(entityB.getId());

    }

    /**
     * Test update.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testUpdate() throws RemoteException, InvalidDataException {
        brenner.add(entityA);
        brenner.add(entityB);

        entityA.setBaujahr(1990);
        entityA.setFabrikat("NewTEST");
        entityA.setFeuerungswarmeleistung(50);

        brenner.update(entityA);
        int idA = entityA.getId();
        BrennerEntity brennerA = brenner.getById(idA);

        assertEquals("NewTEST", brennerA.getFabrikat());
        assertEquals(1990, brennerA.getBaujahr());
        assertEquals(50, brennerA.getFeuerungswarmeleistung());

        brenner.delete(entityA.getId());
        brenner.delete(entityB.getId());

    }

    /**
     * Test delete.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testDelete() throws RemoteException, InvalidDataException {
        brenner.add(entityA);

        int i = brenner.getAll().size();

        brenner.delete(entityA.getId());

        assertEquals(i - 1, brenner.getAll().size());
    }

    /**
     * Test get int.
     *
     * @throws Exception the exception
     */
    @Test
    public void testGetInt() throws Exception {
        System.out.println(brenner.getInt());
    }
}
