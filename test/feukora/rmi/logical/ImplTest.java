package feukora.rmi.logical;

import feukora.exceptions.NoSuchItemException;
import feukora.jpa.EntityManagerPool;
import feukora.jpa.JpaUtility;
import feukora.util.AuthorizationContainer;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;

/**
 * @author Christian Klauenbösch
 * @version 05.05.14
 * @since 17.04.14
 */
abstract public class ImplTest {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(ImplTest.class);

    /**
     * The jpa utility.
     */
    private static JpaUtility jpaUtility = null;

    /**
     * The pool.
     */
    private static EntityManagerPool pool = null;

    /**
     * Sets the test up.
     *
     * @throws Exception the exception
     */
    @BeforeClass
    public static void setTestUp() throws Exception {
        if (jpaUtility == null) {
            logger.debug("Initialize a new jpa utility");
            jpaUtility = new JpaUtility();
        }
        if (pool == null) {
            logger.debug("Initialize a new entity manager pool");
            pool = new EntityManagerPool(jpaUtility);
        }
    }

    /**
     * Gets the pool.
     *
     * @return the pool
     */
    protected static EntityManagerPool getPool() {
        return pool;
    }

    /**
     * Gets the userlist.
     *
     * @return the userlist
     */
    protected static AuthorizationContainer getUserlist() {
        return new AuthorizationContainer() {
            @Override
            public void grantAuth(String id) {
                // ignore
            }

            @Override
            public boolean userHasAuth(String id) {
                return true;
            }

            @Override
            public void refuseAuth(String id) throws NoSuchItemException {
                // ignore
            }

            @Override
            public void setMaxLifetime(int lifetime) {
                // ignore
            }
        };
    }
}
