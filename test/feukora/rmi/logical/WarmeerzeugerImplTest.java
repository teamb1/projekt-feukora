package feukora.rmi.logical;

import feukora.entity.BrennstoffEntity;
import feukora.entity.WarmeerzeugerEntity;
import feukora.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;

import static org.junit.Assert.assertEquals;

/**
 * The Class WarmeerzeugerImplTest.
 * <p/>
 * Test ok, 01.05.14, sne
 *
 * @author Sven Neitzel
 * @version 21.04.14
 * @since 21.04.14
 */
public class WarmeerzeugerImplTest extends ImplTest {

    /**
     * The erster warmeerzeuger.
     */
    private WarmeerzeugerEntity entity1;

    /**
     * The zweiter warmeerzeuger.
     */
    private WarmeerzeugerEntity entity2;

    /**
     * The brennstoff.
     */
    private BrennstoffImpl brennstoff;
    /**
     * The warmeerzeuger.
     */
    private WarmeerzeugerImpl warmeerzeuger;

    /**
     * The brennstoff1.
     */
    private BrennstoffEntity brennstoff1;

    /**
     * The brennstoff2.
     */
    private BrennstoffEntity brennstoff2;

    /**
     * Sets the this up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setThisUp() throws Exception {
        warmeerzeuger = new WarmeerzeugerImpl(13000, getPool(), getUserlist());
        brennstoff = new BrennstoffImpl(13000, getPool(), getUserlist());
    }

    /**
     * Initialize objects.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Before
    public void initializeObjects() throws RemoteException,
            InvalidDataException {
        brennstoff1 = brennstoff.getById(1);
        brennstoff2 = brennstoff.getById(2);

        // ersten Standard erstellen
        entity1 = new WarmeerzeugerEntity();

        entity1.setBaujahr(1980);
        entity1.setBrennstoff(brennstoff1);
        entity1.setFabrikat("test Fabrikat 1");

        // zweiten Standort erstellen
        entity2 = new WarmeerzeugerEntity();

        entity2.setBaujahr(1950);
        entity2.setBrennstoff(brennstoff2);
        entity2.setFabrikat("test Fabrikat 2");
    }

    /**
     * Test add.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testAdd() throws RemoteException, InvalidDataException {

        warmeerzeuger.add(entity1);

        int idErster = entity1.getId();

        WarmeerzeugerEntity testErsterwarmeerzeuger = warmeerzeuger
                .getById(idErster);

        assertEquals(entity1.getBaujahr(), testErsterwarmeerzeuger.getBaujahr());
        assertEquals(entity1.getBrennstoff().getId(), testErsterwarmeerzeuger
                .getBrennstoff().getId());
        assertEquals(entity1.getFabrikat(),
                testErsterwarmeerzeuger.getFabrikat());

        warmeerzeuger.delete(entity1.getId());
    }

    /**
     * Test get by id.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetById() throws RemoteException, InvalidDataException {
        warmeerzeuger.add(entity1);
        warmeerzeuger.add(entity2);

        int idErster = entity1.getId();
        int idZweiter = entity2.getId();

        WarmeerzeugerEntity testErsterwarmeerzeuger = warmeerzeuger
                .getById(idErster);
        assertEquals(entity1.getId(), testErsterwarmeerzeuger.getId());

        WarmeerzeugerEntity testZweiterwarmeerzeuger = warmeerzeuger
                .getById(idZweiter);
        assertEquals(entity2.getId(), testZweiterwarmeerzeuger.getId());

        warmeerzeuger.delete(entity1.getId());
        warmeerzeuger.delete(entity2.getId());
    }

    // public void testFilter() throws RemoteException, InvalidDataException {
    //
    //
    // }

    /**
     * Test get all.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetAll() throws RemoteException, InvalidDataException {
        int i = warmeerzeuger.getAll().size();

        warmeerzeuger.add(entity1);
        warmeerzeuger.add(entity2);

        assertEquals(i + 2, warmeerzeuger.getAll().size());

        warmeerzeuger.delete(entity1.getId());
        warmeerzeuger.delete(entity2.getId());

    }

    // public void testGetAllByBrennstoff() throws RemoteException,
    // InvalidDataException {
    //
    //
    // }

    /**
     * Test update.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testUpdate() throws RemoteException, InvalidDataException {
        warmeerzeuger.add(entity1);
        warmeerzeuger.add(entity2);

        BrennstoffEntity holz = new BrennstoffEntity();
        holz.setBezeichnung("Holz");
        int jahr = 1999;

        entity1.setBaujahr(jahr);
        entity1.setBrennstoff(holz);
        entity1.setFabrikat("testFabrikat");

        warmeerzeuger.update(entity1);

        assertEquals(jahr, entity1.getBaujahr());
        assertEquals("testFabrikat", entity1.getFabrikat());
        assertEquals(holz.getBezeichnung(), entity1.getBrennstoff()
                .getBezeichnung());

        warmeerzeuger.delete(entity1.getId());
        warmeerzeuger.delete(entity2.getId());
    }
}
