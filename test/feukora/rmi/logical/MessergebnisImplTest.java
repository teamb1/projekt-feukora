package feukora.rmi.logical;

import feukora.entity.MessergebnisEntity;
import feukora.entity.MessvorgangEntity;
import feukora.entity.RapportEntity;
import feukora.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/*
 * The Class MessergebnisImplTest.
 * 
 * Test OK, 01.05.14, sne
 * 
 * @author Patrick Schmed
 * @version 21.04.14
 * @since 21.04.14
 */

/**
 * @author Sven Tscherrig, Florian Rieder The Class MessergebnisImplTest.
 */
public class MessergebnisImplTest extends ImplTest {

    /**
     * The messergebnis.
     */
    private static MessergebnisImpl messergebnis;

    /**
     * The entity a.
     */
    private MessergebnisEntity entityA;

    /**
     * The entity b.
     */
    private MessergebnisEntity entityB;

    /**
     * The rapport a.
     */
    private RapportEntity rapportA;

    /**
     * The rapport b.
     */
    private RapportEntity rapportB;

    /**
     * The test list.
     */
    private List<MessergebnisEntity> testList;

    /**
     * Sets the this up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setThisUp() throws Exception {
        messergebnis = new MessergebnisImpl(13000, getPool(), getUserlist());
    }

    /**
     * Initialize objects.
     */
    @Before
    public void initializeObjects() {
        rapportA = new RapportEntity();
        rapportB = new RapportEntity();
        testList = new ArrayList<MessergebnisEntity>();
        MessvorgangEntity messvorgangA = new MessvorgangEntity();
        MessvorgangEntity messvorgangB = new MessvorgangEntity();

        entityA = new MessergebnisEntity();
        entityA.setAbgastemp(100);
        entityA.setAbgasverlust(5);
        entityA.setCo(50);
        entityA.setNo(5);
        entityA.setO2gehalt(5);
        entityA.setRusszahl(1);
        entityA.setVerbrennungstemp(150);
        entityA.setWarmeerzeugertemp(150);
        entityA.setMessvorgang(messvorgangA);
        entityA.setRusszahl(1);
        entityA.setOelanteil(false);

        entityB = new MessergebnisEntity();
        entityB.setAbgastemp(100);
        entityB.setAbgasverlust(5);
        entityB.setCo(50);
        entityB.setNo(5);
        entityB.setO2gehalt(5);
        entityB.setRusszahl(1);
        entityB.setVerbrennungstemp(150);
        entityB.setWarmeerzeugertemp(150);
        entityB.setMessvorgang(messvorgangB);
        entityB.setOelanteil(false);
        entityB.setRusszahl(1);
    }

    /**
     * Test add.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testAdd() throws RemoteException, InvalidDataException {
        int i = messergebnis.getAll().size();

        messergebnis.add(entityA);
        messergebnis.add(entityB);

        assertEquals(i + 1, messergebnis.getAll().size());

        messergebnis.delete(entityA.getId());
        messergebnis.delete(entityB.getId());
    }

    /**
     * Test get by id.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetById() throws RemoteException, InvalidDataException {
        int i = messergebnis.getAll().size();

        messergebnis.add(entityA);
        messergebnis.add(entityB);

        assertEquals(i + 2, messergebnis.getAll().size());

        messergebnis.delete(entityA.getId());
        messergebnis.delete(entityB.getId());
    }

    /**
     * Test get by rapport.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetByRapport() throws RemoteException, InvalidDataException {
        messergebnis.add(entityA);
        messergebnis.add(entityB);

        messergebnis.delete(entityA.getId());
        messergebnis.delete(entityB.getId());
    }

    /**
     * Test get all.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetAll() throws RemoteException, InvalidDataException {
        int anzahl = messergebnis.getAll().size();

        messergebnis.add(entityA);
        messergebnis.add(entityB);

        assertEquals(anzahl + 2, messergebnis.getAll().size());

        messergebnis.delete(entityA.getId());
        messergebnis.delete(entityB.getId());
    }

    /**
     * Test update.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testUpdate() throws RemoteException, InvalidDataException {
        messergebnis.add(entityA);
        messergebnis.add(entityB);

        entityA.setRusszahl(6);
        entityA.setCo(8);
        entityA.setNo(7);
        entityA.setAbgastemp(5);
        entityA.setWarmeerzeugertemp(40);
        entityA.setVerbrennungstemp(90);
        entityA.setO2gehalt(8);
        entityA.setAbgasverlust(4);
        messergebnis.update(entityA);

        int idA = entityA.getId();
        MessergebnisEntity messergebnisA = messergebnis.getById(idA);

        assertEquals(6, messergebnisA.getRusszahl());
        assertEquals(8, messergebnisA.getCo());
        assertEquals(7, messergebnisA.getNo());
        assertEquals(5, messergebnisA.getAbgastemp());
        assertEquals(40, messergebnisA.getWarmeerzeugertemp());
        assertEquals(90, messergebnisA.getVerbrennungstemp());
        assertEquals(8, messergebnisA.getO2gehalt());
        assertEquals(4, messergebnisA.getAbgasverlust());

        messergebnis.delete(entityA.getId());
        messergebnis.delete(entityB.getId());

    }

    /**
     * Test delete.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testDelete() throws RemoteException, InvalidDataException {
        messergebnis.add(entityA);
        messergebnis.add(entityB);

        int i = messergebnis.getAll().size();

        messergebnis.delete(entityA.getId());
        messergebnis.delete(entityB.getId());

        assertEquals(i - 2, messergebnis.getAll().size());
    }

}
