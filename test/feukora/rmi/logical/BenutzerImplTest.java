package feukora.rmi.logical;

import feukora.entity.BenutzerEntity;
import feukora.exceptions.InvalidDataException;
import feukora.exceptions.UnableException;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * The Class BenutzerImplTest.
 * <p/>
 * Test ok, 27.04.14, ckl
 * 
 * @author Patrick Schmed, Florian Rieder
 * @version 21.04.14
 * @since 17.04.14
 */
public class BenutzerImplTest extends ImplTest {

    /**
     * The benutzer.
     */
    private static BenutzerImpl benutzer;

    /**
     * The entity a.
     */
    private BenutzerEntity entityA;

    /**
     * The entity b.
     */
    private BenutzerEntity entityB;

    /**
     * The test list.
     */
    private List<BenutzerEntity> testList;

    /**
     * Sets the this up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setThisUp() throws Exception {
        // WICHTIG! Unbedingt einfügen
        benutzer = new BenutzerImpl(13000, getPool(), getUserlist());
    }

    /**
     * Initialize objects.
     */
    @Before
    public void initializeObjects() throws UnableException {
        testList = new ArrayList<BenutzerEntity>();

        entityA = new BenutzerEntity();
        entityA.setBenutzername("hansmuster");
        entityA.setIsKontrolleur(1);
        entityA.setPasswort("blablabla");
        entityA.setVorname("Hans");
        entityA.setName("Muster");

        entityB = new BenutzerEntity();
        entityB.setBenutzername("testuser");
        entityB.setIsKontrolleur(0);
        entityB.setPasswort("123456789");
        entityB.setVorname("Test");
        entityB.setName("User");
    }

    /**
     * Test add.
     *
     * @throws Exception the exception
     */
    @Test
    public void testAdd() throws Exception {
        benutzer.add(entityA);

        int idA = entityA.getId();

        BenutzerEntity benutzerA = benutzer.getById(idA);
        assertEquals(entityA.getBenutzername(), benutzerA.getBenutzername());
        assertEquals(entityA.getIsKontrolleur(), benutzerA.getIsKontrolleur());
        assertEquals(entityA.getPasswort(), benutzerA.getPasswort());
        assertEquals(entityA.getVorname(), benutzerA.getVorname());
        assertEquals(entityA.getName(), benutzerA.getName());
        benutzer.delete(entityA.getId());
    }

    /**
     * Test get by id.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetById() throws RemoteException, InvalidDataException {
        benutzer.add(entityA);
        benutzer.add(entityB);

        int idA = entityA.getId();
        int idB = entityB.getId();

        BenutzerEntity benutzerA = benutzer.getById(idA);
        assertEquals(entityA.getId(), benutzerA.getId());

        BenutzerEntity benutzerB = benutzer.getById(idB);
        assertEquals(entityB.getId(), benutzerB.getId());

        benutzer.delete(entityB.getId());
        benutzer.delete(entityA.getId());
    }

    /**
     * Test get by funktion.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetByFunktion() throws RemoteException,
            InvalidDataException {
        benutzer.add(entityB);
        int i = benutzer.getKontrolleure().size();
        entityB.setName("NameNeu");
        entityB.setIsKontrolleur(1);
        benutzer.update(entityB);

        assertEquals(i + 1, benutzer.getKontrolleure().size());
        benutzer.delete(entityB.getId());

    }

    /**
     * Test get all.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetAll() throws RemoteException, InvalidDataException {
        int i = benutzer.getAll().size();
        entityA.setIsKontrolleur(1);
        benutzer.add(entityA);
        benutzer.add(entityB);
        assertEquals(i + 2, benutzer.getAll().size());

        benutzer.delete(entityA.getId());
        benutzer.delete(entityB.getId());
    }

    /**
     * Test update.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testUpdate() throws RemoteException, InvalidDataException,
            UnableException {
        benutzer.add(entityA);
        benutzer.add(entityB);

        entityA.setBenutzername("updatedNutzer");
        entityA.setIsKontrolleur(0);
        entityA.setVorname("updatedVorname");
        entityA.setName("updatedName");
        benutzer.update(entityA);

        int idA = entityA.getId();
        BenutzerEntity benutzerA = benutzer.getById(idA);

        assertEquals("updatedNutzer", benutzerA.getBenutzername());
        assertEquals(0, benutzerA.getIsKontrolleur());
        assertEquals("updatedVorname", benutzerA.getVorname());
        assertEquals("updatedName", benutzerA.getName());

        benutzer.delete(entityA.getId());
        benutzer.delete(entityB.getId());

    }

    /**
     * Test delete.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testDelete() throws RemoteException, InvalidDataException {
        benutzer.add(entityA);

        int size = benutzer.getAll().size();

        benutzer.delete(entityA.getId());

        testList = benutzer.getAll();

        assertEquals(size - 1, testList.size());
        assertFalse(testList.contains(entityA));
    }
}
