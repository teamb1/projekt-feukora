package feukora.rmi.logical;

import feukora.entity.OrtschaftEntity;
import feukora.entity.RapportEntity;
import feukora.entity.StandortEntity;
import feukora.entity.TerminEntity;
import feukora.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * The Class TerminImplTest. Test ok, 01.05.14, sne
 *
 * @author Florian Rieder, Sven Neitzel
 * @version 24.04.14
 * @since 21.04.14
 */
public class TerminImplTest extends ImplTest {

    /**
     * The termin.
     */
    private static TerminImpl termin;
    private static KontrolleurImpl kontrolleur;
    private static StandortImpl standort;
    /**
     * The entity a.
     */
    private TerminEntity entityA;

    /**
     * The entity b.
     */
    private TerminEntity entityB;

    /**
     * Sets the this up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setThisUp() throws Exception {

        termin = new TerminImpl(13000, getPool(), getUserlist());
        kontrolleur = new KontrolleurImpl(13000, getPool(), getUserlist());
        standort = new StandortImpl(1300, getPool(), getUserlist());
    }

    /**
     * Elemente zum Testen werden erstellt.
     *
     * @throws InvalidDataException
     * @throws RemoteException
     */
    @Before
    public void initializeObjects() throws RemoteException,
            InvalidDataException {
        entityA = new TerminEntity();
        StandortEntity testStandort = new StandortEntity();
        testStandort.setOrtschaft(new OrtschaftEntity("6043"));

        entityA.setDatum(2014, 10, 5, 11, 00);

        // entityA.setStandort(testStandort);

        entityB = new TerminEntity();
        entityB.setDatum(2015, 2, 15, 8, 00);
        entityB.setStandort(testStandort);
    }

    /**
     * Test add.
     *
     * @throws Exception the exception
     */
    @Test
    public void testAdd() throws Exception {
        int i = termin.getAll().size();

        termin.add(entityA);
        termin.add(entityB);

        assertEquals(i + 2, termin.getAll().size());

        termin.delete(entityA.getId());
        termin.delete(entityB.getId());
    }

    /**
     * Test get by id.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetById() throws RemoteException, InvalidDataException {
        termin.add(entityA);
        termin.add(entityA);

        TerminEntity id1 = termin.getById(entityA.getId());
        TerminEntity id2 = termin.getById(entityB.getId());

        assertEquals(entityA.getId(), id1);
        assertEquals(entityB.getId(), id2);

        termin.delete(entityA.getId());
        termin.delete(entityB.getId());
    }

    /**
     * Test update.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testUpdate() throws RemoteException, InvalidDataException {
        termin.add(entityA);
        termin.add(entityB);

        StandortEntity testStandort2 = new StandortEntity();
        RapportEntity testRapport2 = new RapportEntity();

        RapportImpl rapportImpl = new RapportImpl(13000, getPool(),
                getUserlist());
        rapportImpl.add(testRapport2);
        StandortImpl standortImpl = new StandortImpl(13000, getPool(),
                getUserlist());
        standortImpl.add(testStandort2);

        int idR = testRapport2.getId();
        RapportEntity rapport = rapportImpl.getById(idR);
        int idS = testStandort2.getId();
        StandortEntity standort = standortImpl.getById(idS);

        entityA.setRapport(testRapport2);
        entityA.setDatum(2010, 5, 9, 8, 9);
        entityA.setStandort(testStandort2);

        termin.update(entityA);
        int idA = entityA.getId();
        TerminEntity terminA = termin.getById(idA);

        assertEquals(2010, terminA.getDatum().get(Calendar.YEAR));
        assertEquals(rapport.getId(), terminA.getRapport().getId());
        assertEquals(standort.getId(), terminA.getStandort().getId());

        termin.delete(entityA.getId());
        termin.delete(entityB.getId());
    }

    /**
     * Test delete.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testDelete() throws RemoteException, InvalidDataException {
        termin.add(entityA);
        termin.add(entityB);

        int i = termin.getAll().size();

        termin.delete(entityA.getId());
        termin.delete(entityB.getId());

        assertEquals(i - 2, termin.getAll().size());
    }

}
