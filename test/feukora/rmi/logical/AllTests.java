package feukora.rmi.logical;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({BenutzerImplTest.class, BrennerImplTest.class,
        KontrolleurImplTest.class, MessergebnisImplTest.class,
        StandortMitAnhangTest.class, TerminImplTest.class,
        WarmeerzeugerImplTest.class})
public class AllTests {

}
