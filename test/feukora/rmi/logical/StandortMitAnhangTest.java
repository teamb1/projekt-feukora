package feukora.rmi.logical;

import feukora.entity.*;
import feukora.exceptions.InvalidDataException;
import org.junit.Before;
import org.junit.Test;

import java.rmi.RemoteException;
import java.util.List;

import static org.junit.Assert.assertEquals;

import feukora.entity.EigentumerEntity;
import feukora.entity.HauswartEntity;
import feukora.entity.OrtschaftEntity;
import feukora.entity.StandortEntity;
import feukora.entity.VerwaltungEntity;
import feukora.exceptions.InvalidDataException;

/**
 * @author Florian Rieder
 * @version 21.04.14
 * @since 21.04.14
 */
public class StandortMitAnhangTest extends ImplTest {

    /**
     * The hauswart.
     */
    private static HauswartImpl hauswart;
    /**
     * The eigentumer.
     */
    private static EigentumerImpl eigentumer;
    /**
     * The verwaltung.
     */
    private static VerwaltungImpl verwaltung;
    /**
     * The ortschaft.
     */
    private OrtschaftImpl ortschaft;
    /**
     * The id erster.
     */
    private int idErster = 0;
    /**
     * The id zweiter.
     */
    private int idZweiter = 0;
    /**
     * The erster standort.
     */
    private StandortEntity entity1;
    /**
     * The zweiter standort.
     */
    private StandortEntity entity2;
    /**
     * The standort.
     */
    private StandortImpl standort;
    /**
     * The hauswart1.
     */
    private HauswartEntity hauswart1;
    /**
     * The test list.
     */
    private List<StandortEntity> testList;
    /**
     * The verwaltung e.
     */
    private VerwaltungEntity verwaltungE;

    /**
     * The eigentumer1.
     */
    private EigentumerEntity eigentumer1;

    /**
     * The id eigentumer.
     */
    private int idEigentumer;

    /**
     * Sets the this up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setThisUp() throws Exception {
        standort = new StandortImpl(13000, getPool(), getUserlist());
        ortschaft = new OrtschaftImpl(13000, getPool(), getUserlist());
        hauswart = new HauswartImpl(13000, getPool(), getUserlist());

        eigentumer = new EigentumerImpl(13000, getPool(), getUserlist());
        verwaltung = new VerwaltungImpl(13000, getPool(), getUserlist());
    }

    /**
     * Initialize objects.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Before
    public void initializeObjects() throws RemoteException,
            InvalidDataException {

        // ersten Standort erstellen
        entity1 = new StandortEntity();

        hauswart1 = new HauswartEntity();
        hauswart1.setName("MeierH");
        hauswart1.setVorname("HansH");
        hauswart1.setOrtschaft(new OrtschaftEntity("6043"));
        hauswart1.setStrasse("Besenstrasse");
        hauswart1.setTelefon("01330849040348");

        verwaltungE = new VerwaltungEntity();
        verwaltungE.setName("VerwalterN");
        verwaltungE.setVorname("VerwalterVN");
        verwaltungE.setOrtschaft(new OrtschaftEntity("6043"));
        verwaltungE.setStrasse("VerwalterwegS");
        verwaltungE.setTelefon("0324738750723");

        eigentumer1 = new EigentumerEntity();
        eigentumer1.setName("NameE");
        eigentumer1.setOrtschaft(new OrtschaftEntity("6043"));
        eigentumer1.setStrasse("sTrasseE");
        eigentumer1.setTelefon("098120424809280");
        eigentumer1.setVorname("aewofajwe");
        // entity1.setVerwaltung(verwaltungE);
        // entity1.setHauswart(hauswart1);
        entity1.setAdresse("Zentralstrasse9, 6000 Luzern");
        entity1.setOrtschaft(new OrtschaftEntity("6043"));
        // entity1.getTermine().add(new TerminEntity());
        // entity1.setVerwaltung(new VerwaltungEntity());
        // entity1.setEigentumer(new EigentumerEntity());

        // zweiten Standort erstellen
        entity2 = new StandortEntity();

        entity2.setAdresse("Hauptstrasse 12, 4000 Basel");
        // entity2.setEigentumer(new EigentumerEntity());
        // entity2.setHauswart(new HauswartEntity());
        entity2.setOrtschaft(new OrtschaftEntity("6043"));
        // entity2.getTermine().add(new TerminEntity());
        // entity2.setVerwaltung(new VerwaltungEntity());

    }

    //
    // @After
    // public void delete() throws RemoteException, InvalidDataException {
    // if (idErster != 0) {
    // standort.delete(idErster);
    // }
    // if (idZweiter != 0) {
    // standort.delete(idZweiter);
    // }
    //
    // }

    /**
     * Test add.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testAddVerwaltung() throws RemoteException,
            InvalidDataException {
        verwaltung.add(verwaltungE);
        int idVerwaltung = verwaltungE.getId();

        VerwaltungEntity testVerwaltung = verwaltung.getById(idVerwaltung);

        assertEquals(verwaltungE.getName(), testVerwaltung.getName());

    }

    /**
     * Test add hauswart.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testAddHauswart() throws RemoteException, InvalidDataException {
        hauswart.add(hauswart1);
        int idVerwaltung = hauswart1.getId();

        HauswartEntity testHauswart = hauswart.getById(idVerwaltung);

        assertEquals(hauswart1.getName(), testHauswart.getName());

    }

    /**
     * Test add eigentumer.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testAddEigentumer() throws RemoteException,
            InvalidDataException {
        eigentumer.add(eigentumer1);
        idEigentumer = eigentumer1.getId();

        EigentumerEntity testEigentumer = eigentumer.getById(idEigentumer);

        assertEquals(eigentumer1.getName(), testEigentumer.getName());

    }

    /**
     * Test add.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testAdd() throws RemoteException, InvalidDataException {

        standort.add(entity1);

        idErster = entity1.getId();

        StandortEntity testErsterStandort = standort.getById(idErster);
        assertEquals(entity1.getAdresse(), testErsterStandort.getAdresse());
        assertEquals(entity1.getEigentumer(),
                testErsterStandort.getEigentumer());
        assertEquals(entity1.getHauswart(), testErsterStandort.getHauswart());
        assertEquals(entity1.getOrtschaft().getGemeinde(), testErsterStandort
                .getOrtschaft().getGemeinde());
        assertEquals(entity1.getVerwaltung(),
                testErsterStandort.getVerwaltung());
        // standort.delete(idErster);
    }

    /**
     * Test get by id.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetById() throws RemoteException, InvalidDataException {
        standort.add(entity1);
        standort.add(entity2);

        idErster = entity1.getId();
        idZweiter = entity2.getId();

        StandortEntity testErsterStandort = standort.getById(idErster);
        assertEquals(entity1.getId(), testErsterStandort.getId());

        StandortEntity testZweiterStandort = standort.getById(idZweiter);
        assertEquals(entity2.getId(), testZweiterStandort.getId());

        standort.delete(entity1.getId());
        standort.delete(entity2.getId());
    }

    /**
     * Test get by ortschaft.
     *
     * @throws RemoteException
     *             the remote exception
     * @throws InvalidDataException
     *             the invalid data exception
     */
    // @Test
    // public void testGetByOrtschaft() throws RemoteException,
    // InvalidDataException {
    // OrtschaftEntity ortschaft = entity1.getOrtschaft();
    // List<StandortEntity> testStandortEntity = standort
    // .getByOrtschaft(ortschaft.getPlz());
    // List<StandortEntity> testStandortEntity2 = standort
    // .getByOrtschaft(entity2.getOrtschaft().getPlz());
    //
    // assertEquals(1, testStandortEntity.size());
    // assertEquals(1, testStandortEntity2.size());
    //
    // }

    /**
     * Test get all.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testGetAll() throws RemoteException, InvalidDataException {
        int i = standort.getAll().size();

        standort.add(entity1);
        standort.add(entity2);

        assertEquals(i + 2, standort.getAll().size());
        idErster = entity1.getId();
        idZweiter = entity2.getId();

        // standort.delete(idErster);
        // standort.delete(idZweiter);
    }

    /**
     * Test update.
     *
     * @throws RemoteException      the remote exception
     * @throws InvalidDataException the invalid data exception
     */
    @Test
    public void testUpdate() throws RemoteException, InvalidDataException {

        entity1.setEigentumer(eigentumer.getById(idEigentumer));
        entity1.setVerwaltung(verwaltungE);
        entity1.setHauswart(hauswart1);
        // Attribute setzen
        entity1.setAdresse("teststrasse");

        // Update aufrufen
        standort.update(entity1);

        // vergleichen
        assertEquals("teststrasse", entity1.getAdresse());
        // assertEquals("testEigentümer", entity1.getEigentumer().getName());
        // assertEquals("testHauswart", entity1.getHauswart().getName());
        // assertEquals("testGemeinde", entity1.getOrtschaft().getGemeinde());

        // assertEquals("testVerwaltung", entity1.getVerwaltung().getName());
        // idErster = entity1.getId();
        // idZweiter = entity2.getId();

        // standort.delete(idErster);
        // standort.delete(idZweiter);

    }
}
