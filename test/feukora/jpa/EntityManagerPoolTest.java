package feukora.jpa;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test the EntityManagerPool.
 *
 * @author Christian Klauenboesch
 * @version 21.04.14
 * @since 21.04.14
 */
public class EntityManagerPoolTest {

    /**
     * The pool.
     */
    private static EntityManagerPool pool;

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    @Before
    public void setUp() throws Exception {
        pool = new EntityManagerPool(new AbstractJpaUtility() {
            @Override
            public EntityManager createEntityManager() {
                return new EntityManager() {
                    @Override
                    public void persist(Object entity) {

                    }

                    @Override
                    public <T> T merge(T entity) {
                        return null;
                    }

                    @Override
                    public void remove(Object entity) {

                    }

                    @Override
                    public <T> T find(Class<T> entityClass, Object primaryKey) {
                        return null;
                    }

                    @Override
                    public <T> T find(Class<T> entityClass, Object primaryKey,
                                      Map<String, Object> properties) {
                        return null;
                    }

                    @Override
                    public <T> T find(Class<T> entityClass, Object primaryKey,
                                      LockModeType lockMode) {
                        return null;
                    }

                    @Override
                    public <T> T find(Class<T> entityClass, Object primaryKey,
                                      LockModeType lockMode,
                                      Map<String, Object> properties) {
                        return null;
                    }

                    @Override
                    public <T> T getReference(Class<T> entityClass,
                                              Object primaryKey) {
                        return null;
                    }

                    @Override
                    public void flush() {

                    }

                    @Override
                    public void setFlushMode(FlushModeType flushMode) {

                    }

                    @Override
                    public FlushModeType getFlushMode() {
                        return null;
                    }

                    @Override
                    public void lock(Object entity, LockModeType lockMode) {

                    }

                    @Override
                    public void lock(Object entity, LockModeType lockMode,
                                     Map<String, Object> properties) {

                    }

                    @Override
                    public void refresh(Object entity) {

                    }

                    @Override
                    public void refresh(Object entity,
                                        Map<String, Object> properties) {

                    }

                    @Override
                    public void refresh(Object entity, LockModeType lockMode) {

                    }

                    @Override
                    public void refresh(Object entity, LockModeType lockMode,
                                        Map<String, Object> properties) {

                    }

                    @Override
                    public void clear() {

                    }

                    @Override
                    public void detach(Object entity) {

                    }

                    @Override
                    public boolean contains(Object entity) {
                        return false;
                    }

                    @Override
                    public LockModeType getLockMode(Object entity) {
                        return null;
                    }

                    @Override
                    public void setProperty(String propertyName, Object value) {

                    }

                    @Override
                    public Map<String, Object> getProperties() {
                        return null;
                    }

                    @Override
                    public Query createQuery(String qlString) {
                        return null;
                    }

                    @Override
                    public <T> TypedQuery<T> createQuery(
                            CriteriaQuery<T> criteriaQuery) {
                        return null;
                    }

                    @Override
                    public Query createQuery(CriteriaUpdate updateQuery) {
                        return null;
                    }

                    @Override
                    public Query createQuery(CriteriaDelete deleteQuery) {
                        return null;
                    }

                    @Override
                    public <T> TypedQuery<T> createQuery(String qlString,
                                                         Class<T> resultClass) {
                        return null;
                    }

                    @Override
                    public Query createNamedQuery(String name) {
                        return null;
                    }

                    @Override
                    public <T> TypedQuery<T> createNamedQuery(String name,
                                                              Class<T> resultClass) {
                        return null;
                    }

                    @Override
                    public Query createNativeQuery(String sqlString) {
                        return null;
                    }

                    @Override
                    public Query createNativeQuery(String sqlString,
                                                   Class resultClass) {
                        return null;
                    }

                    @Override
                    public Query createNativeQuery(String sqlString,
                                                   String resultSetMapping) {
                        return null;
                    }

                    @Override
                    public StoredProcedureQuery createNamedStoredProcedureQuery(
                            String name) {
                        return null;
                    }

                    @Override
                    public StoredProcedureQuery createStoredProcedureQuery(
                            String procedureName) {
                        return null;
                    }

                    @Override
                    public StoredProcedureQuery createStoredProcedureQuery(
                            String procedureName, Class... resultClasses) {
                        return null;
                    }

                    @Override
                    public StoredProcedureQuery createStoredProcedureQuery(
                            String procedureName, String... resultSetMappings) {
                        return null;
                    }

                    @Override
                    public void joinTransaction() {

                    }

                    @Override
                    public boolean isJoinedToTransaction() {
                        return false;
                    }

                    @Override
                    public <T> T unwrap(Class<T> cls) {
                        return null;
                    }

                    @Override
                    public Object getDelegate() {
                        return null;
                    }

                    @Override
                    public void close() {

                    }

                    @Override
                    public boolean isOpen() {
                        /*
                         * This must be true because EntityManagerPool::close()
						 * will call this
						 */
                        return true;
                    }

                    @Override
                    public EntityTransaction getTransaction() {
                        return new EntityTransaction() {
                            @Override
                            public void begin() {

                            }

                            @Override
                            public void commit() {

                            }

                            @Override
                            public void rollback() {

                            }

                            @Override
                            public void setRollbackOnly() {

                            }

                            @Override
                            public boolean getRollbackOnly() {
                                return false;
                            }

                            @Override
                            public boolean isActive() {
                                /*
								 * This must be false because
								 * EntityManagerPool::close() calls this
								 */
                                return false;
                            }
                        };
                    }

                    @Override
                    public EntityManagerFactory getEntityManagerFactory() {
                        return null;
                    }

                    @Override
                    public CriteriaBuilder getCriteriaBuilder() {
                        return null;
                    }

                    @Override
                    public Metamodel getMetamodel() {
                        return null;
                    }

                    @Override
                    public <T> EntityGraph<T> createEntityGraph(
                            Class<T> rootType) {
                        return null;
                    }

                    @Override
                    public EntityGraph<?> createEntityGraph(String graphName) {
                        return null;
                    }

                    @Override
                    public EntityGraph<?> getEntityGraph(String graphName) {
                        return null;
                    }

                    @Override
                    public <T> List<EntityGraph<? super T>> getEntityGraphs(
                            Class<T> entityClass) {
                        return null;
                    }
                };
            }

            @Override
            public void close() {
                return;
            }
        });

    }

    /**
     * Test is valid.
     *
     * @throws Exception the exception
     */
    @Test
    public void testIsValid() throws Exception {
        assertEquals(0, pool.getLockedSize());
        assertEquals(0, pool.getUnlockedSize());

		/*
		 * Manual set the registration time back so the object will no longer be
		 * valid
		 */
        EntityManager em = pool.checkOut();
        pool.modifyRegistrationTime(em,
                new Date().getTime() - pool.getExpirationTime() - 1000);
        assertFalse(pool.isOutdated(em));
    }

    /**
     * Test check out.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCheckOut() throws Exception {
        assertEquals(0, pool.getLockedSize());

        EntityManager em = pool.checkOut();

        assertEquals(1, pool.getLockedSize());
    }

    /**
     * Test check in.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCheckIn() throws Exception {
        assertEquals(0, pool.getLockedSize());
        assertEquals(0, pool.getUnlockedSize());

        EntityManager em = pool.checkOut();

        assertEquals(0, pool.getUnlockedSize());
        assertEquals(1, pool.getLockedSize());

        pool.checkIn(em);

        assertEquals(1, pool.getUnlockedSize());
        assertEquals(0, pool.getLockedSize());
    }
}
