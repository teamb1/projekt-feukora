package feukora.stubs;

import feukora.entity.TerminEntity;
import feukora.util.Encoder;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.List;

/**
 * The Class StubTest.
 *
 * @author Christian Klauenboesch
 * @version 15.05.14
 * @since 15.05.14
 */
public class StubTest {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(StubTest.class);

    /**
     * Test soap.
     *
     * @throws Exception the exception
     */
    @Test
    public void testSoap() throws Exception {
        GlobalSoapImplService service = new GlobalSoapImplService();
        GlobalSoapImpl impl = service.getGlobalSoapImplPort();

        String response = impl.loginValidateUser("root", Encoder.hashPassword("root"));

        List<TerminEntity> list = impl.terminGetAll(response);
        for (TerminEntity item : list) {
            System.out.println(item);
        }

    }
}
