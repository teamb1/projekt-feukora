package feukora.gui.util;

import feukora.entity.RapportEntity;
import feukora.interfaces.ConcreteLoginAction;
import feukora.interfaces.ConcreteRapportAction;
import feukora.stubs.GlobalSoapImplService;
import feukora.util.Encoder;
import org.junit.Test;

import java.security.MessageDigest;

/**
 * @author Christian Klauenbösch
 * @version 16.05.14
 * @since 16.04.14
 */
public class SoapStubCasterTest {

    /**
     * Test cast to rapport.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCastToRapport() throws Exception {
        GlobalSoapImplService service = new GlobalSoapImplService();
        ConcreteLoginAction loginAction = SoapStubCaster.castToLogin(service.getGlobalSoapImplPort());

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest("root".getBytes("UTF-8"));
        String hashAsString = Encoder.byteArrayToHexString(hash, ":");

        loginAction.checkBenutzer("root", hashAsString);

        ConcreteRapportAction action = SoapStubCaster.castToRapport(service.getGlobalSoapImplPort());

        RapportEntity entity = new RapportEntity();

        action.add(entity);
    }
}