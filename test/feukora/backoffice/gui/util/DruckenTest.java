package feukora.backoffice.gui.util;

import feukora.entity.*;
import feukora.gui.util.Drucker;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Florian Rieder
 * @version 16.05.14
 * @since 02.05.14
 */
public class DruckenTest {

    /**
     * The rapport.
     */
    RapportEntity rapport = new RapportEntity();

    /**
     * The termin.
     */
    TerminEntity termin = new TerminEntity();

    /**
     * Initialize objects.
     */
    @Before
    public void initializeObjects() {
        TerminEntity termin = new TerminEntity();
        StandortEntity standort = new StandortEntity();
        KontrolleurEntity kontrolleur = new KontrolleurEntity();
        EigentumerEntity eigentumer = new EigentumerEntity();
        HauswartEntity hauswart = new HauswartEntity();
        VerwaltungEntity verwaltung = new VerwaltungEntity();
        OrtschaftEntity ortschaft = new OrtschaftEntity();
        WarmeerzeugerEntity warmeerzeuger = new WarmeerzeugerEntity();
        BrennerEntity brenner = new BrennerEntity();
        BrennerartEntity brennerart = new BrennerartEntity();
        BrennstoffEntity brennstoff = new BrennstoffEntity();
        MessergebnisEntity messergebnis1 = new MessergebnisEntity();
        MessergebnisEntity messergebnis2 = new MessergebnisEntity();
        MessergebnisEntity messergebnis3 = new MessergebnisEntity();
        MessergebnisEntity messergebnis4 = new MessergebnisEntity();
        MessvorgangEntity messvorgang1 = new MessvorgangEntity();
        MessvorgangEntity messvorgang2 = new MessvorgangEntity();
        MessvorgangEntity messvorgang3 = new MessvorgangEntity();
        MessvorgangEntity messvorgang4 = new MessvorgangEntity();
        List<MessergebnisEntity> listeMess = new ArrayList<MessergebnisEntity>();
        messvorgang1.setStufe(1);
        messvorgang1.setMessung(1);

        messvorgang2.setStufe(1);
        messvorgang2.setMessung(2);

        messvorgang3.setStufe(2);
        messvorgang3.setMessung(1);

        messvorgang4.setStufe(2);
        messvorgang4.setMessung(2);

        messergebnis1.setMessvorgang(messvorgang1);
        messergebnis1.setRusszahl(12);
        messergebnis1.setCo(50);
        messergebnis1.setOelanteil(true);
        messergebnis1.setNo(2);
        messergebnis1.setAbgastemp(100);
        messergebnis1.setWarmeerzeugertemp(180);
        messergebnis1.setVerbrennungstemp(600);
        messergebnis1.setO2gehalt(20);
        messergebnis1.setAbgasverlust(80);

        messergebnis2.setMessvorgang(messvorgang2);
        messergebnis2.setRusszahl(1);
        messergebnis2.setCo(5);
        messergebnis2.setOelanteil(false);
        messergebnis2.setNo(12);
        messergebnis2.setAbgastemp(90);
        messergebnis2.setWarmeerzeugertemp(80);
        messergebnis2.setVerbrennungstemp(390);
        messergebnis2.setO2gehalt(790);
        messergebnis2.setAbgasverlust(123);

        messergebnis3.setMessvorgang(messvorgang3);
        messergebnis3.setRusszahl(1);
        messergebnis3.setCo(333);
        messergebnis3.setOelanteil(true);
        messergebnis3.setNo(123);
        messergebnis3.setAbgastemp(90);
        messergebnis3.setWarmeerzeugertemp(1);
        messergebnis3.setVerbrennungstemp(0);
        messergebnis3.setO2gehalt(100);
        messergebnis3.setAbgasverlust(1);

        messergebnis4.setMessvorgang(messvorgang4);
        messergebnis4.setRusszahl(999);
        messergebnis4.setCo(0);
        messergebnis4.setOelanteil(true);
        messergebnis4.setNo(5);
        messergebnis4.setAbgastemp(20);
        messergebnis4.setWarmeerzeugertemp(90);
        messergebnis4.setVerbrennungstemp(23);
        messergebnis4.setO2gehalt(50);
        messergebnis4.setAbgasverlust(90);

        listeMess.add(messergebnis1);
        listeMess.add(messergebnis2);
        listeMess.add(messergebnis3);
        listeMess.add(messergebnis4);

        brennstoff.setBezeichnung("Erdgas");

        warmeerzeuger.setBaujahr(2014);
        warmeerzeuger.setBrennstoff(brennstoff);
        warmeerzeuger.setFabrikat("neustes Superteil");

        brennerart.setBezeichnung("Gebläse");

        brenner.setBaujahr(1908);
        brenner.setBrennerart(brennerart);
        brenner.setFabrikat("Altmodischer Schrott");
        brenner.setFeuerungswarmeleistung(2000);

        ortschaft.setGemeinde("Adligenswil");
        ortschaft.setPlz("6043");
        ortschaft.setKanton("Luzern");

        verwaltung.setName("NAME der Verwaltung");

        hauswart.setName("Hausmeister Krause");
        hauswart.setTelefon("078 987 77 23");

        eigentumer.setName("EigenütmerBoss");

        standort.setAdresse("Müllersbachstrasse 23");
        standort.setEigentumer(eigentumer);
        standort.setHauswart(hauswart);
        standort.setOrtschaft(ortschaft);
        standort.setVerwaltung(verwaltung);

        termin.setDatum(2014, 12, 30, 12, 30);

        termin.setStandort(standort);
        termin.setRapport(rapport);
        termin.setKontrolleur(kontrolleur);

        rapport.setTermin(termin);
        rapport.setEigentumer(eigentumer);
        rapport.setVerwaltung(verwaltung);
        rapport.setHauswart(hauswart);
        rapport.setBemerkung("Bemerkungen gibt es heute mal keine alles TipTop");
        rapport.setWarmeerzeuger(warmeerzeuger);
        rapport.setBrenner(brenner);
        rapport.setKontrollart(1);
        rapport.setMessergebnisse(listeMess);

    }

    /**
     * Drucken.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void drucken() throws IOException {
        Drucker drucker = new Drucker();
        drucker.druckenString(rapport.toString(), "c:\test");
        assertEquals("", "");
    }
}
