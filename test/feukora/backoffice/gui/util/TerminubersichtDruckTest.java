package feukora.backoffice.gui.util;

import feukora.entity.*;
import feukora.gui.util.Drucker;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Neitzel Sven
 * @version 20.04.14
 * @since 20.04.14
 */
public class TerminubersichtDruckTest {

    /**
     * The termin1.
     */
    TerminEntity termin1 = new TerminEntity();

    /**
     * The termin2.
     */
    TerminEntity termin2 = new TerminEntity();

    /**
     * The termin3.
     */
    TerminEntity termin3 = new TerminEntity();

    /**
     * The termin4.
     */
    TerminEntity termin4 = new TerminEntity();

    /**
     * The termine.
     */
    ArrayList<TerminEntity> termine = new ArrayList<TerminEntity>();

    /**
     * Initialize objects.
     */
    @Before
    public void initializeObjects() {

        KontrolleurEntity kontrolleur = new KontrolleurEntity();
        StandortEntity standort = new StandortEntity();
        EigentumerEntity eigentumer = new EigentumerEntity();
        HauswartEntity hauswart = new HauswartEntity();
        VerwaltungEntity verwaltung = new VerwaltungEntity();
        OrtschaftEntity ortschaft = new OrtschaftEntity();

        ortschaft.setGemeinde("TestGemeinde");
        ortschaft.setKanton("testKanton");
        ortschaft.setPlz("testPLZ");

        eigentumer.setName("TestNameEigentümer");
        eigentumer.setVorname("TestVornmaeEigentümer");
        eigentumer.setOrtschaft(ortschaft);
        eigentumer.setStrasse("TestStrasseEigentümer");
        eigentumer.setTelefon("TestTelefonEigentümer");

        verwaltung.setName("TestNameVerwaltung");
        verwaltung.setVorname("TestVornameVerwaltung");
        verwaltung.setStrasse("TestStrasseVerwaltung");
        verwaltung.setOrtschaft(ortschaft);
        verwaltung.setTelefon("TestTelefonVerwaltung");

        hauswart.setName("TestNameHauswart");
        hauswart.setVorname("TestVornameHauswart");
        hauswart.setTelefon("TestTelefonHauswart");
        hauswart.setOrtschaft(ortschaft);
        hauswart.setStrasse("TestStrasseHauswart");

        kontrolleur.setBenutzername("TestBenutzername");
        kontrolleur.setName("TestName");
        kontrolleur.setVorname("TestVorname");
        kontrolleur.setIsKontrolleur(1);
        kontrolleur.setOrtschaft(ortschaft);
        kontrolleur.setStrasse("TestStrasseKontrolleur");
        kontrolleur.setTelefon("TestTelefonKontrolleur");

        standort.setAdresse("TestStandortAdresse");
        standort.setEigentumer(eigentumer);
        standort.setHauswart(hauswart);
        standort.setVerwaltung(verwaltung);
        standort.setOrtschaft(ortschaft);

        termin1.setDatum(2014, 5, 8, 17, 53);
        termin1.setKontrolleur(kontrolleur);
        termin1.setStandort(standort);
        termine.add(termin1);

        termin2.setDatum(2014, 5, 9, 17, 53);
        termin2.setKontrolleur(kontrolleur);
        termin2.setStandort(standort);
        termine.add(termin2);

        termin3.setDatum(2014, 5, 10, 17, 53);
        termin3.setKontrolleur(kontrolleur);
        termin3.setStandort(standort);
        termine.add(termin3);

        termin4.setDatum(2014, 5, 11, 17, 53);
        termin4.setKontrolleur(kontrolleur);
        termin4.setStandort(standort);
        termine.add(termin4);

    }

    /**
     * Drucken.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void drucken() throws IOException {

        Drucker drucker = new Drucker();
        drucker.druckenArray(termine, "C:\test");
        System.out.println(termine.get(0).toString());
        System.out.println(termine.get(1).toString());
        System.out.println(termine.get(2).toString());
        System.out.println(termine.get(3).toString());

        // drucker.druckenString(termin1.toString());
        // JOptionPane.showMessageDialog(null, termin1.toString());
    }
}
