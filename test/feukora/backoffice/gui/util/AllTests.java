package feukora.backoffice.gui.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ConnectionCheckerTest.class, DruckenTest.class, TerminubersichtDruckTest.class})
public class AllTests {

}
