package feukora.backoffice.gui.util;

import feukora.gui.util.ConnectionChecker;
import org.junit.Test;

import java.awt.*;
import java.io.IOException;

/**
 * Klasse zum Testen der Server-Verbindung klasse.
 *
 * @author Sven Neitzel
 * @version 10.05.14
 * @since 10.05.14
 */
public class ConnectionCheckerTest {

    /**
     * Test.
     *
     * @throws HeadlessException the headless exception
     * @throws IOException       Signals that an I/O exception has occurred.
     */
    @Test
    public void test() throws HeadlessException, IOException {
        ConnectionChecker connectionChecker = new ConnectionChecker();
        connectionChecker.ConnectionCheck("192.168.100.100");
    }

}
